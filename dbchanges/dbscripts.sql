CREATE TABLE `ADDRESS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `ADDRESS_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK4whppbic3bi6ykx0w867hi0w8` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `INSPECTOR` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `employee_id` bigint(20) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PATRON` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `MIDDLE_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PATRON_ADDR` (
  `ADDR_ID` bigint(20) DEFAULT NULL,
  `PATRON_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PATRON_ID`),
  KEY `FKanec8k8pu38xkg7kpfvjp4q0f` (`ADDR_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PATRON_ADDR_AUD` (
  `PATRON_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `ADDR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PATRON_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PATRON_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `MIDDLE_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKlpw32rnrkekttcl2p2ko84ab1` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PAYMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `chequeFile` longblob,
  `chequeFileName` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `methodId` int(11) NOT NULL,
  `overPaid` decimal(19,2) DEFAULT NULL,
  `paymentDate` varchar(255) DEFAULT NULL,
  `paymentSource` varchar(255) DEFAULT NULL,
  `processedBy` varchar(255) DEFAULT NULL,
  `processedOn` date DEFAULT NULL,
  `totalDue` decimal(19,2) DEFAULT NULL,
  `PAYMNT_METHOD_ID` bigint(20) NOT NULL,
  `txId` bigint(20) DEFAULT NULL,
  `VIOLATION` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK82dc1w53birxqp6mst09ojqco` (`PAYMNT_METHOD_ID`),
  KEY `FKjirf1ui93hqg6ppwgaf2hx6wf` (`txId`),
  KEY `FKpsu1pc20ug86moqw7xsoppxc2` (`VIOLATION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PAYMENT_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `chequeFile` longblob,
  `chequeFileName` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `methodId` int(11) DEFAULT NULL,
  `overPaid` decimal(19,2) DEFAULT NULL,
  `paymentDate` varchar(255) DEFAULT NULL,
  `paymentSource` varchar(255) DEFAULT NULL,
  `processedBy` varchar(255) DEFAULT NULL,
  `processedOn` date DEFAULT NULL,
  `totalDue` decimal(19,2) DEFAULT NULL,
  `txId` bigint(20) DEFAULT NULL,
  `VIOLATION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKaxqpnxxaxes8gw2be617krsow` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PAYMENT_METHOD` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `TYP_CD` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PLATE_ENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `BODY_TYPE` varchar(255) DEFAULT NULL,
  `LIC_NUM` varchar(255) DEFAULT NULL,
  `MONTH` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `VHCL_COLOR` varchar(255) DEFAULT NULL,
  `VHCL_MAKE` varchar(255) DEFAULT NULL,
  `VHCL_MODEL` varchar(255) DEFAULT NULL,
  `VHCL_NUM` varchar(255) DEFAULT NULL,
  `VIN` varchar(255) DEFAULT NULL,
  `YEAR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PLATE_ENT_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `BODY_TYPE` varchar(255) DEFAULT NULL,
  `LIC_NUM` varchar(255) DEFAULT NULL,
  `MONTH` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `VHCL_COLOR` varchar(255) DEFAULT NULL,
  `VHCL_MAKE` varchar(255) DEFAULT NULL,
  `VHCL_MODEL` varchar(255) DEFAULT NULL,
  `VHCL_NUM` varchar(255) DEFAULT NULL,
  `VIN` varchar(255) DEFAULT NULL,
  `YEAR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKjerch6u9t3f4ef6aq5lib0fc6` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `REVINFO` (
  `REV` int(11) NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `TRANSACTION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `clientToken` varchar(5000) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `paymentNonce` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `statusMessage` varchar(255) DEFAULT NULL,
  `statusResponseCode` varchar(255) DEFAULT NULL,
  `transcationCode` varchar(255) DEFAULT NULL,
  `transcationId` varchar(255) DEFAULT NULL,
  `violationId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `USER` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `USER_NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h029unq4qgmbvesub83df4vok` (`USER_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VIOLATION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `VLN_DATE` varchar(255) DEFAULT NULL,
  `EMP_N0` varchar(255) DEFAULT NULL,
  `FINE_AMNT` decimal(7,2) DEFAULT NULL,
  `ISS_DATE` varchar(255) DEFAULT NULL,
  `ISS_TIME` time DEFAULT NULL,
  `ISS_OFFICER` varchar(255) DEFAULT NULL,
  `VLN_LOCATION` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VLN_TIME` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `VIOLATION_ID` varchar(255) DEFAULT NULL,
  `VLN_CODE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb9a56aeik3bsp16y8oc1v0tn1` (`VLN_CODE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VIOLATION_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `VLN_DATE` varchar(255) DEFAULT NULL,
  `EMP_N0` varchar(255) DEFAULT NULL,
  `FINE_AMNT` decimal(7,2) DEFAULT NULL,
  `ISS_DATE` varchar(255) DEFAULT NULL,
  `ISS_TIME` time DEFAULT NULL,
  `ISS_OFFICER` varchar(255) DEFAULT NULL,
  `VLN_LOCATION` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VLN_TIME` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `VIOLATION_ID` varchar(255) DEFAULT NULL,
  `VLN_CODE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK5rrhgup6glxf6rdls0rx5l4s` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VIOLATION_IMAGES` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `IMG` longblob,
  `IMG_NAME` varchar(255) DEFAULT NULL,
  `IMG_TYPE` varchar(255) DEFAULT NULL,
  `parentfile_id` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `UPD_DATE` varchar(255) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKiip2u3davt7gpoqkag8cljgbc` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VLN_CODE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `CODE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `IS_DELETED` char(1) DEFAULT NULL,
  `ONE` decimal(19,2) DEFAULT NULL,
  `STND_FINE` decimal(19,2) DEFAULT NULL,
  `THREE` decimal(19,2) DEFAULT NULL,
  `TWO` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_eys9ccqkek7jfs821h5w2soks` (`CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VLN_PATRON` (
  `PAT_ID` bigint(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`VLN_ID`),
  KEY `FK97nmfwxx8vvgup8anujvn6j0o` (`PAT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VLN_PATRON_AUD` (
  `VLN_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `PAT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`VLN_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VLN_PLATE_ENT` (
  `VLN_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `PLATE_ENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`VLN_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `VLN_PLATE_ENT_AUD` (
  `VLN_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `PLATE_ENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`VLN_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PERMIT` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `created_at` datetime DEFAULT NULL,
 `created_by` varchar(20) DEFAULT NULL,
 `updated_at` datetime DEFAULT NULL,
 `updated_by` varchar(20) DEFAULT NULL,
 `Amount` varchar(255) DEFAULT NULL,
 `CSU_ID` varchar(255) DEFAULT NULL,
 `FIRST_NAME` varchar(255) DEFAULT NULL,
 `LAST_NAME` varchar(255) DEFAULT NULL,
 `LIC_NUM` varchar(255) DEFAULT NULL,
 `MIDDLE_NAME` varchar(255) DEFAULT NULL,
 `PATRON_TYPE` varchar(255) DEFAULT NULL,
 `PERMIT_TYPE` varchar(255) DEFAULT NULL,
 `END_DATE` date DEFAULT NULL,
 `PARKING_SLOT` varchar(255) DEFAULT NULL,
 `START_DATE` date DEFAULT NULL,
 `END_TIME` time DEFAULT NULL,
 `START_TIME` time DEFAULT NULL,
 `PERMIT_ID` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PERMIT_HSTRY` (
 `id` bigint(20) NOT NULL,
 `REV` int(11) NOT NULL,
 `REVTYPE` tinyint(4) DEFAULT NULL,
 `created_at` datetime DEFAULT NULL,
 `created_by` varchar(20) DEFAULT NULL,
 `updated_at` datetime DEFAULT NULL,
 `updated_by` varchar(20) DEFAULT NULL,
 `Amount` varchar(255) DEFAULT NULL,
 `CSU_ID` varchar(255) DEFAULT NULL,
 `FIRST_NAME` varchar(255) DEFAULT NULL,
 `LAST_NAME` varchar(255) DEFAULT NULL,
 `LIC_NUM` varchar(255) DEFAULT NULL,
 `MIDDLE_NAME` varchar(255) DEFAULT NULL,
 `PATRON_TYPE` varchar(255) DEFAULT NULL,
 `PERMIT_TYPE` varchar(255) DEFAULT NULL,
 `END_DATE` date DEFAULT NULL,
 `PARKING_SLOT` varchar(255) DEFAULT NULL,
 `START_DATE` date DEFAULT NULL,
 `END_TIME` time DEFAULT NULL,
 `START_TIME` time DEFAULT NULL,
 `PERMIT_ID` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`,`REV`),
 KEY `FKrmtd1rn9tik8dpep5ncac39fb` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



CREATE TABLE `PRMT_PYMNT` (
  `PERMIT_PAYMENT_ID` bigint(20) DEFAULT NULL,
  `PERMIT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PERMIT_ID`),
  KEY `FK5ydxtuv4cg04pveu6342ege8c` (`PERMIT_PAYMENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PRMT_PYMNT_AUD` (
  `PERMIT_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `PERMIT_PAYMENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PERMIT_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;










INSERT INTO `USER` (`id`, `USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`) VALUES (1, 'axiom', 'csum123', 'Axiom', 'Xcell');

INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('1', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-1.1', 'NO PERMIT CLEARLY DISPLAYED', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('2', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-1.2', 'PERMIT EXPIRED', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('3', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-1.3', 'PERMIT NOT VALID WHERE PARKED', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('4', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.1', 'CONTROL DEVICE PROHIBITS', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('5', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.4', 'OUTSIDE SPACE MARKERS', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('6', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.5', 'PARKED IN SPECIAL PERMIT ZONE', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('7', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.7', 'PARKED IN A RED ZONE', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('8', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.8', 'LOADING ZONE VIOLATION', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('9', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.9', 'EXCEEDING TIME LIMIT', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('10', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.10', 'RESERVED PARKING ZONE', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('11', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.11', 'USE OF FRAUD/UNAUTHORIZED PERMIT', 'N', '417.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('12', NULL, NULL, NULL, NULL, 'CVC 21113 (A)-4.13', 'NOT MARKED OR APPROVED FOR PARKING', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('13', NULL, NULL, NULL, NULL, 'CVC 2504', 'POSTED NO PARKING', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('14', NULL, NULL, NULL, NULL, 'CVC 2505', 'TEMPORARILY NO PARKING', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('15', NULL, NULL, NULL, NULL, 'CVC 22500A', 'PARKED IN INTERSECTION', 'N', '55.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('16', NULL, NULL, NULL, NULL, 'CVC 22500B', 'PARKED IN CROSSWALK', 'N', '55.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('17', NULL, NULL, NULL, NULL, 'CVC 22500E', 'BLOCKING DRIVEWAY', 'N', '55.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('18', NULL, NULL, NULL, NULL, 'CVC 22500G', 'OBSTRUCTING TRAFFIC', 'N', '55.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('19', NULL, NULL, NULL, NULL, 'CVC 22507.8 (A)', 'DISABLED PERSON ZONE', 'N', '336.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('20', NULL, NULL, NULL, NULL, 'CVC 22507.8 (C)', 'DISABLED PERSON ZONE/HASH MARKS', 'N', '336.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('21', NULL, NULL, NULL, NULL, 'CVC 22500 (F)', 'PARKED ON SIDEWALK', 'N', '51.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('22', NULL, NULL, NULL, NULL, 'CVC 22500.1', 'PARKED IN A FIRE LANE', 'N', '55.00', '0.00', '0.00', '0.00');
INSERT INTO VLN_CODE (id, created_at, created_by, updated_at, updated_by, CODE, DESCRIPTION,IS_DELETED,  STND_FINE, ONE, THREE, TWO) VALUES ('23', NULL, NULL, NULL, NULL, 'CVC 22514', 'PARKING BY A FIRE HYDRANT', 'N', '55.00', '0.00', '0.00', '0.00');

INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('1', 'CASH', '1');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('2', 'CREDIT CARD', '2');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('3', 'DEBIT CARD', '3');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('4', 'MONEY ORDER', '4');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('5', 'CHECK', '5');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('6', 'CASH - BOND', '6');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('7', 'CHECK - BOND', '7');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('8', 'MON ORD - BOND', '8');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('9', 'CRED CRD - BOND', '9');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('10', 'CONSUMER PORTAL � CARD', '10');
INSERT INTO PAYMENT_METHOD (`id`, `DESCRIPTION`, `TYP_CD`) VALUES ('11', 'IVR � CARD', '11');

CREATE TABLE `PERMIT_TRANSACTION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `clientToken` varchar(5000) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `paymentNonce` varchar(255) DEFAULT NULL,
  `permitId` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `statusMessage` varchar(255) DEFAULT NULL,
  `statusResponseCode` varchar(255) DEFAULT NULL,
  `transcationCode` varchar(255) DEFAULT NULL,
  `transcationId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


CREATE TABLE `PERMIT_PAYMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `AMOUNT` varchar(255) DEFAULT NULL,
  `PAYMENT_DATE` date DEFAULT NULL,
  `PAYMENT_TYPE` varchar(255) DEFAULT NULL,
  `txId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKt27bkmmgluc64llgevd3tioe` (`txId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE `PERMIT_PAYMENT_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `AMOUNT` varchar(255) DEFAULT NULL,
  `PAYMENT_DATE` date DEFAULT NULL,
  `PAYMENT_TYPE` varchar(255) DEFAULT NULL,
  `txId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKbu7oob9bqb2fmn32qr634uwrf` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `PNLT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FINE_AMT` decimal(7,2) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `PNLT_CODE` bigint(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKj1f8n5l0cirrgv6ft1fnyjpgf` (`PNLT_CODE`),
  KEY `FK6mwvqqnqqyneb8pt6mck2rbtp` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `PNLT_CODE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `PENALTY1` decimal(7,2) DEFAULT NULL,
  `PENALTY1DATE` date DEFAULT NULL,
  `PENALTY2` decimal(7,2) DEFAULT NULL,
  `PENALTY2DATE` date DEFAULT NULL,
  `PENALTY3` decimal(7,2) DEFAULT NULL,
  `PENALTY3DATE` date DEFAULT NULL,
  `STND_FINE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PNLT_CODE_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `PENALTY1` decimal(7,2) DEFAULT NULL,
  `PENALTY1DATE` date DEFAULT NULL,
  `PENALTY2` decimal(7,2) DEFAULT NULL,
  `PENALTY2DATE` date DEFAULT NULL,
  `PENALTY3` decimal(7,2) DEFAULT NULL,
  `PENALTY3DATE` date DEFAULT NULL,
  `STND_FINE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKbo5ob42lmo9jqg7wxgidw93r0` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PNLT_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FINE_AMT` decimal(7,2) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `PNLT_CODE` bigint(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKj0v61x2v6lj4orw9c4ywv5o8n` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `NOTICE_TYPE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `FULL_NM` varchar(255) NOT NULL,
  `TYPE` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `NOTICES` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `PROCESSED_DATE` date DEFAULT NULL,
  `SENT_DATE` date DEFAULT NULL,
  `NOTICE_TYPE` bigint(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK20wgk0us9brosl57hw6cdh794` (`NOTICE_TYPE`),
  KEY `FKf1tufkv8y62mtywxnfp6fptqo` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `NOTICES_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `PROCESSED_DATE` date DEFAULT NULL,
  `SENT_DATE` date DEFAULT NULL,
  `NOTICE_TYPE` bigint(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKdrqfnpf9lwuu4ll3a0x5k3gyc` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `HEARING` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  `DISPOSITION` varchar(255) DEFAULT NULL,
  `DIPOSITION_DATE` date DEFAULT NULL,
  `DISPOSITION_TIME` time DEFAULT NULL,
  `HEARING_DATE` date DEFAULT NULL,
  `HEARING_OFFICER` varchar(255) DEFAULT NULL,
  `HEARING_TIME` time DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `DATE_MAILED` date DEFAULT NULL,
  `SCHEDULED_AT` datetime DEFAULT NULL,
  `SCHEDULED_BY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmj4po2sidk9jqv2envlmm1hot` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `HEARING_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  `DISPOSITION` varchar(255) DEFAULT NULL,
  `DIPOSITION_DATE` date DEFAULT NULL,
  `DISPOSITION_TIME` time DEFAULT NULL,
  `HEARING_DATE` date DEFAULT NULL,
  `HEARING_OFFICER` varchar(255) DEFAULT NULL,
  `HEARING_TIME` time DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `DATE_MAILED` date DEFAULT NULL,
  `SCHEDULED_AT` datetime DEFAULT NULL,
  `SCHEDULED_BY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK8iboy0xbd4660hvuheqshviv5` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `CORRESP_CODE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `CLS` varchar(255) NOT NULL,
  `CORRESP_DESC` varchar(255) NOT NULL,
  `FULL_NAME` varchar(255) NOT NULL,
  `TYP_CD` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `CORRESPONDENCE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  `CORRESP_DATE` date DEFAULT NULL,
  `letterSent` tinyint(1) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `CORRESPONDENCE` bigint(20) DEFAULT NULL,
  `CORRESP_TIME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK69kj3a2g13dq6ofvbvu4xs827` (`CORRESPONDENCE`),
  KEY `FKerxq2p5quyxo84mssxh2oaajs` (`VLN_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `CORRESPONDENCE_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  `CORRESP_DATE` date DEFAULT NULL,
  `letterSent` tinyint(1) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `CORRESP_TIME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKmjd4x2ftyn0h52ffrxletgcww` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `REVIEWCOMMENTS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `REVIEWPROCESS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `ignoreTime` varchar(255) DEFAULT NULL,
  `question1` varchar(255) DEFAULT NULL,
  `question2` varchar(255) DEFAULT NULL,
  `status` bit(1) NOT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9f033ao1hl15pnt4a1358vckt` (`VLN_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `SUSPENDED_CODES` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `AMOUNT` varchar(255) DEFAULT NULL,
  `CODE` int(11) NOT NULL,
  `DAYS` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FULL_NM` varchar(255) NOT NULL,
  `TYPE` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `SUSPENDS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `REDUCTION` decimal(7,2) DEFAULT NULL,
  `SUSPENDED_DATE` date DEFAULT NULL,
  `SUSPENDED_TIME` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `SUSPENDED_CODES_ID` bigint(20) NOT NULL,
  `PROCESSED_ON` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK53hdud4sbs5ima4x6otd4e6dh` (`SUSPENDED_CODES_ID`),
  KEY `FKqlw7nsgyr95p65cgo2vlbm7c2` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `SUSPENDS_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `REDUCTION` decimal(7,2) DEFAULT NULL,
  `SUSPENDED_DATE` date DEFAULT NULL,
  `SUSPENDED_TIME` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `PROCESSED_ON` date DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK5j00msi56vs260rh7e63q52d8` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE) 
VALUES ('1', NULL, NULL, NULL, NULL, NULL, '213', '25', 'LETTER STATING CITE FOUND TO BE VALID - INITIAL REVIEW', 'VALID TICKET', 'P');

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE) 
VALUES ('2', NULL, NULL, NULL, NULL, NULL, '106', '25', 'REQ FOR INITIAL REVIEW ON PKG CITE HAD PASSED TIME PERIOD', 'TOO LATE TO REQUEST IR', 'P');

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE) 
VALUES ('3', NULL, NULL, NULL, NULL, NULL, '313', '0', 'AMENDED PARKING CITATION', 'AMENDED PARKING CITATION', 'P');

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE)
VALUES ('4', NULL, NULL, NULL, NULL, NULL, '150', '0', 'LETTER STATING CITATION HAS BEEN DISMISSED - INITIAL REVIEW', 'CITATION DISMISSED', 'P');

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE) 
VALUES ('5', NULL, NULL, NULL, NULL, NULL, '330', '0', 'OUTSTANDING VIOLATION', 'OUTSTANDING VIOLATION', 'P');

INSERT INTO SUSPENDED_CODES (id, created_at, created_by, updated_at, updated_by, AMOUNT, CODE, DAYS, DESCRIPTION, FULL_NM, TYPE) 
VALUES ('6', NULL, NULL, NULL, NULL, NULL, '220', '30', 'CORRESPONDENCE FOR INITIAL REVIEW', 'PENDING REVIEW', 'T');

INSERT INTO CORRESP_CODE (id, created_at, created_by, updated_at, updated_by, CLS, CORRESP_DESC, FULL_NAME, TYP_CD) 
VALUES ('1', NULL, NULL, NULL, NULL, 'V', 'LETTER STATING CITATION HAS BEEN FOUND TO BE VALID', 'VALID TICKET', '213');

INSERT INTO CORRESP_CODE (id, created_at, created_by, updated_at, updated_by, CLS, CORRESP_DESC, FULL_NAME, TYP_CD) 
VALUES ('2', NULL, NULL, NULL, NULL, '', 'PARKING - REQ FOR INITIAL REVIEW RECD AFTER DEADLINE', 'TOO LATE TO REQUEST IR', '106');

INSERT INTO CORRESP_CODE (id, created_at, created_by, updated_at, updated_by, CLS, CORRESP_DESC, FULL_NAME, TYP_CD) 
VALUES ('3', NULL, NULL, NULL, NULL, '', 'AMENDED PARKING CITATION', 'AMENDED PARKING CITATION', '313');

INSERT INTO CORRESP_CODE (id, created_at, created_by, updated_at, updated_by, CLS, CORRESP_DESC, FULL_NAME, TYP_CD) 
VALUES ('4', NULL, NULL, NULL, NULL, 'D', 'LETTER STATING CITE HAS BEEN DISMISSED', 'CITATION DISMISSED', '150');

INSERT INTO CORRESP_CODE (id, created_at, created_by, updated_at, updated_by, CLS, CORRESP_DESC, FULL_NAME, TYP_CD) 
VALUES ('5', NULL, NULL, NULL, NULL, '', 'OUTSTANDING VIOLATION', 'OUTSTANDING VIOLATION', '330');

CREATE TABLE `COMMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `COMMENT` longtext,
  `COMMENT_TYPE` varchar(255) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8mi15t8oyrhjv8kdmpd51oamv` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('1', '2018-05-10 05:10:00', 'DISMISSED IN THE INTERESTS OF JUSTICE. AS PER TAP DEPT, PATRON HAD VALID ARE MEDIA AT THE TIME OF VIOLATION ISSUANCE.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('2', '2018-05-10 05:10:00', 'EVIDENCE PROVIDED IS NOT ENOUGH TO REFUSE THE OFFICER\'S OBSERVATION. MUST PARK IN MARKED STALLS.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('3', '2018-05-10 05:10:00', 'BASED ON THE EVIDENCE PROVIDED, REVENUE DEPT WAS ABLE TO DETERMINE MALFUNCTIONING OF TICKET VENDING MACHINES. PATRON�S CLAIM IS VALID.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('4', '2018-05-10 05:10:00', 'BASED ON THE EVIDENCE PROVIDED, REVENUE DEPT VERIFIED THAT TICKET VENDING MACHINES WERE NOT MALFUNCTIONING DURING THE TIME OF VIOLATION ISSUANCE.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('5', '2018-05-10 05:10:00', 'BASED ON TAP REVIEW, DISMISSED IN THE INTERESTS OF JUSTICE. PATRON WAS IN POSSESSION OF VALID FARE MEDIA AT TIME OF VIOLATION ISSUANCE BUT FAILED TO TAP.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('6', '2018-05-10 05:10:00', 'BASED ON TAP REVIEW, PATRON WAS NOT IN POSSESSION OF VALID FARE MEDIA AT TIME OF VIOLATION ISSUANCE.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('7', '2018-05-10 05:10:00', 'BASED ON TAP REVIEW, PATRON WAS NOT IN POSSESSION OF VALID FARE MEDIA AT TIME OF VIOLATION ISSUANCE, INITIAL REVIEW RESULTS DELIVERED IN PERSON.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('8', '2018-05-10 05:10:00', 'EVIDENCE PROVIDED IS NOT ENOUGH TO REFUSE THE OFFICER\'S OBSERVATION. INITIAL REVIEW RESULTS DELIVERED IN PERSON.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('9', '2018-05-10 05:10:00', 'MUST PROVIDE FULL TAP CARD NUMBER IN ORDER FOR TAP DEPT TO CONDUCT RESEARCH. NUMBER PROVIDED IN LETTER FROM PATRON IS NOT THE CORRECT NUMBER.');

INSERT INTO `REVIEWCOMMENTS` (`id`, `created_at`, `comment`) VALUES ('10', '2018-05-10 05:10:00', 'EVIDENCE PROVIDED IS NOT ENOUGH TO REFUSE THE OFFICER\'S OBSERVATION. MUST PROVIDE FULL TAP CARD NUMBER IN ORDER FOR TAP RESEARCH TO BE CONDUCTED.');

CREATE TABLE `ADMIN` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `USER_NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nufky9i71cwl68ap7ib5mhh7n` (`USER_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `ADMIN` (`id`, `USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`) VALUES (1, 'axiom', 'csum123', 'Axiom', 'Xcell');

ALTER TABLE USER ADD COLUMN ROLE VARCHAR(255) DEFAULT NULL;

INSERT INTO `USER` (`id`, `USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`,`ROLE`) VALUES (2, 'Petar', 'csum123', 'Petar', 'Seldac','Student');

INSERT INTO `USER` (`id`, `USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`,`ROLE`) VALUES (3, 'Vinay', 'csum123', 'Vinay', 'Parsi','Student');

UPDATE USER SET ROLE='Employee' WHERE id='1';



CREATE TABLE `HEARING_SCHEDULER` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO HEARING_SCHEDULER (id,value,description,type) values (1,'09:00','9:00 AM - 9:30 AM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type) values (2,'09:30','9:30 AM - 10:00 AM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type) values (3,'10:00','10:00 AM - 10:30 AM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (4,'10:30','10:30 AM - 11:00 AM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (5,'11:00','11:00 AM - 11:30 AM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (6,'11:30','11:30 AM - 12:00 PM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (7,'13:00','01:00 PM - 01:30 PM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (8,'13:30','01:30 PM - 02:00 PM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (9,'14:00','02:00 PM - 02:30 PM','time');
INSERT INTO HEARING_SCHEDULER (id,value,description,type)values (10,'14:30','02:30 PM - 03:00 PM','time');

CREATE TABLE `HOLIDAY_LIST` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `HOLIDAY_DATE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/01/2018','New Year�s Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/15/2018','Birthday of Martin Luther King, Jr.');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('02/19/2018','Washington�s Birthday');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('05/28/2018','Memorial Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('07/04/2018','Independence Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('09/03/2018','Labor Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/11/2018','Veterans Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/22/2018','Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/23/2018','After Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('12/25/2018','Christmas Day');

INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/01/2019','New Year�s Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/21/2019','Birthday of Martin Luther King, Jr.');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('02/18/2019','Washington�s Birthday');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('05/27/2019','Memorial Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('07/04/2019','Independence Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('09/02/2019','Labor Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/11/2019','Veterans Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/28/2019','Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/29/2019','After Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('12/25/2019','Christmas Day');


INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/01/2020','New Year�s Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('01/20/2020','Birthday of Martin Luther King, Jr.');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('02/17/2020','Washington�s Birthday');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('05/25/2020','Memorial Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('07/04/2020','Independence Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('09/07/2020','Labor Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/11/2020','Veterans Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/26/2020','Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('11/27/2020','After Thanksgiving Day');
INSERT INTO HOLIDAY_LIST (HOLIDAY_DATE,DESCRIPTION) VALUES ('12/25/2020','Christmas Day');


DROP TABLE HEARING;

DROP TABLE HEARING_HSTRY;



CREATE TABLE `HEARING` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) NOT NULL,
  `DISPOSITION` varchar(255) DEFAULT NULL,
  `DIPOSITION_DATE` date DEFAULT NULL,
  `DISPOSITION_TIME` time DEFAULT NULL,
  `HEARING_DATE` date DEFAULT NULL,
  `HEARING_OFFICER` varchar(255) DEFAULT NULL,
  `HEARING_TIME` time DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `DATE_MAILED` date DEFAULT NULL,
  `SCHEDULED_AT` datetime DEFAULT NULL,
  `SCHEDULED_BY` varchar(255) DEFAULT NULL,
  `noFineChange` tinyint(1) DEFAULT NULL,
  `REDUCTION` decimal(7,2) DEFAULT NULL,
  `IS_RESCHEDULED` bit(1) DEFAULT NULL,
  `DECISION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsmxlk4qh3rae3annw9bvol8qb` (`DECISION`),
  KEY `FKmj4po2sidk9jqv2envlmm1hot` (`VLN_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



CREATE TABLE `HEARING_DECISION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `isdeleted_ind` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



CREATE TABLE `HEARING_DETAILS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `LANG` varchar(255) DEFAULT NULL,
  `HEARING_NOTES` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `TRANSLATION` varchar(255) DEFAULT NULL,
  `DECISION` bigint(20) DEFAULT NULL,
  `DISPOSITION` bigint(20) DEFAULT NULL,
  `HEARING_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgvfh3nui49dmb2ywlgsuis56l` (`DECISION`),
  KEY `FKpallk1trmadh57vrk9bpgno6q` (`DISPOSITION`),
  KEY `FKc6w2nl141aibj82yy8a26k3t8` (`HEARING_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



CREATE TABLE `HEARING_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  `DISPOSITION` varchar(255) DEFAULT NULL,
  `DIPOSITION_DATE` date DEFAULT NULL,
  `DISPOSITION_TIME` time DEFAULT NULL,
  `HEARING_DATE` date DEFAULT NULL,
  `HEARING_OFFICER` varchar(255) DEFAULT NULL,
  `HEARING_TIME` time DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOT_DUE` decimal(7,2) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `DATE_MAILED` date DEFAULT NULL,
  `SCHEDULED_AT` datetime DEFAULT NULL,
  `SCHEDULED_BY` varchar(255) DEFAULT NULL,
  `noFineChange` tinyint(1) DEFAULT NULL,
  `REDUCTION` decimal(7,2) DEFAULT NULL,
  `IS_RESCHEDULED` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK8iboy0xbd4660hvuheqshviv5` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



CREATE TABLE `DISPOSITION_CODE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  `AMOUNT` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `DAYS` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `dispclass` varchar(255) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `Priority` int(11) DEFAULT NULL,
  `Rule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE USER ADD COLUMN CSU_ID VARCHAR(255) NOT NULL;	
UPDATE USER SET `CSU_ID`='CSUE0001' WHERE `id`='1';

UPDATE USER SET `CSU_ID`='CSUS0001' WHERE `id`='2';

UPDATE USER SET `CSU_ID`='CSUS0002' WHERE `id`='3';

ALTER TABLE PERMIT ADD COLUMN TYPE varchar(20) DEFAULT NULL;
ALTER TABLE PERMIT_HSTRY ADD COLUMN TYPE varchar(20) DEFAULT NULL;


ALTER TABLE PERMIT ADD COLUMN COMMENT VARCHAR(255) DEFAULT NULL;
ALTER TABLE PERMIT_HSTRY ADD COLUMN COMMENT VARCHAR(255) DEFAULT NULL;

ALTER TABLE VIOLATION ADD COLUMN PROCESSDATE VARCHAR(255) DEFAULT NULL;
ALTER TABLE VIOLATION_HSTRY ADD COLUMN PROCESSDATE VARCHAR(255) DEFAULT NULL;
ALTER TABLE VIOLATION ADD COLUMN SUSPENDPROCESSDATE VARCHAR(255) DEFAULT NULL;
ALTER TABLE VIOLATION_HSTRY ADD COLUMN SUSPENDPROCESSDATE VARCHAR(255) DEFAULT NULL;

INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `dispclass`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (1,'101','2','LIABLE','2','Z-ENTER REDUCTION AMOUNT','Yes','LIABLE',50);
INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `dispclass`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (2,'102','5','NOT LIABLE','5','F-REDUCE BY TTL FINE & PENALTIES','Yes','NOT LIABLE',50);
INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `dispclass`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (3,'103','2','FAILURE TO APPEAR','2','Z-ENTER REDUCTION AMOUNT','Yes','FAILURE TO APPEAR FOR HEARING - ALL MONIES DUE',50);
INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (4,'107','LIABLE- IPP APPROVED','1','Z-ENTER REDUCTION AMOUNT','Yes','LIABLE AND APPROVED FOR IPP',50);
INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (5,'119','CS HRG LIABLE (P)','1','Z-ENTER REDUCTION AMOUNT','Yes','COMMUNITY SRVC HEARING LIABLE OFFICER COMMENTS (PARKING)',50);
INSERT INTO DISPOSITION_CODE (`id`, `CODE`, `FullName`, `Priority`, `Rule`, `Active`, `Description`,`Days`) VALUES (6,'130','LIABLE CMMNTY SVC(P)','1','Z-ENTER REDUCTION AMOUNT','Yes','LIABLE COMMUNITY SERVICE (PARKING)',50);

INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('Liable','LIABLE','N');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('Not_Liable','NOT LIABLE','N');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('LiableComSer','LIABLE WITH COMMUNITY SERVICE','Y');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('AdmsnComSer','ADMISSION COMMUNITY SERVICE','Y');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('LiableIpp','LIABLE WITH IPP','N');

CREATE TABLE `ADMIN_ROLE` (
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `ADMIN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ADMIN_ID`),
  KEY `FK86ea1dwyam3bmgnjfksdhif1r` (`ROLE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `ADMIN_TYPE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `TYPE` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_13rw8s4mhswndylnbgyvgu4ak` (`TYPE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `ROLE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `ADMN_TYP_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_lqaytvswxwacb7s84gcw7tk7l` (`NAME`),
  KEY `FKntlpkx1w1g33fmplnsh7vc13p` (`ADMN_TYP_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO ADMIN_ROLE (`ROLE_ID`, `ADMIN_ID`) VALUES ('1', '1');
INSERT INTO ROLE (`id`,`NAME`, `ADMN_TYP_ID`) VALUES ('1','HearingAdmin', '1');
INSERT INTO ADMIN_TYPE (`id`, `TYPE`) VALUES ('1', 'HearingAdmin');

INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('Liable','LIABLE','N');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('Not_Liable','NOT LIABLE','N');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('LiableComSer','LIABLE WITH COMMUNITY SERVICE','Y');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('AdmsnComSer','ADMISSION COMMUNITY SERVICE','Y');
INSERT INTO HEARING_DECISION (`code`,`description`,`isdeleted_ind`) VALUES ('LiableIpp','LIABLE WITH IPP','N');

CREATE TABLE `IPP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `DOWN_PAYMENT` decimal(19,2) DEFAULT NULL,
  `ENROLL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `INSTALL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `InstallmentDate_1` varchar(255) DEFAULT NULL,
  `InstallmentDate_2` varchar(255) DEFAULT NULL,
  `InstallmentDate_3` varchar(255) DEFAULT NULL,
  `PAYMENTS` int(11) DEFAULT NULL,
  `ORIGINAL_DUE` decimal(19,2) DEFAULT NULL,
  `planNumber` bigint(20) NOT NULL,
  `PREV_CREDITS` decimal(19,2) DEFAULT NULL,
  `START_DATE` varchar(255) DEFAULT NULL,
  `IPP_STATUS` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `UNENROLL_AMOUNT` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `IPP_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `DOWN_PAYMENT` decimal(19,2) DEFAULT NULL,
  `ENROLL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `INSTALL_AMOUNT` decimal(19,2) DEFAULT NULL,
  `InstallmentDate_1` varchar(255) DEFAULT NULL,
  `InstallmentDate_2` varchar(255) DEFAULT NULL,
  `InstallmentDate_3` varchar(255) DEFAULT NULL,
  `PAYMENTS` int(11) DEFAULT NULL,
  `ORIGINAL_DUE` decimal(19,2) DEFAULT NULL,
  `planNumber` bigint(20) DEFAULT NULL,
  `PREV_CREDITS` decimal(19,2) DEFAULT NULL,
  `START_DATE` varchar(255) DEFAULT NULL,
  `IPP_STATUS` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `UNENROLL_AMOUNT` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK6kbnmu1mtdw2c2e41i33njph6` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `IPP_VLN` (
  `VLN_ID` bigint(20) DEFAULT NULL,
  `IPP_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`IPP_ID`),
  KEY `FKry3snqionkc27a91mjklfw8o8` (`VLN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `IPP_VLN_AUD` (
  `IPP_ID` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `VLN_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IPP_ID`,`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE PAYMENT;
DROP TABLE PAYMENT_HSTRY;

CREATE TABLE `PAYMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `chequeFile` longblob,
  `chequeFileName` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `methodId` int(11) NOT NULL,
  `overPaid` decimal(19,2) DEFAULT NULL,
  `paymentDate` varchar(255) DEFAULT NULL,
  `paymentSource` varchar(255) DEFAULT NULL,
  `processedBy` varchar(255) DEFAULT NULL,
  `processedOn` date DEFAULT NULL,
  `totalDue` decimal(19,2) DEFAULT NULL,
  `PAYMNT_METHOD_ID` bigint(20) NOT NULL,
  `txId` bigint(20) DEFAULT NULL,
  `VIOLATION` bigint(20) NOT NULL,
  `IPP_TYPE` varchar(255) DEFAULT NULL,
  `IPP_ID` bigint(20) DEFAULT NULL,
  `ipp` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKble2fjvb88nwh4wmg9yf67vw1` (`IPP_ID`),
  KEY `FK82dc1w53birxqp6mst09ojqco` (`PAYMNT_METHOD_ID`),
  KEY `FKjirf1ui93hqg6ppwgaf2hx6wf` (`txId`),
  KEY `FKpsu1pc20ug86moqw7xsoppxc2` (`VIOLATION`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;


CREATE TABLE `PAYMENT_HSTRY` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `chequeFile` longblob,
  `chequeFileName` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `methodId` int(11) DEFAULT NULL,
  `overPaid` decimal(19,2) DEFAULT NULL,
  `paymentDate` varchar(255) DEFAULT NULL,
  `paymentSource` varchar(255) DEFAULT NULL,
  `processedBy` varchar(255) DEFAULT NULL,
  `processedOn` date DEFAULT NULL,
  `totalDue` decimal(19,2) DEFAULT NULL,
  `txId` bigint(20) DEFAULT NULL,
  `VIOLATION` bigint(20) DEFAULT NULL,
  `IPP_TYPE` varchar(255) DEFAULT NULL,
  `ipp` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKaxqpnxxaxes8gw2be617krsow` (`REV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



UPDATE PAYMENT_METHOD SET `DESCRIPTION`='CONSUMER PORTAL - CARD' WHERE `TYP_CD`='10';