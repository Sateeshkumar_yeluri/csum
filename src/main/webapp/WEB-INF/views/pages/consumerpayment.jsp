<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content" id="blockui_consumerpayment_data">
		<br /> <br />
		<div class="page-bar" style="margin: -20px 0px 10px;" align="center">
			<span style="font-size: 16px; white-space: nowrap;"><b>License
					Number:</b><span style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
				&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 18px"
				class="fa font-black"><b>Violation Number:</b></span> &nbsp;&nbsp;<b>${violationId}
			</b></span> <a href="${pageContext.request.contextPath}/consumerLogin"
				class="btn blue-dark btn-sm" style="float: right;">BACK</a>
		</div>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<div class="portlet box blue-dark">
						<div class="portlet-title">
							<div class="caption">Payment Details</div>
						</div>
						<div class="portlet-body">
							<!-- BEGIN FORM-->
							<form:form id="payment-form" method="post"
								action="${pageContext.request.contextPath}/consumerCheckout/${violationId}"
								modelAttribute="paymentForm" class="form-horizontal">
								<div class="form-body">
									<div class="col-md-12">
										<span style="margin-left: 25px;">Please select from the
											following:</span><br>
											<form:input path="ipp" type="hidden"
											value="${paymentForm.ipp}" id="ipp" />
										<c:choose>
											<c:when test="${paymentForm.ipp==true}">
												<div class="radio-inline">
													<label class="radio label-sm"> <form:radiobutton
															path="amount" id="optionsRadios25" name="amount"
															value="${totalDue}" onclick="clearOtherAmount();" />Total
														Amount Due:&nbsp;$${totalDue} <span></span>
													</label> <label class="radio label-sm"> <form:radiobutton
															path="amount" id="optionsRadios28" name="amount"
															checked="checked" value="${instAmount}" onclick="read();" />Other
														Amount:&nbsp;<input type="text" id="amounttext"
														name="otheramount" value="${instAmount}"> <span></span>
													</label>
												</div>
											</c:when>
											<c:otherwise>
												<div class="radio-inline">
													<label class="radio label-sm"> <form:radiobutton
															path="amount" id="optionsRadios25" name="amount"
															value="${totalDue}" onclick="clearOtherAmount();"
															checked="checked" />Total Amount Due:&nbsp;$${totalDue}
														<span></span>
													</label> <label class="radio label-sm"> <form:radiobutton
															path="amount" id="optionsRadios28" name="amount"
															value="0.00" onclick="read();" />Other Amount:&nbsp;<input
														type="text" id="amounttext" name="otheramount"> <span></span>
													</label>
												</div>
											</c:otherwise>
										</c:choose>
									</div>
									<br /> <br />
									<div class="row">
										<div class="col-md-12">
											<div class="form-group form-md-line-input has-success">
												<label class="col-md-3 control-label" for="form_control_1"
													style="width: 10%;"><b>Credit Card</b></label>
												<div class="col-md-9">
													<div class="form-row">
														<div id="card-element">
															<!-- a Stripe Element will be inserted here. -->
														</div>
														<!-- Used to display form errors -->
														<div id="card-errors"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions" style="margin-top: 12px;">
										<div class="row" style="margin-bottom: 12px;">
											<div class="col-md-offset-3 col-md-9">
												<span style="color: red;" id="clearError">${errorMessage}</span>
												<span style="color: red;" id="paymentAmountError"></span>
											</div>
										</div>
									</div>
									<div class="row" style="margin-bottom: 70px;">
										<div class=" col-md-12" align="center">
											<div class="form-actions">
												<input type="submit" class="btn blue-dark"
													value="PROCESS PAYMENT" /> <a
													href="<c:url value='/consumerPayment/${violation.violationId}' />"
													class="btn default">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="/csum/static/assets/global/plugins/jquery.min.js"
	type="text/javascript"></script>

<script src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">
	function clearOtherAmount() {
		if ($("#ipp").value = false) {
		var amount = $('#amounttext').val();
		if (amount.length) {
			document.getElementById('amounttext').value = '';
		}
		}
		$('#amounttext').attr("readonly", true);
	}
	function read() {
		$('#amounttext').attr("readonly", false);
	}

	//Create a Stripe client
	var stripe = Stripe('${stripePublishableKey}');

	// Create an instance of Elements
	var elements = stripe.elements();

	// Custom styling can be passed to options when creating an Element.
	// (Note that this demo uses a wider set of styles than the guide below.)
	var style = {
		base : {
			color : '#004b85',
			lineHeight : '24px',
			fontFamily : '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing : 'antialiased',
			fontSize : '16px',
			'::placeholder' : {
				color : '#aab7c4'
			}
		},
		invalid : {
			color : '#fa755a',
			iconColor : '#fa755a'
		}
	};

	//Create an instance of the card Element
	var card = elements.create('card', {
		style : style
	});

	// Add an instance of the card Element into the `card-element` <div>
	card.mount('#card-element');

	// Handle real-time validation errors from the card Element.
	card.addEventListener('change', function(event) {
		var displayError = document.getElementById('card-errors');
		if (event.error) {
			displayError.textContent = event.error.message;
		} else {
			displayError.textContent = '';
		}
	});

	// Handle form submission
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event) {
		event.preventDefault();
		$(this).find(':input[type=submit]').prop('disabled', true);
		stripe.createToken(card).then(function(result) {
			if (result.error) {
				// Inform the user if there was an error
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
			} else {
				// Send the token to your server
				stripeTokenHandler(result.token);
			}
		});
	});

	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);
		// Submit the form
		form.submit();

		App.blockUI({
			target : '#blockui_consumerpayment_data'
		});
	}
</script>

<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>

<script>
	$(document).ready(function() {

		$('#amounttext').attr("readonly", true);
		$('#amounttext1').attr("readonly", false);
		$('#amounttext').keyup(
				function() {
					document.getElementById("optionsRadios28").value = $(
							'#amounttext').val();
				});
		$("#amounttext").keypress(function(event) {
			if (event.which == 45 || event.which == 189) {
				event.preventDefault();
			}
		});
		$('#amounttext1').keyup(
				function() {

					document.getElementById("optionsRadios28").value = $(
							'#amounttext1').val();
				});
		$("#amounttext1").keypress(function(event) {
			if (event.which == 45 || event.which == 189) {
				event.preventDefault();
			}
		});
	});
	
	function clearErrorMsg() {
		document.getElementById('clearError').innerHTML = '';
		document.getElementById('paymentAmountError').innerHTML = '';
		if (!checkFloatingPointNum('totDue')) {
			document.getElementById('paymentAmountError').innerHTML = 'Invalid Amount';
			return false;
		}
		var payTotDue = <c:out value="${totalDue}"/>
		var givenAmt = document.getElementById('totDue').value;
		if (parseFloat(givenAmt) > parseFloat(payTotDue)) {
			document.getElementById('paymentAmountError').innerHTML = 'Amount should be less than or equal to total due';
			return false;
		}
	}
</script>
<!-- <script>
function clickback(){
	var language = "${language}";
	var searchType = "${searchType}"
		var citationId="${citationId}";
	if(language == 'en'){
		window.location.href = "${pageContext.request.contextPath}/consumerdetails/"+searchType+"/"+citationId;
	}else{
		window.location.href = "${pageContext.request.contextPath}/consumerspanishdetails/"+searchType+"/"+citationId;
	}
}
</script> -->

<style>
.StripeElement {
	background-color: white;
	padding: 8px 12px;
	border-radius: 4px;
	border: 1px solid transparent;
	box-shadow: 0 1px 3px 0 #e6ebf1;
	-webkit-transition: box-shadow 150ms ease;
	transition: box-shadow 150ms ease;
}

.StripeElement--focus {
	box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
	border-color: #fa755a;
}

.StripeElement--webkit-autofill {
	background-color: #fefde5 !important;
}
</style>