<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
  @media print {
     @page{size:auto; margin-bottom:.5cm;}
  }
 </style>
 <title>Metro Transit Court &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copy Printer </title>
</head>
<body onload="callPrintPages()" style="background:#173249;">
	<div>
		<h2 id="printMessage"></h2>
		<c:choose>
			<c:when test="${empty images}">
				<td colspan="3" align="center"><b>Unable to load PDF Pages.
						Try again</b></td>
			</c:when>
			<c:otherwise>
				<c:forEach items="${images}" var="page" varStatus="status">
					<div>
						<img src="<c:url value='/previewImage/${page.id}' />" />
					</div>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</div>

	<script>
	function callPrintPages() {
		window.print();
		window.onfocus=function(){
			window.close();
		}
	}
	</script>
</body>
</html>