<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<div class="page-content">
		<br></br>
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<br>
			<div style="text-align: center;">
				<span style="font-size: 16px;"> <b>License Number:</b><span
					style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
					&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
							Number:</b></span>&nbsp;${violationId}
				</span>
			</div>
			<br>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="citationPortlet" class="portlet box blue-dark ">
						<div class="portlet-title">
							<div class="caption">Installment Payment Plan Details</div>
						</div>
						<div class="portlet-body">
							<div class="row"
								style="padding-bottom: 20px; padding-right: 10px; padding-left: 5px;">
								<div class="col-md-12 col-sm-12">
									<div class="col-md-8 col-sm-8"
										style="padding-left: 10px; padding-right: 0px; padding-top: 15px;">
										<div class="portlet box table-bordered"
											style="border-color: #5CD1DB;">
											<div class="portlet-body ">
												<form:form class="form-horizontal" action="#" method="GET"
													modelAttribute="ippForm">
													<div class="form-actions">
														<div class="table-responsive">
															<div class="col-md-12 col-sm-12">
																<div class="col-md-1 col-sm-1"></div>
																<div class="col-md-5 col-sm-5">
																	<table style="font-size: 14px;">
																		<tr>
																			<th><b>Plan&nbsp;Number:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.planNumber}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Plan&nbsp;Status:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.status}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Name:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.violation.patron.firstName}" />
																					&nbsp; <c:out
																						value="${ippForm.violation.patron.lastName}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Address:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.violation.patron.address.address}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>City:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.violation.patron.address.city}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>State:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.violation.patron.address.state}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Zip:&nbsp;</b></th>
																			<td><font
																				style="font-weight: 400px; text-transform: uppercase;"><c:out
																						value="${ippForm.violation.patron.address.zip}" /></font></td>
																		</tr>
																		<tr>
																				<th><b>State Plate:&nbsp;</b></th>
																				<td><font
																					style="font-weight: 400px; text-transform: uppercase;"><c:out
																							value="${ippForm.violation.plateEntity.licenceNumber}" /></font></td>
																		</tr>
																	</table>
																	<br>
																</div>
																<div class="col-md-5 col-sm-5">
																	<table style="font-size: 14px;">
																		<tr>
																			<th><b>Financial&nbsp;Summary</b></th>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Original&nbsp;Due:&nbsp;</td>
																			<td style="text-transform: uppercase;"><font><c:out
																						value="${ippForm.violation.fineAmount}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Previous&nbsp;Credits:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.previousCredits}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Paid&nbsp;to&nbsp;Date:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${paidToDate}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Enroll&nbsp;Amount:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.enrollAmount}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Installment&nbsp;Payment&nbsp;1:&nbsp;<br>(DownPayment)&nbsp;
																			</b></td>
																			<td style="text-transform: uppercase;"
																				style="padding-bottom: 18.5px;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.downPayment}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate1}" />)</font></td>
																		</tr>
																		<%-- <tr style="font-weight: 400px;">
																		<td>Un-enroll&nbsp;Amount:&nbsp;</td>
																		<td>$<font style="font-weight: 400px;"><c:out
																					value="${ippForm.unenrollAmount}" /></font></td>
																	</tr> --%>
																		<tr>
																			<td><p></td>
																		</tr>
																		<tr>
																			<td
																				style="border-top: dashed; border-top-width: thin;"><p></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<th>Balance&nbsp;Due:&nbsp;</th>
																			<td style="text-transform: uppercase;"><b>$<font
																					style="font-weight: 400px;"><c:out
																							value="${ippForm.violation.totalDue}" /></font></b></td>
																		</tr>
																		<%-- <c:forEach var="amount" items="${amountList}" varStatus="loop">
																		<tr style="font-weight: 400px;">
																			<td>Installment&nbsp;Payment&nbsp;${loop.index+2}:&nbsp;</td>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${amount}" /></font></td>
																		</tr>
																	</c:forEach> --%>
																		<tr style="font-weight: 400px;">
																			<td>Installment&nbsp;Payment&nbsp;2:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.installmentAmount}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate2}" />)</font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Installment&nbsp;Payment&nbsp;3:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.installmentAmount}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate3}" />)</font></td>
																		</tr>
																		<c:if test="${ippForm.noOfPayments > 3}">
																			<c:forEach begin="${4}" end="${ippForm.noOfPayments}"
																				varStatus="loop">
																				<tr style="font-weight: 400px;">
																					<td>Installment&nbsp;Payment&nbsp;${loop.index}:&nbsp;</td>
																					<td style="text-transform: uppercase;">$<font
																						style="font-weight: 400px;"><c:out
																								value="${ippForm.installmentAmount}" /></font></td>
																				</tr>
																			</c:forEach>
																		</c:if>
																		<tr style="font-weight: 400px;">
																			<td>Next&nbsp;Payment&nbsp;Amount:&nbsp;</td>
																			<td style="text-transform: uppercase;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${instAmount}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Total&nbsp;Number&nbsp;Of&nbsp;Installments:&nbsp;</td>
																			<td style="text-transform: uppercase;"><font
																				style="font-weight: 400px;"> <c:out
																						value="${ippForm.noOfPayments}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Remaining&nbsp;Installments:&nbsp;</b></td>
																			<td style="text-transform: uppercase;"><b><font
																					style="font-weight: 400px;"> <c:out
																							value="${remainPayemnts}" />
																				</font></b></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td>Starting&nbsp;On:&nbsp;</td>
																			<td style="text-transform: uppercase;"><font
																				style="font-weight: 400px;"> <c:out
																						value="${ippForm.startDate}" /></font></td>
																		</tr>
																	</table>
																	<br>
																</div>
																<div class="col-md-1 col-sm-1"></div>
															</div>
														</div>
														<div class="row" style="margin: 0px;" align="center">
															<c:if test="${ippForm.status == 'PENDING'}">
																<a style="text-decoration: none; color: #fff;"
																	href="${pageContext.request.contextPath}/ippedit/${violationId}"><span
																	class="btn blue-dark btn-sm"
																	style="width: 78px; margin: 2px;">EDIT PLAN</span></a>
															</c:if>
															<c:if
																test="${not empty buttonText &&  buttonText!=null && buttonText!='Paid' && (ippForm.status == 'PENDING' || ippForm.status == 'ACTIVE')}">
																<a style="text-decoration: none; color: #fff;"
																	href="${pageContext.request.contextPath}/payment/${violationId}"><span
																	class="btn blue-dark btn-sm"
																	style="width: 150px; margin: 2px; text-transform: uppercase;">
																		<c:out value="${buttonText}" />
																</span></a>
															</c:if>
														</div>
													</div>
												</form:form>
											</div>
										</div>
									</div>
									<c:if test="${ippForm.status == 'PENDING'}">
										<div class="col-md-3 col-sm-3"
											style="padding-left: 10px; padding-right: 0px; padding-top: 15px;">
											<div class="portlet box blue-dark bg-inverse">
												<div class="portlet-title"
													style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>Cancel Plan</b>
													</div>
												</div>
												<div class="portlet-body " style="padding: 6px;">
													<div class="row" style="margin: 0px;">
														<font style="font-weight: 400px; font-size: 14px;">Cancel
															this payment plan using the button below</font>
													</div>
													<br />
													<div class="row" align="center" style="margin: 0px;">
														<form:form
															action="${pageContext.request.contextPath}/cancelPlan/${ippForm.planNumber}"
															method="post" id="cancelForm">
															<input type="button" class="btn blue-dark btn-sm"
																id="cancelPlan" style="width: 98px;" value="CANCEL PLAN" />
														</form:form>
													</div>
												</div>
											</div>
										</div>
									</c:if>
								</div>
							</div>
							<%-- <h3 class="form-section"
								style="margin-top: -1px; border-bottom-color: #32c5d2"></h3>
							<div class="row" style="margin: 30px;">
								<table class="table  table-hover table-bordered table-sm"
									id="sample_editable_1">
									<thead>
										<tr>
											<th>Plate/License</th>
											<th>Entity Name</th>
											<th>Entity Address</th>
											<th>Amt Enrolled</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.otherId}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.firstName}" /> &nbsp; <c:out
														value="${ippForm.violation.patron.lastName}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.address.formattedAddress}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.enrollAmount}" /></font></td>
										</tr>
									</tbody>
								</table>

							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div id="Confirmation" class="modal"
			style="display: none; top: -55px;">
			<div class="modal-content" align="center" style="width: 27%;">
				<span style="text-align: center;" id="confirmation">Are you
					sure you want to cancel IPP?</span>
				<div class="modal-body" align="center">
					<button type="button" id="confirm" class="btn blue-dark btn-sm">YES</button>
					<button type="button" id="cancel" class="btn blue-dark btn-sm">NO</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <script>

window.onload = function(){
var str = document.getElementById("nixie").value;
    var res = str.toUpperCase();
    document.getElementById("nixie").value = res;
}
    </script> -->

<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>
<style>
.modal {
	display: none;
	position: fixed;
	margin-top: 50px;
	z-index: 1;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	overflow: auto;
	background-color: rgb(0, 0, 0);
	background-color: rgba(0, 0, 0, 0.4);
}

.modal-content {
	background-color: #fefefe;
	margin: 15% auto;
	padding: 20px;
	border: 1px solid #888;
	width: 50%;
	height: auto;
}
</style>
<script type="text/javascript">
$(document).ready( function () {
	$("#cancelPlan").click(function(){
		var modal = document.getElementById("Confirmation");
		modal.style.display = "block";
		$("#confirm").click(function() {
			modal.style.display = "none";
			$("#cancelForm").submit();
		});
		$("#cancel").click(function() {
			modal.style.display = "none";
		});
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		}
	});
	
});
</script>