<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><span><spring:message
							code="lbl.page.bar.permit.search" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet">
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet-body form">
									<!-- BEGIN  SEARCH FORM-->
									<form:form id="permitSearchForm"
										action="${pageContext.request.contextPath}/permitSearch/violation"
										modelAttribute="permitSearchForm" method="post"
										autocomplete="on">
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3"></div>
													<div class="col-md-6">
														<div class="col-md-12">
															<%-- <div class="col-md-4">
																<div class="form-group">
																	<form:input path="ticketNumber" type="text"
																		placeholder="Search..." />
																</div>
															</div>
															<div class="col-md-1"></div>
															<div class="col-md-6" align="center">
																<div class="form-group">
																	<label
																		class="control-label label-sm col-md-4">Search&nbsp;Type</label>
																	<div class="col-md-5">
																		<form:select id="searchType" name="searchType"
																			path="searchType">
																			<form:option value="SL">Select All</form:option>
																			<form:option value="PN">Permit Number</form:option>
																			<form:option value="LN">License Number</form:option>
																		</form:select>
																	</div>
																</div>
															</div>
															<div class="col-md-1">
																<div class="btn-group">
																	<button id="search" type="submit"
																		class="btn blue-dark btn-sm">SEARCH</button>
																</div>
															</div> --%>
															<div class="col-md-1"></div>
													<div class="col-md-10">
														<div >
															<div class="portlet-body " style="padding: 6px;">
																<br />
																<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-5">
																			<div class="form-group">
																				<form:input path="ticketNumber" type="text"
																					placeholder="Search..."
																					style="width: 100%;" />
																			</div>																			
																			<span class="help-block">&nbsp;</span>
																		</div>
																		<div class="col-md-5">
																			<form:select id="searchType" name="searchType"
																				path="searchType" style="width: 100%;height: 30px;">
																				<form:option value="SL">Select All</form:option>
																				<form:option value="PN">Permit Number</form:option>
																				<form:option value="LN">License Number</form:option>
																			</form:select>																			
																			<span class="help-block">&nbsp;</span>
																		</div>
																		<div class="col-md-2">
																			<div class="btn-group">
																				<button id="search" type="submit"
																					class="btn blue-dark btn-sm">SEARCH</button>
																			</div>																			
																			<span class="help-block">&nbsp;</span>
																		</div>
																	</div>
																</div>
																<br />
															</div>
														</div>
													</div>
													<div class="col-md-1"></div>
												</div>
														</div>
													</div>
													<div class="col-md-3"></div>
												<div class="col-md-12">
													<div class="col-md-2"></div>
													<div class="col-md-4" align="center">
														<span class="alert alert-danger" style="display: none;"
															id="searchTextError"></span>
													</div>
													<div class="col-md-6"></div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<div class="row" style="margin: 0px;">
							<div class="col-md-12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<c:if
									test="${not empty permitSearchForm && not empty permitSearchForm.searchType}">
										<div class="portlet light portlet-fit bordered">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-subject font-blue sbold uppercase">Permit
													List</span>
											</div>
											<form:form action="${pageContext.request.contextPath}/samplePermitData"><button type="submit" style="float: right;"
																						class="btn blue-dark btn-sm">Sample Data</button></form:form>
										</div>
										<div class="portlet-body">
											<table class="table  table-hover table-bordered table-sm" id="permitList">
												<thead style="background-color: #004b85; border-color: black;">
													<tr>
													<th class="hidden"></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Permit&nbsp;Number</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Permit&nbsp;Type</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Parking&nbsp;Slot</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Name</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Licence&nbsp;Number&nbsp;/&nbsp;CSU Id</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Start&nbsp;Date&nbsp;Time</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">End&nbsp;Date&nbsp;Time</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Amount&nbsp;Paid</font></th>
													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${not empty permitList}">
															<c:forEach items="${permitList}" var="permit">
																<tr style="text-transform: uppercase;">
																	<%-- <td><b><a onclick="enablePageLoadBar()"
																		style="color: #004b85"
																		href="<c:url value='/violationView/${permit.violationId}' />">
																			<c:out value="${permit.violationId}" />
																	</a></b></td> --%>
																	<td class="hidden"><c:out value="${permit.id}" /></td>
																	<td><c:out value="${permit.permitId}" /></td>
																	<td><c:out value="${permit.permitType}" /></td>
																	<td><c:out value="${permit.parkingSlot}" /></td>
																	<td><c:out value="${permit.firstName}" /> <c:out
																			value="${permit.middleName}" /> <c:out
																			value="${permit.lastName}" /></td>
																	<td><c:out value="${permit.licenceNumber}" /> / <c:out
																			value="${permit.csuId}" /></td>
																	<td><c:out value="${permit.formattedStartDate}" /> <c:out
																			value="${permit.formattedStartTime}" /></td>
																	<td><c:out value="${permit.formattedEndDate}" /> <c:out
																			value="${permit.formattedEndTime}" /></td>
																	<td><c:out value="${permit.amount}" /></td>
																</tr>
															</c:forEach>
														</c:when>
														<c:otherwise>
															<tr style="text-transform: uppercase;">
																<td colspan="10" align="center"><b>No results
																		found</b></td>
															</tr>
														</c:otherwise>
													</c:choose>
												</tbody>

											</table>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
$(document).ready(function() {
    $('#permitList').DataTable({
    	"searching" : false,
    	"info" : false,
    	"orderClasses" : false,
    	"order" : [ [ 0, "desc" ]]
    });
} );
	function validateSearchForm() {
		var searchText = $("#ticketNumber").val();
		if (searchText.length == 0) {
			$("#searchTextError").attr('style', "display:block");
			$("#searchTextError").text(
					"Please enter permit Number or License Number");
			return false;
		}
		return true;
	}
</script>