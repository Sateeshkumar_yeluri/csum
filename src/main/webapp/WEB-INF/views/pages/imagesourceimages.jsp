<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<link
	href="<c:url value='/static/assets/global/plugins/datatables/datatables.min.css' />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value='/static/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css' />"
	rel="stylesheet" type="text/css" />


<!-- BEGIN CONTAINER -->

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" style="background:#173249;">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content" align="center">
		<!-- BEGIN PAGE HEADER-->
			<img src="<c:url value='/previewImage/${defaultPreview}' />" />
		<div>
		</div>
	</div>
</div>