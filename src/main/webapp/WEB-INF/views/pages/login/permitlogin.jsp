<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5" style="background-color: #173249;">
	<div class="row bs-reset">
		<div class="logo">
			<img
				src="<c:url value='/static/assets/layouts/layout/img/logo1.png' />"
				alt="" class="logo-default" />
		</div>
		<div class="content" >
			<!-- BEGIN LOGIN FORM -->
			<div class="row" align="center" style="margin:0px;">
			<h3>
				<b> CSU Maritime Permit Login </b>
			</h3>
			</div>
			<span class="help-block">&nbsp;</span>
			<form:form id="loginForm" action="permitLogin" class="login-form"
				method="post" modelAttribute="permitLoginBean">
				<div class="row">
					<div class="input-icon">
						<i class="fa fa-user"></i>
						<form:input
							class="form-control form-control-solid placeholder-no-fix form-group"
							type="text" autocomplete="off" placeholder="EmpNo"
							name="username" autofocus="username" path="username"
							required="true" />
					</div>
					<div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="input-icon">
						<i class="fa fa-lock"></i>
						<form:input
							class="form-control form-control-solid placeholder-no-fix form-group"
							type="password" autocomplete="off" placeholder="Password"
							name="password" path="password" required="true"
							onkeypress="submitFormByEnter(event,this)" />
					</div>
				</div>
				<div class="row" align="center">
				<div class="form-actions">
					<button type="submit" class="btn blue-dark uppercase">Sign In</button>
					<%-- <a href="${pageContext.request.contextPath}/forgotpassword" style="color:white;" id="forget-password" class="forget-password">Forgot Password?</a> --%>
				</div>
				</div>
				<div class="form-actions blink" align="center"><span style="color:white;">Are you a visitor?&nbsp;<a href="${pageContext.request.contextPath}/visitorPermitPurchase" style="color:#deb40f;">click here</a></span></div>
				<br />
				<c:if test="${not empty resultMessage}">
					<div class="alert alert-danger">
						<span>${resultMessage}</span> Invalid Credentials
					</div>
				</c:if>
				<div class="alert alert-danger display-hide">
					<button class="close" data-close="alert"></button>
					<span>Enter any username and password. </span>
				</div>

			</form:form>


		</div>
		<div class="copyright">2018 &copy; Axiom xCell Inc.</div>
	</div>
</div>

<script type="text/javascript">
/*$(function() {
    $("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('button[type=submit] .default').click();
            return false;
        } else {
            return true;
        }
    });
});*/

function submitFormByEnter(e, text){
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) { //Enter keycode
     document.getElementById("loginForm").submit();
    }
}
</script>
