<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<!-- BEGIN HEADER -->
<header class="page-header"
	style="border-bottom-width: 0px; margin-bottom: 0px; padding-bottom: 0px;">
	<nav class="navbar mega-menu" role="navigation"
		style="">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon" style="color: #deb40f;">
                                    <span class="fa fa-bars"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
			<!-- BEGIN HEADER MENU -->
			<div
				class="nav-collapse collapse navbar-collapse navbar-responsive-collapse navbar-fixed"
				style="margin-left: 50px; margin-top: 20px;">
				<input type="hidden" id="numOfRec" name="numOfRec" value="10" /> <input
					type="hidden" name="pagenatorVal" id="pagenatorVal" />
				<ul class="nav navbar-nav nav-pills nav-pills-nav-tab"
					style="margin-right: 30px; margin-bottom: 0px; background-color: #173249;">
					<c:choose>
						<c:when
							test="${not empty activeOnPending && activeOnPending=='active'}">
							<li class="nav-tab ${activeOnPending}" id="Pending"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Pending/All' />"><b><font
										color="#364150">Pending</font></b></a></li>
						</c:when>
						<c:otherwise>
							<li class="nav-tab ${activeOnPending}" id="Pending"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Pending/All' />"><b><font
										color="#fff">Pending</font></b></a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when
							test="${not empty activeOnComplete && activeOnComplete=='active'}">
							<li class="nav-tab ${activeOnComplete}"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Complete/All' />"><b><font
										color="#364150">Complete</font></b></a></li>
						</c:when>
						<c:otherwise>
							<li class="nav-tab ${activeOnComplete}"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Complete/All' />"><b><font
										color="#fff">Complete</font></b></a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when
							test="${not empty activeOnSearch && activeOnSearch=='active'}">
							<li class="nav-tab ${activeOnSearch}"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Search/All' />"><b><font
										color="#364150">Lookup</font></b></a></li>
						</c:when>
						<c:otherwise>
							<li class="nav-tab ${activeOnSearch}"
								style="background-color: #004b85;"><a
								href="<c:url value='/adjudicatorDashboardSupervisor/Search/All' />"><b><font
										color="#fff">Lookup</font></b></a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
					<c:when test="${hearingRequired=='Pending'}">
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chb" id="All">
								<font color="#fff" size="3px">All</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chb" id="InPerson">
								<font color="#fff" size="3px">In-Person / Scheduled</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chb" id="WalkIn"><font
								color="#fff" size="3px">In-Person / Walk In</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px;">
							<label class="navbar-link"> <input id="Written"
								style="margin-top: 7px;" type="radio" class="chb"> <font
								color="#fff" size="3px">Written</font>
							</label>
						</li>
					</c:when>
					<c:when test="${hearingRequired=='Complete'}">
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc" id="All">
								<font color="#fff" size="3px">All</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px; padding-left: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc"
								id="InProgress"> <font color="#fff" size="3px">In-Progress</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px; padding-left: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc"
								id="PendingReview"><font color="#fff" size="3px">Pending Review</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px; padding-left: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc"
								id="PendingMail"> <font color="#fff" size="3px">Pending Mail</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px; padding-left: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc" id="FTA">
								<font color="#fff" size="3px">FTA</font>
							</label>
						</li>
						<li class="radio radio-inline navbar-btn"
							style="margin-bottom: 0px; margin-top: 8px; padding-left: 0px;">
							<label class="navbar-link"> <input
								style="margin-top: 7px;" type="radio" class="chbc" id="Complete">
								<font color="#fff" size="3px">Complete</font>
							</label>
						</li>
					</c:when>
				</c:choose>
				</ul>
				
			</div>
			<!-- END HEADER MENU -->


		</div>
	</nav>
</header>
<div class="page-container" style="margin-top: 8px;">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br />
		<c:choose>
			<c:when test="${hearingRequired=='Search'}">
				<div class="row">
					<form:form
						action="${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Search/${checkbox}"
						method="GET" modelAttribute="hearingForm" id="dateForm">
						<div class="row" align="center">
							<label>Search Type</label>
							<form:select id="searchType" style="text-transform: uppercase;"
								path="searchType">
								<form:option value="">&nbsp;&nbsp;&nbsp;</form:option>
								<form:option value="VN">&nbsp;Violation#</form:option>
								<form:option value="FN">&nbsp;Name</form:option>
								<%-- 
							<form:option value="LP">&nbsp;Lic.Plate# or ID#</form:option> --%>
							</form:select>
							<form:input type="text" name="searchtext" id="myInput"
								style="text-transform: uppercase;height:27px;"
								path="searchValue"></form:input>
							<button id="search" type="submit" class="btn blue-dark btn-sm"
								style="margin-bottom: 5px; padding-bottom: 3px; padding-top: 3px;">Search</button>
						</div>
					</form:form>
					<span class="help-block"> &nbsp;</span>
					<div class="row" style="margin-left: 48px; margin-right: 48px;">
						<table class="table table-striped table-hover order-column"
							id="myTable">
							<thead style="background-color: #004b85; border-color: black;">
								<tr>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Violation&nbsp;Number</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Status&nbsp;/ Issued&nbsp;On</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Name</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Address</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Violation&nbsp;Code&nbsp;& Description</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Violation&nbsp;Location</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Fine&nbsp;Amt</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Penalties</font></th>
									<th style="border-right: 1px solid #fff; padding-right: 15px;"><font
										color="#fff">Total&nbsp;Due</font></th>
								</tr>
							</thead>
							<tbody id="tableBody">
								<c:choose>
									<c:when test="${not empty violationList}">
										<c:forEach items="${violationList}" var="violation">
											<tr>
												<td style="padding-right: 15px;"><a
													style="color: #004b85"
													href="<c:url value='/hearingsEdit/${violation.violationId}/0' />"><b><c:out
																value="${violation.violationId}"></c:out></b></a></td>
												<td style="padding-right: 15px; text-transform: uppercase;"><c:choose>
														<c:when test="${violation.status == 'OVERPAID'}">
															<b><c:out value="CLOSED - OVERPAID" /></b>
														</c:when>
														<c:when test="${violation.status == 'VIOD'}">
															<b><c:out value="CLOSED - VOID" /></b>
														</c:when>
														<c:otherwise>
															<b><c:out value="${violation.status}" /></b>
														</c:otherwise>
													</c:choose> / <c:out value="${violation.dateOfViolation}" /></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${violation.patron.firstName}" /> <c:out
																value="${violation.patron.lastName}" /></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${violation.patron.address.formattedAddress}" /></td>
													
												<td style="text-transform: uppercase; padding-right: 15px;"><c:out
														value="${violation.violationCode.code}" />/<c:out
														value="${violation.violationCode.description}" /></td>
												<td style="text-transform: uppercase; padding-right: 15px;"><c:out
														value="${violation.locationOfViolation}" /></td>
												<!--<td><a style="color:#004b85" data-toggle="modal" href="#myModal"><b>$<c:out
																					value="${violation.totalDue}" /></b></a></td>-->
												<td style="text-transform: uppercase; padding-right: 15px;"><c:out
														value="${violation.fineAmount}" /></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;">$<c:out
																value="0.00" /></td>
												<td style="text-transform: uppercase; padding-right: 15px;">$<c:out
														value="${violation.totalDue}" /></td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="9" align="center"
												style="text-transform: uppercase;">No results found</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</c:when>
			<c:when test="${hearingRequired=='Pending'}">
			<div class="row" style="margin-left: 25px;margin-right: 0px;">
					<div class="col-md-12">
				<form:form
					action="${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Search/${checkbox}"
					method="GET" modelAttribute="hearingForm" id="dateForm">
					<div class="col-md-1" style="margin-right: 20px;">
						<div class="form-group">
							<form:select id="pendingDropdown"
								style="padding-bottom: 3px;text-transform: uppercase;"
								name="dropdown" path="HearingBasis">
								<form:option value="Daily">&nbsp;&nbsp;&nbsp;Daily</form:option>
								<form:option value="Weekly">&nbsp;&nbsp;&nbsp;Weekly</form:option>
								<form:option value="Monthly">&nbsp;&nbsp;&nbsp;Monthly</form:option>
								<form:option value="Annually">&nbsp;&nbsp;&nbsp;Annually</form:option>
							</form:select>
						</div>
					</div>
					<div class="col-md-2">
						<div
							class="form-group input-group input-group-sm date date-picker"
							data-date-format="mm/dd/yyyy" data-date-viewmode="months">
							<form:input class="form-control" id="pendingTextbox"
								path="requiredDate" name="dateTextbox" />
							<span class="input-group-btn">
								<button class="btn default" type="button" id="calender">
									<i class="fa fa-calendar"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="row" style="float: right; margin-right: 47px;">
						<label>Search Type</label>
						<!-- <select id="searchType"
						style="padding-bottom: 3px;">
						<option value="">&nbsp;&nbsp;&nbsp;</option>
						<option value="VN">&nbsp;Violation#</option>
						<option value="FN">&nbsp;Name</option>
						<option value="LP">&nbsp;Lic.Plate# or ID#</option>
					</select> <input type="text" name="searchtext" id="myInput"
						onkeyup="myFunction()"> -->
						<form:select id="searchType" style="text-transform: uppercase;"
							path="searchType">
							<form:option value="">&nbsp;&nbsp;&nbsp;</form:option>
							<form:option value="VN">&nbsp;Violation#</form:option>
							<form:option value="FN">&nbsp;Name</form:option>
							<%-- 
							<form:option value="LP">&nbsp;Lic.Plate# or ID#</form:option> --%>
						</form:select>
						<form:input type="text" name="searchtext" id="myInput"
							style="text-transform: uppercase;height:27px;" path="searchValue"></form:input>
						<button id="search" type="submit" class="btn blue-dark btn-sm"
							style="margin-bottom: 5px; padding-bottom: 3px; padding-top: 3px;">Search</button>
					</div>
				</form:form>
				</div>
				</div>
				<div class="row" style="margin-left: 48px; margin-right: 48px;">
					<table class="table table-striped table-hover order-column"
						id="myTable">
						<thead style="background-color: #004b85; border-color: black;">
							<tr><th
									style="width: 5%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Hearing&nbsp;Date</font></th>
								<th
									style="width: 13%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Hearing&nbsp;Time</font></th>
								<th
									style="width: 11.5%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Violation#</font></th>
								<th
									style="width: 13%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Hearing&nbsp;Type</font></th>
								<th
									style="width: 4%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Translator</font></th>
								<th
									style="width: 14%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Last&nbsp;Name</font></th>
								<th
									style="width: 10%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">First&nbsp;Name</font></th>
								<th
									style="width: 24%; border-right: 1px solid #fff; padding-right: 15px;"><font
									color="#fff">Violation&nbsp;Code</font></th>
								<!-- <th style="width: 10%; border-right: 1px solid #fff;padding-right:15px;"><font
											color="#fff">Lic.Plate#&nbsp;or&nbsp;ID#</font></th> -->
								<th style="width: 26%; padding-right: 15px;"><font
									color="#fff">Assignment</font></th>
								<th class="hidden">Hearing&nbsp;Time</th>
								<th class="hidden">Hearing&nbsp;Date</th>
							</tr>
						</thead>
						<tbody id="tableBody">
							<c:if test="${not empty hearingListWritten}">
								<c:forEach items="${hearingListWritten}" var="hearing">
									<tr>
										<form:form id="form_${hearing.id}"
											action="${pageContext.request.contextPath}/assignHearing/${hearing.id}"
											method="POST" autocomplete="on" enctype="multipart/form-data">
											<td style="padding-right: 15px;"><c:out
																value="NA"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="NA"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><a
												style="color: #004b85"
												href="<c:url value='/hearingsEdit/${hearing.violation.violationId}/${hearing.id}' />"><b><c:out
															value="${hearing.violation.violationId}"></c:out></b></a></td>

											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="Written"></c:out></td>

											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.hearingdetail.translation}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.patron.lastName}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.patron.firstName}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.violationCode.code}"></c:out> <br>
												<c:out value="${hearing.violation.violationCode.description}"></c:out></td>

											<%-- <td style="text-transform: uppercase;padding-right: 15px;"><c:out
															value="${hearing.violation.plateEntity.licenceNumber}"></c:out></td> --%>
											<c:choose>
												<c:when test="${empty hearing.hearingOfficer}">
													<td style="text-transform: uppercase; padding-right: 15px;"><select
														class="assign"
														id="${hearing.id}_${hearing.violation.violationId}"
														style="margin-top: 8px;">
															<option value="Unassigned">Unassigned</option>
															<c:forEach items="${userList}" var="user">
																<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
															</c:forEach>
															<option value="cancelHearing">Cancel</option>
													</select>&nbsp;<input type="submit"
														class="btn blue-dark btn-sm submit"
														style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
														value="Submit" id="assignSubmit_${hearing.id}" /></td>
												</c:when>
												<c:otherwise>
													<td style="padding-right: 15px;"><select
														class="assign" id="${hearing.id}"
														style="margin-top: 8px; text-transform: uppercase;">
															<option value="Unassigned">Unassigned</option>
															<c:forEach items="${userList}" var="user">
																<c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.hearingOfficer, user.userName)}">
																		<option selected value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																	</c:when>
																	<c:otherwise>
																		<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
															<option value="cancelHearing">Cancel</option>
													</select>&nbsp;<input type="submit"
														class="btn blue-dark btn-sm submit"
														style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
														value="Submit" id="assignSubmit_${hearing.id}" /></td>
												</c:otherwise>
											</c:choose>
											<td class="hidden" style="padding-right: 15px;"><c:out
													value="${hearing.hearingTime}" /></td>
											<td class="hidden" style="padding-right: 15px;"><c:out
													value="${hearing.hearingDate}" /></td>
										</form:form>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${not empty hearingList}">
								<c:forEach items="${hearingList}" var="hearing">
									<tr>
										<form:form id="form_${hearing.id}"
											action="${pageContext.request.contextPath}/assignHearing/${hearing.id}"
											method="POST" autocomplete="on" enctype="multipart/form-data">
											<c:choose>
												<c:when test="${not empty hearing.hearingDate}">
													<td style="padding-right: 15px;"><c:out
																value="${hearing.formattedHearingDate}"></c:out></td>
												</c:when>
												<c:otherwise>
													<td style="padding-right: 15px;"><c:out
																value="NA"></c:out></td>
												</c:otherwise>
											</c:choose>											
											<c:choose>
												<c:when
													test="${fn:containsIgnoreCase(hearing.type,'WRITTEN')}">
													<td style="padding-right: 15px; text-transform: uppercase;"><c:out
															value="NA"></c:out></td>
												</c:when>
												<c:otherwise>
													<td style="padding-right: 15px; text-transform: uppercase;"><fmt:formatDate
															type="time" timeStyle="short" pattern="hh:mm a"
															value="${hearing.hearingTime}" /></td>
												</c:otherwise>
											</c:choose>
											<td style="padding-right: 15px; text-transform: uppercase;"><a
												style="color: #004b85"
												href="<c:url value='/hearingsEdit/${hearing.violation.violationId}/${hearing.id}' />"><b><c:out
															value="${hearing.violation.violationId}"></c:out></b></a></td>
											<c:choose>
												<c:when test="${hearing.type == 'PERSON'}">
													<td style="text-transform: uppercase; padding-right: 15px;"><c:out
															value="IN-PERSON / SCHEDULED"></c:out></td>
												</c:when>
												<c:when test="${hearing.type == 'WALKIN'}">
													<td style="text-transform: uppercase; padding-right: 15px;"><c:out
															value="IN-PERSON / WALK IN"></c:out></td>
												</c:when>
												<c:when test="${hearing.type == 'WRITTEN'}">
													<td style="text-transform: uppercase; padding-right: 15px;"><c:out
															value="Written"></c:out></td>
												</c:when>
												<c:otherwise>
																<td><c:out value=""></c:out></td>
												</c:otherwise>
											</c:choose>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.hearingdetail.translation}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.patron.lastName}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.patron.firstName}"></c:out></td>
											<td style="text-transform: uppercase; padding-right: 15px;"><c:out
													value="${hearing.violation.violationCode.code}"></c:out> <br>
												<c:out value="${hearing.violation.violationCode.description}"></c:out></td>
											<%-- 
													<td style="text-transform: uppercase;padding-right: 15px;"><c:out
															value="${hearing.violation.plateEntity.licenceNumber}"></c:out></td> --%>
											<c:choose>
												<c:when test="${empty hearing.hearingOfficer}">
													<td style="padding-right: 15px;"><select
														class="assign" id="${hearing.id}"
														style="margin-top: 8px; text-transform: uppercase;">
															<option value="Unassigned">Unassigned</option>
															<c:forEach items="${userList}" var="user">
																<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
															</c:forEach>
															<option value="cancelHearing">Cancel</option>
													</select>&nbsp;<input type="submit"
														class="btn blue-dark btn-sm submit"
														style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
														value="Submit" id="assignSubmit_${hearing.id}" /></td>
												</c:when>
												<c:otherwise>
													<td style="padding-right: 15px;"><select
														class="assign" id="${hearing.id}"
														style="margin-top: 8px; text-transform: uppercase;">
															<option value="Unassigned">Unassigned</option>
															<c:forEach items="${userList}" var="user">
																<c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.hearingOfficer, user.userName)}">
																		<option selected value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																	</c:when>
																	<c:otherwise>
																		<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
															<option value="cancelHearing">Cancel</option>
													</select>&nbsp;<input type="submit"
														class="btn blue-dark btn-sm submit"
														style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
														value="Submit" id="assignSubmit_${hearing.id}" /></td>
												</c:otherwise>
											</c:choose>
											<td class="hidden"><c:out value="${hearing.hearingTime}" /></td>
											<td class="hidden"><c:out value="${hearing.hearingDate}" /></td>
										</form:form>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
					<div>
						</div>
					<!-- <div style="margin-bottom: 15px">
						<a style="text-decoration: none; color: #fff;" id="testScript"><span
							class="btn blue-dark btn-sm"
							style="float: right; margin-bottom: 2px;">REGENERATE TEST
								SCRIPTS</span></a>
					</div> -->
				</div>

			</c:when>
			<c:otherwise>
				<div class="row" style="margin-left: 45px;">
					<div class="col-md-12">
						<form:form
							action="${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Complete/${checkbox}"
							method="GET" modelAttribute="hearingForm" id="dateForm">
							<%-- <div class="col-md-3" style="width: 20%;">
								<div class="form-group">
									<form:select style="padding-bottom: 3px;"
										path="hearingOrViolation">
										<form:option value="Hearing">&nbsp;&nbsp;&nbsp;Hearings Only</form:option>
										<form:option value="Violation">&nbsp;&nbsp;&nbsp;Violations Only</form:option>
										<form:option value="HearingAndViolation">&nbsp;&nbsp;&nbsp;Both Hearings and Violations</form:option>

									</form:select>
								</div>
							</div> --%>
							<div class="col-md-1" style="margin-right: 20px;">
								<div class="form-group">
									<form:select id="dropdown"
										style="padding-bottom: 3px;text-transform: uppercase;"
										name="dropdown" path="HearingBasis">
										<form:option value="Daily">&nbsp;&nbsp;&nbsp;Daily</form:option>
										<form:option value="Weekly">&nbsp;&nbsp;&nbsp;Weekly</form:option>
										<form:option value="Monthly">&nbsp;&nbsp;&nbsp;Monthly</form:option>
										<form:option value="Annually">&nbsp;&nbsp;&nbsp;Annually</form:option>
									</form:select>
								</div>
							</div>
							<div class="col-md-2">
								<div
									class="form-group input-group input-group-sm date date-picker"
									data-date-format="mm/dd/yyyy" data-date-viewmode="months">
									<form:input class="form-control" id="textbox"
										path="requiredDate" name="dateTextbox" />
									<span class="input-group-btn">
										<button class="btn default" type="button" id="calender">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<a style="text-decoration: none; color: #fff;" id="export"><span
										class="btn blue-dark btn-sm"
										style="width: 65px; margin-bottom: 2px; margin-left: 10px;">EXPORT</span></a>
								</div>
							</div>
							<div class="inline" style="float: right; margin-right: 75px;">
								<!-- <label>Search Type</label> <select id="searchType"
									style="padding-bottom: 3px;">
									<option value=""></option>
									<option value="VN">&nbsp;Violation#</option>
									<option value="HT">&nbsp;Hearing Type</option>
									<option value="FN">&nbsp;Name</option>
									<option value="LP">&nbsp;Lic.Plate# or ID#</option>
								</select> <input type="text" name="searchtext" id="myInput"
									onkeyup="myFunction()"> -->
								<label>Search Type</label>
								<!-- <select id="searchType"
						style="padding-bottom: 3px;">
						<option value="">&nbsp;&nbsp;&nbsp;</option>
						<option value="VN">&nbsp;Violation#</option>
						<option value="FN">&nbsp;Name</option>
						<option value="LP">&nbsp;Lic.Plate# or ID#</option>
					</select> <input type="text" name="searchtext" id="myInput"
						onkeyup="myFunction()"> -->
								<form:select id="searchType" style="text-transform: uppercase;"
									path="searchType">
									<form:option value="">&nbsp;&nbsp;&nbsp;</form:option>
									<form:option value="VN">&nbsp;Violation#</form:option>
									<form:option value="FN">&nbsp;Name</form:option>
									<%-- 
							<form:option value="LP">&nbsp;Lic.Plate# or ID#</form:option> --%>
								</form:select>
								<form:input type="text" name="searchtext" id="myInput"
									style="text-transform: uppercase;height:27px;"
									path="searchValue"></form:input>
								<button id="search" type="submit" class="btn blue-dark btn-sm"
									formaction="${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Search/${checkbox}"
									style="margin-bottom: 5px; padding-bottom: 3px; padding-top: 3px;">Search</button>
							</div>
						</form:form>
					</div>
				</div>


				<div class="row" style="margin-left: 75px; margin-right: 75px;">

					<c:choose>
						<c:when test="${not empty hearingList}">
							<div class="table-responsive scroller "
								style="width: 100%; padding-right: 0px;">
								<table class="table table-striped table-hover order-column "
									id="myTable" style="width: 2150px; margin-bottom: 0px;">
									<thead style="background-color: #004b85;">
										<tr>
											<th
												style="width: 5%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Hearing&nbsp;Date</font></th>
											<th
												style="width: 5%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Hearing&nbsp;Time</font></th>
											<th
												style="border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Violation#</font></th>
											<th
												style="width: 10.2%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Status</font></th>
											<th
												style="width: 5%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Hearing&nbsp;Type</font></th>
											<th
												style="width: 4%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Translator</font></th>
											<th
												style="border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Last&nbsp;Name</font></th>
											<th
												style="border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">First&nbsp;Name</font></th>
											<th
												style="border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Violation&nbsp;Code</font></th>
											<!-- <th style="width: 6%; border-right: 1px solid #fff;padding-right:15px;"><font
												color="#fff">Lic.Plate#&nbsp;or&nbsp;ID#</font></th> -->
											<th
												style="width: 15%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff">Hearing&nbsp;Officer </font></th>
											<th
												style="width: 6%; border-right: 1px solid #fff; padding-right: 15px;"><font
												color="#fff"> Decision </font></th>
											<!-- <th style="border-right: 1px solid #fff;padding-right:15px;"><font
												color="#fff">Notes</font></th> -->
											<th style="width: 6%; padding-right: 15px;"><font
												color="#fff">Date&nbsp;Mailed </font></th>
											<th class="hidden">Hearing Time</th>
										</tr>
									</thead>
									<tbody id="tableBody">
										<c:if test="${not empty hearingList}">
											<c:forEach items="${hearingList}" var="hearing">
												<tr>
													<form:form id="form_${hearing.id}"
														action="${pageContext.request.contextPath}/assignHearing/${hearing.id}"
														method="POST" autocomplete="on"
														enctype="multipart/form-data">
														<td style="padding-right: 15px;"><c:out
																value="${hearing.formattedHearingDate}"></c:out></td>
														<td style="padding-right: 15px;"><fmt:formatDate
																type="time" timeStyle="short" pattern="hh:mm a"
																value="${hearing.hearingTime}" /></td>
														<td style="padding-right: 15px;"><a
															style="color: #004b85"
															href="<c:url value='/hearingsEdit/${hearing.violation.violationId}/${hearing.id}' />"><b><c:out
																		value="${hearing.violation.violationId}"></c:out></b></a></td>
														<c:choose>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: #32c5d2;"><b><c:out
																				value="In-Progress"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: blue;"><b><c:out
																				value="Pending Review-Updated"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: blue;"><b><c:out
																				value="Pending Review"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: green;"><b><c:out
																				value="Complete"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: #004b85;"><b><c:out
																				value="Pending Mail"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: #e51010;"><b><c:out
																				value="Needs Rework"></c:out></b></font></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><font
																	style="color: red;"><b><c:out
																				value="Failed to Appear"></c:out></b></font></td>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${hearing.type == 'PERSON'}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="IN-PERSON / SCHEDULED"></c:out></td>
															</c:when>
															<c:when test="${hearing.type == 'WALKIN'}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="IN-PERSON / WALK IN"></c:out></td>
															</c:when>
															<c:when test="${hearing.type == 'WRITTEN'}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Written"></c:out></td>
															</c:when>
															<c:otherwise>
																<td><c:out value=""></c:out></td>
															</c:otherwise>
														</c:choose>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${hearing.hearingdetail.translation}"></c:out></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${hearing.violation.patron.lastName}"></c:out></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${hearing.violation.patron.firstName}"></c:out></td>
														<td
															style="text-transform: uppercase; padding-right: 15px;"><c:out
																value="${hearing.violation.violationCode.code}"></c:out>
															<br> <c:out
																value="${hearing.violation.violationCode.description}"></c:out></td>
														<%-- <td style="text-transform: uppercase;padding-right: 15px;"><c:out
																value="${hearing.violation.plateEntity.licenceNumber}"></c:out></td> --%>
														<td style="padding-right: 15px;"><c:choose>
																<c:when
																	test="${fn:containsIgnoreCase(hearing.status,'COMPLETE') || fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																	<c:out value="${hearing.hearingOfficerName}"></c:out>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${empty hearing.hearingOfficer}">
																			<select class="assign" id="${hearing.id}"
																				style="margin-top: 8px; text-transform: uppercase;">
																				<option value="Unassigned">Unassigned</option>
																				<c:forEach items="${userList}" var="user">
																					<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>

																				</c:forEach>
																			</select>&nbsp;<input type="submit"
																				class="btn blue-dark btn-sm submit"
																				style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
																				value="Submit" id="assignSubmit_${hearing.id}" />
																		</c:when>
																		<c:otherwise>
																			<select class="assign" id="${hearing.id}"
																				style="margin-top: 8px;">
																				<option value="Unassigned">Unassigned</option>
																				<c:forEach items="${userList}" var="user">
																					<c:choose>
																						<c:when
																							test="${fn:containsIgnoreCase(hearing.hearingOfficer, user.userName)}">
																							<option selected value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																						</c:when>
																						<c:otherwise>
																							<option value="${user.userName}">${user.firstName}&nbsp;${user.lastName}</option>
																						</c:otherwise>
																					</c:choose>
																				</c:forEach>
																			</select>&nbsp;<input type="submit"
																				class="btn blue-dark btn-sm submit"
																				style="visibility: hidden; padding-left: 6px; padding-right: 6px; padding-top: 2px; padding-bottom: 2px; margin-bottom: 2px;"
																				value="Submit" id="assignSubmit_${hearing.id}" />
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose></td>
														<c:choose>
															<c:when
																test="${fn:containsIgnoreCase(hearing.hearingdetail.decision,'Not_Liable')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Not Liable"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.hearingdetail.decision,'LiableComSer')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Liable with Community Service"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.hearingdetail.decision,'AdmsnComSer')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Admission Community Service"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.hearingdetail.decision,'LiableIpp')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Liable with IPP"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.hearingdetail.decision,'Liable')}">
																<td
																	style="text-transform: uppercase; padding-right: 15px;"><c:out
																		value="Liable"></c:out></td>
															</c:when>
															<c:otherwise>
																<td style="padding-right: 15px;"><c:out value=""></c:out></td>
															</c:otherwise>
														</c:choose>
														<%-- <td style="text-transform: uppercase;padding-right: 15px;"><c:out value="${hearing.hearingdetail.notes}"></c:out></td> --%>
														<td style="padding-right: 15px;"><c:out
																value="${hearing.formattedDateMailed}"></c:out></td>
														<td class="hidden" style="padding-right: 15px;"><c:out
																value="${hearing.hearingTime}" /></td>
													</form:form>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>

								</table>
							</div>
						</c:when>
						<c:otherwise>
							<div class="table-responsive scroller "
								style="width: 100%; padding-right: 0px; overflow-x: scroll;">
								<table class="table table-striped table-hover order-column "
									style="margin-bottom: 0px;">
									<thead style="background-color: #004b85;">
										<tr>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Hearing&nbsp;Date</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Hearing&nbsp;Time</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Violation#</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Status</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Hearing&nbsp;Type</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Translator</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Last&nbsp;Name</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">First&nbsp;Name</font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Violation&nbsp;Code</font></th>
											<!-- <th style="border-right: 1px solid #fff;"><font
												color="#fff">Lic.Plate#&nbsp;or&nbsp;ID#</font></th> -->
											<th style="border-right: 1px solid #fff;"><font
												color="#fff">Hearing&nbsp;Officer </font></th>
											<th style="border-right: 1px solid #fff;"><font
												color="#fff"> Decision </font></th>
											<!-- 	<th style="border-right: 1px solid #fff;"><font
												color="#fff">Notes</font></th> -->
											<th><font color="#fff">Date&nbsp;Mailed </font></th>
											<th class="hidden">Hearing Time</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="12" align="center"
												style="text-transform: uppercase;">No results found</td>
										</tr>
									</tbody>
								</table>
							</div>

						</c:otherwise>

					</c:choose>
					<!-- <div style="margin-bottom: 15px">
						<a onclick="enablePageLoadBar()"
							style="text-decoration: none; color: #fff;"><span
							class="btn blue-dark btn-sm" id="testScript"
							style="float: right; margin-bottom: 2px; margin-top: 10px;">REGENERATE
								TEST SCRIPTS</span></a>
					</div> -->
				</div>

			</c:otherwise>
		</c:choose>
		<input class="hidden" type="text" id="isEdited" value="${isEdited}">
	</div>
	<div style="height: 35px;"></div>
</div>

<div id="Confirmation" class="modal" style="display: none; top: -55px;">
	<div class="modal-content" align="left">
		<span style="text-align: center;" id="confirmation"
			style="text-transform: uppercase;"> Thanks for making the
			request. Once the reports are ready will send an email. </span>
		<div class="modal-body" align="center">

			<button type="button" id="ok" class="btn blue-dark btn-sm">OK</button>
		</div>
	</div>
</div>

<!-- END CONTENT -->
<!-- END CONTAINER -->
<style>
.modal {
	display: none;
	position: absolute;
	z-index: 1;
	left: 0;
	top: 50;
	width: 100%;
	height: 100%;
	overflow: auto;
	background-color: rgb(0, 0, 0);
	background-color: rgba(0, 0, 0, 0.4);
}

.modal-content {
	background-color: #fefefe;
	margin: 15% auto;
	padding: 20px;
	border: 1px solid #888;
	width: 30%;
	height: auto;
}

table.dataTable tbody th, table.dataTable tbody td {
	white-space: nowrap;
}
</style>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>
<script>
	function setDatepicker(e) {
		var selectedValue = e.value;
		var presentDate = "${presentDate}";

	}
	$(document)
			.ready(
					function() {
						document.documentElement.style.overflowX = 'hidden';
						var checkbox = "${checkbox}";
						var hearingRequired = "${hearingRequired}";
						$('#search').click(
								function() {
									$('#dateForm').submit(
											function() {
												if ($('#myInput').val() != ""
														&& $('#searchType')
																.val() != "") {
													return true;
												} else {
													return false;
												}
											})
								});
						$('#myInput')
								.keyup(
										function() {
											var val = $('#myInput').val()
													.toUpperCase().trim();
											if (val == '' || val == '') {
												$("#searchType").val("");
											} else if (val.startsWith("T") === true
													|| val.startsWith("P") === true) {
												$("#searchType").val("VN");
												if (val.length > 1) {
													if ($
															.isNumeric(val
																	.substring(
																			1,
																			val.length))) {
														$("#searchType").val(
																"VN");
													} else if (/^[a-zA-Z]+$/
															.test(val)) {
														$("#searchType").val(
																"FN");
													} else {
														$("#searchType").val(
																"VN");
													}
												}
											} else if (/^[a-zA-Z]+$/.test(val)) {
												$("#searchType").val("FN");
											} else {
												$("#searchType").val("VN");
											}
										});
						if (hearingRequired == 'Complete') {
							document.getElementById(checkbox).checked = true;
							$('#myTable').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"stateSave" : true,
								"columnDefs" : [ {
									"orderData" : [ 12 ],
									"targets" : [ 1 ]
								}, {
									"targets" : [ 12 ],
									"visible" : false,
									"searchable" : false
								} ],
								"order" : [ [ 0, "asc" ], [ 1, "asc" ] ]
							});
							var presentDate = "${presentDate}";
							$(".chbc").change(
									function() {
										$(".chbc").prop('checked', false);
										$(this).prop('checked', true);
										var checkbox = $(this).attr('id');
										$("#dateForm").attr(
												"action",
												"${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Complete/"
														+ checkbox);
										$("#dateForm").submit();
									});
							$(".assign")
									.change(
											function() {
												var id = $(this).attr("id");
												document
														.getElementById('assignSubmit_'
																+ id).style.visibility = 'visible';
												$('#form_' + id).attr(
														"action",
														"${pageContext.request.contextPath}/assignHearing/"
																+ id + "/"
																+ $(this).val());
											});
							if ($("#textbox").val() == "") {
								$("#textbox").datepicker({
									format : 'mm/dd/yyyy'
								}).datepicker("setDate", presentDate);
							}
							var date = $("#dropdown").val() + " "
									+ $("#textbox").val();
							$('#date').html(date);
							$("#dropdown").change(function() {

								$("#textbox").datepicker({
									format : 'mm/dd/yyyy'
								}).datepicker("setDate", presentDate);

							});
							$("#textbox").on(
									"change paste keyup",
									function() {
										var checkbox = "${checkbox}";
										$("#dateForm").attr(
												"action",
												"${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Complete/"
														+ checkbox);
										$("#dateForm").submit();
									});
						} else if (hearingRequired == 'Search') {
							var table = $("#myTable").DataTable();
							table.destroy();
							$('#myTable').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"stateSave" : true,

							});
						} else {
							var presentDate = "${presentDate}";
							if ($("#pendingTextbox").val() == "") {
							$("#pendingTextbox").datepicker({
							format : 'mm/dd/yyyy'
							}).datepicker("setDate", presentDate);
							}
							var date = $("#pendingDropdown").val() + " "
							+ $("#pendingTextbox").val();
							$('#date').html(date);

							$("#pendingDropdown").change(function() {

							$("#pendingTextbox").datepicker({
							format : 'mm/dd/yyyy'
							}).datepicker("setDate", presentDate); 

							});

							$("#pendingTextbox").on(
							"change paste keyup",
							function() {
							var checkbox = "${checkbox}";
							$("#dateForm").attr(
							"action",
							"${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Pending/"
							+ checkbox);
							$("#dateForm").submit();	
							});
							document.getElementById(checkbox).checked = true;
							$('#myTable').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"stateSave" : true,
								"columnDefs" : [ {
									"orderData" : [ 9 ],
									"targets" : [ 1 ]
								},{
									"orderData" : [ 10 ],
									"targets" : [ 0 ]
								}, {
									"targets" : [ 9 ],
									"visible" : false,
									"searchable" : false
								},{
									"targets" : [ 10 ],
									"visible" : false,
									"searchable" : false
								}  ],
								"order" : [  [ 0, "asc" ], [ 1, "asc" ]  ]
							});
							$(".chb")
									.change(
											function() {
												$(".chb")
														.prop('checked', false);
												$(this).prop('checked', true);
												var checkbox = $(this).attr(
														'id');
												$("#dateForm").attr(
														"action",
														"${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Pending/"
														+ checkbox);
														$("#dateForm").submit();
											});
							$(".assign")
									.change(
											function() {
												var mixId = $(this).attr("id");
												var id = mixId.split('_')[0];
												if ($(this).val() == 'cancelHearing') {
													document
															.getElementById('assignSubmit_'
																	+ id).style.visibility = 'visible';
													document
															.getElementById('assignSubmit_'
																	+ id).value = 'Cancel';
													$('#form_' + id)
															.attr(
																	"action",
																	"${pageContext.request.contextPath}/cancelHearing/"
																			+ id
																			+ "/"
																			+ mixId
																					.split('_')[1]);
												} else {
													document
															.getElementById('assignSubmit_'
																	+ id).style.visibility = 'visible';
													document
															.getElementById('assignSubmit_'
																	+ id).value = 'Submit';
													$('#form_' + id)
															.attr(
																	"action",
																	"${pageContext.request.contextPath}/assignHearing/"
																			+ id
																			+ "/"
																			+ $(
																					this)
																					.val());
												}
											});
						}
						$('#searchType').change(function() {
							if ($('#searchType').val() == "") {
								$('#myInput').va("");
								$('#myInput').attr("readonly", true);
							} else {
								$('#myInput').attr("readonly", false);
							}
						});
						$('#export')
								.click(
										function() {
											var checkbox = "${checkbox}";
											var hearingRequired = "${hearingRequired}";
											var url = '${pageContext.request.contextPath}/exportHearings/'
													+ hearingRequired
													+ "/"
													+ checkbox;
											$
													.ajax({
														type : "POST",
														url : url,
														data : {
															hearingBasis : $(
																	'#dropdown')
																	.val(),
															requiredDate : $(
																	'#textbox')
																	.val(),
														},
														success : function(data) {
															if (data != null) {
																var modal = document
																		.getElementById("Confirmation");
																modal.style.display = "block";
																$("#ok")
																		.click(
																				function() {
																					modal.style.display = "none";
																				});
																window.onclick = function(
																		event) {
																	if (event.target == modal) {
																		modal.style.display = "none";
																	}
																}
															}
														}
													});
										});
					});
</script>
<script>
	function myFunction() {
		// Declare variables 
		var input, filter, table, tr, td, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		var searchType = $('#searchType').val();
		var hearingRequired = "${hearingRequired}";
		var index, index2 = 00;
		if (hearingRequired == 'Pending') {
			if (searchType == 'VN') {
				index = 1;
			} else if (searchType == 'FN') {
				index = 5, index2 = 4;
			}
		} else {
			if (searchType == 'VN') {
				index = 2;
			} else if (searchType == 'FN') {
				index = 7;
			}else if (searchType == 'HT') {
				index = 4;
			}
		}
		if (index2 == 00) {
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[index];
				if (td) {
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		} else {
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[index];
				td2 = tr[i].getElementsByTagName("td")[index2];
				if (td || td2) {
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1
							|| td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	}
</script>