<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />

		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 20px;"
					href="${pageContext.request.contextPath}/hearingsEdit/${violationId}/${hearingId}">Home</a> <i class="fa fa-circle"></i></li>
				<li><span>Address</span></li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="citationPortlet" class="portlet box blue-dark bg-inverse">
						<div class="portlet-title">
							<div class="caption">Address</div>
						</div>
						<div class="portlet-body form">
								<div class="alert alert-danger" id="error" style="display:none">
									<span id="errorInfo" ></span>
								</div>
								
								<c:if test="${not empty errorInfoEx}">
								<div class="alert alert-danger">
									<span>${errorInfoEx}</span>
								</div>
							</c:if>
							<!-- BEGIN FORM-->
							<form:form id="hearingDetails"
								onsubmit="" action="${pageContext.request.contextPath}/addressEdit/${violationId}/${hearingId}"
								modelAttribute="addressDetailsForm" method="POST"
								autocomplete="on">
								<div class="form-body">
									<form:hidden path="id" />
									  <div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">First
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.firstName" placeholder="FIRST NAME"
															id="fnamePatron"></form:input>
														<span class="help-block">&nbsp;<form:errors 
																path="patron.firstName" cssClass="alert-danger" /> <span
															class="alert-danger" id="fnameError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Middle
														Name</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.middleName" placeholder="MIDDLE NAME"
															id="mnamePatron"></form:input>
														<span class="help-block"> <span
															class="alert-danger" id="mnameError"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Last
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.lastName" placeholder="LAST NAME"
															id="lnamePatron"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.lastName" cssClass="alert-danger" /> <span
															class="alert-danger" id="lnameError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Address</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.address.address" placeholder="ADDRESS"
															id="patronAddress"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.address" cssClass="alert-danger" />
															<span class="alert-danger" id="aptError"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">City
													</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.address.city" placeholder="CITY"
															id="cityPatron"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.city" cssClass="alert-danger" /> <span
															class="alert-danger" id="cityError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Address
														State</label>
													<div class="col-md-9">
														<form:select class="bs-select form-control" 
															style="font-size:13px;width:455px;text-transform: uppercase;"
															path="patron.address.state" placeholder="STATE">
															<form:option value="" label="Select an Option"></form:option>
															<c:forEach items="${states}" var="state"> 
																<form:option value="${state.description}"
																	label="${state.description}"></form:option>
															</c:forEach>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.state" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Zip
														Code </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" style="text-transform: uppercase;"
															path="patron.address.zip" placeholder="ZIP CODE"
															id="zipcodePatron"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.zip" cssClass="alert-danger" /> <span
															class="alert-danger" id="zipcodeError"></span>
														</span>
													</div>
												</div>
											</div>
										</div>

</div>

										<div class="form-actions">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-offset-12 col-md-12">
																	<input type="submit" class="btn blue-dark btn-sm" style=""
																		value="Update" />
															<a href="${pageContext.request.contextPath}/hearingsEdit/${violationId}/${hearingId}"
																class="btn blue-dark btn-sm" style="">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-6"></div>
											</div>
										</div>
								<!--FORM Body-->
							</form:form>
						</div>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CONTENT BODY -->

<!-- END CONTENT -->

<!-- END CONTAINER -->


<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<script>
$(document).ready(
		function() {
		$('.timepicker-no-seconds').timepicker({
            minuteStep: 1
        });
		$('.date').datepicker({
	        daysOfWeekDisabled: [0,1,3,5,6]
	    })
		$("#hearingDate").on("change paste keyup", function() {
			   var date = $(this).val();
			   var parts =date.split('/');
			   var mydate = new Date(parts[2],parts[0]-1,parts[1]);
			   if(mydate.getDay()==2 || mydate.getDay()==4){
				   $("#errorInfo").text("");
				   document.getElementById("error").style.display = 'none'
			   }else{
				   $("#errorInfo").text("Hearing will processed on Tuesday and Thursday. Please select valid date");
				   document.getElementById("error").style.display = 'block'
			   }
		});
		$('select').on('change', function() {
			  if(this.value=='WRITTEN'){
				  $("#hearingDate").attr( "disabled", "disabled");
				  $("#hearingTime").attr("disabled", "disabled");
				  $("#dateButton").attr("disabled", "disabled");
			  }else{
				  $("#hearingDate").prop('disabled', false);
				  $("#hearingTime").prop('disabled', false);
				  $("#dateButton").prop('disabled', false);
			  }
		});			
});
</script>


<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>

<script type="text/javascript">
	
</script>