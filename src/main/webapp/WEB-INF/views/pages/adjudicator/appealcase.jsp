<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- BEGIN CONTAINER -->
<header></header>
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li>
					<%-- <c:choose>
						<c:when
							test="${fn:containsIgnoreCase(user.role.userType.type, 'HearingAdmin')}"> --%>
					<a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/adjudicatorDashboardSupervisor/Pending/All"><spring:message
							code="lbl.adjudicator.home" /></a> <%--< /c:when> <c:otherwise> <a
					style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/adjudicatorDashboard/Pending/All/N/1/10"><spring:message
					code="lbl.home" /></a> </c:otherwise> </c:choose> --%><i class="fa
					fa-circle"></i>
				</li>
				<li><span><spring:message
							code="lbl.page.bar.adjudicator.activecase" /></span></li>
			</ul>
		</div>
		<div class="row"
			style="padding-bottom: 20px; padding-right: 10px; padding-left: 5px;">
			<div class="col-md-12 col-sm-12" style="margin: 0 100px">
				<div class="col-md-9 col-sm-9"
					style="padding-left: 0px; padding-right: 0px; margin: 0 80px">
					<div class="portlet box bg-inverse"
						style="margin: 10px 0 0 10px; margin-left: 50px">
						<div class="col-md-12 col-sm-12">
							<div class="portlet box blue-dark bg-inverse"
								style="margin-bottom: 20px;">
								<table class="table  table-hover  order-column" id="myTable"
									style="margin-bottom: 0px">
									<caption>
										<div align="center">
											<font class="caption"
												style="padding-top: 0px; padding-bottom: 1px; color: white;">
												<b>Upcoming Hearings</b>
											</font>
										</div>
									</caption>
									<thead style="background-color: #004b85;">
										<tr>
											<th><font color="#fff">Hearing&nbsp;Time</font></th>
											<th><font color="#fff">Violation#</font></th>
											<th><font color="#fff">Hearing&nbsp;Type</font></th>
											<th><font color="#fff">Translator</font></th>
											<th><font color="#fff">Last&nbsp;Name</font></th>
											<th><font color="#fff">First&nbsp;Name</font></th>
											<th><font color="#fff">Violation&nbsp;Code</font></th>
											<th><font color="#fff">Lic.Plate#&nbsp;or&nbsp;ID#</font></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${not empty hearingList}">
												<c:forEach items="${hearingList}" var="hearing">
													<tr>
														<td><fmt:formatDate type="time" timeStyle="short"
																pattern="hh:mm a" value="${hearing.hearingTime}" /></td>
														<td><a style="color: #004b85;"
															href="<c:url value='/hearingsEdit/${hearing.violation.violationId}/${hearing.id}' />"><c:out
																	value="${hearing.violation.violationId}"></c:out></a></td>
														<c:choose>
															<c:when
																test="${fn:containsIgnoreCase(hearing.type,'PERSON')}">
																<td style="text-transform: uppercase;"><c:out
																		value="IN-PERSON / SCHEDULED"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.type,'WALKIN')}">
																<td style="text-transform: uppercase;"><c:out
																		value="IN-PERSON / WALK IN"></c:out></td>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearing.type,'WRITTEN')}">
																<td style="text-transform: uppercase;"><c:out
																		value="WRITTEN"></c:out></td>
															</c:when>
														</c:choose>
														<td><c:out value=""></c:out></td>
														<td style="text-transform: uppercase;"><c:out
																value="${hearing.violation.patron.lastName}"></c:out></td>
														<td style="text-transform: uppercase;"><c:out
																value="${hearing.violation.patron.firstName}"></c:out></td>
														<td style="text-transform: uppercase;"><c:out
																value="${hearing.violation.violationCode.code}"></c:out>
															<br> <c:out
																value="${hearing.violation.violationCode.description}"></c:out></td>
														<td style="text-transform: uppercase;"><c:out
																value="${hearing.violation.plateEntity.licenceNumber}"></c:out></td>
													</tr>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<tr>
													<td colspan="8" align="center"
														style="text-transform: uppercase;">No Upcoming
														Hearings</td>
												</tr>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin: 0 50px;">
			<div class="col-md-12 col-sm-12" style="margin-bottom: 80px;">
				<div class="row">
					<div class="col-md-6 col-sm-6" style="padding-right: 0px;">
						<div class="row col-md-12 col-sm-12" style="padding-right: 0px;">
							<div class="col-md-5 col-sm-5" style="padding-left: 0px;">
								<div class="portlet box blue-dark inverse"
									style="margin-bottom: 20px;">
									<div class="portlet-title"
										style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
										<div class="caption"
											style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
											<b>VIOLATOR NAME AND ADDRESS</b>
										</div>
									</div>
									<div class="portlet-body " style="padding: 6px;">
										<table style="font-size: 13px;">
											<tr>
												<td width="100%" style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:out value="${lastName}" /></font></td>
											</tr>
											<tr>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"> <c:out
															value="${address1}" /></font></td>
											</tr>
											<tr>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"> <c:out
															value="${address2}" /></font></td>
											</tr>
											<tr>
												<td style="float: right;"></td>
											</tr>
										</table>
										<div class="row" align="center">
											<a id="button2"
												href="${pageContext.request.contextPath}/addressEdit/${violation.violationId}/0"><span
												class="btn blue-dark btn-sm"
												style="width: 70px; height: 25px;">Edit</span></a>
										</div>
									</div>
								</div>
								<div class="portlet box blue-dark bg-inverse">
									<div class="portlet-title"
										style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
										<div class="caption"
											style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
											<b>FINANCIAL</b>
										</div>
									</div>
									<div class="portlet-body " style="padding: 6px;">
										<!--Fine, Penalty 1, Penalty 2, Penalty 3, Penalty 4,Penalty 5, Reduction, Total Due, Unapplied Amt -->
										<table style="font-size: 13px;">
											<tr>
												<th><b>Penalty 1:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"> <c:if
															test="${penaltyDetails.penaltyCode.penalty1!='' && penaltyDetails.penaltyCode.penalty1!=NULL}">$<c:out
																value="${penaltyDetails.penaltyCode.penalty1}" />
														</c:if>
												</font></td>
											</tr>
											<tr>
												<th><b>Penalty 2:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:if
															test="${penaltyDetails.penaltyCode.penalty2!='' && penaltyDetails.penaltyCode.penalty2!=NULL}">$<c:out
																value="${penaltyDetails.penaltyCode.penalty2}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Penalty 3:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:if
															test="${penaltyDetails.penaltyCode.penalty3!='' && penaltyDetails.penaltyCode.penalty3!=NULL}">$<c:out
																value="${penaltyDetails.penaltyCode.penalty3}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Penalty 4:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:if
															test="${penaltyDetails.penaltyCode.penalty4!='' && penaltyDetails.penaltyCode.penalty4!=NULL}">$<c:out
																value="${penaltyDetails.penaltyCode.penalty4}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Penalty 5:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:if
															test="${penaltyDetails.penaltyCode.penalty5!='' && penaltyDetails.penaltyCode.penalty5!=NULL}">$<c:out
																value="${penaltyDetails.penaltyCode.penalty5}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Total Amount Paid:</b></th>
												<td><font style="font-weight: 400;"><c:if
															test="${totalPaid!='' && totalPaid!=NULL}">$<c:out
																value="${totalPaid}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Overpaid:</b></th>
												<td><font
													style="font-weight: 400; color: #FEFDFD; background-color: #F50C2D;"><c:if
															test="${paymentDetails.overPaid!='' && paymentDetails.overPaid!=NULL&& paymentDetails.overPaid!='0.00'}">$<c:out
																value="${paymentDetails.overPaid}" />
														</c:if> </font></td>
											</tr>
											<tr>
												<th><b>Unapplied Amt:&nbsp;&nbsp;</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="" /></font></td>
											</tr>
											<%-- <tr>
												<th><b>Next Penalty Date:&nbsp;&nbsp;</b></th>
												<c:if
													test="${violation.status!='PAID' && violation.status!='CLOSED' && violation.status!='OVERPAID' &&  violation.status!='VIOD' && violation.totalDue>0.00}">
													<td><font style="font-weight: 400;"><c:out
																value="${violation.penaltyDueDate}" /></font></td>
												</c:if>
											</tr> --%>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-7 col-sm-7" style="padding-right: 0px;">
								<div class="portlet box blue-dark bg-inverse"
									style="margin-bottom: 20px;">
									<div class="portlet-title"
										style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
										<div class="caption"
											style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
											<b>PRIOR CITATIONS</b>
										</div>
									</div>
									<div class="portlet-body "
										style="padding: 6px; min-height: 30px;">
										<table style="font-size: 13px;">
										</table>
									</div>
								</div>
								<div class="portlet box blue-dark bg-inverse">
									<div class="portlet-title"
										style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
										<div class="caption"
											style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
											<b>VIOLATION</b>
										</div>
									</div>
									<div class="portlet-body " style="padding: 6px;">
										<table style="font-size: 13px;">
											<tr>
												<th><b>Violation Number:</b></th>
												<td><font style="font-weight: 400;"><b><c:out
																value="${violationId}" /></b></font></td>
											</tr>
											<tr>
												<th><b>Status:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400; color: #FEFDFD; background-color: #F50C2D;"><c:choose>
															<c:when test="${violation.status == 'VIOD'}">
																<b><c:out value="CLOSED - VOID" /></b>
															</c:when>
															<c:when test="${violation.status == 'OVERPAID'}">
																<b><c:out value="CLOSED - OVERPAID" /></b>
															</c:when>
															<c:otherwise>
																<b><c:out value="${violation.status}" /></b>
															</c:otherwise>
														</c:choose></font></td>
											</tr>
											<tr>
												<th><b>Violation:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:out
															value="${violation.violationCode.description}" /></font></td>
											</tr>
											<tr>
												<th><b>Violation Code:</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="${violation.violationCode.code}" /></font></td>
											</tr>
											<tr>
												<th><b>Issued:</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="${violation.dateOfViolation}" />&nbsp;<c:out
															value="${dayOfWeek}" /></font></td>
											</tr>
											<tr>
												<th><b>Issued Time:</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="${violation.timeOfViolation}" /></font></td>
											</tr>
											<tr>
												<th><b>Processed:</b></th>
												<td>
													<%-- <font style="font-weight: 400;"><c:out
															value="${violation.processDate}" />&nbsp;<c:out
															value="${processDay}" /></font> --%>
												</td>
											</tr>
											<tr>
												<th><b>Vehicle:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:out value="${vehicle}" /></font></td>
											</tr>
											<tr>
												<th><b>Location:</b></th>
												<td style="text-transform: uppercase;"><font
													style="font-weight: 400;"><c:out
															value="${violation.locationOfViolation}" /></font></td>
											</tr>
											<tr>
												<th><b>VIN:</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="${violation.plateEntity.vinNumber}" /></font></td>
											</tr>
											<tr>
												<th style="width: 50%"><b>Related
														Violations:&nbsp;&nbsp;</b></th>
												<td><font style="font-weight: 400;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Fine Amount: </b></th>
												<td><font style="font-weight: 400;"><b>$<c:out
																value="${violation.totalDue }" /></b></font></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12"
								style="padding-right: 0px; padding-left: 0px;">
								<div class="portlet box blue-dark bg-inverse"
									style="margin-bottom: 20px;">
									<div class="portlet-title"
										style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
										<div class="caption"
											style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
											<b>CASE ATTACHMENTS</b>
										</div>
									</div>
									<div class="portlet-body "
										style="padding: 6px; min-height: 300px; overflow-y: scroll;">
										<table style="font-size: 13px;">
											<c:if test="${not empty images}">
												<tr>
													<td style="color: #004b85;"><b>Attachments:</b></td>
												</tr>
												<c:forEach items="${images}" var="images">
													<tr>
														<td id="partFileInfoColumn"
															style="width: 450px; font-size: 13px;"><span
															id="fileNameInfo_${images.id}"> <c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																		<%-- <iframe src="http://docs.google.com/gview?url=<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />" style="width:600px; height:500px;" frameborder="0"></iframe> --%>
																		<a style="color: #004b85;"
																			href="<c:url value='/downloadImage/${images.id}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:when>
																	<c:otherwise>
																		<a target="_blank"
																			onclick="window.open('<c:url value='/viewReviewAttachment/${images.id}/${violationId}' />','_blank','width=500,height=500'); return false;"
																			style="color: #004b85;"
																			href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:otherwise>
																</c:choose>
														</span></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${not empty checks}">
												<tr>
													<td style="color: #004b85;"><b>Checks:</b></td>
												</tr>
												<c:forEach items="${checks}" var="images">
													<tr>
														<td id="partFileInfoColumn"
															style="width: 450px; font-size: 13px;"><span
															id="fileNameInfo_${images.id}"> <c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																		<%-- <iframe src="http://docs.google.com/gview?url=<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />" style="width:600px; height:500px;" frameborder="0"></iframe> --%>
																		<a style="color: #004b85;"
																			href="<c:url value='/downloadImage/${images.id}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:when>
																	<c:otherwise>
																		<a target="_blank"
																			onclick="window.open('<c:url value='/viewReviewAttachment/${images.id}/${violationId}' />','_blank','width=500,height=500'); return false;"
																			style="color: #004b85;"
																			href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:otherwise>
																</c:choose>
														</span></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${not empty corresp}">
												<tr>
													<td style="color: #004b85;"><b>Correspondence
															Files:</b></td>
												</tr>
												<c:forEach items="${corresp}" var="images">
													<tr>
														<td id="partFileInfoColumn"
															style="width: 450px; font-size: 13px;"><span
															id="fileNameInfo_${images.id}"> <c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																		<%-- <iframe src="http://docs.google.com/gview?url=<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />" style="width:600px; height:500px;" frameborder="0"></iframe> --%>
																		<a style="color: #004b85;"
																			href="<c:url value='/downloadImage/${images.id}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:when>
																	<c:otherwise>
																		<a target="_blank"
																			onclick="window.open('<c:url value='/viewReviewAttachment/${images.id}/${violationId}' />','_blank','width=500,height=500'); return false;"
																			style="color: #004b85;"
																			href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"
																			target="pop-up"><span style="width: 50px;"><c:out
																					value="${images.uploadedDate} ${images.imageName}" /></span></a>
																	</c:otherwise>
																</c:choose>
														</span></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
												</c:forEach>
											</c:if>
										</table>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="row col-md-12 col-sm-12" style="padding-right: 0px;">
							<div class="portlet box blue-dark bg-inverse"
								style="margin-bottom: 20px;">
								<div class="portlet-title"
									style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
									<div class="caption"
										style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
										<b>CASE HISTORY</b>
									</div>
								</div>
								<div class="portlet-body "
									style="padding: 6px; font-size: 12px;">
									<table id="hstryTable">
										<thead class="hidden">
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty addressHistory }">
												<c:forEach items="${addressHistory}" var="address">
													<c:if test="${not empty address.formattedAddress}">
														<tr>
															<c:choose>
																<c:when test="${address.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${address.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(address.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(address.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(address.formattedCreatedAt,' ')[2]}" />
																	</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${address.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(address.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(address.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(address.formattedUpdatedAt,' ')[2]}" />
																	</td>
																</c:otherwise>
															</c:choose>
															<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																		value="ADDRESS:" /></b></td>
															<td style="padding: 2px; text-transform: uppercase;"><c:out
																	value="${address.formattedAddress}" /></td>
														</tr>
													</c:if>
												</c:forEach>
											</c:if>
											<c:if test="${not empty historyBean.penaltyHistory }">
												<c:forEach items="${historyBean.penaltyHistory}" var="penalty">
													<tr>
														<c:choose>
															<c:when test="${penalty.updatedAt == null}">
																<td class="hidden"><c:out
																		value="${penalty.createdAt}" /></td>
																<td width="10%"
																	style="padding: 2px; text-transform: uppercase;"><c:out
																		value="${fn:split(penalty.formattedCreatedAt,' ')[0]}" /></td>
																<td style="padding: 2px; text-transform: uppercase;">
																	<c:set
																		value="${fn:split(penalty.formattedCreatedAt,' ')[1]}"
																		var="time" /> <c:set
																		value="${fn:replace(time,':',': ')}" var="time1" /> <c:out
																		value="${fn:split(time1,' ')[0]}" /> <c:out
																		value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																		value="${fn:split(penalty.formattedCreatedAt,' ')[2]}" />
																</td>
															</c:when>
															<c:otherwise>
																<td class="hidden"><c:out
																		value="${penalty.updatedAt}" /></td>
																<td width="10%"
																	style="padding: 2px; text-transform: uppercase;"><c:out
																		value="${fn:split(penalty.formattedUpdatedAt,' ')[0]}" /></td>
																<td style="padding: 2px; text-transform: uppercase;">
																	<c:set
																		value="${fn:split(penalty.formattedUpdatedAt,' ')[1]}"
																		var="time" /> <c:set
																		value="${fn:replace(time,':',': ')}" var="time1" /> <c:out
																		value="${fn:split(time1,' ')[0]}" /> <c:out
																		value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																		value="${fn:split(penalty.formattedUpdatedAt,' ')[2]}" />
																</td>
															</c:otherwise>
														</c:choose>
														<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																	value="PENALTY:" /></b></td>
														<td style="padding: 2px; text-transform: uppercase;">&#36;<c:out
																	value="${penalty.violation.fineAmount}" />, <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty1}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty1}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty2}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty2}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:when>
																	<c:otherwise>
																								&#36;<c:out value="0.00" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty3}">
																							&#36;<c:out
																			value="${penalty.penaltyCode.penalty3}" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:otherwise>
																</c:choose>$<c:out value="${penalty.violation.totalDue}" />&nbsp;</td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${not empty historyBean.paymentHistory }">
												<c:forEach items="${historyBean.paymentHistory}" var="paymentList">
													<c:forEach items="${paymentList}" var="pymnt">
														<tr>
															<c:choose>
																<c:when test="${pymnt.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${pymnt.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(pymnt.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(pymnt.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(pymnt.formattedCreatedAt,' ')[2]}" />
																	</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${pymnt.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(pymnt.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(pymnt.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(pymnt.formattedUpdatedAt,' ')[2]}" />
																	</td>
																</c:otherwise>
															</c:choose>
															<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																		value="PAYMENTS:" /></b></td>
															<td style="padding: 2px; text-transform: uppercase;"><c:if
																		test="${not empty pymnt.amount}">$<c:out
																			value="${pymnt.amount}" />
																	</c:if> <c:if test="${not empty pymnt.paymentDate}">/
																		<c:out value="${pymnt.paymentDate}" />,</c:if> <c:choose>
																		<c:when test="${pymnt.paymentSource == 'P'}">
																			<c:out value="PAY-IN-PERSON" />
																		</c:when>
																		<c:when test="${pymnt.paymentSource == 'M'}">
																			<c:out value="PAY-BY-MAIL" />
																		</c:when>
																		<c:otherwise>
																			<c:out value="PAY-BY-WEB" />
																		</c:otherwise>
																	</c:choose> <c:if
																		test="${not empty pymnt.paymentMethod.description}">/<c:out
																			value="${pymnt.paymentMethod.description}" />
																	</c:if> <c:if test="${not empty pymnt.account}">,
																		<c:out value="${pymnt.account}" />
																	</c:if> <c:if test="${not empty pymnt.processedOn}">,
																		<c:out value="${pymnt.processedOn}" />
																	</c:if> <c:if test="${not empty pymnt.processedBy}">/
																		<c:out value="${pymnt.processedBy}" />
																	</c:if>,<c:if test="${not empty pymnt.overPaid}">$<c:out
																			value="${pymnt.overPaid}" />, </c:if> <c:if
																		test="${not empty pymnt.totalDue}">$<c:out
																			value="${pymnt.totalDue}" />
																	</c:if></td>
														</tr>
													</c:forEach>
												</c:forEach>
											</c:if>
												<c:if test="${not empty historyBean.hearingHistory }">
													<c:set value="" var="decision" />
													<c:forEach items="${historyBean.hearingHistory}" var="hearing">
														<tr>
															<c:choose>
																<c:when test="${hearing.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${hearing.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(hearing.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(hearing.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(hearing.formattedCreatedAt,' ')[2]}" />
																	</td>
																	<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																				value="HEARING:" /></b></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																				<c:out value="HEARING STATUS:"></c:out>
																				<c:choose>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																						<c:out value="In-Progress"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='PENDINGREVIEWUPDATED'}">
																						<c:out value="Pending Review-Updated"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='PENDINGREVIEW'}">
																						<c:out value="Pending Review"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='COMPLETE'}">
																						<c:out value="Complete"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='PENDINGMAIL'}">
																						<c:out value="Pending Mail"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='REWORK'}">
																						<c:out value="Needs Rework"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='FAILED TO APPEAR'}">
																						<c:out value="Failed to Appear"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:toUpperCase(hearing.status)=='CANCELLED'}">
																						<c:out value="Cancelled"></c:out>
																					</c:when>
																					<c:otherwise>
																						<c:out value="Pending"></c:out>
																					</c:otherwise>
																				</c:choose>&nbsp;<c:if test="${not empty hearing.decision}">
																					<c:if test="${empty decision}">
																						<c:set var="decision" value="${hearing.decision}"></c:set>
																						<c:out value="Decision:"></c:out>
																						<c:choose>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																								<c:out value="Not Liable"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																								<c:out value="Liable with Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																								<c:out value="Admission Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																								<c:out value="Liable with IPP"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																								<c:out value="Liable"></c:out>
																							</c:when>
																						</c:choose>	&nbsp;
																				</c:if>
																					<c:if test="${hearing.decision!=decision}">
																						<c:out value="Decision:"></c:out>
																						<c:choose>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																								<c:out value="Not Liable"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																								<c:out value="Liable with Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																								<c:out value="Admission Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																								<c:out value="Liable with IPP"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																								<c:out value="Liable"></c:out>
																							</c:when>
																						</c:choose>	&nbsp;
																<c:set var="decision" value="${hearing.decision}"></c:set>
																					</c:if>
																				</c:if>&nbsp;
																				<c:if
																					test="${not empty hearing.formattedHearingDate}">,
															<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																						type="time" timeStyle="short" pattern="hh:mm a"
																						value="${hearing.hearingTime}" />,</c:if>
																				<c:if test="${not empty hearing.formattedDispDate}">/
																		<c:out value="${hearing.formattedDispDate}" />
																				</c:if>
																				<c:if test="${not empty hearing.dispositionTime}">/
															<fmt:formatDate type="time" timeStyle="short"
																						pattern="hh:mm a"
																						value="${hearing.dispositionTime}" />
																				</c:if>
																				<c:if test="${not empty hearing.disposition}">,
															<c:out value="${hearing.disposition}" />
																				</c:if>
																				<c:if test="${not empty hearing.hearingOfficer}">,
																		<c:out value="${hearing.hearingOfficer}" />
																				</c:if>
																				<c:if test="${not empty hearing.reduction}">,  &#36;
															<c:out value="${hearing.reduction}" />
																				</c:if>
																				<c:if test="${not empty hearing.totalDue}">&#36;
															<c:out value="${hearing.totalDue}" />
																				</c:if>
																				<c:if test="${hearing.dateMailed != null}">/<c:out
																						value="MAIL DATE: "></c:out>
																					<c:out value="${hearing.formattedDateMailed}"></c:out>
																				</c:if>
																			</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${hearing.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(hearing.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(hearing.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(hearing.formattedUpdatedAt,' ')[2]}" />
																	</td>
																	<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																				value="HEARING:" /></b></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																				<c:out value="HEARING STATUS:"></c:out>
																				<c:choose>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																						<c:out value="In-Progress"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																						<c:out value="Pending Review-Updated"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																						<c:out value="Pending Review"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																						<c:out value="Complete"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																						<c:out value="Pending Mail"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																						<c:out value="Needs Rework"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																						<c:out value="Failed to Appear"></c:out>
																					</c:when>
																					<c:when
																						test="${fn:containsIgnoreCase(hearing.status,'Cancelled')}">
																						<c:out value="Cancelled"></c:out>
																					</c:when>
																					<c:otherwise>
																						<c:out value="Pending"></c:out>
																					</c:otherwise>
																				</c:choose>&nbsp;<c:if test="${not empty hearing.decision}">/
																					<c:if test="${empty decision}">
																						<c:set var="decision" value="${hearing.decision}"></c:set>
																						<c:out value="Decision:"></c:out>
																						<c:choose>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																								<c:out value="Not Liable"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																								<c:out value="Liable with Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																								<c:out value="Admission Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																								<c:out value="Liable with IPP"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																								<c:out value="Liable"></c:out>
																							</c:when>
																						</c:choose>	&nbsp;
																				</c:if>
																					<c:if test="${hearing.decision!=decision}">
																						<c:out value="Decision:"></c:out>
																						<c:choose>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																								<c:out value="Not Liable"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																								<c:out value="Liable with Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																								<c:out value="Admission Community Service"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																								<c:out value="Liable with IPP"></c:out>
																							</c:when>
																							<c:when
																								test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																								<c:out value="Liable"></c:out>
																							</c:when>
																						</c:choose>	&nbsp;
																<c:set var="decision" value="${hearing.decision}"></c:set>
																					</c:if>
																				</c:if>&nbsp;
																				<c:if
																					test="${not empty hearing.formattedHearingDate}">,
															<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																						type="time" timeStyle="short" pattern="hh:mm a"
																						value="${hearing.hearingTime}" />,</c:if>
																				<c:if test="${not empty hearing.formattedDispDate}">/
																		<c:out value="${hearing.formattedDispDate}" />
																				</c:if>
																				<c:if test="${not empty hearing.dispositionTime}">/
															<fmt:formatDate type="time" timeStyle="short"
																						pattern="hh:mm a"
																						value="${hearing.dispositionTime}" />
																				</c:if>
																				<c:if test="${not empty hearing.disposition}">,
															<c:out value="${hearing.disposition}" />
																				</c:if>
																				<c:if test="${not empty hearing.hearingOfficer}">,
																		<c:out value="${hearing.hearingOfficer}" />
																				</c:if>
																				<c:if test="${not empty hearing.reduction}">, &#36;
															<c:out value="${hearing.reduction}" />
																				</c:if>
																				<c:if test="${not empty hearing.totalDue}">&#36;
																					<c:out value="${hearing.totalDue}" />
																				</c:if>
																				<c:if test="${hearing.dateMailed != null}">/<c:out
																						value="MAIL DATE: "></c:out>
																					<c:out value="${hearing.formattedDateMailed}"></c:out>
																				</c:if>
																			</td>
																</c:otherwise>
															</c:choose>
														</tr>
												</c:forEach>
											</c:if>
											<c:if test="${not empty historyBean.suspendHistory }">
												<c:forEach items="${historyBean.suspendHistory}" var="suspendList">
													<c:forEach items="${suspendList}" var="suspend">
														<tr>
															<c:choose>
																<c:when test="${suspend.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${suspend.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(suspend.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(suspend.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(suspend.formattedCreatedAt,' ')[2]}" />
																	</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${suspend.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(suspend.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(suspend.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(suspend.formattedUpdatedAt,' ')[2]}" />
																	</td>
																</c:otherwise>
															</c:choose>
															<td style="padding: 2px; text-transform: uppercase;"><b><c:out
																		value="SUSPENDS:" /></b></td>
															<td style="padding: 2px; text-transform: uppercase;"><c:if
																		test="${not empty suspend.suspendedCodes.description}">
																		<c:out value="${suspend.suspendedCodes.description}" />
																	</c:if> <c:if test="${not empty suspend.suspendedCodes.code}">,
																		<c:out value="${suspend.suspendedCodes.code}" />
																	</c:if> <c:if test="${not empty suspend.formattedSuspendDate}">,
																		<c:out value="${suspend.formattedSuspendDate}" />
																	</c:if> <c:if test="${not empty suspend.formattedProcessedOn}">,
																		<c:out value="${suspend.formattedProcessedOn}" />
																	</c:if> <c:if test="${not empty suspend.reduction}">,
																		 $<c:out value="${suspend.reduction}" />,
																		</c:if>$<c:out value="${suspend.totalDue}" /></td>
														</tr>
													</c:forEach>
												</c:forEach>
											</c:if>
											<c:if test="${not empty historyBean.correspHistory}">
												<c:forEach items="${historyBean.correspHistory}" var="corresList">
													<c:forEach items="${corresList}" var="correspond">
														<tr>
															<c:choose>
																<c:when test="${correspond.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${correspond.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(correspond.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(correspond.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(correspond.formattedCreatedAt,' ')[2]}" />
																	</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${correspond.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(correspond.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(correspond.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(correspond.formattedUpdatedAt,' ')[2]}" />
																	</td>
																</c:otherwise>
															</c:choose>
															<td><b><c:out value="CORRESPONDENCE:" /></b></td>
															<td style="text-transform: uppercase;"><c:if
																		test="${not empty correspond.correspCode.correspDesc}">
																		<c:out value="${correspond.correspCode.correspDesc}" />
																	</c:if> <c:if
																		test="${not empty correspond.formattedCorresDate}">,
																		<c:out value="${correspond.formattedCorresDate}" />
																	</c:if> <c:if test="${not empty correspond.corresp_time}">,
																		<c:out value="${correspond.corresp_time}" />
																	</c:if> <c:if test="${correspond.letterSent==true}">,
																		<c:out value="Yes" />
																	</c:if> <c:if test="${correspond.letterSent==false}">,
																		<c:out value="No" />
																	</c:if></td>
														</tr>
													</c:forEach>
												</c:forEach>
											</c:if>
											<c:if test="${not empty historyBean.noticeHistory }">
												<c:forEach items="${historyBean.noticeHistory}" var="noticesList">
													<c:forEach items="${noticesList}" var="notice">
														<tr>
															<c:choose>
																<c:when test="${notice.updatedAt == null}">
																	<td class="hidden"><c:out
																			value="${notice.createdAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(notice.formattedCreatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(notice.formattedCreatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(notice.formattedCreatedAt,' ')[2]}" />
																	</td>
																</c:when>
																<c:otherwise>
																	<td class="hidden"><c:out
																			value="${notice.updatedAt}" /></td>
																	<td width="10%"
																		style="padding: 2px; text-transform: uppercase;"><c:out
																			value="${fn:split(notice.formattedUpdatedAt,' ')[0]}" /></td>
																	<td style="padding: 2px; text-transform: uppercase;">
																		<c:set
																			value="${fn:split(notice.formattedUpdatedAt,' ')[1]}"
																			var="time" /> <c:set
																			value="${fn:replace(time,':',': ')}" var="time1" />
																		<c:out value="${fn:split(time1,' ')[0]}" /> <c:out
																			value="${fn:split(time1,': ')[1]}" />&nbsp;<c:out
																			value="${fn:split(notice.formattedUpdatedAt,' ')[2]}" />
																	</td>
																</c:otherwise>
															</c:choose>
															<td><b><c:out value="NOTICES:" /></b></td>
															<td style="text-transform: uppercase;"><c:if
																	test="${not empty notice.noticeType.fullNm}">
																	<c:out value="${notice.noticeType.fullNm}" />
																</c:if> <c:if test="${not empty notice.formattedSentDate}">,
															<c:out value="${notice.formattedSentDate}" />
																</c:if>&nbsp; <c:if
																	test="${not empty notice.formattedProcessedDate}">,
															<c:out value="${notice.formattedProcessedDate}" />
																</c:if>&nbsp; <c:out value="Yes" /></td>
														</tr>
													</c:forEach>
												</c:forEach>
											</c:if>
										</tbody>
									</table>
									<div align="right">
										<div class="row" style="padding-right: 18px;">
											<a id="historyAll" class="btn blue-dark btn-sm"><c:out
													value="View All"></c:out></a>
										</div>
										<form:form>
											<div class="form-actions">
												<div style="margin-top: 10px;">
													<input type="submit" class="btn blue-dark btn-sm"
														style="width: 130px;" value="Export Case History"
														formaction="${pageContext.request.contextPath}/exportCaseHistory/${violation.violationId}/0" />
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div id="Confirmation" class="modal"
			style="display: none; top: -55px;">
			<div class="modal-content" align="center" style="width: 32%;">
				<span style="text-align: center;" id="confirmation">Confirm
					patron Failed to Appear ?</span>
				<div class="modal-body" align="center">
					<button type="button" id="confirm" class="btn blue-dark btn-sm">YES</button>
					<button type="button" id="cancel" class="btn blue-dark btn-sm">NO</button>
				</div>
			</div>
		</div>
		<div id="appealConfirmation" class="modal"
			style="display: none; top: -55px;">
			<div class="modal-content" align="center" style="width: 32%;">
				<span style="text-align: center;" id="confirmation">Confirm
					change to 3 <sup>rd</sup> Level Appeal status?
				</span>
				<div class="modal-body" align="center">
					<button type="button" id="yes" class="btn blue-dark btn-sm">YES</button>
					<button type="button" id="no" class="btn blue-dark btn-sm">NO</button>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- Modal -->
<div id="popupHstry" class="modal">
	<div class="modal-content" align="center">
		<div class="modal-body" align="center">

			<div class="portlet-body">
				<div class="table-responsive scroller">
					<table id="allDetailsTbl" class="table table-hover table-bordered ">
						<thead>
							<tr style="background-color: #f5f3eb;">
								<th style="color: #004b85;" width="9%"><b>Type</b></th>
								<th style="color: #004b85;" width="64%"><b>Data</b></th>
								<th style="color: #004b85;" width="10%"><b>Updated By</b></th>
								<th style="color: #004b85;" width="15%"><b>Updated Date</b></th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty addressHistory }">
								<c:forEach items="${addressHistory}" var="address">
									<tr>
										<td><c:out value="ADDRESS" /></td>
										<td style="text-transform: uppercase;"><c:out
												value="${address.formattedAddress}" /></td>
										<c:choose>
											<c:when test="${address.updatedAt == null}">
												<c:if test="${not empty address.formattedAddress}">
													<td style="text-transform: uppercase;"><c:out
															value="${address.createdBy}" /></td>
													<td><c:out value="${address.formattedCreatedAt}" /></td>
												</c:if>
											</c:when>
											<c:otherwise>
												<td style="text-transform: uppercase;"><c:out
														value="${address.updatedBy}" /></td>
												<td style="text-transform: uppercase;"><c:out
														value="${address.formattedUpdatedAt}" /></td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.penaltyHistory }">
								<c:forEach items="${historyBean.penaltyHistory}" var="penalty">
									<tr>
										<td><c:out value="PENALTY" /></td>
										<td style="text-transform: uppercase;">&#36;<c:out
																	value="${penalty.violation.fineAmount}" />, <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty1}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty1}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty2}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty2}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:when>
																	<c:otherwise>
																								&#36;<c:out value="0.00" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty3}">
																							&#36;<c:out
																			value="${penalty.penaltyCode.penalty3}" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:otherwise>
																</c:choose>$<c:out value="${penalty.violation.totalDue}" />&nbsp;</td>
										<c:choose>
											<c:when test="${penalty.updatedAt == null}">
												<td style="text-transform: uppercase;"><c:out
														value="${penalty.createdBy}" /></td>
												<td style="text-transform: uppercase;"><c:out
														value="${penalty.formattedCreatedAt}" /></td>
											</c:when>
											<c:otherwise>
												<td style="text-transform: uppercase;"><c:out
														value="${penalty.updatedBy}" /></td>
												<td style="text-transform: uppercase;"><c:out
														value="${penalty.formattedUpdatedAt}" /></td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.paymentHistory }">
								<c:forEach items="${historyBean.paymentHistory}" var="paymentList">
									<c:forEach items="${paymentList}" var="payment">
										<tr>
											<td><c:out value="PAYMENTS" /></td>
											<td style="text-transform: uppercase;"><c:if
																		test="${not empty payment.amount}">$<c:out
																			value="${payment.amount}" />
																	</c:if> <c:if test="${not empty payment.paymentDate}">/
																		<c:out value="${payment.paymentDate}" />,</c:if> <c:choose>
																		<c:when test="${payment.paymentSource == 'P'}">
																			<c:out value="PAY-IN-PERSON" />
																		</c:when>
																		<c:when test="${payment.paymentSource == 'M'}">
																			<c:out value="PAY-BY-MAIL" />
																		</c:when>
																		<c:otherwise>
																			<c:out value="PAY-BY-WEB" />
																		</c:otherwise>
																	</c:choose> <c:if
																		test="${not empty payment.paymentMethod.description}">/<c:out
																			value="${payment.paymentMethod.description}" />
																	</c:if> <c:if test="${not empty payment.account}">,
																		<c:out value="${payment.account}" />
																	</c:if> <c:if test="${not empty payment.processedOn}">,
																		<c:out value="${payment.processedOn}" />
																	</c:if> <c:if test="${not empty payment.processedBy}">/
																		<c:out value="${payment.processedBy}" />
																	</c:if>,<c:if test="${not empty payment.overPaid}">$<c:out
																			value="${payment.overPaid}" />, </c:if> <c:if
																		test="${not empty payment.totalDue}">$<c:out
																			value="${payment.totalDue}" />
																	</c:if></td>
											<c:choose>
												<c:when test="${payment.updatedAt == null}">
													<td style="text-transform: uppercase;"><c:out
															value="${payment.createdBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${payment.formattedCreatedAt}" /></td>
												</c:when>
												<c:otherwise>
													<td style="text-transform: uppercase;"><c:out
															value="${payment.updatedBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${payment.formattedUpdatedAt}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.hearingHistory }">
								<c:forEach items="${historyBean.hearingHistory}"  var="hearing">
										<tr>
											<td><c:out value="HEARING" /></td>
											<c:choose>
												<c:when test="${hearing.updatedAt == null}">
													<td style="text-transform: uppercase;">
																<c:out value="HEARING STATUS:"></c:out>
																<c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																		<c:out value="In-Progress"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																		<c:out value="Pending Review-Updated"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																		<c:out value="Pending Review"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																		<c:out value="Complete"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																		<c:out value="Pending Mail"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																		<c:out value="Needs Rework"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																		<c:out value="Failed to Appear"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'Cancelled')}">
																		<c:out value="Cancelled"></c:out>
																	</c:when>
																	<c:otherwise>
																		<c:out value="Pending"></c:out>
																	</c:otherwise>
																</c:choose>&nbsp;<c:if test="${not empty hearing.decision}">
																	<c:out value="Decision:"></c:out>
																	<c:choose>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																			<c:out value="Not Liable"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																			<c:out value="Liable with Community Service"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																			<c:out value="Admission Community Service"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																			<c:out value="Liable with IPP"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																			<c:out value="Liable"></c:out>
																		</c:when>
																	</c:choose>	&nbsp;															
																</c:if>
																<c:if test="${not empty hearing.hearingOfficer}">/
																		<c:out value="${hearing.hearingOfficer}" />
																</c:if>
																<c:if test="${not empty hearing.formattedHearingDate}">,
															<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																		type="time" timeStyle="short" pattern="hh:mm a"
																		value="${hearing.hearingTime}" />
																</c:if>
																<c:if test="${not empty hearing.formattedDispDate}">,
																	<c:out value="${hearing.formattedDispDate}" />
																</c:if>
																<c:if test="${not empty hearing.dispositionTime}">/
															<fmt:formatDate type="time" timeStyle="short"
																		pattern="hh:mm a" value="${hearing.dispositionTime}" />
																</c:if>
																<c:if test="${not empty hearing.disposition}">,
															<c:out value="${hearing.disposition}" />
																</c:if>
																<c:if test="${not empty hearing.reduction}">,&#36;
															<c:out value="${hearing.reduction}" />
																</c:if>
																<c:if test="${not empty hearing.totalDue}"> &#36;
															<c:out value="${hearing.totalDue}" />
																</c:if>
															</td>
													<td style="text-transform: uppercase;"><c:out
															value="${hearing.createdBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${hearing.formattedCreatedAt}" /></td>
												</c:when>
												<c:otherwise>
													<td style="text-transform: uppercase;">
																<c:out value="HEARING STATUS:"></c:out>
																<c:choose>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																		<c:out value="In-Progress"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																		<c:out value="Pending Review-Updated"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																		<c:out value="Pending Review"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																		<c:out value="Complete"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																		<c:out value="Pending Mail"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																		<c:out value="Needs Rework"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																		<c:out value="Failed to Appear"></c:out>
																	</c:when>
																	<c:when
																		test="${fn:containsIgnoreCase(hearing.status,'Cancelled')}">
																		<c:out value="Cancelled"></c:out>
																	</c:when>
																	<c:otherwise>
																		<c:out value="Pending"></c:out>
																	</c:otherwise>
																</c:choose>&nbsp;<c:if test="${not empty hearing.decision}">
																	<c:out value="Decision:"></c:out>
																	<c:choose>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'Not_Liable')}">
																			<c:out value="Not Liable"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'LiableComSer')}">
																			<c:out value="Liable with Community Service"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'AdmsnComSer')}">
																			<c:out value="Admission Community Service"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'LiableIpp')}">
																			<c:out value="Liable with IPP"></c:out>
																		</c:when>
																		<c:when
																			test="${fn:containsIgnoreCase(hearing.decision,'Liable')}">
																			<c:out value="Liable"></c:out>
																		</c:when>
																	</c:choose>	&nbsp;															
																</c:if>
																<c:if test="${not empty hearing.hearingOfficer}">/
																		<c:out value="${hearing.hearingOfficer}" />
																</c:if>
																<c:if test="${not empty hearing.formattedHearingDate}">,
															<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																		type="time" timeStyle="short" pattern="hh:mm a"
																		value="${hearing.hearingTime}" />
																</c:if>
																<c:if test="${not empty hearing.formattedDispDate}">,
																	<c:out value="${hearing.formattedDispDate}" />
																</c:if>
																<c:if test="${not empty hearing.dispositionTime}">/
															<fmt:formatDate type="time" timeStyle="short"
																		pattern="hh:mm a" value="${hearing.dispositionTime}" />
																</c:if>
																<c:if test="${not empty hearing.disposition}">,
															<c:out value="${hearing.disposition}" />
																</c:if>
																<c:if test="${not empty hearing.reduction}">,&#36;
															<c:out value="${hearing.reduction}" />
																</c:if>
																<c:if test="${not empty hearing.totalDue}">&#36;
															<c:out value="${hearing.totalDue}" />
																</c:if>
															</td>
													<td style="text-transform: uppercase;"><c:out
															value="${hearing.updatedBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${hearing.formattedUpdatedAt}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.suspendHistory }">
								<c:forEach items="${historyBean.suspendHistory}" var="suspendList">
									<c:forEach items="${suspendList}" var="suspend">
										<tr>
											<td><c:out value="SUSPENDS" /></td>
											<td style="text-transform: uppercase;"><c:if
																		test="${not empty suspend.suspendedCodes.description}">
																		<c:out value="${suspend.suspendedCodes.description}" />
																	</c:if> <c:if test="${not empty suspend.suspendedCodes.code}">,
																		<c:out value="${suspend.suspendedCodes.code}" />
																	</c:if> <c:if test="${not empty suspend.formattedSuspendDate}">,
																		<c:out value="${suspend.formattedSuspendDate}" />
																	</c:if> <c:if test="${not empty suspend.formattedProcessedOn}">,
																		<c:out value="${suspend.formattedProcessedOn}" />
																	</c:if> <c:if test="${not empty suspend.reduction}">,
																		 $<c:out value="${suspend.reduction}" />,
																		</c:if>$<c:out value="${suspend.totalDue}" /></td>
											<c:choose>
												<c:when test="${suspend.updatedAt == null}">
													<td style="text-transform: uppercase;"><c:out
															value="${suspend.createdBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${suspend.formattedCreatedAt}" /></td>
												</c:when>
												<c:otherwise>
													<td style="text-transform: uppercase;"><c:out
															value="${suspend.updatedBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${suspend.formattedUpdatedAt}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.correspHistory }">
								<c:forEach items="${historyBean.correspHistory}" var="corresList">
									<c:forEach items="${corresList}" var="correspond">
										<tr>
											<td><c:out value="CORRESPONDENCE" /></td>
											<td style="text-transform: uppercase;"><c:if
																		test="${not empty correspond.correspCode.correspDesc}">
																		<c:out value="${correspond.correspCode.correspDesc}" />
																	</c:if> <c:if
																		test="${not empty correspond.formattedCorresDate}">,
																		<c:out value="${correspond.formattedCorresDate}" />
																	</c:if> <c:if test="${not empty correspond.corresp_time}">,
																		<c:out value="${correspond.corresp_time}" />
																	</c:if> <c:if test="${correspond.letterSent==true}">,
																		<c:out value="Yes" />
																	</c:if> <c:if test="${correspond.letterSent==false}">,
																		<c:out value="No" />
																	</c:if></td>
											<c:choose>
												<c:when test="${correspond.updatedAt == null}">
													<td style="text-transform: uppercase;"><c:out
															value="${correspond.createdBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${correspond.formattedCreatedAt}" /></td>
												</c:when>
												<c:otherwise>
													<td style="text-transform: uppercase;"><c:out
															value="${correspond.updatedBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${correspond.formattedUpdatedAt}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.noticeHistory }">
								<c:forEach items="${historyBean.noticeHistory}" var="noticesList">
									<c:forEach items="${noticesList}" var="notice">
										<tr>
											<td><c:out value="NOTICES" /></td>
											<td style="text-transform: uppercase;"><c:if
													test="${not empty notice.noticeType.fullNm}">
													<c:out value="${notice.noticeType.fullNm}" />
												</c:if> <c:if test="${not empty notice.formattedSentDate}">,
															<c:out value="${notice.formattedSentDate}" />
												</c:if>&nbsp; <c:if
													test="${not empty notice.formattedProcessedDate}">,
															<c:out value="${notice.formattedProcessedDate}" />
												</c:if>&nbsp; <c:out value="Yes" /></td>
											<c:choose>
												<c:when test="${notice.updatedAt == null}">
													<td style="text-transform: uppercase;"><c:out
															value="${notice.createdBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${notice.formattedCreatedAt}" /></td>
												</c:when>
												<c:otherwise>
													<td style="text-transform: uppercase;"><c:out
															value="${notice.updatedBy}" /></td>
													<td style="text-transform: uppercase;"><c:out
															value="${notice.formattedUpdatedAt}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</c:if>
							<c:if test="${not empty historyBean.plateHistory }">
								<c:forEach items="${historyBean.plateHistory}" var="plateEntity">
									<tr>
										<td style="text-transform: uppercase;"><c:out
												value="PLATE ENTITY" /></td>
										<td style="text-transform: uppercase;"><c:if
												test="${not empty plateEntity.licenceNumber}">
												<c:out value="${plateEntity.licenceNumber}" />
											</c:if> <c:if test="${not empty plateEntity.vinNumber}">,
																									<c:out value="${plateEntity.vinNumber}" />
											</c:if> <c:if test="${not empty plateEntity.licenseState}">,
																									<c:out value="${plateEntity.licenseState}" />
											</c:if> <c:if test="${not empty plateEntity.licenseMonth}">,
																									<c:out value="${plateEntity.licenseMonth}" />
											</c:if> <c:if test="${not empty plateEntity.licenseYear}">/
																									<c:out value="${plateEntity.licenseYear}" />
											</c:if> <c:if test="${not empty plateEntity.bodyType}">,
																									<c:out value="${plateEntity.bodyType}" />
											</c:if> <c:if test="${not empty plateEntity.vehicleMake}">,
																									<c:out value="${plateEntity.vehicleMake}" />
											</c:if> <c:if test="${not empty plateEntity.vehicleModel}">,
																									<c:out value="${plateEntity.vehicleModel}" />
											</c:if> <c:if test="${not empty plateEntity.vehicleColor}">,
																									<c:out value="${plateEntity.vehicleColor}" />
											</c:if></td>
										<c:choose>
											<c:when test="${plateEntity.updatedAt == null}">
												<td style="text-transform: uppercase;"><c:out
														value="${plateEntity.createdBy}" /></td>
												<td style="text-transform: uppercase;"><c:out
														value="${plateEntity.formattedCreatedAt}" /></td>
											</c:when>
											<c:otherwise>
												<td style="text-transform: uppercase;"><c:out
														value="${plateEntity.updatedBy}" /></td>
												<td style="text-transform: uppercase;"><c:out
														value="${plateEntity.formattedUpdatedAt}" /></td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			<button type="button" id="ok" class="btn blue-dark btn-sm">OK</button>
		</div>
	</div>
</div>
<!-- END CONTENT -->
<!-- END CONTAINER -->
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready( function () {
	document.documentElement.style.overflowX = 'hidden';	
	$("#historyAll").click(function (e) {
		var modal= document.getElementById("popupHstry");
		modal.style.display="block";
		var table = $("#allDetailsTbl").DataTable();
		table.destroy();
		$('#allDetailsTbl').DataTable({
			"paging" : false,
			"info" : false,
			"searching" : false,
			"orderClasses" : false,
			"autoWidth" : false,
			"columnDefs" : [ {
				"targets" : [ 0, 1, 2 ],
				"orderable" : false,
			} ],
			"order" : [ [ 3, "desc" ] ]
		});
		$("#ok").click(function () {
			modal.style.display="none";
		});
		window.onclick = function(event) {
		    if (event.target == modal) {
		        modal.style.display = "none";
		    }
		}
	});
			$("form").submit( function () {
			    $("#disposition").prop("disabled", false);
				$("#disposition2").prop("disabled", false);
				$("#dropDown").prop("disabled", false);
			});
			if("${errorMsg}"!='' && "${errorMsg}"!=' '){
			document.getElementById('errorMsg').innerHTML = "${errorMsg}";
			}
    		
	    	 
});	
</script>
<style>
.modal {
	display: none;
	position: fixed;
	margin-top: 50px;
	z-index: 1;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	overflow: auto;
	background-color: rgb(0, 0, 0);
	background-color: rgba(0, 0, 0, 0.4);
}

.modal-content {
	background-color: #fefefe;
	margin: 15% auto;
	padding: 20px;
	border: 1px solid #888;
	width: 50%;
	height: auto;
}

#notes {
	resize: none;
}
</style>
<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>