	<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />

		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
			<li><a href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.hearing.scheduler" /></span></li>
				<%-- <li><a style="margin-left: 20px;"
					href="${pageContext.request.contextPath}/violationView/${violationId}">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><span>Hearing Scheduler</span></li> --%>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<div id="citationPortlet" class="portlet box blue-dark bg-inverse">
						<div class="portlet-title">
							<div class="caption">Hearing Scheduler</div>
						</div>
						<div class="portlet-body form">
							<div class="alert alert-danger" id="error" style="display: none">
								<span id="errorInfo"></span>
							</div>
							<span class="help-block">&nbsp;</span>
							<c:if test="${not empty errorInfoEx}">
								<div class="alert alert-danger">
									<span >${errorInfoEx}</span>
								</div>
							</c:if>
							<!-- BEGIN FORM-->
							<form:form id="hearingDetails"
								onsubmit="return validateHearing();" action="${pageContext.request.contextPath}/hearingDetails/add/${hearingDetailsForm.violation.violationId}"
								modelAttribute="hearingDetailsForm" method="POST"
								autocomplete="on">
								<div class="form-body">
									<form:hidden path="id" />
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Violation
													Number<span class="required">*</span>
												</label>
												<div class="col-md-9">
													<form:input class="form-control input-sm" style="text-transform: uppercase;"
														path="violation.violationId" placeholder="Violation Number"
														value="" id="violationId" readonly="true"></form:input>
													<span class="help-block">&nbsp;<form:errors
															cssClass="alert-danger" /></span>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Hearing Type</label>
												<div class="col-md-9">
													<form:select path="type" id="type"
														class="form-control input-sm">
															<form:option value="PERSON" label="IN-PERSON / SCHEDULED"></form:option>
															<form:option value="WALKIN" label="IN-PERSON / WALK IN"></form:option>
														<form:option value="WRITTEN" label="WRITTEN"></form:option>
													</form:select>
													<span class="help-block">&nbsp;</span>
												</div>
												<!-- /input-group -->
												<span class="help-block">&nbsp;</span>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Hearing
													Date</label>
												<div class="col-md-9">
													<div class="input-group input-group-sm date date-picker"
														data-date-format="mm/dd/yyyy" data-date-start-date="+0d"
														data-date-viewmode="years" >
														<form:input class="form-control" path="hearingDate"
															id="hearingDate" readonly="true"/>
														<span class="input-group-btn">
															<button class="btn default" type="button" id="dateButton">
																<i class="fa fa-calendar"></i>
															</button>
														</span>
													</div>
													<!-- /input-group -->
													<span class="help-block">&nbsp;</span>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Hearing Time</label>
												<div class="col-md-9">
													<form:select path="hearingTime" id="hearingTime"
														class="form-control input-sm">
														<form:option value="" label="Select an Option"></form:option>
														<c:forEach items="${hearingSchedulers}" var="scheduler">
															<form:option value="${scheduler.value}"
																label="${scheduler.description}"></form:option>
														</c:forEach>
													</form:select>
													<input type="hidden" id="previousTime">
													<span class="help-block">&nbsp;</span>
												</div>
												<span class="help-block">&nbsp;</span>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<!--/row-->



								<div class="form-actions">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-12 col-md-12">
													<c:choose>
														<c:when test="${hearingDetailsForm.id == null}">
															<input type="submit" class="btn blue-dark btn-sm" style=""
																value="SUBMIT"
																formaction="${pageContext.request.contextPath}/hearingDetails/add/${hearingDetailsForm.violation.violationId}" />
														</c:when>
														<c:otherwise>
															<input type="submit" class="btn blue-dark btn-sm" style=""
																value="UPDATE" />
														</c:otherwise>
													</c:choose>
													<a href="${pageContext.request.contextPath}/violationView/${hearingDetailsForm.violation.violationId}"
														class="btn blue-dark btn-sm" style="">CANCEL</a>
												</div>
											</div>
										</div>
										<div class="col-md-6"></div>
									</div>
								</div>
								<!--FORM Body-->
							</form:form>
						</div>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CONTENT BODY -->

<!-- END CONTENT -->

<!-- END CONTAINER -->


<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<script>
$(document).ready(
		function() {
		$('.timepicker-no-seconds').timepicker({
            minuteStep: 1
        });
		$('.date').datepicker({
	        daysOfWeekDisabled: [0,1,3,5,6],
			minDate: new Date(),
			beforeShowDay: noWeekendsOrHolidays, 
			changeYear: true ,
			changeMonth: true
	    })
		$("#hearingDate").on("change paste keyup", function() {
			getschedules();
			   $('.datepicker').hide();
		});
		/* $('select').on('change', function() {
			  if(this.value=='WRITTEN'){
				  $("#hearingDate").attr( "disabled", "disabled");
				  $("#hearingTime").attr("disabled", "disabled");
				  $("#dateButton").attr("disabled", "disabled");
			  }else{
				  $("#hearingDate").prop('disabled', false);
				  $("#hearingTime").prop('disabled', false);
				  $("#dateButton").prop('disabled', false);
			  }
		});			
		 */
		 $("#previousTime").val($("#hearingTime").val());
		 $('#hearingTime').on('change', function() {
				if($(this).val()!=null && $(this).val()!=''){
					 $("#previousTime").val($(this).val());
				}
			});
		 enableOrDisable();
		$('#type').on('change', function() {
			enableOrDisable();
		});
		
		 
		
		
});

var disabledDates = ${holidays}; 

function noWeekendsOrHolidays(date) { 
            return disableDays(date); 
} 

function disableDays(date) { 
    for (var i = 0; i < disabledDates.length; i++) { 
        if (new Date(disabledDates[i]).toString() == date.toString()) {              
            return false; 
        } 
    } 
    return true; 
}  



function getschedules(){
	$.ajax({
		url : '../getSchedulers',
		data : {
			hearingDate : $('#hearingDate').val()
		},
		success : function(data) {
			//$hearingSchedulers = data;
			var obj = jQuery.parseJSON(data);
			$("#hearingTime").empty();
			$('#hearingTime')
	         .append($("<option></option>")
	         .attr("value","")
	         .text("Select an Option"));
			$.each(obj, function(key,value) {
				console.log(key,value);
				$('#hearingTime')
		         .append($("<option></option>")
		         .attr("value",value.value)
		         .text(value.description));
			})
			
		}
	});
}

function enableOrDisable(){
	var type=document.getElementById("type").value;
	var date=new Date();
	date.setHours(0);
	date.setMinutes(0);
	date.setSeconds(0);
	if(type=='WALKIN'){
		if(!disableDays(date)){
			$("#hearingTime").prop('disabled', false);
			document.getElementById("errorInfo").innerHTML="Today is an Holiday. Please WALK-IN another day.";
			document.getElementById("error").style.display="block";
		}else{
			var today = new Date();
			if(today.getDay() == 2 || today.getDay() == 4){
				$( "#hearingDate" ).datepicker( "setDate", today);				
			}else{
				$("#hearingTime").prop('disabled', false);
				document.getElementById("errorInfo").innerHTML="Today adjudication is not available. Please WALK-IN another day.";
				document.getElementById("error").style.display="block";	
			}
		}
		$("#hearingTime").val($("#previousTime").val());
		$("#hearingTime").prop('disabled', false);
		$("#hearingDate").attr( "disabled", true);
		$("#dateButton").attr("disabled", true);
		getschedules();
	}else if(type=='WRITTEN'){
		document.getElementById("error").style.display="none";
		$("#hearingDate").val("");
		$("#hearingTime").val("");
		 $("#hearingDate").attr( "disabled", true);
		 $("#hearingTime").attr("disabled", true);
		 $("#dateButton").attr("disabled", true);
	}else{
		document.getElementById("error").style.display="none";
		date="${hearingDetailsForm.hearingDate}";
		if(date!=null && date!=''){
			$("#hearingDate").val(date.split("-")[1] + "/" + date.split("-")[2] + "/" + date.split("-")[0]);
		}
		$("#hearingTime").val($("#previousTime").val());
		 $("#hearingDate").prop('disabled', false);
		 $("#hearingTime").prop('disabled', false);
		 $("#dateButton").prop('disabled', false);
	}
}
function validateHearing(){
	$("#hearingDate").prop('disabled', false);
	 $("#hearingTime").prop('disabled', false);
	 $("#dateButton").prop('disabled', false);
	var type=document.getElementById("type").value;
	var today = new Date();
	if(type!='WRITTEN'){
		var hearingDate=document.getElementById("hearingDate").value;
		var hearingTime=document.getElementById("hearingTime").value;
		if(hearingDate==null || hearingDate==''){
			if(type=='WALKIN' && (today.getDay() != 2 || today.getDay() != 4)){
				$("#hearingTime").prop('disabled', true);
				$("#hearingDate").attr( "disabled", true);
				$("#dateButton").attr("disabled", true);
				document.getElementById("errorInfo").innerHTML="Today adjudication is not available. Please WALK-IN another day.";	
				document.getElementById("error").style.display="block";
				return false;			
			}else{
				document.getElementById("errorInfo").innerHTML="Please select date.";
				document.getElementById("error").style.display="block";
				return false;				
			}
		}if(hearingTime==null || hearingTime==''){
			document.getElementById("errorInfo").innerHTML="Please select hearing slot.";
			document.getElementById("error").style.display="block";
			return false;
		}
		document.getElementById("error").style.display="none";
		return true;
	}
	 
	document.getElementById("error").style.display="none";
	return true;
}

</script>


<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>

<script type="text/javascript">
	
</script>