<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a href="${pageContext.request.contextPath}/home"><spring:message
							code="lbl.home" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.violation.view" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-1">
							<a class="btn blue-dark btn-sm"
								href="${pageContext.request.contextPath}/violationDetails/${violation.violationId}"><span>EDIT</span></a>
						</div>
						<div class="col-md-10"></div>
						<div class="col-md-1">
							<a class="btn blue-dark btn-sm"
								href="${pageContext.request.contextPath}/payment/${violation.violationId}"><span>PAY</span></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span class="help-block"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>VIOLATION</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<th style="width: 33%"><b>Violation&nbsp; Number:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><b><c:out
														value="${violation.violationId}" /></b></font></td>
									</tr>
									<tr>
										<th><b>Status:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase; color: #FEFDFD; background-color: #F50C2D;"><c:choose>
													<c:when test="${violation.status == 'VIOD'}">
														<b><c:out value="CLOSED - VOID" /></b>
													</c:when>
													<c:when test="${violation.status == 'OVERPAID'}">
														<b><c:out value="CLOSED - OVERPAID" /></b>
													</c:when>
													<c:otherwise>
														<b><c:out value="${violation.status}" /></b>
													</c:otherwise>
												</c:choose></font></td>
									</tr>
									<tr>
										<th><b>Violation:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.violationCode.description}" /></font></td>
									</tr>
									<tr>
										<th><b>Violation Code:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.violationCode.code}" /></font></td>
									</tr>
									<tr>
										<th><b>Issued:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.dateOfViolation}" />&nbsp;<c:out
													value="${dayOfWeek}" /></font></td>
									</tr>
									<tr>
										<th><b>Issued Time:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.timeOfViolation}" /></font></td>
									</tr>

									<tr>
										<th><b>Processed:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.issueDate}" />&nbsp;<c:out
													value="${processDay}" /></font></td>
									</tr>
									<tr>
										<th><b>Vehicle:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${vehicle}" /></font></td>
									</tr>
									<tr>
										<th><b>Location:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.locationOfViolation}" /></font></td>
									</tr>
								</table>
							</div>
						</div>

					</div>
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>VIOLATOR NAME AND ADDRESS</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px; width: 209px;">
									<tr>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${name}" /></font></td>
									</tr>
									<tr>
										<td><font
											style="font-weight: 400; text-transform: uppercase;">
												<c:out
													value="${violation.patron.address.formattedAddress}" />
										</font></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>VIOLATION NOTES</b>
								</div>
							</div>
							<%-- <div class="portlet-body" style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.comments[0].formattedCreatedAtNoss}" /></font></td>
									</tr>
								</table>
							</div> --%>
							<div class="portlet-body scroller"
								style="padding: 6px; overflow: scroll; height: 100px;">
								<table style="font-size: 13px;">
									<tr>

										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${violation.comments}" /></font></td>

									</tr>

								</table>
							</div>
							<%-- <div class="portlet-body"
								style="padding-top: 5px; padding-bottom: 5px; padding-left: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<td><a style="color: blue"
											href="${pageContext.request.contextPath}/history/${violation.violationId}?historyItem=Notes"><b>View
													All Notes</b></a></td>
									</tr>
									<tr>
										<td><a style="color: blue" data-toggle="modal"
											href="#addNotesModal"><b>Add Notes</b></a></td>
									</tr>
								</table>
							</div> --%>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>ISSUANCE INFORMATION</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<div class="table-responsive">
									<div class="col-md-12 col-sm-12"
										style="padding-left: 0px; padding-right: 0px;">
										<table style="font-size: 13px;">
											<tr>
												<th><b>Badge:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${violation.employeeNumber}" /></font></td>
											</tr>
											<tr>
												<th><b>Agency:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${Agency}" /></font></td>
											</tr>
											<tr>
												<th><b>Title:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${title}" /></font></td>
											</tr>
											<tr>
												<th><b>Division:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>RD:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>

										</table>

									</div>
									<div class="col-md-12 col-sm-12 form-body"
										style="padding-left: 0px; padding-right: 0px;">
										<div
											style="border-top: dashed; border-top-width: thin; color: #004b85; margin-bottom: 15px; margin-top: 15px;"></div>

										<b
											style="text-align: left; margin-bottom: 10px; font-size: 13px;">VEHICLE
											DESCRIPTION</b><br>
										<div class="col-md-12 col-sm-12"
											style="padding-left: 0px; padding-right: 0px;">
											<table style="font-size: 13px;">
												<tr>
													<th><b>Tag Expiration:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;">
															<c:if
																test="${not empty violation.plateEntity.month}">
																<c:out value="${violation.plateEntity.month}" />
															</c:if> <c:if
																test="${not empty violation.plateEntity.year}">
																/<c:out value="${violation.plateEntity.year}" />
															</c:if>
													</font></td>
												</tr>

												<tr>
													<th><b>Make:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${vhclMakeDscrption}" /></font></td>
												</tr>
												<tr>
													<th><b>Model:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${violation.plateEntity.vehicleModel}" /></font></td>
												</tr>
												<tr>
													<th><b>Color:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${vhclColorDscrption}" /></font></td>
												</tr>
												<tr>
													<th><b>Body Type:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${vhclBodyTypeDscrption}" /></font></td>
												</tr>
												<tr>
													<th><b>Vehicle License State:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${violation.plateEntity.state}" /></font></td>
												</tr>
												<tr>
													<th><b>Vehicle License Number: &nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${violation.plateEntity.licenceNumber}" /></font></td>
												</tr>
												<tr>
													<th><b>VIN:</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;">
															<%-- <c:choose>
																<c:when
																	test="${not empty violation.plateEntity.vinNumber && fn:length(violation.plateEntity.vinNumber)>3}">
																	<c:out
																		value="${fn:substring(violation.plateEntity.vinNumber, fn:length(violation.plateEntity.vinNumber)-5, fn:length(violation.plateEntity.vinNumber))}" />
																
																</c:when>
																<c:otherwise>
																		<c:out value="${violation.plateEntity.vinNumber}" />
																</c:otherwise>
															</c:choose> --%>
															<c:out value="${violation.plateEntity.vinNumber}" />
													</font></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse"
							style="margin-bottom: 20px;">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>VEHICLE DMV REGISTRY INFORMATION</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<c:choose>
									<c:when test="${dmvHistory != null && not empty dmvHistory}">
										<c:forEach items="${dmvHistory}" var="dmv">
											<table style="font-size: 13px;">
												<tr>
													<th><b>Violator&nbsp;Name:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${dmv.firstName}" />&nbsp;<c:out
																value="${dmv.lastName}" /></font></td>
												</tr>
												<tr>
													<th><b>Address:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;">
														<c:if test="${dmv.address != null && not empty dmv.address}">
														<c:out
																value="${dmv.address}" />,&nbsp;</c:if>
															<c:if
																test="${dmv.addressLine2 != null && not empty dmv.addressLine2}"><c:out
																	value="${dmv.addressLine2}" />,&nbsp;
															</c:if> <c:if
																test="${dmv.addressLine1 != null && not empty dmv.addressLine1}"><c:out
																	value="${dmv.addressLine1}" />,&nbsp;
															</c:if> <c:if test="${dmv.city != null && not empty dmv.city}"><c:out
																	value="${dmv.city}" />,&nbsp;
															</c:if> <c:if test="${dmv.zip != null && not empty dmv.zip}"><c:out
																	value="${dmv.zip}" />
															</c:if> </font></td>
												</tr>
												<tr>
													<th><b>VIN:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${dmv.VINNumber}" /></font></td>
												</tr>
												<tr>
													<th><b>Make:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${dmv.vehicleMake}" /></font></td>
												</tr>
												<tr>
													<th><b>Tag&nbsp;Expiration&nbsp;Year:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;">
														<c:if test="${dmv.getLicenseYear != 0}">
																<c:out value="${dmv.getLicenseYear}" />
															</c:if>
													</font></td>
												</tr>
												<tr>
													<th><b>Tag&nbsp;Expiration&nbsp;Month:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;">
															<c:if test="${dmv.getLicenseMonth != 0}">
																<c:out value="${dmv.getLicenseMonth}" />
															</c:if>															
													</font></td>
												</tr>
												<tr>
													<th><b>Co&#8209;Owner:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${violation.patron.coOwnerFirstName}" /> <c:if
																test="${violation.patron.coOwnermiddleName != null && not empty violation.patron.coOwnermiddleName}">
														&nbsp;<c:out
																	value="${violation.patron.coOwnermiddleName}" />
															</c:if>&nbsp;<c:out
																value="${violation.patron.coOwnerlastName}" /></font></td>
												</tr>
												<tr>
													<th><b>Co&#8209;Owner&nbsp;Type:&nbsp;</b></th>
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="${violation.patron.coOwnerType}" /></font></td>
												</tr>
											</table>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<table style="font-size: 13px;">
											<tr>
												<th><b>Violator Name:</b></th>
											</tr>
											<tr>
												<th><b>Address:</b></th>
											</tr>
											<tr>
												<th><b>VIN:</b></th>
											</tr>
											<tr>
												<th><b>Make:</b></th>
											</tr>
											<tr>
												<th><b>Tag Expiration Year:</b></th>
											</tr>
											<tr>
												<th><b>Tag Expiration Month:</b></th>
											</tr>
											<tr>
												<th><b>Co-Owner:</b></th>
											</tr>
											<tr>
												<th><b>Co-Owner Type:</b></th>
											</tr>
										</table>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>FINANCIAL</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<th><b>Fine:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${not empty violation.fineAmount}">
													<c:out value="${violation.fineAmount}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Penalty 1:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.penaltyCode.penalty1!='' && penalty.penaltyCode.penalty1!=NULL}">$<c:out
														value="${penalty.penaltyCode.penalty1}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Penalty 2:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.penaltyCode.penalty2!='' && penalty.penaltyCode.penalty2!=NULL}">$<c:out
														value="${penalty.penaltyCode.penalty2}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Penalty 3:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.penaltyCode.penalty3!='' && penalty.penaltyCode.penalty3!=NULL}">$<c:out
														value="${penalty.penaltyCode.penalty3}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Penalty 4:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.penaltyCode.penalty4!='' && penalty.penaltyCode.penalty4!=NULL}">$<c:out
														value="${penalty.penaltyCode.penalty4}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Penalty 5:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.penaltyCode.penalty5!='' && penalty.penaltyCode.penalty5!=NULL}">$<c:out
														value="${penalty.penaltyCode.penalty5}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Reduction:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${penalty.reducedAmount!='' && penalty.reducedAmount!=NULL}">$<c:out
														value="${penalty.reducedAmount}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Court Fee Due:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${superiorCourtReduction!='' && superiorCourtReduction!=NULL}">$<c:out
														value="${superiorCourtReduction}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Total Amount Paid:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${totalPaid!='' && totalPaid!=NULL}">$<c:out
														value="${totalPaid}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Total Due:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase; color: #FEFDFD; background-color: #F50C2D;"><c:if
													test="${violation.totalDue!='' && violation.totalDue!=NULL}">$<c:out
														value="${violation.totalDue}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Overpaid:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase; color: #FEFDFD; background-color: #F50C2D;"><c:if
													test="${paymentDetails.overPaid!='' && paymentDetails.overPaid!=NULL&& paymentDetails.overPaid!='0.00'}">$<c:out
														value="${paymentDetails.overPaid}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Unapplied Amt:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="" /></font></td>
									</tr>
									<tr>
										<th><b>Next Penalty Date:&nbsp;&nbsp;</b></th>
										<c:if
											test="${violation.status!='PAID' && violation.status!='CLOSED'  &&  violation.status!='VIOD' && violation.status!='OVERPAID' && violation.totalDue>0.00}">
											<td><font
												style="font-weight: 400; text-transform: uppercase;"><c:out
														value="" /></font></td>
										</c:if>
									</tr>
								</table>

							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>CORRESPONDENCE</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<th><b>Type:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.correspCode.typCd}" /></font></td>
									</tr>
									<tr>
										<th><b>Description:&nbsp;&nbsp;</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.correspCode.correspDesc}" /></font></td>
									</tr>
									<tr>
										<th><b>Letter Type:&nbsp;&nbsp;</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="" /></font></td>
									</tr>
									<tr>
										<th><b>Date:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.formattedCorresDate}" /></font></td>
									</tr>
									<tr>
										<th><b>Time:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.formattedCorresp_time}" /></font></td>
									</tr>
									<tr>
										<th><b>User ID:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.createdBy}" /></font></td>
									</tr>
									<%-- <tr>
										<th><b>Sent:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${correspondenceDetails.corresp_sent}" /></font></td>
									</tr> --%>
								</table>

							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>PAYMENTS</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<th><b>Accounts:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${paymentDetails.account}" /></font></td>
									</tr>
									<tr>
										<th><b>Type:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="" /></font></td>
									</tr>
									<tr>
										<th><b>Batch:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="" /></font></td>
									</tr>
									<tr>
										<th><b>Process Date:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${paymentDetails.formattedProcessedOn}" /></font></td>
									</tr>
									<tr>
										<th><b>Payment Dep:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${paymentDetails.paymentDate}" /></font></td>
									</tr>
									<tr>
										<th><b>Method:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${paymentDetails.paymentMethod.description}" /></font></td>
									</tr>
									<tr>
										<th><b>Amount:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${paymentDetails.amount!='' && paymentDetails.amount!=NULL}">$<c:out
														value="${paymentDetails.amount}" />
												</c:if> </font></td>
									</tr>
									<tr>
										<th><b>Refund Check:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:if
													test="${refundCheck!='' && refundCheck!=NULL}">
													<c:out value="${refundCheck}" />
												</c:if></font></td>
									</tr>
									<tr>
										<th><b>Reapply Source:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="" /></font></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>HEARING AND DISPOSITION</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
											<tr>
												<th><b>Hearing&nbsp;Officer:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.hearingOfficer}" /></font></td>
											</tr>
											<tr>
												<th><b>Hearing&nbsp;Date:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.formattedHearingDate}" /></font></td>
											</tr>
											<tr>
												<th><b>Hearing&nbsp;Time:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><fmt:formatDate
															type="time" timeStyle="short" pattern="hh:mm a"
															value="${hearingDetails.hearingTime}" /></font></td>
											</tr>
											<tr>
												<th><b>Hearing&nbsp;Proc&nbsp;Date:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.formattedScheduledAt}" /></font></td>
											</tr>
											<tr>
												<th><b>Disposition&nbsp;Code:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.disposition}" /></font></td>
											</tr>
											<tr>
												<th><b>Description:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:choose>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.decision,'Not_Liable')}">
																<c:out value="Not Liable"></c:out>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.decision,'LiableComSer')}">
																<c:out value="Liable with Community Service"></c:out>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.decision,'AdmsnComSer')}">
																<c:out value="Admission Community Service"></c:out>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.decision,'LiableIpp')}">
																<c:out value="Liable with IPP"></c:out>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.decision,'Liable')}">
																<c:out value="Liable"></c:out>
															</c:when>
															<c:when
																test="${fn:containsIgnoreCase(hearingDetails.status,'Failed to Appear')}">
																<c:out value="Failed to Appear"></c:out>
															</c:when>
															<c:otherwise>
																<c:out value="${hearingDetails.decision}"></c:out>
															</c:otherwise>
														</c:choose></font></td>
											</tr>
											<tr>
												<th><b>Disposition&nbsp;Date:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.formattedDispDate}" /></font></td>
											</tr>
											<tr>
												<th><b>Disp&nbsp;process&nbsp;Date:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<%-- <tr>
												<th><b>Disposition&nbsp;User&nbsp;ID:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.userId}" /></font></td>
											</tr> --%>
											<tr>
												<th><b>Reduction&nbsp;Amount:&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${hearingDetails.reduction}" /></font></td>
											</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>ACTIVITY SUSPENSION</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<table style="font-size: 13px;">
									<tr>
										<th><b>Suspend Code:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.suspendedCodes.code}" /></font></td>
									</tr>
									<tr>
										<th><b>Description:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.suspendedCodes.description}" /></font></td>
									</tr>
									<tr>
										<th><b>Date:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.formattedSuspendDate}" /></font></td>
									</tr>
									<tr>
										<th><b>Process Date:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.formattedProcessedOn}" /></font></td>
									</tr>
									<tr>
										<th><b>Time:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.formattedSuspended_time}" /></font></td>
									</tr>
									<%-- <tr>
										<th><b>User ID:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;"><c:out
													value="${suspendDetails.userId}" /></font></td>
									</tr> --%>
									<tr>
										<th><b>Suspend Until:</b></th>
										<td><font
											style="font-weight: 400; text-transform: uppercase;">
												<c:if
													test="${suspendDetails.suspendedCodes.code != null && suspendDetails.suspendedCodes.code != ''}">
													<c:out value="${violation.suspendProcessDate}" />
												</c:if>
										</font></td>
									</tr>
								</table>
							</div>
						</div>


					</div>

					<div class="col-md-4 col-sm-4">
						<div class="portlet box blue-dark bg-inverse">
							<div class="portlet-title"
								style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
								<div class="caption"
									style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
									<b>NOTICES</b>
								</div>
							</div>
							<div class="portlet-body " style="padding: 6px;">
								<!-- Notice Type, Description,Date, Date Proc, Notice Date, Status -->
								<c:choose>
									<c:when test="${not empty noticeDetails}">
										<table style="font-size: 13px; text-transform: uppercase;">
											<tr>
												<th><b>Notice Type:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${noticeType}" /></font></td>
											</tr>
											<tr>
												<th><b>Description:&nbsp;&nbsp;</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${description}" /></font></td>
											</tr>
											<tr>
												<th><b>Date:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${noticeDate}" /></font></td>
											</tr>
											<tr>
												<th><b>Date Proc:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="${dateProc}" /></font></td>
											</tr>
											<tr>
												<th><b>Status:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Next Notice Date:&nbsp;&nbsp;</b></th>
												<c:if
													test="${violation.status!='PAID' && violation.status!='CLOSED'  &&  violation.status!='VIOD' && violation.status!='OVERPAID' && violation.totalDue>0.00}">
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="" /></font></td>
												</c:if>
											</tr>
										</table>
									</c:when>
									<c:otherwise>
										<table style="font-size: 13px;">
											<tr>
												<th><b>Notice Type:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Description:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Date:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Date Proc:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Status:</b></th>
												<td><font
													style="font-weight: 400; text-transform: uppercase;"><c:out
															value="" /></font></td>
											</tr>
											<tr>
												<th><b>Next Notice Date:&nbsp;&nbsp;</b></th>
												<c:if
													test="${violation.status!='PAID' && violation.status!='CLOSED'  &&  violation.status!='VIOD' && violation.status!='OVERPAID' && violation.totalDue>0.00}">
													<td><font
														style="font-weight: 400; text-transform: uppercase;"><c:out
																value="" /></font></td>
												</c:if>
											</tr>
										</table>

									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>