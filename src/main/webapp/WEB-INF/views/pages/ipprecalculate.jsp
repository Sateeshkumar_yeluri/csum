<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" id="blockui_citationdetails_data">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="portlet light portlet-fit bordered">
			<div style="text-align: center;">
				<span style="font-size: 16px;">
						<b>License Number:</b><span
						style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
					&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
							Number:</b></span><span style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
			</div>
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-blue sbold uppercase">Ipp
						Details Information</span>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div class="portlet-body">
				<div class="page-bar">
					<div class="row">
						<div class="col-md-12">
							<div class="tabbable-line boxless tabbable-reversed">
								<div id="citationPortlet" class="portlet box table-bordered">
									<div class="portlet-body form">

										<!-- BEGIN FORM-->
										<form:form id="citationForm"
											onsubmit="return validateCitDetails()" action="#"
											modelAttribute="ippForm" method="GET" autocomplete="on"
											enctype="multipart/form-data">
											<div class="form-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group form-group-sm">
															<label class="control-label label-sm col-md-3">Plan
																Status </label>
															<div class="col-md-9">
																<form:input class="form-control input-sm" path="status"
																	id="status" readonly="true"
																	style="background-color : white;text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<%-- <form:errors
															path="status" cssClass="alert-danger" /> --%> <span
																	class="alert-danger" id="statusError"></span>
																</span>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Original
																Due</label>
															<div class="col-md-9">
																<form:input class="form-control input-sm"
																	path="originalDue" id="status" readonly="true"
																	style="background-color : white;text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<%-- <form:errors
															path="originalDue" cssClass="alert-danger" /> --%> <span
																	class="alert-danger" id="originalDueError"></span>
																</span>
															</div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Previous
																Credits </label>
															<div class="col-md-9">
																<form:input path="previousCredits"
																	class="form-control input-sm" readonly="true"
																	style="background-color : white;text-transform: uppercase;" />
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Enroll
																Amount</label>
															<div class="col-md-9">
																<form:input class="form-control input-sm"
																	path="enrollAmount" id="enroll"
																	value="${ippForm.enrollAmount}" readonly="true"
																	style="background-color : white;text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<%-- <form:errors
															path="enrollAmount" cssClass="alert-danger" /> --%> <span
																	class="alert-danger" id="enrollAmountError"></span>
																</span> <input type="hidden" value="${ippForm.enrollAmount}"
																	id="originalenrollAmount" />
															</div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Down
																Payment </label>
															<div class="col-md-9">
																<form:input path="downPayment" type="text" id="down"
																	class="form-control input-sm" onchange="downpay()"
																	value="${ippForm.downPayment}" onfocus="downpay1()" style="text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<form:errors
																		path="downPayment" cssClass="alert-danger" /><span
																	class="alert-danger" id="downPaymentError">${errorMessage}</span>
																</span>
															</div>
															<input type="hidden" value="${ippForm.downPayment}"
																id="originaldownPayment" />
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Installment
																Amount </label>
															<div class="col-md-9">
																<form:input path="installmentAmount" type="text"
																	id="installment" class="form-control input-sm"
																	onchange="instpay()"
																	value="${ippForm.installmentAmount}"
																	onfocus="instpay1()" style="text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<form:errors
																		path="installmentAmount" cssClass="alert-danger" /><span
																	class="alert-danger" id="installmentAmountError">${errorMessage}</span>
																</span>
															</div>
															<input type="hidden" value="${ippForm.installmentAmount}"
																id="originalinstallment" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Number
																of Payments</label>
															<div class="col-md-9">
																<form:input path="noOfPayments" type="text"
																	id="noOfPayments" class="form-control input-sm"
																	onchange="payments()" value="${ippForm.noOfPayments}"
																	onfocus="payments1()" style="text-transform: uppercase;"></form:input>
																<span class="help-block">&nbsp;<%-- <form:errors
															path="noOfPayments" cssClass="alert-danger" /> --%> <span
																	class="alert-danger" id="noOfPaymentsError"></span>
																</span> <input type="hidden" value="${ippForm.noOfPayments}"
																	id="originalPayments" />
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label label-sm col-md-3">Start
																On </label>
															<div class="col-md-9">
																<form:input path="startDate"
																	class="form-control input-sm" readonly="true"
																	style="background-color : white;text-transform: uppercase;" />
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-offset-11 col-md-11">
																<input type="submit" class="btn blue-dark btn-sm"
																	onclick="return update()"
																	formaction="${pageContext.request.contextPath}/ippDetails/${violationId}"
																	style="position: static; top: -2px; -left: -51px; right: 30px; bottom: 40px; float: none;"
																	value="SAVE" id="blockui_savebtn" /> <input
																	class="btn blue-dark btn-sm" type="submit"
																	formaction="${pageContext.request.contextPath}/ippDetails/${violationId}"
																	class="btn blue-dark btn-sm"
																	style="width: 70px; margin: 2px;" value="CANCEL"
																	formmethod="get" onclick="cancel()" />
															</div>
															<input type="hidden" value="${ippForm}" id="ippForm" />
														</div>
													</div>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function downpay() {
		var downPayment = parseFloat(document.getElementById("down").value);
		var installmentAmount = parseFloat(document
				.getElementById("installment").value);
		var enrollAmount = parseFloat(document.getElementById("enroll").value);
		var originaldown = parseFloat(document
				.getElementById("originaldownPayment").value);
		var originalinstallment = parseFloat(document
				.getElementById("originalinstallment").value);
		var originalPayments = document.getElementById("originalPayments").value;
		var due = parseFloat(enrollAmount - downPayment);
		document.getElementById('installmentAmountError').innerHTML = '';
		document.getElementById('noOfPaymentsError').innerHTML = '';
		var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
		if (isNaN(downPayment) == false) {
			if (downPayment > enrollAmount) {
				document.getElementById('downPaymentError').innerHTML = 'Down Payment cannot be greater than Enroll Amount';
			} else if (downPayment < downPayAmt) {
				document.getElementById('downPaymentError').innerHTML = 'Down Payment should be greater than or equals to 30 percent of Enroll Amount';
			} else if (0 == due) {
				document.getElementById('downPaymentError').innerHTML = '';
				document.getElementById("installment").value = 0.00;
				document.getElementById("noOfPayments").value = 0;
			} else {
				document.getElementById('downPaymentError').innerHTML = '';
				document.getElementById("installment").value = (enrollAmount - downPayment) / (2);
				document.getElementById("noOfPayments").value = 3;
			}
		} else if (downPayment < 0 || isNaN(downPayment) == true) {
			document.getElementById('downPaymentError').innerHTML = 'Down Payment should be greater than or equals to 30 percent of Enroll Amount';
		}
	}
	function instpay() {
		var downPayment = parseFloat(document.getElementById("down").value);
		var enrollAmount = parseFloat(document.getElementById("enroll").value);
		var installmentAmount = parseFloat(document
				.getElementById("installment").value);
		var originalinstallment = parseFloat(document
				.getElementById("originalinstallment").value);
		var originalPayments = parseFloat(document
				.getElementById("originalPayments").value);
		document.getElementById('downPaymentError').innerHTML = '';
		document.getElementById('noOfPaymentsError').innerHTML = '';
		var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
		if (isNaN(installmentAmount) == false && installmentAmount >= 0) {
			if ((downPayment + installmentAmount) > enrollAmount) {
				document.getElementById('installmentAmountError').innerHTML = 'Installment Amount should be less than Enroll Amount minus Downpayment';
			} else if (installmentAmount == 0) {
				document.getElementById('installmentAmountError').innerHTML = '';
				document.getElementById("noOfPayments").value = 1;
				document.getElementById("down").value = enrollAmount;
			} else {
				document.getElementById('installmentAmountError').innerHTML = '';
				if (installmentAmount == originalinstallment) {
					document.getElementById("noOfPayments").value = originalPayments;
				} else {
					document.getElementById("noOfPayments").value = parseFloat(Math
							.floor(((enrollAmount - downPayAmt) / installmentAmount)
									.toFixed(2)) + 1);
				}
				document.getElementById("down").value = downPayAmt
						+ ((enrollAmount - downPayAmt) % installmentAmount);
			}
		} else {
			document.getElementById('installmentAmountError').innerHTML = 'Installment Amount should not be empty or negative';
		}
	}
	function payments() {
		document.getElementById('installmentAmountError').innerHTML = '';
		document.getElementById('downPaymentError').innerHTML = '';
		var payments = document.getElementById("noOfPayments").value;
		var originalinstallment = parseFloat(document
				.getElementById("originalinstallment").value);
		var originalPayments = parseFloat(document
				.getElementById("originalPayments").value);
		var downPayment = parseFloat(document.getElementById("down").value);
		var enrollAmount = parseFloat(document.getElementById("enroll").value);
		var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
		if (isNaN(payments) == false && payments > 0) {
			if (1 == payments) {
				document.getElementById("installment").value = 0.00;
				document.getElementById("down").value = enrollAmount;
			} else {
				if (downPayment == enrollAmount) {
					document.getElementById("installment").value = ((enrollAmount - downPayAmt) / (payments - 1))
					.toFixed(2);
					document.getElementById("down").value = downPayAmt;
				}else{
					document.getElementById("installment").value = ((enrollAmount - downPayment) / (payments - 1))
					.toFixed(2);
				}
				
			}
		} else {
			document.getElementById("installment").value = 0.00;
			document.getElementById('noOfPaymentsError').innerHTML = 'No of payments should be greater than zero';
		}

	}

	function payments1() {
		document.getElementById('noOfPaymentsError').innerHTML = '';
	}
	function downpay1() {
		document.getElementById('downPaymentError').innerHTML = '';
	}
	function instpay1() {
		document.getElementById('installmentAmountError').innerHTML = '';
	}

	function cancel() {
		var originaldown = parseFloat(document
				.getElementById("originaldownPayment").value);
		var originalinstallment = parseFloat(document
				.getElementById("originalinstallment").value);
		var originalPayments = parseFloat(document
				.getElementById("originalPayments").value);
		document.getElementById("installment").value = originalinstallment;
		document.getElementById("noOfPayments").value = originalPayments;
		document.getElementById("down").value = originaldown;
	}

	function update() {
		var downPayment = parseFloat(document.getElementById("down").value);
		var installmentAmount = parseFloat(document
				.getElementById("installment").value);
		var enrollAmount = parseFloat(document.getElementById("enroll").value);
		var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
		var payments = document.getElementById("noOfPayments").value;
		if (downPayment > enrollAmount) {
			document.getElementById('downPaymentError').innerHTML = 'Down Payment cannot be greater than Enroll Amount';
			return false;
		} else if (downPayment < downPayAmt || downPayment < 1
				|| isNaN(downPayment) == true) {
			document.getElementById('downPaymentError').innerHTML = 'DownPayment should be greater than or equal to 30 percentage of enrollAmount';
			return false;
		} else if ((downPayment + installmentAmount) > enrollAmount
				|| installmentAmount < 0) {
			document.getElementById('installmentAmountError').innerHTML = 'Installment amount should be less than Enroll Amount minus Downpayment';
			return false;
		} else if (payments < 1) {
			document.getElementById('noOfPaymentsError').innerHTML = 'No of payments should be greater than zero';
			return false;
		}
	}

	/* function update() {
		var down = parseFloat(document.getElementById("installment").value);
		var installment = parseFloat(document.getElementById("noOfPayments").value);
		var payments = parseFloat(document.getElementById("down").value);
		var enrollAmount = parseFloat(document.getElementById("enroll").value);
		if (0 == installment || 0 == payments) {
			document.getElementById("installment").value = 0.00;
			document.getElementById("noOfPayments").value = 0;
			document.getElementById("down").value = enrollAmount;
		}
	} */
</script>