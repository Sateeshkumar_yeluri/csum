<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/home"><spring:message
							code="lbl.home" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.topmenu.tab.violation.entry" /></span></li>
			</ul>
		</div>
		<div class="row" style="margin: 0px;">
			<div class="col-md-12 ">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="citationPortlet" class="portlet box blue-dark bg-inverse">
						<div class="portlet-title">
							<div class="caption">Violation Form</div>
						</div>
						<div class="portlet-body form">
							<c:if test="${not empty errorInfo}">
								<div class="alert alert-danger">
									<span>${errorInfo}</span>
								</div>
							</c:if>
							<div class="form-body">
								<form:form onsubmit="return validateForm();"
									action="${pageContext.request.contextPath}/saveViolation"
									modelAttribute="violationForm" method="POST" autocomplete="on">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Violation&nbsp;Number<span class="required">*</span></label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" id="vlnNum"
															placeholder="Violation Number" path="violationId"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<span
															id="vlnNumError" class="alert-danger"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Location
														of Violation</label>
													<div class="col-md-9">
														<%-- <form:input class="form-control input-sm"
															path="locationOfViolation" placeholder="Location"
															style="text-transform: uppercase;"></form:input> --%>
															<form:select class="bs-select form-control" path="locationOfViolation" style="text-transform: uppercase;">
																<form:option value="" label="Select an Option"></form:option>
																<form:option value="LOT A" label="LOT A"></form:option>
																<form:option value="LOT B" label="LOT B"></form:option>
																<form:option value="LOT C" label="LOT C"></form:option>
																<form:option value="LOT D" label="LOT D"></form:option>
																<form:option value="LOT E" label="LOT E"></form:option>
																<form:option value="LOT F" label="LOT F"></form:option>
																<form:option value="LOT G" label="LOT G"></form:option>
																<form:option value="LOT I" label="LOT I"></form:option>
																<form:option value="LOT J" label="LOT J"></form:option>
																<form:option value="LOT K" label="LOT K"></form:option>
																<form:option value="LOT L" label="LOT L"></form:option>
																<form:option value="LOT M" label="LOT M"></form:option>
																<form:option value="LOT O" label="LOT O"></form:option>
															</form:select>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Date
														Of Violation</label>
													<div class="col-md-9">
														<div class="input-group input-group-sm date date-picker"
															data-date-format="mm/dd/yyyy" data-date-end-date="0d"
															data-date-viewmode="years">
															<form:input path="dateOfViolation" class="form-control" id="dateOfViolation"/>
															<span class="input-group-btn">
																<button class="btn default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
															</span>
														</div>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Time</label>
													<div class="col-md-9">
														<div
															class="input-group input-group-sm bootstrap-timepicker timepicker">
															<form:input path="timeOfViolation" type="text" id="timeOfViolation"
																class="form-control" data-minute-step="1"></form:input>
															<span class="input-group-addon"><i
																class="glyphicon glyphicon-time"></i></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<c:if test="${not empty violationForm && not empty violationId}">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">First
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.firstName" placeholder="First Name"
															id="fnamePatron" style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.firstName" cssClass="alert-danger" /> <span
															class="alert-danger" id="fnameError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Middle
														Name</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.middleName" placeholder="Middle Name"
															id="mnamePatron" style="text-transform: uppercase;"></form:input>
														<span class="help-block"> <span
															class="alert-danger" id="mnameError"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Last
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.lastName" placeholder="Last Name"
															id="lnamePatron" style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.lastName" cssClass="alert-danger" /> <span
															class="alert-danger" id="lnameError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Address</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.address.address" placeholder="Address"
															id="patronAddress" style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.address" cssClass="alert-danger" />
															<span class="alert-danger" id="aptError"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">City
													</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.address.city" placeholder="City"
															id="cityPatron" style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.city" cssClass="alert-danger" /> <span
															class="alert-danger" id="cityError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Address
														State</label>
													<div class="col-md-9">
														<form:select class="bs-select form-control"
															style="font-size:13px;width:455px;"
															path="patron.address.state" placeholder="State">
															<form:option value="" label="Select an Option"></form:option>
															<c:forEach items="${states}" var="state">
																<form:option value="${state.description}"
																	label="${state.description}"
																	style="text-transform: uppercase;"></form:option>
															</c:forEach>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.state" cssClass="alert-danger" /></span>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Zip
														Code </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="patron.address.zip" placeholder="Zip Code"
															id="zipcodePatron" style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="patron.address.zip" cssClass="alert-danger" /> <span
															class="alert-danger" id="zipcodeError"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6"></div>
										</div>
									</div>
									</c:if>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Vehicle
														Plate Number </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="plateEntity.licenceNumber"
															placeholder="Vehicle Plate Number" id="vhclLicNmbr"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block"><form:errors
																path="plateEntity.licenceNumber" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">VIN
														</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="plateEntity.vinNumber" placeholder="VIN"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block"><form:errors
																path="plateEntity.vinNumber" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Body
														Type </label>
													<div class="col-md-9">
														<form:select path="plateEntity.bodyType"
															class="bs-select form-control input-sm">
															<form:option value="" label="Select an Option"></form:option>
															<form:option value="PASS" label="PASS"></form:option>
															<form:option value="TRUCK" label="TRUCK"></form:option>
															<form:option value="MOTORCYCLE" label="MOTORCYCLE"></form:option>
															<form:option value="SUV" label="SUV"></form:option>
															<form:option value="OTHER" label="OTHER"></form:option>
															<%-- <form:option value="" label="Select an Option"></form:option>
															<c:forEach items="${vhclBodyTypes}" var="vhclBodyType">
																<form:option value="${vhclBodyType.code}"
																	label="${vhclBodyType.description}" style="text-transform: uppercase;"></form:option>
															</c:forEach> --%>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="plateEntity.bodyType" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Make
													</label>
													<div class="col-md-9" id="makeDiv">
														<form:select path="plateEntity.vehicleMake" id="vhclMake"
															class="bs-select form-control input-sm">
															<form:option value="" label="Select an Option"></form:option>
															<form:option value="BMW" label="BMW"></form:option>
															<form:option value="BUICK" label="BUICK"></form:option>
															<form:option value="CAD" label="CAD"></form:option>
															<form:option value="CHEV" label="CHEV"></form:option>
															<form:option value="CADI" label="CADI"></form:option>
															<form:option value="DODG" label="DODG"></form:option>
															<form:option value="FORD" label="FORD"></form:option>
															<form:option value="GMC" label="GMC"></form:option>
															<form:option value="HOND" label="HOND"></form:option>
															<form:option value="HYUN" label="HYUN"></form:option>
															<form:option value="INFI" label="INFI"></form:option>
															<form:option value="JEEP" label="JEEP"></form:option>
															<form:option value="KIA" label="KIA"></form:option>
															<form:option value="LEX" label="LEX"></form:option>
															<form:option value="MAZD" label="MAZD"></form:option>
															<form:option value="MER" label="MER"></form:option>
															<form:option value="NISS" label="NISS"></form:option>
															<form:option value="PONT" label="PONT"></form:option>
															<form:option value="SCION" label="SCION"></form:option>
															<form:option value="TOYT" label="TOYT"></form:option>
															<form:option value="VOLK" label="VOLK"></form:option>
															<%-- <form:option value="OTHER" label="OTHER"></form:option> --%>
															<form:option value="" label="Select an Option"></form:option>
															<%-- <c:forEach items="${vhclMakes}" var="vhclMake">
																<form:option value="${vhclMake.code}"
																	label="${vhclMake.description}" style="text-transform: uppercase;"></form:option>
															</c:forEach> --%>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="plateEntity.vehicleMake" cssClass="alert-danger" /></span>
													</div>

													<%-- <div class="col-md-4" style="display: none" id="otherVhclMake">
										<form:input class="form-control input-sm"
											path="plateEntity.otherVhclMake" placeholder="Vehicle Moke"></form:input>
									</div> --%>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Model
													</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="plateEntity.vehicleModel"
															placeholder="Vehicle Model"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="plateEntity.vehicleModel" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Vehicle Color
													</label>
													<div class="col-md-9">
														<form:select path="plateEntity.vehicleColor"
															class="bs-select form-control input-sm">
															<form:option value="" label="Select an Option"></form:option>
															<form:option value="BGE" label="BGE"></form:option>
															<form:option value="BLK" label="BLK"></form:option>
															<form:option value="BLU" label="BLU"></form:option>
															<form:option value="BRN" label="BRN"></form:option>
															<form:option value="GLD" label="GLD"></form:option>
															<form:option value="GRY" label="GRY"></form:option>
															<form:option value="GRN" label="GRN"></form:option>
															<form:option value="MAR" label="MAR"></form:option>
															<form:option value="RED" label="RED"></form:option>
															<form:option value="SIL" label="SIL"></form:option>
															<form:option value="WHI" label="WHI"></form:option>
															<form:option value="YEL" label="YEL"></form:option>
															<form:option value="OTHER" label="OTHER"></form:option>
															<form:option value="" label="Select an Option"></form:option>
															<c:forEach items="${vhclColors}" var="vhclColor">
																<form:option value="${vhclColor.code}"
																	label="${vhclColor.description}"
																	style="text-transform: uppercase;"></form:option>
															</c:forEach>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="plateEntity.vehicleColor" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Violation
														Code<span class="required">*</span></label>
													<div class="col-md-9">
														<form:select class="form-control select2" id="vlnCode"
															name="adminCode" style="font-size:13px;width:455px;"
															path="violationCode.id" placeholder="Violation Code">
															<form:option value="" label="Select an Option"></form:option>
															<c:forEach items="${violationCodes}" var="violationCode">
																<form:option value="${violationCode.id}"
																data-value='{"fine":"${violationCode.standardFine}","desc":"${violationCode.description}"}'
																	label="${violationCode.code}-${violationCode.description}"
																	style="text-transform: uppercase;"></form:option>
															</c:forEach>
														</form:select>
														<span class="help-block">&nbsp;<form:errors
																path="violationCode" cssClass="alert-danger" /> <span
															id="violationCodeError" class="alert-danger"></span>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Fine
														Amount</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" id="fineAmount"
															placeholder="$0.00" path="fineAmount" readonly="false"></form:input>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Status</label>
													<div class="col-md-9">
														<form:select path="status" class="form-control input-sm">
															<form:option value="PENDING" label="PENDING"></form:option>
															<form:option value="PAID" label="PAID"></form:option>
															<form:option value="ISSUED" label="ISSUED"></form:option>
															<form:option value="CLOSED" label="CLOSED"></form:option>
															<form:option value="VIOD" label="CLOSED - VOID"></form:option>
															<form:option value="OVERPAID" label="CLOSED - OVERPAID"></form:option>
														</form:select>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Total
														Due</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" path="totalDue"
															placeholder="Total Due" id="totalDue"></form:input>
														<%-- <form:hidden path="totalDuehide" placeholder="Total Due"
															id="totalDueCithide"></form:hidden> --%>
														<span class="help-block"> <span
															id="totalDueCitError" class="alert-danger"></span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Issuing
														Officer</label>
													<div class="col-md-9">
														<form:select class="form-control select2"
															id="inspIdParking" name="inspId"
															style="font-size:13px;width:455px;" path="issuingOfficer"
															placeholder="Issuing Officer">
															<form:option value="" label="Select an Option"></form:option>
															<%-- <c:forEach items="${inspectors}" var="inspector">
																	<form:option
																		value="${inspector.lastName} ${inspector.firstName}"
																		data-value='{"inspId":"${inspector.employeeId}"}'
																		data-id="${inspector.employeeId}"
																		label="${inspector.lastName} ${inspector.firstName}" style="text-transform: uppercase;"></form:option>
																</c:forEach> --%>
														</form:select>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Employee
														No </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="employeeNumber" placeholder="Employee Number"
															id="employeeNumberParking"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;<form:errors
																path="employeeNumber" cssClass="alert-danger" /></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6"></div>
											<div class="col-md-6"></div>
										</div>
									</div>
									<div class="row">
										<div class="form-actions">
											<div class="row">
												<div class="col-md-12" align="center">
													<c:choose>
														<c:when test="${not empty violationForm && not empty violationId}">
															<input type="submit" class="btn blue-dark btn-sm"
																formaction="${pageContext.request.contextPath}/updateViolation/${violationId}"value="UPDATE" />
														</c:when>
														<c:otherwise>
															<input type="submit" class="btn blue-dark btn-sm"
																value="SAVE" />
														</c:otherwise>
													</c:choose>
													<button type="button"
														onclick="window.location='${pageContext.request.contextPath}/violationEntry';"
														class="btn blue-dark btn-sm">CANCEL</button>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
	$(document).ready(function() {
		var vlnNum = $("#vlnNum").val();
		if($.trim(vlnNum)!='' && $.trim(vlnNum).length>0){
			$("#vlnNum").attr('readonly', true);
		}
		$("#timeOfViolation").timepicker('setTime',
				new Date());
		$("#dateOfViolation").datepicker({
			format : 'mm/dd/yyyy'
		}).datepicker("setDate", 'today');
		$("#vlnCode").change(function(){
			var newFine = $(this).find(':selected').data("value").fine;
			var oldDue = $("#totalDue").val();
			var oldFine = $("#fineAmount").val();
			var newDue = Number(oldDue)-Number(oldFine)+Number(newFine);
			$("#totalDue").val(newDue);
			$("#fineAmount").val(newFine);
		});		
	});
	function validateForm(){
		var vlnNum = $("#vlnNum").val();
		if($.trim(vlnNum).length==0){
			$("#vlnNumError").text("Violation Number can not be empty");
			$("#vlnNum").focus();
			return false;
		}else{
			$("#vlnNumError").text("");
		}
		var vlnCode = $("#vlnCode").val();
		if($.trim(vlnCode).length==0){
			$("#violationCodeError").text("Please select Violation Code");
			$("#vlnCode").focus();
			return false;
		}else{
			$("#violationCodeError").text("");
		}
		
	}
</script>