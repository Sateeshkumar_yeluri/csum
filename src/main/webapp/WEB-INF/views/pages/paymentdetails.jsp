<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a
					href="${pageContext.request.contextPath}/violationview/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.payment.details" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit bordered">
					<div class="row">
						<div class="col-md-12" align="center">
							<span style="font-size: 16px;"><b>License Number:</b><span
								style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
								style="font-size: 17px"><b>Violation Number:</b></span><span
								style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
						</div>
					</div>
					<div class="row" style="margin: 0px;">
						<div class="col-md-12">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-blue sbold uppercase">Payment
										Details Information</span>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table  table-bordered table-hover order-column">
									<thead>
										<tr>
											<th>Violation Number#</th>
											<th>Amount/Payment Date</th>
											<th>Type/Method</th>
											<th>Account</th>
											<th>Processed On/User Id</th>
											<th>IPP</th>
											<th>Overpaid</th>
											<th>Total Due</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${empty paymentList}">
												<tr>
													<td colspan="11" align="center"><b>No results
															found</b></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${paymentList}" var="payment">
													<tr class="odd gradeX">
														<!-- <td><label
													class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
														<input type="checkbox" class="checkboxes" value="1" /> <span></span>
												</label></td> -->
														<td style="text-transform: uppercase;"><a
															style="color: blue"
															href="<c:url value='../editpayments/${payment.id}' />">
																<c:out value="${payment.violation.violationId}" />
														</a></td>
														<td style="text-transform: uppercase;">$<c:out
																value="${payment.amount}" />/<c:out
																value="${payment.paymentDate}" /></td>
														<td style="text-transform: uppercase;"><c:choose>
																<c:when test="${payment.paymentSource == 'P'}">
																	<c:out value="PAY-IN-PERSON" />
																</c:when>
																<c:when test="${payment.paymentSource == 'M'}">
																	<c:out value="PAY-BY-MAIL" />
																</c:when>
																<c:otherwise>
																	<c:out value="PAY-BY-WEB" />
																</c:otherwise>
															</c:choose>/<c:out value="${payment.paymentMethod.description}" /></td>
														<td style="text-transform: uppercase;"><c:out
																value="${payment.account}" /></td>
														<td style="text-transform: uppercase;"><c:out
																value="${payment.processedOn}" />/<c:out
																value="${payment.processedBy}" /></td>
														<c:if test="${payment.ipp!=false}">
															<td style="text-transform: uppercase;"><c:out
																	value="Yes" /></td>
														</c:if>
														<c:if test="${payment.ipp!=true}">
															<td style="text-transform: uppercase;"><c:out
																	value="No" /></td>
														</c:if>
														<td style="text-transform: uppercase;">$<c:out
																value="${payment.overPaid}" /></td>
														<td style="text-transform: uppercase;">$<c:out
																value="${payment.totalDue}" /></td>

													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>