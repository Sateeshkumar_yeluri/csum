<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin:-25px 0px 0px">
			<ul class="page-breadcrumb">
				<li><a 
					href="${pageContext.request.contextPath}/home"><spring:message
							code="lbl.home" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.violation.info" /></span></li>
			</ul>
			<!-- END PAGE HEADER-->
		</div>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-body form">
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet-title" style="margin-left: 20px;">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-blue sbold uppercase">Violation
										Info</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="row" style="margin-bottom: 50px;">
									<div class="col-md-12">
										<div class="form-body">
											<div class="form-group">
												<div class="col-md-12">
													<table class="table  table-hover table-bordered table-sm">
														<thead>
															<tr>
																<th>Violation&nbsp;Number</th>
																<th>Status&nbsp;/&nbsp;Issued On</th>
																<th>Plate&nbsp;Details</th>
																<th>Violation&nbsp;Code&nbsp;&nbsp;Description</th>
																<th>Violation&nbsp;Location</th>
																<th>Fine&nbsp;Amt</th>
																<th>Penalties</th>
																<th>Total&nbsp;Due</th>
															</tr>
														</thead>
														<tbody>
															<c:choose>
																<c:when test="${empty violationInfo}">
																	<tr style="text-transform: uppercase;">
																		<td style="text-transform: uppercase;" colspan="6"
																			align="center"><b>No results found</b></td>
																	</tr>
																</c:when>
																<c:otherwise>
																	<tr style="text-transform: uppercase;">
																		<td><b><a onclick="enablePageLoadBar()"
																				style="color: #004b85"
																				href="<c:url value='/violationView/${violationInfo.violationId}' />">
																					<c:out value="${violationInfo.violationId}" />
																			</a></b> <!-- <a href=""> <img
																		border="0" alt="Documents" src=<c:url value='/static/img/preview.png' />
																		width="25" height="25">
																</a> --></td>
																		<td><c:choose>
																				<c:when test="${violationInfo.status == 'VIOD'}">
																					<b><c:out value="CLOSED - VOID" /></b>
																				</c:when>
																				<c:when test="${violationInfo.status == 'OVERPAID'}">
																					<b><c:out value="CLOSED - OVERPAID" /></b>
																				</c:when>
																				<c:otherwise>
																					<b><c:out value="${violationInfo.status}" /></b>
																				</c:otherwise>
																			</c:choose> / <c:out value="${violationInfo.dateOfViolation}" /></td>
																		<td><c:out
																				value="${violationInfo.plateEntity.licenceNumber}" /></td>
																		<td><c:out
																				value="${violationInfo.violationCode.code}" />/<c:out
																				value="${violationInfo.violationCode.description}" />
																		</td>
																		<td><c:out
																				value="${violationInfo.locationOfViolation}" /></td>
																		<td><b><font color="#004b85">$<c:out
																						value="${violationInfo.fineAmount}" /></font></b></td>
																		<td><b><font color="#004b85">$<c:out
																						value="0.00" /></font></b></td>
																		<c:choose>
																			<c:when test="${violationInfo.totalDue == '0' }">
																				<td><b><font color="#004b85">$<c:out
																								value="0.00" /></font></b></td>
																			</c:when>
																			<c:otherwise>
																				<td><b><font color="#004b85">$<c:out
																								value="${violationInfo.totalDue}" /></font></b></td>
																			</c:otherwise>
																		</c:choose>
																		<c:if
																			test="${violationInfo.status!='PAID' && violationInfo.status!='CLOSED' &&violationInfo.status!='VIOD' && violationInfo.status!='OVERPAID' && violationInfo.totalDue>0.00}">
																			<td><b><a style="color: #004b85"
																					href="<c:url value='/payment/${violationInfo.violationId}' />">Pay</a></b>
																			</td>
																		</c:if>
																	</tr>
																</c:otherwise>
															</c:choose>
														</tbody>
													</table>

													<!-- Payment Acknowledgement -->
													<c:if test="${not empty errorMessagePayment}">
														<div class="alert alert-info">
															<span>${errorMessagePayment}</span>
														</div>
														<c:choose>
															<c:when test="${empty payedInfo}">
																<tr style="text-transform: uppercase;">
																	<td colspan="6" align="center"><b>No Receipt
																			found</b></td>
																</tr>
															</c:when>
															<c:otherwise>
																<div style="margin-left: 300px; width: 50%;">
																	<div id="printContentId">
																		<table style="font-weight: bold"
																			class="table table-bordered table-hover">

																			<tbody>
																				<tr style="text-transform: uppercase;">
																					<td style="background-color: #004b85; color: #fff"
																						colspan="2" align="center">Payment
																						Acknowledgement</td>
																				</tr>

																				<tr style="text-transform: uppercase;">
																					<td style="width: 50%">Transaction Number</td>
																					<td>${payedInfo.transaction.transcationId}</td>
																				</tr>
																				<tr style="text-transform: uppercase;">
																					<td style="width: 50%">Violation Number</td>
																					<td>${violationId}</td>
																				</tr>
																				<tr style="text-transform: uppercase;">
																					<td style="width: 50%">Name</td>
																					<td>${payedInfo.violation.patron.firstName}&nbsp;${payedInfo.violation.patron.lastName}</td>
																				</tr>
																				<tr style="text-transform: uppercase;">
																					<td style="width: 50%">Payment Amount</td>
																					<td>${paymentForm.amount}</td>
																				</tr>
																				<tr style="text-transform: uppercase;">
																					<td style="width: 50%">Payment Type</td>
																					<td>${payedInfo.paymentMethod.description}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div style="margin-left: 590px;">
																	<button class="btn blue-dark btn-sm icon-printer"
																		onclick="printAcknowledgement('printContentId')">
																		<b>PRINT</b>
																	</button>
																</div>
															</c:otherwise>
														</c:choose>
													</c:if>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</div>
	
	</div>
