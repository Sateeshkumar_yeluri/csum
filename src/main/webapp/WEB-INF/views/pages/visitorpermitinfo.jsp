<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/visitorPermitPurchase"><spring:message
							code="lbl.page.bar.permit" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.permit.info" /></span></li>
			</ul>
			<!-- END PAGE HEADER-->
		</div>
		<div class="row" style="margin-right: 0px;margin-top: 10px;">
		<div class="col-md-offset-6 col-md-6">
			<a href="${pageContext.request.contextPath}/permitLogin"  class="btn blue-dark btn-sm"
					style="width: 70px; float: right;" >HOME</a>
		</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet-body form">
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet-title" style="margin: 20px;">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject sbold uppercase" style="color: #004b85;">Permit
											Info</span>
											<span class="help-block"></span>
									</div>
								</div>
								<div class="portlet-body">
									<div class="row" style="margin-bottom: 50px;">
										<div class="col-md-12">
											<table class="table  table-hover table-bordered table-sm">
												<thead>
													<tr>
														<th>Permit&nbsp;Number</th>
														<th>Permit&nbsp;Type</th>
														<th>Parking Slot</th>
														<th>Name</th>
														<th>Licence&nbsp;Number</th>
														<th>Start&nbsp;Date&nbsp;Time</th>
														<th>End&nbsp;Date&nbsp;Time</th>
														<th>Amount&nbsp;Paid</th>
													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${empty permitInfo}">
															<tr style="text-transform: uppercase;">
																<td style="text-transform: uppercase;" colspan="7"
																	align="center"><b>No results found</b></td>
															</tr>
														</c:when>
														<c:otherwise>
															<tr style="text-transform: uppercase;">
																<%-- <td><b><a onclick="enablePageLoadBar()"
																		style="color: #004b85"
																		href="<c:url value='/violationView/${permitInfo.violationId}' />">
																			<c:out value="${permitInfo.violationId}" />
																	</a></b></td> --%>
																<td><c:out
																		value="${permitInfo.permitId}" /></td>
																<td><c:out
																		value="${permitInfo.permitType}" /></td>
																<td><c:out
																		value="${permitInfo.parkingSlot}" /></td>		
																<td><c:out
																		value="${permitInfo.firstName}" /> <c:out
																		value="${permitInfo.middleName}" /> <c:out
																		value="${permitInfo.lastName}" /></td>
																<td><c:out
																		value="${permitInfo.licenceNumber}" /></td>
																<td><c:out	
																		value="${permitInfo.formattedStartDate}" /> <c:out
																		value="${permitInfo.formattedStartTime}" /></td>
																<td><c:out
																		value="${permitInfo.formattedEndDate}" /> <c:out
																		value="${permitInfo.formattedEndTime}" /></td>
																<td><c:out
																		value="${permitInfo.amount}" /></td>
															</tr>
														</c:otherwise>
													</c:choose>
												</tbody>
											</table>

											<!-- Payment Acknowledgement -->
											<c:if test="${not empty errorMessagePayment}">
												<div class="alert alert-info">
													<span>${errorMessagePayment}</span>
												</div>
												<c:choose>
													<c:when test="${empty payedInfo}">
														<tr style="text-transform: uppercase;">
															<td colspan="6" align="center"><b>No Receipt
																	found</b></td>
														</tr>
													</c:when>
													<c:otherwise>
														<div style="margin-left: 300px; width: 50%;">
															<div id="printContentId">
																<table style="font-weight: bold"
																	class="table table-bordered table-hover">

																	<tbody>
																		<tr style="text-transform: uppercase;">
																			<td style="background-color: #004b85; color: #fff"
																				colspan="2" align="center">Payment
																				Acknowledgement</td>
																		</tr>

																		<tr style="text-transform: uppercase;">
																			<td style="width: 50%">Transaction Number</td>
																			<td>${payedInfo.permitTransaction.transcationId}</td>
																		</tr>
																		<tr style="text-transform: uppercase;">
																			<td style="width: 50%">Permit Number</td>
																			<td>${permitInfo.permitId}</td>
																		</tr>
																		<tr style="text-transform: uppercase;">
																			<td style="width: 50%">Name</td>
																			<td>${permitInfo.firstName}&nbsp;${permitInfo.lastName}</td>
																		</tr>
																		<tr style="text-transform: uppercase;">
																			<td style="width: 50%">Payment Amount</td>
																			<td>${permitInfo.amount}</td>
																		</tr>
																		<%-- <tr style="text-transform: uppercase;">
																			<td style="width: 50%">Payment Type</td>
																			<td>${payedInfo.paymentMethod.description}</td>
																		</tr> --%>
																	</tbody>
																</table>
															</div>
														</div>
														<div style="margin-left: 590px;">
															<button class="btn blue-dark btn-sm icon-printer"
																onclick="printAcknowledgement('printContentId')">
																<b>PRINT</b>
															</button>
														</div>
													</c:otherwise>
												</c:choose>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

function printAcknowledgement(printContentId) {
     var preContent = "<div style='background-color:#fff'>";
     var postContent = "</div>";
     var bodyContent = document.all.item(printContentId).innerHTML;
     var currBodyCon = document.body.innerHTML;
     document.body.innerHTML = preContent+bodyContent+postContent;
     window.print();
     document.body.innerHTML = currBodyCon;
}

</script>