<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a href="${pageContext.request.contextPath}/violationview/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.penalty.details" /></span></li>
			</ul>
		</div>
		<%-- <div class="page-bar" style="margin-top: 30px;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/home"><span
						class="title"><spring:message code="lbl.home" /></span></a> <i
					class="fa fa-circle"></i></li>
				<li><span class="title"><spring:message
							code="lbl.page.bar.penalty.details" /></span></li>
			</ul>
		</div> --%>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
		<div class="portlet light portlet-fit bordered">
			<div class="row">
				<div class="col-md-12" align="center">
					<span style="font-size: 16px;"><b>License Number:</b><span
						style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
						style="font-size: 17px"><b>Violation Number:</b></span><span
						style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
				</div>
			</div>
			<div class="row" style="margin: 0px;">
				<div class="col-md-12">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue sbold uppercase">Penalty
								Details Information</span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table  table-bordered table-hover order-column">
							<thead>
								<tr style="font-size: 11px;">
									<th>Violation Number</th>
									<th>Fine Amount</th>
									<th>Penalty 1<b>&nbsp;/</b>Date
									</th>
									<th>Penalty 2<b>&nbsp;/</b>Date
									</th>
									<th>Penalty 3<b>&nbsp;/</b>Date
									</th>
									<th>Total Due</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
								<c:when test="${empty penalty}">
									<tr>
										<td colspan="6" align="center"
											style="text-transform: uppercase;"><b>No results
												found</b></td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<td style="text-transform: uppercase;"><a style="color: blue" href="#">
										${penalty.violation.violationId}</a></td>
										<td style="text-transform: uppercase;">&#36;${penalty.violation.fineAmount}</td>
										<td style="text-transform: uppercase;">&#36;
										<c:choose>
												<c:when test="${not empty penalty.penaltyCode.penalty1}">
												${penalty.penaltyCode.penalty1}</c:when>
												<c:otherwise>0.00</c:otherwise>
											</c:choose><b>&nbsp;/</b><br />${penalty.penaltyCode.formattedpenalty1}
										</td>
										<td style="text-transform: uppercase;">&#36;
										<c:choose>
												<c:when test="${not empty penalty.penaltyCode.penalty2}">
												${penalty.penaltyCode.penalty2}</c:when>
												<c:otherwise>0.00</c:otherwise>
											</c:choose><b>&nbsp;/</b><br />${penalty.penaltyCode.formattedpenalty2}
										</td>
										<td style="text-transform: uppercase;">&#36;
										<c:choose>
												<c:when test="${not empty penalty.penaltyCode.penalty3}">
												${penalty.penaltyCode.penalty3}</c:when>
												<c:otherwise>0.00</c:otherwise>
											</c:choose><b>&nbsp;/</b><br />${penalty.penaltyCode.formattedpenalty3}
										</td>
										<td style="text-transform: uppercase;">&#36;${penalty.violation.totalDue}</td>
									</tr>
								</c:otherwise>
							</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		</div>
	</div>
</div>