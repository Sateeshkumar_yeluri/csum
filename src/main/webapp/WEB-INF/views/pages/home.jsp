<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><span><spring:message code="lbl.home" /></span></li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet">
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet-body form">
									<!-- BEGIN  SEARCH FORM-->
									<form:form 
										action="${pageContext.request.contextPath}/violationSearch"
										modelAttribute="violationSearchForm" method="post"
										autocomplete="on">
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-3"></div>
													<div class="col-md-6">
														<div class="col-md-12">
															<%-- <div class="col-md-4">
																<div class="form-group">
																	<form:input path="ticketNumber" type="text"
																		placeholder="Search..." />
																</div>
															</div>
															<div class="col-md-1"></div>
															<div class="col-md-6" align="center">
																<div class="form-group">
																	
																	<div class="col-md-5">
																		<form:select id="searchType" name="searchType"
																			path="searchType">
																			<option value="VN">Violation Number</option>
																			<option value="LN">License Number</option>
																		</form:select>
																	</div>
																</div>
															</div>
															<div class="col-md-1">
																<div class="btn-group">
																	<button id="search" type="submit"
																		class="btn blue-dark btn-sm">SEARCH</button>
																</div>
															</div> --%>
															<div class="col-md-1"></div>
															<div class="col-md-10" >
															<!-- <div class="portlet light portlet-fit bordered"> -->
															<div>
																<!-- <div class="portlet-title"
																	style="padding: 3px 6px; margin-bottom: -3px; height: 32px; min-height: 10px;">
																	<div class="col-md-12" align="center"
																		style="width: -10px; font-size: 16px; padding-top: 2px; padding-bottom: 1px;">
																		<b>Violations Search</b>
																	</div>
																</div> -->
																<div class="portlet-body " style="padding: 6px;">
																	<br />
																	<div class="row">
																		<div class="col-md-12">
																			<div class="col-md-5" >
																				<div class="form-group">
																					<form:input path="ticketNumber" type="text"
																						placeholder="Search..." style="width: 100%;"/>
																				</div>																				
																			<span class="help-block">&nbsp;</span>
																			</div>
																			<div class="col-md-5">
																				<form:select id="searchType" name="searchType"
																					path="searchType" style="width: 100%;height:30px;">
																					<option value="SL">Select All</option>
																					<option value="VN">Violation Number</option>
																					<option value="LN">License Number</option>
																				</form:select>
																			<span class="help-block">&nbsp;</span>
																			</div>
																			<div class="col-md-2">
																				<div class="btn-group">
																					<button id="search" type="submit"
																						class="btn blue-dark btn-sm">SEARCH</button>
																				</div>																				
																			<span class="help-block">&nbsp;</span>
																			</div>
																		</div>
																	</div><br/>
																</div>
															</div>		
															</div>		
															<div class="col-md-1"></div>				
														</div>
													</div>
													<div class="col-md-3"></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-2"></div>
													<div class="col-md-4" align="center">
														<span class="alert alert-danger" style="display: none;"
															id="searchTextError"></span>
													</div>
													<div class="col-md-6"></div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<div class="row" style="margin: 0px;"> 
							<div class="col-md-12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
									<div class="portlet light portlet-fit bordered">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-subject font-blue sbold uppercase">Violation
													List</span>
											</div>
											<form:form action="${pageContext.request.contextPath}/sampleData"><button type="submit" style="float: right;"
																						class="btn blue-dark btn-sm">Sample Data</button></form:form>
										</div>
										<div class="portlet-body">
											<table class="table  table-hover table-bordered table-sm" id="violationList">
												<thead style="background-color: #004b85; border-color: black;">
													<tr>
													<th class="hidden">id</th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Violation Number</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Status</font></th>
												<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Issued On</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Name</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Address</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Violation Code & Description</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Violation&nbsp;Location</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Fine&nbsp;Amt</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Penalties</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"><font
												color="#fff">Total&nbsp;Due</font></th>
														<th style="border-right: 1px solid #fff;padding-right: 15px;"></th>

													</tr>
												</thead>
												<tbody style="text-transform: uppercase;">
													<c:choose>
														<c:when test="${not empty violationList}">
															<c:forEach items="${violationList}" var="violation">
																<tr style="text-transform: uppercase;">
																	<td class="hidden"><c:out value="${violation.id}" /></td>
																	<td><b><a style="color: #004b85"
																			href="${pageContext.request.contextPath}/violationView/${violation.violationId}"><c:out
																					value="${violation.violationId}" /></a></b></td>
																	<td><c:out value="${violation.status}" /></td>
																	<td><c:out value="${violation.dateOfViolation}" /></td>
																	<td><c:out value="${violation.patron.firstName}" />
																		<c:out value="${violation.patron.lastName}" /></td>
																	<td><c:out
																			value="${violation.patron.address.formattedAddress}" /></td>
																	<td><c:out value="${violation.violationCode.code}" />/<c:out
																			value="${violation.violationCode.description}" /></td>
																	<td><c:out
																			value="${violation.locationOfViolation}" /></td>
																	<td>$<c:out value="${violation.fineAmount}" /></td>
																	<td>$<c:out value="0.00" /></td>
																	<td>$<c:out value="${violation.totalDue}" /></td>
																	<td><c:if test="${violation.totalDue>0.00}">
																		<b><a style="color: #004b85"
																				href="<c:url value='/payment/${violation.violationId}' />">Pay</a></b>
																		
																	</c:if></td>
																</tr>
															</c:forEach>
														</c:when>
														<c:otherwise>
															<tr style="text-transform: uppercase;">
																<td colspan="10" align="center"><b>No results
																		found</b></td>
															</tr>
														</c:otherwise>
													</c:choose>
												</tbody>

											</table>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
$(document).ready(function() {
    $('#violationList').DataTable({
    	"searching" : false,
    	"info" : false,
    	"orderClasses" : false,
    	"order" : [ [ 0, "desc" ]]
    });
} );
	function validateSearchForm() {
		var searchText = $("#ticketNumber").val();
		if (searchText.length == 0) {
			$("#searchTextError").attr('style', "display:block");
			$("#searchTextError").text(
					"Please enter Violation Number or License Number");
			return false;
		}
		return true;
	}
</script>