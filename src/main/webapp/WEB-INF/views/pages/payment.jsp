<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<!-- BEGIN CONTENT -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->

	<br />
	<!-- BEGIN CONTENT BODY -->

	<!-- BEGIN PAGE HEADER-->
	<br />
	<div class="page-content" style="background-color: #f5f3eb;"
		id="blockui_adminpayment_data">
		<!-- BEGIN THEME PANEL -->
		<br /> <br />
		<!-- BEGIN CONTENT BODY -->
		<div class="page-bar"
			style="position: static; padding: 0 20px; margin: 0px 20px 30px;">
			<ul class="page-breadcrumb">
				<li><a
					href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span>Payment</span></li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<div style="text-align: center;">
			<span style="font-size: 16px;"> <b>License Number:</b> <span
				style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>

				&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
						Number:</b></span><span style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
		</div>
		<h3 class="payment-Title"
			style="padding-top: 5px; margin-top: 5px; margin-left: 5px">
			Payment Methods</h3>

		<div style="color: red; text-transform: uppercase;">${TxResultStatus}</div>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable-line boxless tabbable-reversed">
					<ul class="nav nav-tabs">
						<li
							class="active${unactiveOncheckPay}${unactiveOncashPay}${unactiveOndmvPay}"><a
							href="#tab_0" data-toggle="tab"> Credit Card </a></li>
						<li class="${unactiveOncheckPay}"><a href="#tab_1"
							data-toggle="tab"> Checks </a></li>
						<li class="${unactiveOncashPay}"><a href="#tab_2"
							data-toggle="tab"> Cash </a></li>
						<c:if test="${type == 'Parking'}">
							<li class="${unactiveOndmvPay}"><a href="#tab_3"
								data-toggle="tab"> DMV </a></li>
						</c:if>
					</ul>
					<div class="tab-content">
						<div
							class="tab-pane active${unactiveOncheckPay}${unactiveOncashPay}${unactiveOndmvPay}"
							id="tab_0">
							<div class="portlet box blue-dark">
								<div class="portlet-title">
									<div class="caption">Provide your billing and credit card
										details</div>
								</div>
								<div class="portlet-body form">

									<div class="form-wizard" style="float: left; width: 855px;">
										<div class="form-body">

											<form:form id="paymentForm" method="post"
												onsubmit="return validateCardInput()"
												action="${pageContext.request.contextPath}/checkout/${violationId}"
												modelAttribute="paymentForm">
												<form:input path="ipp" type="hidden"
													value="${paymentForm.ipp}" id="ipp" />
												<div id="payment-form">
													<%-- <label class="control-label col-md-3">Amount <span
															class="required"> * </span>
														</label>
														<div class="col-md-4">
															<form:input type="text" class="form-control"
																path="amount" value="${totalDue}" />
															<span class="help-block"> 
															</span>
														</div> --%>
													<span>Please select from the following:</span><br>
													<div class="radio-inline">
														<label class="radio label-sm"> <form:radiobutton
																path="amount" id="optionsRadios25" name="amount"
																value="${totalDue}" onclick="clearOtherAmount();"
																checked="checked" />Total Amount Due:&nbsp;$${totalDue}
															<span></span>
														</label>
														<c:choose>
															<c:when test="${paymentForm.ipp==true}">
																<label class="radio label-sm"> <form:radiobutton
																		checked="checked" path="amount" id="optionsRadios28"
																		name="amount" value="${instAmount}" onclick="read();"/>Other
																	Amount:&nbsp;<input type="text" id="amounttext"
																	name="otheramount" value="${instAmount}"> <span></span>
																</label>
															</c:when>
															<c:otherwise>
																<label class="radio label-sm"> <form:radiobutton
																		path="amount" id="optionsRadios28" name="amount"
																		value="0.00" onclick="read();" />Other Amount:&nbsp;<input
																	type="text" id="amounttext" name="otheramount">
																	<span></span>
																</label>
															</c:otherwise>
														</c:choose>
														<span class="help-block"><span id="cardInputError"
															class="alert-danger"></span> <c:if
																test="${not empty errorMessageCard}">
																<div class="alert-danger">
																	<span>${errorMessageCard}</span>
																</div>
															</c:if> </span>
													</div>
													<div class="form-group">
														<label class="control-label label-sm col-md-3">Payment
															Source<span class="required"> * </span>
														</label>
														<div class="col-md-4">
															<form:select class="bs-select form-control"
																style="font-size:13px;width:455px; text-transform: uppercase;"
																path="paymentSource" id="sourceCredit1">
																<form:option value="" label="Select an Option"></form:option>
																<form:option value="P" label="In-Person"></form:option>
																<form:option value="W" label="By-Web"></form:option>
															</form:select>
															<span class="help-block"><span
																class="alert-danger" id="sourceCreditError1"></span></span>
														</div>

													</div>
													<br />
													<div class="form-group form-md-line-input has-success">
														<label class="col-md-3 control-label" for="form_control_1"
															style="width: 20%;"><b>Credit Card</b></label>
														<div class="col-md-9">
															<div class="form-row">
																<div id="card-element">
																	<!-- a Stripe Element will be inserted here. -->
																</div>
																<!-- Used to display form errors -->
																<div id="card-errors"></div>
															</div>
														</div>
													</div>
												</div>
												<br />
												<br />
												<div class="form-actions" style="margin-top: 12px;">
													<div class="row" style="margin-bottom: 10px;">
														<div class="col-md-offset-3 col-md-9">
															<input type="submit" id="cardPayment"
																class="btn blue-dark" value="PROCESS PAYMENT" /> <a
																href="${pageContext.request.contextPath}/violationView/${violationId}"
																class="btn blue-dark">CANCEL</a>
														</div>
													</div>
												</div>
												<div class="col-md-offset-3 col-md-9">
													<span class="alert-danger" id="clearError">${errorMessage}</span>
													<span class="alert-danger" id="paymentAmountError"></span>
												</div>
											</form:form>
											<c:choose>
												<c:when test="${empty ClientToken}">
													<script
														src="/csum/static/assets/global/plugins/jquery.min.js"
														type="text/javascript"></script>
													<script type="text/javascript">
															$(document)
																	.ready(
																			function() {
																				container: "payment-form"
																			});
														</script>
												</c:when>
												<c:otherwise>
													<script
														src="/csum/static/assets/global/plugins/jquery.min.js"
														type="text/javascript"></script>
													<script type="text/javascript">
															$(document)
																	.ready(
																			function() {
																				container: "payment-form"
																			});
														</script>
												</c:otherwise>
											</c:choose>
											<div id="form_payment_error"></div>

										</div>



									</div>

								</div>
							</div>
						</div>
						<div class="tab-pane ${unactiveOncheckPay}" id="tab_1">
							<div class="portlet box blue-dark">
								<div class="portlet-title">
									<div class="caption">Pay by Check</div>
								</div>
								<div class="portlet-body form">
									<form:form class="form-horizontal"
										onsubmit="return validateCheckInput()" id="paymentForm1"
										action="${pageContext.request.contextPath}/checkPayment/${violationId}"
										modelAttribute="paymentForm" method="POST"
										enctype="multipart/form-data">
										<form:input path="ipp" type="hidden"
											value="${paymentForm.ipp}" id="ipp" />
										<div class="form-wizard">
											<div class="form-body">
												<div class="form-group">
													<span style="padding-left: 225px;">Please select
														from the following:</span><br>
													<div class="radio-inline" style="padding-left: 246px;">
														<label class="radio label-sm"> <form:radiobutton
																path="amount" id="optionsRadios29" name="amount"
																value="${totalDue}" onclick="clearOtherAmountCheck();"
																checked="checked" />Total Amount Due:&nbsp;$${totalDue}
															<span></span>
														</label>
														<c:choose>
															<c:when test="${paymentForm.ipp==true}">
																<label class="radio label-sm"> <form:radiobutton
																		checked="checked" path="amount" id="optionsRadios32"
																		name="amount" value="${instAmount}" onclick="readCheck();"/>Other
																	Amount:&nbsp;<input type="text" id="amounttext2"
																	value="${instAmount}" name="otheramount"> <span></span>
																</label>
															</c:when>
															<c:otherwise>
																<label class="radio label-sm"> <form:radiobutton
																		path="amount" id="optionsRadios32" name="amount"
																		value="0.00" onclick="readCheck();" />Other
																	Amount:&nbsp;<input type="text" id="amounttext2"
																	name="otheramount"> <span></span>
																</label>
															</c:otherwise>
														</c:choose>
														<span class="help-block"> <span
															id="checkInputError" class="alert-danger"></span> <c:if
																test="${not empty errorMessageCheque}">
																<div class="alert-danger">
																	<span>${errorMessageCheque}</span>
																</div>
															</c:if>
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Payment
														Source<span class="required"> * </span>
													</label>
													<div class="col-md-4">
														<form:select class="bs-select form-control"
															style="font-size:13px;width:455px; text-transform: uppercase;"
															path="paymentSource" id="paymentSourceCheck">
															<form:option value="" label="Select an Option"></form:option>
															<form:option value="P" label="In-Person"></form:option>
															<form:option value="M" label="Mail"></form:option>
														</form:select>
														<span class="help-block"><span class="alert-danger"
															id="paymentSourceCheckInputError"></span></span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Check Number
														<span class="required"> * </span>
													</label>
													<div class="col-md-4">
														<form:input type="text" id="entCheckNumber"
															style="text-transform: uppercase;" class="form-control"
															path="chequeNumber" />
														<span class="help-block"> <span
															id="checkNumberInputError" class="alert-danger"> </span>
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Deposit Date
														<span class="required"> * </span>
													</label>
													<div class="col-md-4">
														<div class="input-group input-group-sm date date-picker"
															data-date-format="mm/dd/yyyy" data-date-viewmode="years">
															<form:input path="paymentDate" class="form-control"
																id="paymentDate" />
															<span class="input-group-btn">
																<button class="btn default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
															</span>
														</div>
														<span class="help-block"> <span
															id="paymentDateInputError" class="alert-danger"> </span></span>
													</div>
												</div>


												<div class="form-group">
													<label class="control-label col-md-3">&nbsp;</label>
													<div class="col-md-4" style="margin-left: 15px;">
														<div class="row fileupload-buttonbar">
															<span class="btn blue-dark btn-sm fileinput-button"><span>UPLOAD
																	CHECK</span> <form:input type="file" path="uploadedCheque"
																	multiple="multiple" /> </span> <span
																class="fileupload-process"> </span>
														</div>
														<span class="help-block">&nbsp; <span
															class="alert-danger" id="chequeError"></span>
														</span>
													</div>
												</div>



												<div class="form-group" id="showUploadedFiles"
													style="display: none">
													<label class="control-label col-md-3"><span
														id="fileTypeError" style="color: #E82734"></span></label>
													<div class="col-md-4">

														<table
															class="table table-hover table-bordered  table-sortable"
															id="upload_files">
															<thead style="background-color: #004b85; color: #fff">
																<tr>
																	<th>File Name</th>
																	<th class="hidden-xs">File Type</th>
																	<th class="hidden-xs">File size</th>
																</tr>
															</thead>
															<tbody id="selectedFilesRow">
															</tbody>
														</table>
													</div>
												</div>

											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<input type="submit" id="chequePayment"
															class="btn blue-dark button-submit"
															value="PROCESS PAYMENT" data-toggle="modal"
															id="payByChequeBtn">
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>


						</div>
						<div class="tab-pane ${unactiveOncashPay}" id="tab_2">
							<div class="portlet box blue-dark">
								<div class="portlet-title">
									<div class="caption">Pay by Cash</div>
								</div>
								<div class="portlet-body form">
									<form:form class="form-horizontal"
										onsubmit="return validateCashInput()" id="paymentForm2"
										action="${pageContext.request.contextPath}/cashPayment/${violationId}/cash"
										modelAttribute="paymentForm" method="POST">
										<form:input path="ipp" type="hidden"
											value="${paymentForm.ipp}" id="ipp" />
										<div class="form-wizard">
											<div class="form-body">
												<div class="form-group">
													<span style="padding-left: 225px;">Please select
														from the following:</span><br>
													<div class="radio-inline" style="padding-left: 246px;">
														<label class="radio label-sm"> <form:radiobutton
																path="amount" id="optionsRadios33" name="amount"
																value="${totalDue}" onclick="clearOtherAmountCash();"
																checked="checked" />Total Amount Due:&nbsp;$${totalDue}
															<span></span>
														</label>
														<c:choose>
															<c:when test="${paymentForm.ipp==true}">
																<label class="radio label-sm"> <form:radiobutton
																		checked="checked" path="amount" id="optionsRadios36"
																		name="amount" value="${instAmount}" onclick="readCash();" />Other
																	Amount:&nbsp;<input type="text" id="amounttext3"
																	value="${instAmount}" name="otheramount"> <span></span>
																</label>
															</c:when>
															<c:otherwise>
																<label class="radio label-sm"> <form:radiobutton
																		path="amount" id="optionsRadios36" name="amount"
																		value="0.00" onclick="readCash();" />Other
																	Amount:&nbsp;<input type="text" id="amounttext3"
																	name="otheramount"> <span></span>
																</label>
															</c:otherwise>
														</c:choose>
														<span class="help-block"><span id="cashInputError"
															class="alert-danger"></span> <c:if
																test="${not empty errorMessageCash}">
																<div class="alert-danger">
																	<span>${errorMessageCash}</span>
																</div>
															</c:if> </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Payment
														Source<span class="required"> * </span>
													</label>
													<div class="col-md-4">
														<form:select class="bs-select form-control"
															style="font-size:13px;width:455px; text-transform: uppercase;"
															path="paymentSource" id="paymentSourceCash">
															<form:option value="" label="Select an Option"></form:option>
															<form:option value="P" label="In-Person"></form:option>
															<form:option value="M" label="Mail"></form:option>
														</form:select>
														<span class="help-block"><span class="alert-danger"
															id="paymentSourceCashError"></span></span>
													</div>
												</div>
												<%-- <div class="form-group">
														<label class="control-label col-md-3">Amount <span
															class="required"> * </span>
														</label>
														<div class="col-md-4">
															<form:input type="text" id="entCashAmt" class="form-control"
																path="amount" value="${totalDue}" />
															<span class="help-block"><span id="cashInputError" style="color:red"></span>
																<c:if test="${not empty errorMessageCash}">
                            <div class="alert-danger">
                                <span>${errorMessageCash}</span>
                            </div>
                            </c:if>
															 </span>
														</div>
													</div> --%>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<input type="submit" id="cashPayment"
															class="btn blue-dark button-submit"
															value="PROCESS PAYMENT" data-toggle="modal">
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<div class="tab-pane ${unactiveOndmvPay}" id="tab_3">
							<div class="portlet box blue-dark">
								<div class="portlet-title">
									<div class="caption">DMV</div>
								</div>
								<div class="portlet-body form">
									<form:form class="form-horizontal"
										onsubmit="return validateDmvInput()" id="paymentForm3"
										action="${pageContext.request.contextPath}/cashPayment/${violationId}/dmv"
										modelAttribute="paymentForm" method="POST">
										<div class="form-wizard">
											<div class="form-body" style="padding-left: 100px;">
												<div class="form-group">
													<label class="control-label col-md-3">Amount Paid </label>
													<div class="col-md-3">
														<form:input type="text" id="dmvAmount"
															style="text-transform: uppercase;" class="form-control"
															path="amount" />
														<span class="help-block"><span id="dmvInputError"
															class="alert-danger"></span> <c:if
																test="${not empty errorMessageDmv}">
																<div class="alert-danger">
																	<span>${errorMessageDmv}</span>
																</div>
															</c:if> </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Paid Date <span
														class="required"> * </span>
													</label>
													<div class="col-md-3">
														<div class="input-group input-group-sm date date-picker"
															data-date-format="mm/dd/yyyy" data-date-viewmode="years">
															<form:input path="paymentDate" class="form-control"
																id="paymentDateDmv" />
															<span class="input-group-btn">
																<button class="btn default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
															</span>
														</div>
														<span class="help-block"> <span
															id="paymentDateInputErrorDmv" class="alert-danger">
														</span></span>
													</div>
												</div>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<input type="submit" id="dmvPayment"
															class="btn blue-dark button-submit"
															value="PROCESS PAYMENT" data-toggle="modal">
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>
</body>



<!-- END CONTENT BODY -->
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->

<!-- END CONTAINER -->


<script>
		$(document)
				.ready(
						function() {

							$("#paymentDate").datepicker({
								format : 'mm/dd/yyyy'
							}).datepicker("setDate", 'today');

							$("#paymentDateDmv").datepicker({
								format : 'mm/dd/yyyy'
							}).datepicker("setDate", 'today');

							$('input[type="file"]')
									.change(
											function(e) {
												document
														.getElementById('fileTypeError').innerHTML = '';
												/* document
														.getElementById('payByChequeBtn').disabled = false; */
												var files = e.target.files;
												//alert(files);
												$("#selectedFilesRow").empty();
												document
														.getElementById('showUploadedFiles').style.display = 'inline';
												for (var i = 0; i < files.length; i++) {
													var newRowContent = "<tr><td>"
															+ files[i].name
															+ "</td><td>"
															+ files[i].name
																	.substr(
																			files[i].name
																					.lastIndexOf('.') + 1)
																	.toUpperCase()
															+ "</td><td>"
															+ Math
																	.round(files[i].size / 1024)
															+ " Kb"
															+ "</td></tr>";
													$(newRowContent).appendTo(
															$("#upload_files"));
													var fileTypeUploaded = files[i].name
															.substr(
																	files[i].name
																			.lastIndexOf('.') + 1)
															.toUpperCase();
													var validFileTypes = [
															'JPEG', 'PDF',
															'JPG', 'PNG' ];
													if (validFileTypes
															.indexOf(fileTypeUploaded) == -1) {
														document
																.getElementById('payByChequeBtn').disabled = true;
														document
																.getElementById('fileTypeError').innerHTML = 'Invalid File Uploaded. Allowed PDF,JPG,PNG type of files';
													}
												}
											});
							if ($("#ipp").value = false) {
							$('#amounttext').attr("readonly", true);
							$('#amounttext2').attr("readonly", true);
							$('#amounttext3').attr("readonly", true);
							}
							$('#entCheckNumber').keypress(function(e) {
								alphaNumericValidation(e);
							});

						});
		$('#amounttext').keyup(
				function() {

					document.getElementById("optionsRadios28").value = $(
							'#amounttext').val();
				});
		$("#amounttext").keypress(function(event) {
			if (event.which == 45 || event.which == 189) {
				event.preventDefault();
			}
		});

		$('#amounttext2').keyup(
				function() {

					document.getElementById("optionsRadios32").value = $(
							'#amounttext2').val();
				});
		$("#amounttext2").keypress(function(event) {
			if (event.which == 45 || event.which == 189) {
				event.preventDefault();
			}
		});

		$('#amounttext3').keyup(
				function() {

					document.getElementById("optionsRadios36").value = $(
							'#amounttext3').val();
				});
		$("#amounttext3").keypress(function(event) {
			if (event.which == 45 || event.which == 189) {
				event.preventDefault();
			}
		});
	</script>
<script type="text/javascript">		
	function clearOtherAmount() {
		if ($("#ipp").value = false) {
			var amount = $('#amounttext').val();
			if (amount.length) {
				document.getElementById('amounttext').value = '';
			}
		}
		$('#amounttext').attr("readonly", true);
	}
	function read() {
		$('#amounttext').attr("readonly", false);
	}	
	function clearOtherAmountCheck() {
		if ($("#ipp").value = false) {
			var amount = $('#amounttext2').val();
			if (amount.length) {
				document.getElementById('amounttext2').value = '';
			}
		}
		$('#amounttext2').attr("readonly", true);
	}
	function readCheck() {
		$('#amounttext2').attr("readonly", false);
	}
	function clearOtherAmountCash() {
		if ($("#ipp").value = false) {
			var amount = $('#amounttext3').val();
			if (amount.length) {
				document.getElementById('amounttext3').value = '';
			}
		}
		$('#amounttext3').attr("readonly", true);
	}
	function readCash() {
		$('#amounttext3').attr("readonly", false);
	}
</script>
<script>
		function validateCheckInput() {
			var checkAmt = document.getElementById('optionsRadios32').value;
			var checkNumber = document.getElementById('entCheckNumber').value;
			var paymentDate = document.getElementById('paymentDate').value;
			var paymentSourceCheck = document
					.getElementById('paymentSourceCheck').value;
			var selectedFilesRow = $('input[type="file"]').val();
			var isNumbr = /^[0-9.]+$/.test(checkAmt);
			document.getElementById('paymentSourceCheckInputError').innerHTML = '';
			document.getElementById('checkInputError').innerHTML = ' ';
			document.getElementById('paymentDateInputError').innerHTML = '';
			document.getElementById('checkNumberInputError').innerHTML = '';
			document.getElementById('chequeError').innerHTML = '';
			if (!isNumbr && $("#optionsRadios29").checked) {
				document.getElementById('checkInputError').innerHTML = 'Invalid Amount';
				document.getElementById('chequePayment').disabled = false;
				return false;
			} else {
				document.getElementById('checkInputError').innerHTML = ' ';
				document.getElementById('chequePayment').disabled = true;
			}
			if (paymentSourceCheck == '' || paymentSourceCheck == null) {
				document.getElementById('paymentSourceCheckInputError').innerHTML = 'Please Select Payment Source';
				document.getElementById('chequePayment').disabled = false;
				return false;
			} else {
				document.getElementById('chequePayment').disabled = true;
			}
			if (paymentDate == '' || paymentDate == null) {
				document.getElementById('paymentDateInputError').innerHTML = 'Please Select Deposit Date';
				document.getElementById('chequePayment').disabled = false;
				return false;
			} else {
				document.getElementById('chequePayment').disabled = true;
			}
			if (checkNumber.length < 2) {
				document.getElementById('checkNumberInputError').innerHTML = 'Invalid Check Number';
				document.getElementById('chequePayment').disabled = false;
				return false;
			} else {
				document.getElementById('chequePayment').disabled = true;
			}
			if (selectedFilesRow == '' || selectedFilesRow == null) {
				document.getElementById('chequeError').innerHTML = 'Please Upload Cheque';
				document.getElementById('chequePayment').disabled = false;
				return false;
			} else {
				document.getElementById('chequePayment').disabled = true;
			}
			return true;
		}

		function validateCashInput() {
			var cashAmt = document.getElementById('optionsRadios36').value;
			var paymentSource = document.getElementById('paymentSourceCash').value;
			document.getElementById('cashInputError').innerHTML = ' ';
			document.getElementById('paymentSourceCashError').innerHTML = '';
			var isNumbr = /^[0-9.]+$/.test(cashAmt);
			if (!isNumbr && $("#optionsRadios33").checked) {
				document.getElementById('cashInputError').innerHTML = 'Invalid Amount';
				document.getElementById('cashPayment').disabled = false;
				return false;
			} else {
				document.getElementById('cashPayment').disabled = true;
			}
			if (paymentSource == '' || paymentSource == null) {
				document.getElementById('paymentSourceCashError').innerHTML = 'Please Select Payment Source';
				document.getElementById('cashPayment').disabled = false;
				return false;
			} else {
				document.getElementById('cashPayment').disabled = true;
			}
		}

		function validateCardInput() {
			var cardAmt = document.getElementById('optionsRadios28').value;
			document.getElementById('cardInputError').innerHTML = ' ';
			var isNumbr = /^[0-9.]+$/.test(cardAmt);
			if (!isNumbr && $("#optionsRadios25").checked) {
				document.getElementById('cardInputError').innerHTML = 'Invalid Amount';
				document.getElementById('cardPayment').disabled = false;
				return false;
			} else {
				document.getElementById('cardPayment').disabled = true;
			}
		}
		function validateDmvInput() {
			var dmvAmount = document.getElementById('dmvAmount').value;
			var paymentDate = document.getElementById('paymentDateDmv').value;
			document.getElementById('dmvInputError').innerHTML = ' ';
			document.getElementById('paymentDateInputErrorDmv').innerHTML = ' ';
			var isNumbr = /^[0-9.]+$/.test(dmvAmount);
			if (!isNumbr) {
				document.getElementById('dmvInputError').innerHTML = 'Invalid Amount';
				document.getElementById('dmvPayment').disabled = false;
				return false;
			} else {
				document.getElementById('dmvPayment').disabled = true;
			}
			if (paymentDate == '' || paymentDate == null) {
				document.getElementById('paymentDateInputErrorDmv').innerHTML = 'Please Select Paid Date';
				document.getElementById('dmvPayment').disabled = false;
				return false;
			} else {
				document.getElementById('dmvPayment').disabled = true;
			}
		}
	</script>
<script src="/csum\/static/assets/global/plugins/jquery.min.js"
	type="text/javascript"></script>

<script src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">
		//Create a Stripe client
		var stripe = Stripe('${stripePublishableKey}');

		// Create an instance of Elements
		var elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base : {
				color : '#32325d',
				lineHeight : '24px',
				fontFamily : '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing : 'antialiased',
				fontSize : '16px',
				'::placeholder' : {
					color : '#aab7c4'
				}
			},
			invalid : {
				color : '#fa755a',
				iconColor : '#fa755a'
			}
		};

		//Create an instance of the card Element
		var card = elements.create('card', {
			style : style
		});

		// Add an instance of the card Element into the `card-element` <div>
		card.mount('#card-element');

		// Handle real-time validation errors from the card Element.
		card.addEventListener('change', function(event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
				document.getElementById('cardPayment').disabled = false;
			} else {
				displayError.textContent = '';
			}
		});

		// Handle form submission
		var form = document.getElementById('paymentForm');
		form
				.addEventListener(
						'submit',
						function(event) {
							event.preventDefault();
							$(this).find(':input[type=submit]').prop(
									'disabled', true);
							stripe
									.createToken(card)
									.then(
											function(result) {
												var cardAmt = document
														.getElementById('optionsRadios28').value;
												document
														.getElementById('cardInputError').innerHTML = ' ';
												document
														.getElementById('sourceCreditError1').innerHTML = '';
												var paymentSource = document
														.getElementById('sourceCredit1').value;
												if (paymentSource == ''
														|| paymentSource == null) {
													document
															.getElementById('sourceCreditError1').innerHTML = 'Please Select Payment Source';
													document
															.getElementById('cardPayment').disabled = false;
													return false;
												} else if (result.error) {
													// Inform the user if there was an error
													var errorElement = document
															.getElementById('card-errors');
													errorElement.textContent = result.error.message;
													document
															.getElementById('cardPayment').disabled = false;
												} else {
													// Send the token to your server
													stripeTokenHandler(result.token);
													document
															.getElementById('cardPayment').disabled = true;
												}
											});
						});

		function stripeTokenHandler(token) {
			// Insert the token ID into the form so it gets submitted to the server
			var form = document.getElementById('paymentForm');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'stripeToken');
			hiddenInput.setAttribute('value', token.id);
			form.appendChild(hiddenInput);
			// Submit the form
			form.submit();

			App.blockUI({
				target : '#blockui_adminpayment_data'
			});
		}
	</script>

<script src="<c:url value='/static/assets/global/fieldValidation.js' />"
	type="text/javascript"></script>

<script>
		function clearErrorMsg() {
			document.getElementById('clearError').innerHTML = '';
			document.getElementById('paymentAmountError').innerHTML = '';
			if (!checkFloatingPointNum('totDue')) {
				document.getElementById('paymentAmountError').innerHTML = 'Invalid Amount';
				return false;
			}
			var payTotDue = "${totalDue}";
			var givenAmt = document.getElementById('totDue').value;
			if (parseFloat(givenAmt) > parseFloat(payTotDue)) {
				document.getElementById('paymentAmountError').innerHTML = 'Amount should be less than or equal to total due';
				return false;
			}
		}
		function alphaNumericValidation(e) {
			var regex = new RegExp("^[a-zA-Z0-9]+$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			e.preventDefault();
			return false;
		}
	</script>


<style>
.StripeElement {
	background-color: white;
	padding: 8px 12px;
	border-radius: 4px;
	border: 1px solid transparent;
	box-shadow: 0 1px 3px 0 #e6ebf1;
	-webkit-transition: box-shadow 150ms ease;
	transition: box-shadow 150ms ease;
}

.StripeElement--focus {
	box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
	border-color: #fa755a;
}

.StripeElement--webkit-autofill {
	background-color: #fefde5 !important;
}
</style>