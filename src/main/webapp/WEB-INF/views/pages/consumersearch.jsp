<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<!-- END PAGE BAR -->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet">
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet-body form">
									<!-- BEGIN  SEARCH FORM-->
									<form:form 
										action="${pageContext.request.contextPath}/consumerSearch"
										modelAttribute="violationSearchForm" method="post"
										autocomplete="on">
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-4"></div>
													<div class="col-md-4">
														<div class="col-md-12">
															<div class="portlet box blue-dark bg-inverse">
																<div class="portlet-title"
																	style="padding: 3px 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
																	<div class="col-md-12" align="center"
																		style="width: -10px; font-size: 14px; padding-top: 2px; padding-bottom: 1px;">
																		<b>CSUM PAYMENT SYSTEM</b>
																	</div>
																	
																</div>
																	<div class="portlet-body " style="padding: 6px;">
																	<br/>
																	<div class="row" align="center">
																		<div class="col-md-12">
																			<div class="form-group">
																				<form:input path="ticketNumber" type="text"
																					placeholder="Ticket Number" />
																			</div>
																		</div>
																		<div class="col-md-12">
																			<div class="form-group">
																				<form:input path="lastName" type="text"
																					placeholder="Last Name" />
																			</div>
																		</div>
																		<div class="col-md-12">
																			<div class="btn-group">
																				<button id="search" type="submit"
																					class="btn blue-dark btn-sm">SEARCH</button>
																			</div>
																		</div>
																		</div>
																		<br>
																	</div>
															</div>
															<%--<div class="col-md-4">
																<div class="form-group">
																	<form:input path="ticketNumber" type="text"
																		placeholder="Search..." />
																</div>
															</div>
															<div class="col-md-1"></div>
															<div class="col-md-6" align="center">
																<div class="form-group">
																	<label
																		class="control-label label-sm font-blue col-md-4">Search&nbsp;Type</label>
																	<div class="col-md-5">
																		<form:select id="searchType" name="searchType"
																			path="searchType">
																			<option value="VN">Violation Number</option>
																			<option value="LN">License Number</option>
																		</form:select>
																	</div>
																</div>
															</div>
															<div class="col-md-1">
																<div class="btn-group">
																	<button id="search" type="submit"
																		class="btn blue-dark btn-sm">SEARCH</button>
																</div>
															</div>--%>
														</div>
													</div>
													<div class="col-md-4"></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-2"></div>
													<div class="col-md-4" align="center">
														<span class="alert alert-danger" style="display: none;"
															id="searchTextError"></span>
													</div>
													<div class="col-md-6"></div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<c:if
									test="${not empty violationSearchForm && not empty violationSearchForm.searchType}">
									<div class="portlet light portlet-fit bordered">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-subject font-blue sbold uppercase">Violation
													List</span>
											</div>
										</div>
										<div class="portlet-body">
											<table class="table  table-hover table-bordered table-sm">
												<thead>
													<tr>
														<th>Violation Number</th>
														<th>Status / Issued On</th>
														<th>Name</th>
														<th>Address</th>
														<th>Violation Code & Description</th>
														<th>Violation&nbsp;Location</th>
														<th>Fine&nbsp;Amt</th>
														<th>Penalties</th>
														<th>Total&nbsp;Due</th>

													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${not empty violationList}">
															<c:forEach items="${violationList}" var="violation">
																<tr style="text-transform: uppercase;">
																	<td><b style="color: #004b85;"><%-- <a style="color: #004b85"
																			href="${pageContext.request.contextPath}/violationView/${violation.violationId}"><c:out
																					value="${violation.violationId}" /></a> --%>${violation.violationId}</b></td>
																	<td><c:out value="${violation.status}" />/<c:out
																			value="${violation.issueDate}" /></td>
																	<td><c:out value="${violation.patron.firstName}" />
																		<c:out value="${violation.patron.lastName}" /></td>
																	<td><c:out
																			value="${violation.patron.address.formattedAddress}" /></td>
																	<td><c:out value="${violation.violationCode.code}" />/<c:out
																			value="${violation.violationCode.description}" /></td>
																	<td><c:out
																			value="${violation.locationOfViolation}" /></td>
																	<td>$<c:out value="${violation.fineAmount}" /></td>
																	<td>$<c:out value="0.00" /></td>
																	<td>$<c:out value="${violation.totalDue}" /></td>
																	<c:if test="${violation.totalDue>0.00}">
																		<td><b><a style="color: #004b85"
																				href="<c:url value='/consumerPayment/${violation.violationId}' />">Pay</a></b>
																		</td>
																	</c:if>
																</tr>
															</c:forEach>
														</c:when>
														<c:otherwise>
															<tr style="text-transform: uppercase;">
																<td colspan="10" align="center"><b>No results
																		found</b></td>
															</tr>
														</c:otherwise>
													</c:choose>
												</tbody>

											</table>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
	function validateSearchForm() {
		var searchText = $("#ticketNumber").val();
		if (searchText.length == 0) {
			$("#searchTextError").attr('style', "display:block");
			$("#searchTextError").text(
					"Please enter Violation Number or License Number");
			return false;
		}
		return true;
	}
</script>