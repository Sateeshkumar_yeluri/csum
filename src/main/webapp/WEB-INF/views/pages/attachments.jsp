<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- BEGIN CONTAINER -->
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" id="blockui_fileUpload_data">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
			<li><a href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.attachments" /></span></li>
				<%-- <li><a style="margin-left: 20px;"
					href="${pageContext.request.contextPath}/violationView/${violationId}">Home</a> <i
					class="fa fa-circle"></i></li>
				<li><span>Hearing Scheduler</span></li> --%>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<div>
			<div class="portlet light portlet-fit bordered">
				<div style="text-align: center;">
					<span style="font-size: 16px;">
								<b>License Number:</b>
								<span style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
							&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px;"><b>Violation
								Number:</b></span><span style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
				</div>
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-blue sbold">File Upload</span>

					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<form:form id="fileupload"
								action="${pageContext.request.contextPath}/fileupload/${violationId}"
								modelAttribute="uploadForm" method="POST" autocomplete="on"
								enctype="multipart/form-data">
								<div class="form-actions">
									<div class="row" style="margin-left: 0px;">
										<div class="form-group">
											<label class="control-label label-sm col-md-2">Type
												of File Upload:</label>
											<div class="col-md-3">
												<form:select path="fileType" id="fileType" style="text-transform: uppercase;">
													<form:option value="Attachments">Attachments</form:option>
													<form:option value="Checks">Checks</form:option>
													<form:option value="Correspondence">Correspondence Files</form:option>

												</form:select>
											</div>
										</div>
									</div>
									<span class="help-block">&nbsp;</span>
									<div class="row" style="margin-left: 17px;">
										<span class="btn blue-dark btn-sm fileinput-button"> <span>
												BROWSE </span> <input type="file" name="files" multiple="multiple">
										</span> <span> <input type="submit"
											class="btn blue-dark btn-sm fileinput-button"
											id="blockui_uploadbtn" value=" UPLOAD ">
										</span>
									</div>
								</div>
								<div class="row fileupload-buttonbar"></div>
							</form:form>
						</div>
					</div>
					<div class="row" id="viewLoadFiles" style="display: none">
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<table
											class="table table-hover table-bordered  table-sortable"
											id="upload_files" style="width:">
											<thead style="background-color: #004b85; color: #fff">
												<tr>
													<th>File Name</th>
													<th class="hidden-xs">File Type</th>
													<th class="hidden-xs">File size</th>
												</tr>
											</thead>
											<tbody id="selectedFilesRow">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<span id="fileTypeError" style="color: #E82734"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<span id="fileTypeError" style="color: #E82734">${printError}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<span style="color: #3597DB">${result}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="portlet-title" style="padding-top: 0px;">
						<div class="caption">
							<i class="fa font-blue"></i> <span
								class="caption-subject font-blue sbold">Uploaded
								Attachments</span>
						</div>
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<table
											class="table table-bordered table-hover  table-sortable"
											id="img_logic"
											style="margin-bottom: 0px; margin-left: -25px; margin-top: -10px;">
											<thead style="background-color: #004b85; color: #fff">
												<tr>
													<th>File Name</th>
													<th>File Type</th>
													<th class="hidden-xs">Uploaded Date</th>
													<th class="hidden-xs">Download</th>
													<th class="hidden-xs">Delete</th>
													<th class="hidden-xs">Print</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
													<c:when test="${empty images}">
														<tr>
															<td colspan="6" align="center"
																style="text-transform: uppercase;"><b>No
																	Documents Uploaded</b></td>
														</tr>
													</c:when>
													<c:otherwise>
														<c:forEach items="${images}" var="images">
															<c:if
																test="${(!fn:containsIgnoreCase(images.type,'Hearing') )|| (fn:containsIgnoreCase(images.type,'Hearing') && showHearing)}">
																<tr>
																	<td id="partFileInfoColumn"
																		style="width: 450px; font-size: 13px;"><span
																		id="fileNameInfo_${images.id}"> <c:choose>
																				<c:when
																					test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																					<span
																						style="width: 50px; text-transform: uppercase;"><c:out
																							value="${images.imageName}" /></span>
																				</c:when>
																				<c:otherwise>
																					<a onclick="enablePageLoadBar()"
																						style="color: #004b85;"
																						href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"><span
																						style="width: 50px; text-transform: uppercase;"><c:out
																								value="${images.imageName}" /></span></a>
																				</c:otherwise>
																			</c:choose>
																			<button class="btn blue-dark btn-sm" data-id=""
																				id="btnRename_${images.id}"
																				style="float: right; font-size: 10px;">RENAME</button>
																	</span> <span id="fileNameUpdateSpan_${images.id}"
																		style="display: none; text-transform: uppercase;">
																			<input type="text" class="form-control input-sm"
																			name="fileName" value="${images.imageName}"
																			id="fileNameField_${images.id}" style="width: 80%;" />
																			<button class="btn blue-dark btn-sm"
																				id="fileNameUpdateBtn_${images.id}" data-id=""
																				style="float: right; margin-top: -30px; text-transform: uppercase;">UPDATE</button>
																	</span> <script type="text/javascript">
																		$(
																				"#btnRename_<c:out value='${images.id}'/>")
																				.click(
																						function() {
																							$(
																									"#fileNameUpdateSpan_<c:out value='${images.id}'/>")
																									.show();
																							$(
																									"#fileNameInfo_<c:out value='${images.id}'/>")
																									.hide();
																						});

																		$(
																				"#fileNameUpdateBtn_<c:out value='${images.id}'/>")
																				.click(
																						function() {
																							var updatedNm = $(
																									"#fileNameField_<c:out value='${images.id}'/>")
																									.val();
																							var urlAddress = "<c:out value='${pageContext.request.contextPath}'/>/updateAttachmentFileName/"
																									+ updatedNm
																									+ "/<c:out value='${images.id}'/>/"
																									+ "<c:out value='${violationId}'/>";
																							window.location.href = urlAddress;
																						});
																	</script></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${images.imageType}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${images.uploadedDate}" /></td>
																	<td><a class="btn blue-dark btn-sm"
																		href="<c:url value='/downloadImage/${images.id}' />">
																			<c:out value="DOWNLOAD" />
																	</a></td>
																	<td><a onclick="enablePageLoadBar()"
																		style="color: #FF0000"
																		href="<c:url value='/deleteImage/${images.id}/${violationId}' />">
																			<c:out value="Delete" />
																	</a></td>

																	<td><c:choose>
																			<c:when
																				test="${fn:toUpperCase(images.imageType) == 'PDF' || fn:toUpperCase(images.imageType) == '4X6M'}">
																				<a class="btn blue-dark btn-sm icon-printer"
																					target="_blank"
																					href="<c:url value='/printPDFPages/${images.id}/${violationId}' />">&nbsp;&nbsp;PRINT</a>
																			</c:when>
																			<c:when
																				test="${fn:toUpperCase(images.imageType)== 'DOC' || fn:toUpperCase(images.imageType) == 'DOCX'}">
																				<a class="btn blue-dark btn-sm icon-printer"
																					target="_blank"
																					href="<c:url value='/printDocPages/${images.id}/${violationId}' />">&nbsp;&nbsp;PRINT</a>
																			</c:when>
																			<c:otherwise>
																				<a class="btn blue-dark btn-sm icon-printer" href="#"
																					onclick="printImageTypeFile('${pageContext.request.contextPath}/previewImage/${images.id}')">&nbsp;&nbsp;PRINT</a>
																			</c:otherwise>
																		</c:choose> <%-- <c:if
																		test="${images.imageType != 'PDF' && images.imageType != '4X6M'}">
																		
																	</c:if> --%></td>
																</tr>
															</c:if>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="portlet-title" style="padding-top: 0px;">
						<div class="caption">
							<i class="fa font-blue"></i> <span
								class="caption-subject font-blue sbold">Uploaded Checks</span>
						</div>
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<table
											class="table table-bordered table-hover  table-sortable"
											style="margin-bottom: 0px; margin-left: -25px; margin-top: -10px;">
											<thead style="background-color: #004b85; color: #fff">
												<tr>
													<th>File Name</th>
													<th>File Type</th>
													<th class="hidden-xs">Uploaded Date</th>
													<th class="hidden-xs">Download</th>
													<th class="hidden-xs">Delete</th>
													<th class="hidden-xs">Print</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
													<c:when test="${not empty checks}">
														<c:if test="${not empty checks}">
															<c:forEach items="${checks}" var="images">
																<tr>
																	<td id="partFileInfoColumn"
																		style="width: 450px; font-size: 13px; text-transform: uppercase;"><span
																		id="fileNameInfo_${images.id}"> <c:choose>
																				<c:when
																					test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																					<span><c:out value="${images.imageName}" /></span>
																				</c:when>
																				<c:otherwise>
																					<a onclick="enablePageLoadBar()"
																						style="color: #004b85; text-transform: uppercase;"
																						href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"><span
																						style="width: 50px;"><c:out
																								value="${images.imageName}" /></span></a>
																				</c:otherwise>
																			</c:choose>
																	</span> <span id="fileNameUpdateSpan_${images.id}"
																		style="display: none;"> <input type="text"
																			class="form-control input-sm" name="fileName"
																			value="${images.imageName}"
																			id="fileNameField_${images.id}" style="width: 80%;" />
																			<button class="btn blue-dark btn-sm"
																				id="fileNameUpdateBtn_${images.id}" data-id=""
																				style="float: right; margin-top: -30px;">UPDATE</button>
																	</span> <script type="text/javascript">
																		$(
																				"#btnRename_<c:out value='${images.id}'/>")
																				.click(
																						function() {
																							$(
																									"#fileNameUpdateSpan_<c:out value='${images.id}'/>")
																									.show();
																							$(
																									"#fileNameInfo_<c:out value='${images.id}'/>")
																									.hide();
																						});

																		$(
																				"#fileNameUpdateBtn_<c:out value='${images.id}'/>")
																				.click(
																						function() {
																							var updatedNm = $(
																									"#fileNameField_<c:out value='${images.id}'/>")
																									.val();
																							var urlAddress = "<c:out value='${pageContext.request.contextPath}'/>/updateAttachmentFileName/"
																									+ updatedNm
																									+ "/<c:out value='${images.id}'/>/"
																									+ "<c:out value='${violationId}'/>";
																							window.location.href = urlAddress;
																						});
																	</script></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${images.imageType}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${images.uploadedDate}" /></td>
																	<td><a class="btn blue-dark btn-sm"
																		href="<c:url value='/downloadImage/${images.id}' />">
																			<c:out value="DOWNLOAD" />
																	</a></td>
																	<td><a onclick="enablePageLoadBar()"
																		style="color: #FF0000"
																		href="<c:url value='/deleteImage/${images.id}/${violationId}' />">
																			<c:out value="Delete" />
																	</a></td>

																	<td><c:if
																			test="${fn:toUpperCase(images.imageType) == 'PDF' || fn:toUpperCase(images.imageType) == '4X6M'}">
																			<a class="btn blue-dark btn-sm icon-printer"
																				target="_blank"
																				href="<c:url value='/printPDFPages/${images.id}/${violationId}' />">&nbsp;&nbsp;PRINT</a>
																		</c:if> <c:if
																			test="${fn:toUpperCase(images.imageType) != 'PDF' && fn:toUpperCase(images.imageType) != '4X6M'}">
																			<a class="btn blue-dark btn-sm icon-printer" href="#"
																				onclick="printImageTypeFile('${pageContext.request.contextPath}/previewImage/${images.id}')">&nbsp;&nbsp;PRINT</a>
																		</c:if></td>
																</tr>
															</c:forEach>
														</c:if>
													</c:when>
													<c:otherwise>
														<tr>
															<td colspan="6" align="center"><b>NO CHECKS
																	UPLOADED</b></td>
														</tr>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="portlet-title" style="padding-top: 0px;">
						<div class="caption">
							<i class="fa font-blue"></i> <span
								class="caption-subject font-blue sbold">Uploaded
								Correspondence Files</span>
						</div>
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<table
											class="table table-bordered table-hover  table-sortable"
											style="margin-bottom: 0px; margin-left: -25px; margin-top: -10px;">
											<thead
												style="background-color: #004b85; color: #fff; padding-top: 0px;">
												<tr>
													<th>File Name</th>
													<th>File Type</th>
													<th class="hidden-xs">Uploaded Date</th>
													<th class="hidden-xs">Download</th>
													<th class="hidden-xs">Delete</th>
													<th class="hidden-xs">Print</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>												
													<c:when test="${not empty corresp}">
														<c:forEach items="${corresp}" var="images">
															<tr>
																<td id="partFileInfoColumn"
																	style="width: 450px; font-size: 13px;"><span
																	id="fileNameInfo_${images.id}"> <c:choose>
																			<c:when
																				test="${fn:containsIgnoreCase(images.imageType,'DOC')}">
																				<span style="text-transform: uppercase;"><c:out
																						value="${images.imageName}" /></span>
																			</c:when>
																			<c:otherwise>
																				<a onclick="enablePageLoadBar()"
																					style="color: #004b85;"
																					href="<c:url value='/viewAttachmentAsImages/${images.id}/${violationId}' />"><span
																					style="width: 50px; text-transform: uppercase;"><c:out
																							value="${images.imageName}" /></span></a>
																			</c:otherwise>
																		</c:choose>
																		<button class="btn blue-dark btn-sm" data-id=""
																			id="btnRename_${images.id}"
																			style="float: right; font-size: 10px;">RENAME</button>
																</span> <span id="fileNameUpdateSpan_${images.id}"
																	style="display: none; text-transform: uppercase;">
																		<input type="text" class="form-control input-sm"
																		name="fileName" value="${images.imageName}"
																		id="fileNameField_${images.id}" style="width: 80%;" />
																		<button class="btn blue-dark btn-sm"
																			id="fileNameUpdateBtn_${images.id}" data-id=""
																			style="float: right; margin-top: -30px;">UPDATE</button>
																</span> <script type="text/javascript">
																	$(
																			"#btnRename_<c:out value='${images.id}'/>")
																			.click(
																					function() {
																						$(
																								"#fileNameUpdateSpan_<c:out value='${images.id}'/>")
																								.show();
																						$(
																								"#fileNameInfo_<c:out value='${images.id}'/>")
																								.hide();
																					});

																	$(
																			"#fileNameUpdateBtn_<c:out value='${images.id}'/>")
																			.click(
																					function() {
																						var updatedNm = $(
																								"#fileNameField_<c:out value='${images.id}'/>")
																								.val();
																						var urlAddress = "<c:out value='${pageContext.request.contextPath}'/>/updateAttachmentFileName/"
																								+ updatedNm
																								+ "/<c:out value='${images.id}'/>/"
																								+ "<c:out value='${violationId}'/>";
																						window.location.href = urlAddress;
																					});
																</script></td>
																<td style="text-transform: uppercase;"><c:out
																		value="${images.imageType}" /></td>
																<td style="text-transform: uppercase;"><c:out
																		value="${images.uploadedDate}" /></td>
																<td><a class="btn blue-dark btn-sm"
																	href="<c:url value='/downloadImage/${images.id}' />">
																		<c:out value="DOWNLOAD" />
																</a></td>
																<td><a onclick="enablePageLoadBar()"
																	style="color: #FF0000"
																	href="<c:url value='/deleteImage/${images.id}/${violationId}' />">
																		<c:out value="Delete" />
																</a></td>

																<td><c:if
																		test="${fn:toUpperCase(images.imageType) == 'PDF' || fn:toUpperCase(images.imageType) == '4X6M'}">
																		<a class="btn blue-dark btn-sm icon-printer"
																			target="_blank"
																			href="<c:url value='/printPDFPages/${images.id}/${violationId}' />">&nbsp;&nbsp;PRINT</a>
																	</c:if> <c:if
																		test="${fn:toUpperCase(images.imageType) != 'PDF' && fn:toUpperCase(images.imageType) != '4X6M'}">
																		<a class="btn blue-dark btn-sm icon-printer" href="#"
																			onclick="printImageTypeFile('${pageContext.request.contextPath}/previewImage/${images.id}')">&nbsp;&nbsp;PRINT</a>
																	</c:if></td>
															</tr>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<tr>
															<td colspan="6" align="center"><b>NO
																	CORRESPONDENCE FILES UPLOADED</b></td>
														</tr>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<span id="fileTypeError"
											style="color: #E82734; text-transform: uppercase;">${printError}</span>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>



			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//Print Images
	function printImageTypeFile(url) {
		currHost = window.location.host;
		protocolUsed = window.location.protocol;
		fullUrl = protocolUsed + '//' + currHost + url;
		var preContent = "<img id='printReadyImage' src='"+fullUrl+"'/>";
		var currBodyCon = document.body.innerHTML;
		document.body.innerHTML = preContent;
		$('#printReadyImage')
				.on(
						'load',
						function() {
							setTimeout(
									function() {
										window.print();
										document.body.innerHTML = currBodyCon;
										window.location.href = "<c:url value='/attachments/${violationId}' />";
									}, 500);
						});
	}
</script>


<style type='text/css'>
@
-webkit-keyframes uil-default-anim { 0% {
	opacity: 1
}

100%
{
opacity




:


 


0
}
}
@
keyframes uil-default-anim { 0% {
	opacity: 1
}

100%
{
opacity




:


 


0
}
}
.uil-default-css>div:nth-of-type(1) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.5s;
	animation-delay: -0.5s;
}

.uil-default-css {
	position: relative;
	top: 175px;
	left: 600px;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(2) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.4166666666666667s;
	animation-delay: -0.4166666666666667s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(3) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.33333333333333337s;
	animation-delay: -0.33333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(4) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.25s;
	animation-delay: -0.25s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(5) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.16666666666666669s;
	animation-delay: -0.16666666666666669s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(6) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.08333333333333331s;
	animation-delay: -0.08333333333333331s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(7) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0s;
	animation-delay: 0s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(8) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.08333333333333337s;
	animation-delay: 0.08333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(9) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.16666666666666663s;
	animation-delay: 0.16666666666666663s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(10) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.25s;
	animation-delay: 0.25s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(11) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.33333333333333337s;
	animation-delay: 0.33333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(12) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.41666666666666663s;
	animation-delay: 0.41666666666666663s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}
</style>

<div style="display: none" id="uil-default-css-id"
	class='uil-default-css' style='transform:scale(1);'>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(0deg) translate(0, -60px); transform: rotate(0deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(30deg) translate(0, -60px); transform: rotate(30deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(60deg) translate(0, -60px); transform: rotate(60deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(90deg) translate(0, -60px); transform: rotate(90deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(120deg) translate(0, -60px); transform: rotate(120deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(150deg) translate(0, -60px); transform: rotate(150deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(180deg) translate(0, -60px); transform: rotate(180deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(210deg) translate(0, -60px); transform: rotate(210deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(240deg) translate(0, -60px); transform: rotate(240deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(270deg) translate(0, -60px); transform: rotate(270deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(300deg) translate(0, -60px); transform: rotate(300deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(330deg) translate(0, -60px); transform: rotate(330deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
</div>

<script>
	document.onreadystatechange = function() {
		var state = document.readyState;
		if (state == 'interactive') {
			document.getElementById('uil-default-css-id').style.display = "block";
		}
		if (state == 'complete') {
			document.getElementById('uil-default-css-id').style.display = "none";
		}
	}
</script>

<script>
	function enablePageLoadBar() {
		document.getElementById('uil-default-css-id').style.display = "block";
	}
</script>

<script>
	$(document)
			.ready(
					function() {

						$('input[type="file"]')
								.change(
										function(e) {
											$("#viewLoadFiles").show();
											document
													.getElementById('fileTypeError').innerHTML = '';
											document
													.getElementById('blockui_uploadbtn').disabled = false;
											var files = e.target.files;
											$("#selectedFilesRow").empty();
											for (var i = 0; i < files.length; i++) {
												var newRowContent = "<tr><td style='text-transform: uppercase;'>"
														+ files[i].name
														+ "</td><td style='text-transform: uppercase;'>"
														+ files[i].name
																.substr(
																		files[i].name
																				.lastIndexOf('.') + 1)
																.toUpperCase()
														+ "</td><td style='text-transform: uppercase;'>"
														+ Math
																.round(files[i].size / 1024)
														+ " Kb" + "</td></tr>";
												$(newRowContent).appendTo(
														$("#upload_files"));
												var fileTypeUploaded = files[i].name
														.substr(
																files[i].name
																		.lastIndexOf('.') + 1)
														.toUpperCase();
												var validFileTypes = [ 'JPEG',
														'PDF', 'JPG', 'TIFF',
														'TIF', '4X6M', 'PRC','DOC','DOCX' ];
												if (validFileTypes
														.indexOf(fileTypeUploaded) == -1) {
													document
															.getElementById('blockui_uploadbtn').disabled = true;
													document
															.getElementById('fileTypeError').innerHTML = 'Invalid File Uploaded. Allowed PDF,JPG,TIFF,DOC type of files';
												}
											}
										});
					});
</script>


<script type="text/javascript">
	$('#blockui_uploadbtn').click(function() {
		App.blockUI({
			target : '#blockui_fileUpload_data'
		});
	});
</script>
