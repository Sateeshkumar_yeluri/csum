<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<div class="page-content">
		<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.ipp.details" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit bordered">
					<div class="row">
						<div class="col-md-12" align="center">
							<span style="font-size: 16px;"> <b>License Number:</b><span
								style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
								&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
										Number:</b></span>&nbsp;${ippForm.violation.violationId}
							</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-offset-2 col-md-8">
								<div class="tabbable-line boxless tabbable-reversed"
									style="margin: 45px;">
									<div id="citationPortlet" class="portlet box blue-dark ">
										<div class="portlet-title">
											<div class="caption">Plan Details</div>
										</div>
										<div class="portlet-body">
											<div class="row">
												<form:form class="form-horizontal" action="#" method="POST"
													modelAttribute="ippForm">
													<div class="form-actions">
														<div class="table-responsive">
															<div class="col-md-12 col-sm-12">
																<div class="col-md-1 col-sm-1"></div>
																<div class="col-md-4 col-sm-4"
																	style="padding-left: 0px;">
																	<table style="font-size: 14px;">
																		<tr>
																			<th><b>Plan&nbsp;Status:&nbsp;</b></th>
																			<td><font style="font-weight: 400px;"><c:out
																						value="${ippForm.status}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Original Due:&nbsp;</b></th>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${ippForm.originalDue}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Paid&nbsp;To&nbsp;Date:&nbsp;</b></td>
																			<td>$<font><c:out value="${paidToDate}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Previous&nbsp;Credits:&nbsp;</b></td>
																			<td>$<font><c:out
																						value="${ippForm.previousCredits}" /></font></td>
																		</tr>
																		<tr>
																			<th><b>Enroll Amount:&nbsp;</b></th>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${ippForm.enrollAmount}" /></font></td>
																		</tr>
																		<%-- <tr>
															<th><b>Un-enroll Amount:&nbsp;</b></th>
															<td>$<font style="font-weight: 400px;"><c:out
																		value="${ippForm.unenrollAmount}" /></font></td>
														</tr> --%>
																	</table>
																	<br>
																</div>
																<div class="col-md-6 col-sm-6">
																	<table style="font-size: 14px;">

																		<tr style="font-weight: 400px;">
																			<td><b>Installment&nbsp;Payment&nbsp;1:&nbsp;<br>(DownPayment)&nbsp;
																			</b></td>
																			<td style="padding-bottom: 18.5px;">$<font
																				style="font-weight: 400px;"><c:out
																						value="${ippForm.downPayment}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate1}" />)</font></td>
																		</tr>
																		<%-- <c:forEach var="amount" items="${amountList}" varStatus="loop">
																		<tr style="font-weight: 400px;">
																			<td>Installment&nbsp;Payment&nbsp;${loop.index+2}:&nbsp;</td>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${amount}" /></font></td>
																		</tr>
																	</c:forEach> --%>
																		<tr style="font-weight: 400px;">
																			<td><b>Installment&nbsp;Payment&nbsp;2:&nbsp;</b></td>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${ippForm.installmentAmount}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate2}" />)</font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Installment&nbsp;Payment&nbsp;3:&nbsp;</b></td>
																			<td>$<font style="font-weight: 400px;"><c:out
																						value="${ippForm.installmentAmount}" />&nbsp;(<c:out
																						value="${ippForm.installmentDate3}" />)</font></td>
																		</tr>
																		<c:if test="${ippForm.noOfPayments > 3}">
																			<c:forEach begin="${4}" end="${ippForm.noOfPayments}"
																				varStatus="loop">
																				<tr style="font-weight: 400px;">
																					<td><b>Installment&nbsp;Payment&nbsp;${loop.index}:&nbsp;</b></td>
																					<td>$<font style="font-weight: 400px;"><c:out
																								value="${ippForm.installmentAmount}" /></font></td>
																				</tr>
																			</c:forEach>
																		</c:if>
																		<tr>
																			<th><b>Total&nbsp;Number&nbsp;of&nbsp;Installments:&nbsp;</b></th>
																			<td><font style="font-weight: 400px;"><c:out
																						value="${ippForm.noOfPayments}" /></font></td>
																		</tr>
																		<tr style="font-weight: 400px;">
																			<td><b>Starting&nbsp;On:&nbsp;</b></td>
																			<td><font style="font-weight: 400px;"> <c:out
																						value="${ippForm.startDate}" /></font></td>
																		</tr>
																	</table>
																	<form:input type="hidden" path="downPayment" />
																	<form:input type="hidden" path="installmentAmount" />
																	<form:input type="hidden" path="noOfPayments" />
																	<br>
																</div>
																<div class="col-md-1 col-sm-1"></div>
															</div>
														</div>
														<span class="help-block">&nbsp;</span>
														<div class="row" style="margin: 0px;" align="center">
															<input class="btn blue-dark btn-sm" style="margin: 7px;"
																type="submit"
																formaction="${pageContext.request.contextPath}/createIpp/${ippForm.violation.violationId}"
																value="CREATE PLAN" /> <input
																class="btn blue-dark btn-sm" style="margin: 7px;"
																type="submit" formmethod="get" value="RECALCULATE"
																formaction="${pageContext.request.contextPath}/recalculate/${ippForm.violation.violationId}" />
														</div>
													</div>
												</form:form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<%-- <h3 class="form-section"
								style="margin-top: -1px; border-bottom-color: #32c5d2"></h3>
							<div class="row" style="margin: 30px;">
								<table class="table  table-hover table-bordered table-sm"
									id="sample_editable_1">
									<thead>
										<tr>
											<th>Plate/License</th>
											<th>Entity Name</th>
											<th>Entity Address</th>
											<th>Amt Enrolled</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.otherId}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.firstName}" /> &nbsp; <c:out
														value="${ippForm.violation.patron.lastName}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.violation.patron.address.formattedAddress}" /></font></td>
											<td><font style="font-weight: 400px;"><c:out
														value="${ippForm.enrollAmount}" /></font></td>
										</tr>
									</tbody>
								</table>

							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
