<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT BODY -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -25px 0px 0;">
			<div class="row">
				<div class="col-md-8">
					<ul class="page-breadcrumb">
						<li><a
							href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
									code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
						<li><span><spring:message code="lbl.page.bar.history" /></span></li>
					</ul>
				</div>
			
		</div>
				<form
					action='${pageContext.request.contextPath}/history/${violationId}/'
					id="historySearchForm" method='GET'>
					<div class="form-group" style="margin-left: -20px;">
						<label class="control-label label-sm col-md-4"></label>
						<div class="col-md-3">
							<select class="form-control inut-sm" id="historyItem"
								name="historyItem">
								<option value="All">VIEW ALL</option>
								<option value="Correspondence">CORRESPONDENCE</option>
								<option value="Hearing">HEARING</option>
								<option value="Ipp">Ipp</option>
								<option value="Notices">NOTICES</option>
								<option value="Payment">PAYMENTS</option>
								<option value="Penalty">PENALTY</option>
								<option value="PlateEntity">PLATE ENTITY</option>
								<option value="Suspend">SUSPENDS</option>
							</select>
						</div>
						<div class="col-md-2" style="margin-left: 10px;">
							<button type="button" class="btn blue-dark btn-sm" id="printBtn"
								style="position: static; float: none; top: -2px; left: -500px; right: 30px; bottom: 40px">
								<i class="icon-printer"></i> PRINT
							</button>
						</div>
					</div>
				</form>
			</div>
			<br />
			<!-- END PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div id="printarea" class="portlet light portlet-fit bordered">
								<div style="text-align: center;">
									<span style="font-size: 16px;"><span
										style="font-size: 17px"> <b>License Number:</b></span>&nbsp;<span
										style="text-transform: uppercase;">${licenseNumber}</span>
										&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
												Number:</b></span>&nbsp;${violationId} </span>
								</div>
								<div class="portlet-title">
									<div class="caption">Violation History</div>
								</div>
								<div class="portlet-body">
									<div class="table-responsive scroller">
										<c:choose>
											<c:when test="${historyItem == 'All'}">
												<c:choose>
													<c:when
														test="${not empty historyBean.penaltyHistory || not empty historyBean.paymentHistory || not empty historyBean.hearingHistory || not empty historyBean.suspendHistory || not empty historyBean.correspHistory || not empty historyBean.noticeHistory || not empty historyBean.plateHistory}">
														<div class="portlet-body">
															<div class="table-responsive scroller">
																<table id="allDetailsTbl"
																	class="table table-hover table-bordered ">
																	<thead>
																		<tr style="background-color: #f5f3eb;">
																			<th style="color: #004b85;" width="9%"><b>Type</b></th>
																			<th style="color: #004b85;" width="64%"><b>Data</b></th>
																			<th style="color: #004b85;" width="10%"><b>Updated&nbsp;By</b></th>
																			<th style="color: #004b85;" width="17%"><b>Updated&nbsp;Date</b></th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty historyBean.penaltyHistory }">
																			<c:forEach items="${historyBean.penaltyHistory}"
																				var="penalty">
																				<tr>
																					<td><c:out value="PENALTY" /></td>
																					<td style="text-transform: uppercase;">&#36;<c:out
																							value="${penalty.violation.fineAmount}" />, <c:choose>
																							<c:when
																								test="${not empty penalty.penaltyCode.penalty1}">
																								&#36;<c:out
																									value="${penalty.penaltyCode.penalty1}" />/
																								<c:out
																									value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:when>
																							<c:otherwise>
																							&#36;<c:out value="0.00" />/<c:out
																									value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:otherwise>
																						</c:choose> <c:choose>
																							<c:when
																								test="${not empty penalty.penaltyCode.penalty2}">
																								&#36;<c:out
																									value="${penalty.penaltyCode.penalty2}" />/
																								<c:out
																									value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:when>
																							<c:otherwise>
																								&#36;<c:out value="0.00" />/
																								<c:out
																									value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:otherwise>
																						</c:choose> <c:choose>
																							<c:when
																								test="${not empty penalty.penaltyCode.penalty3}">
																							&#36;<c:out
																									value="${penalty.penaltyCode.penalty3}" />/
																							<c:out
																									value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:when>
																							<c:otherwise>
																							&#36;<c:out value="0.00" />/
																							<c:out
																									value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:otherwise>
																						</c:choose>$<c:out value="${penalty.violation.totalDue}" />&nbsp;
																					</td>
																					<c:choose>
																						<c:when test="${penalty.updatedAt == null}">
																							<td style="text-transform: uppercase;"><c:out
																									value="${penalty.createdBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${penalty.formattedCreatedAt}" /></td>
																						</c:when>
																						<c:otherwise>
																							<td style="text-transform: uppercase;"><c:out
																									value="${penalty.updatedBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${penalty.formattedUpdatedAt}" /></td>
																						</c:otherwise>
																					</c:choose>
																				</tr>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.paymentHistory }">
																			<c:forEach items="${historyBean.paymentHistory}"
																				var="paymentList">
																				<c:forEach items="${paymentList}" var="payment">
																					<tr>
																						<td><c:out value="PAYMENTS" /></td>
																						<td style="text-transform: uppercase;"><c:if
																								test="${not empty payment.amount}">$<c:out
																									value="${payment.amount}" />
																							</c:if> <c:if test="${not empty payment.paymentDate}">/
																		<c:out value="${payment.paymentDate}" />,</c:if> <c:choose>
																								<c:when test="${payment.paymentSource == 'P'}">
																									<c:out value="PAY-IN-PERSON" />
																								</c:when>
																								<c:when test="${payment.paymentSource == 'M'}">
																									<c:out value="PAY-BY-MAIL" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="PAY-BY-WEB" />
																								</c:otherwise>
																							</c:choose> <c:if
																								test="${not empty payment.paymentMethod.description}">/<c:out
																									value="${payment.paymentMethod.description}" />
																							</c:if> <c:if test="${not empty payment.account}">,
																		<c:out value="${payment.account}" />
																							</c:if> <c:if test="${not empty payment.processedOn}">,
																		<c:out value="${payment.processedOn}" />
																							</c:if> <c:if test="${not empty payment.processedBy}">/
																		<c:out value="${payment.processedBy}" />
																							</c:if>,<c:if test="${not empty payment.overPaid}">$<c:out
																									value="${payment.overPaid}" />, </c:if> <c:if
																								test="${not empty payment.totalDue}">$<c:out
																									value="${payment.totalDue}" />
																							</c:if></td>
																						<c:choose>
																							<c:when test="${payment.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${payment.createdBy}" /></td>
																								<td><c:out
																										value="${payment.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${payment.updatedBy}" /></td>
																								<td><c:out
																										value="${payment.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.suspendHistory }">
																			<c:forEach items="${historyBean.suspendHistory}"
																				var="suspendList">
																				<c:forEach items="${suspendList}" var="suspend">
																					<tr>
																						<td><c:out value="SUSPENDS" /></td>
																						<td style="text-transform: uppercase;"><c:if
																								test="${not empty suspend.suspendedCodes.description}">
																								<c:out
																									value="${suspend.suspendedCodes.description}" />
																							</c:if> <c:if
																								test="${not empty suspend.suspendedCodes.code}">,
																		<c:out value="${suspend.suspendedCodes.code}" />
																							</c:if> <c:if
																								test="${not empty suspend.formattedSuspendDate}">,
																		<c:out value="${suspend.formattedSuspendDate}" />
																							</c:if> <c:if
																								test="${not empty suspend.formattedProcessedOn}">,
																		<c:out value="${suspend.formattedProcessedOn}" />
																							</c:if> <c:if test="${not empty suspend.reduction}">,
																		 $<c:out value="${suspend.reduction}" />,
																		</c:if>$<c:out value="${suspend.totalDue}" /></td>
																						<c:choose>
																							<c:when test="${suspend.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${suspend.createdBy}" /></td>
																								<td><c:out
																										value="${suspend.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${suspend.updatedBy}" /></td>
																								<td><c:out
																										value="${suspend.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.correspHistory }">
																			<c:forEach items="${historyBean.correspHistory}"
																				var="corresList">
																				<c:forEach items="${corresList}" var="correspond">
																					<tr>
																						<td><c:out value="CORRESPONDENCE" /></td>
																						<td style="text-transform: uppercase;"><c:if
																								test="${not empty correspond.correspCode.correspDesc}">
																								<c:out
																									value="${correspond.correspCode.correspDesc}" />
																							</c:if> <c:if
																								test="${not empty correspond.formattedCorresDate}">,
																		<c:out value="${correspond.formattedCorresDate}" />
																							</c:if> <c:if
																								test="${not empty correspond.corresp_time}">,
																		<c:out value="${correspond.corresp_time}" />
																							</c:if> <c:if test="${correspond.letterSent==true}">,
																		<c:out value="Yes" />
																							</c:if> <c:if test="${correspond.letterSent==false}">,
																		<c:out value="No" />
																							</c:if></td>
																						<c:choose>
																							<c:when test="${correspond.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${correspond.createdBy}" /></td>
																								<td><c:out
																										value="${correspond.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${payment.updatedBy}" /></td>
																								<td><c:out
																										value="${payment.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.noticeHistory }">
																			<c:forEach items="${historyBean.noticeHistory}"
																				var="noticesList">
																				<c:forEach items="${noticesList}" var="notice">
																					<tr>
																						<td><c:out value="NOTICES" /></td>
																						<td style="text-transform: uppercase;"><c:if
																								test="${not empty notice.noticeType.fullNm}">
																								<c:out value="${notice.noticeType.fullNm}" />
																							</c:if> <c:if
																								test="${not empty notice.formattedSentDate}">, <c:out
																									value="${notice.formattedSentDate}" />
																							</c:if>&nbsp; <c:if
																								test="${not empty notice.formattedProcessedDate}">, <c:out
																									value="${notice.formattedProcessedDate}" />, </c:if>&nbsp;
																							<c:out value="Yes" /></td>
																						<c:choose>
																							<c:when test="${notice.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${notice.createdBy}" /></td>
																								<td><c:out
																										value="${notice.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${notice.updatedBy}" /></td>
																								<td><c:out
																										value="${notice.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.hearingHistory }">
																			<c:forEach items="${historyBean.hearingHistory}"
																				var="hearing">
																				<tr>
																					<td><c:out value="HEARINGS" /></td>
																					<td style="text-transform: uppercase;"><c:if
																							test="${not empty hearing.hearingOfficer}">
																							<c:out value="${hearing.hearingOfficer}" />
																						</c:if> <c:if
																							test="${not empty hearing.formattedHearingDate}">
																							<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																								type="time" timeStyle="short" pattern="hh:mm a"
																								value="${hearing.hearingTime}" />
																						</c:if> <c:if
																							test="${not empty hearing.formattedDispDate}">,
																				<c:out value="${hearing.formattedDispDate}" />
																						</c:if> <c:if test="${not empty hearing.dispositionTime}">/
																				<fmt:formatDate type="time" timeStyle="short"
																								pattern="hh:mm a"
																								value="${hearing.dispositionTime}" />
																						</c:if> <c:if test="${not empty hearing.disposition}">,
																				<c:out value="${hearing.disposition}" />
																						</c:if> <c:if test="${not empty hearing.totalDue}">&#36;
																				<c:out value="${hearing.totalDue}" />
																						</c:if> <c:if test="${not empty hearing.scheduledAt}">/
																				<c:out value="${hearing.formattedScheduledAt}" />
																						</c:if> <c:if test="${not empty hearing.scheduledBy}">/
																				<c:out value="${hearing.scheduledBy}" />/</c:if> <c:if
																							test="${not empty hearing.status}">,
																				<c:out value="${hearing.status}" />/</c:if> <c:if
																							test="${hearing.dateMailed != null}">/<c:out
																								value="MAIL DATE: "></c:out>
																							<c:out value="${hearing.formattedDateMailed}"></c:out>
																						</c:if></td>
																					<c:choose>
																						<c:when test="${hearing.updatedAt == null}">
																							<td style="text-transform: uppercase;"><c:out
																									value="${hearing.createdBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${hearing.formattedCreatedAt}" /></td>
																						</c:when>
																						<c:otherwise>
																							<td style="text-transform: uppercase;"><c:out
																									value="${hearing.updatedBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${hearing.formattedUpdatedAt}" /></td>
																						</c:otherwise>
																					</c:choose>
																				</tr>
																			</c:forEach>
																		</c:if>
																		<c:if test="${not empty historyBean.ippHistory}">
																			<c:forEach items="${historyBean.ippHistory}"
																				var="ipp">
																				<tr>
																					<td><c:out value="IPP" /></td>
																					<td style="text-transform: uppercase;"><c:if
																							test="${not empty ipp.planNumber}">
																							<c:out value="${ipp.planNumber}" />
																						</c:if> <c:if test="${not empty ipp.startDate}">,
																									<c:out value="${ipp.startDate}" />
																						</c:if> <c:if test="${not empty ipp.enrollAmount}">,
																									<c:out value="${ipp.enrollAmount}" />
																						</c:if> <c:if test="${not empty ipp.downPayment}">/
																									<c:out value="${ipp.downPayment}" />
																						</c:if> <c:if test="${not empty ipp.installmentAmount}">,
																									<c:out value="${ipp.installmentAmount}" />
																						</c:if> <c:if test="${not empty ipp.noOfPayments}">/
																									<c:out value="${ipp.noOfPayments}" />
																						</c:if> <c:if test="${not empty ipp.status}">,
																									<c:out value="${ipp.status}" />
																						</c:if> <c:if test="${not empty ipp.type}">/
																									<c:out value="${ipp.type}" />
																						</c:if></td>
																					<c:choose>
																						<c:when test="${ipp.updatedAt == null}">
																							<td style="text-transform: uppercase;"><c:out
																									value="${ipp.createdBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${ipp.formattedCreatedAt}" /></td>
																						</c:when>
																						<c:otherwise>
																							<td style="text-transform: uppercase;"><c:out
																									value="${ipp.updatedBy}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${ipp.formattedUpdatedAt}" /></td>
																						</c:otherwise>
																					</c:choose>
																				</tr>
																			</c:forEach>
																		</c:if>
																	</tbody>
																</table>
															</div>
														</div>
													</c:when>
													<c:otherwise>
														<div class="portlet-body">
															<table class="table table-hover table-bordered ">
																<thead></thead>
																<tbody>
																	<tr>
																		<td colspan="4" align="center"><b>No results
																				found</b></td>
																	</tr>
																</tbody>
															</table>
														</div>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${historyItem =='PlateEntity'}">
														<c:choose>
															<c:when test="${not empty historyBean.plateHistory }">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="pltEntTbl"
																			class="table table-hover table-bordered">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="20%"><b>Vehicle
																							License Number</b></th>
																					<th style="color: #004b85" width="20%"><b>VIN
																							Number</b></th>
																					<th style="color: #004b85" width="20%"><b>License
																							State</b></th>
																					<th style="color: #004b85" width="20%"><b>License
																							Month/Year</b></th>
																					<th style="color: #004b85" width="10%"><b>Vehicle
																							Body Type</b></th>
																					<th style="color: #004b85" width="10%"><b>Vehicle
																							Make</b></th>
																					<th style="color: #004b85" width="10%"><b>Vehicle
																							Model</b></th>
																					<th style="color: #004b85" width="10%"><b>Vehicle
																							Color</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.plateHistory}"
																					var="plateEntity">
																					<tr>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.licenceNumber}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.vinNumber}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.licenseState}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.licenseMonth}" />/<c:out
																								value="${plateEntity.licenseYear}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.bodyType}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.vehicleMake}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.vehicleModel}" /></td>
																						<td style="text-transform: uppercase;"><c:out
																								value="${plateEntity.vehicleColor}" /></td>
																						<c:choose>
																							<c:when test="${plateEntity.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${plateEntity.createdBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${plateEntity.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${plateEntity.updatedBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${plateEntity.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Penalty'}">
														<c:choose>
															<c:when test="${not empty historyBean.penaltyHistory }">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="pnltyTbl"
																			class="table table-hover table-bordered">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="10%"><b>Fine
																							Amt</b></th>
																					<th style="color: #004b85" width="10%"><b>Penalty
																							1 / Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Penalty
																							2 / Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Penalty
																							3 / Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Total
																							Paid</b></th>
																					<th style="color: #004b85" width="10%"><b>Total
																							Due/Over paid</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.penaltyHistory}"
																					var="penalty">
																					<tr>
																						<td style="text-transform: uppercase;">&#36;<c:out
																								value="${penalty.fineAmount}" /></td>
																						<td style="text-transform: uppercase;"><c:choose>
																								<c:when
																									test="${not empty penalty.penaltyCode.penalty1}">
																								&#36;<c:if
																										test="${not empty penalty.penaltyCode.penalty1}">
																										<c:out value="${penalty.penaltyCode.penalty1}" />
																									</c:if>
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty1}">
																								/<c:out
																											value="${penalty.penaltyCode.formattedpenalty1}" />
																									</c:if>
																								</c:when>
																								<c:otherwise>
																							&#36;<c:out value="0.00" />
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty1}">/
																											<c:out
																											value="${penalty.penaltyCode.formattedpenalty1}" />
																									</c:if>
																								</c:otherwise>
																							</c:choose></td>
																						<td style="text-transform: uppercase;"><c:choose>
																								<c:when
																									test="${not empty penalty.penaltyCode.penalty2}">
																								&#36;<c:out
																										value="${penalty.penaltyCode.penalty2}" />
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty2}">/
																											<c:out
																											value="${penalty.penaltyCode.formattedpenalty2}" />
																									</c:if>
																								</c:when>
																								<c:otherwise>
																								&#36;<c:out value="0.00" />
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty2}">/
																								<c:out
																											value="${penalty.penaltyCode.formattedpenalty2}" />
																									</c:if>
																								</c:otherwise>
																							</c:choose></td>
																						<td style="text-transform: uppercase;"><c:choose>
																								<c:when
																									test="${not empty penalty.penaltyCode.penalty3}">
																							&#36;<c:out
																										value="${penalty.penaltyCode.penalty3}" />
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty3}">/<c:out
																											value="${penalty.penaltyCode.formattedpenalty3}" />
																									</c:if>
																								</c:when>
																								<c:otherwise>
																							&#36;<c:out value="0.00" />
																									<c:if
																										test="${not empty penalty.penaltyCode.formattedpenalty3}">/
																							<c:out
																											value="${penalty.penaltyCode.formattedpenalty3}" />
																									</c:if>
																								</c:otherwise>
																							</c:choose></td>
																						<td style="text-transform: uppercase;"><c:choose>
																								<c:when
																									test="${not empty penalty.totalPaidAmount}">
																							&#36;<c:out value="${penalty.totalPaidAmount}" />
																								</c:when>
																								<c:otherwise>
																							&#36;<c:out value="0.00" />
																								</c:otherwise>
																							</c:choose></td>
																						<td style="text-transform: uppercase;">&#36;<c:out
																								value="${penalty.violation.totalDue}" /></td>
																						<c:choose>
																							<c:when test="${penalty.updatedAt == null}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${penalty.createdBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${penalty.formattedCreatedAt}" /></td>
																							</c:when>
																							<c:otherwise>
																								<td style="text-transform: uppercase;"><c:out
																										value="${penalty.updatedBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${penalty.formattedUpdatedAt}" /></td>
																							</c:otherwise>
																						</c:choose>
																					</tr>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Payment'}">
														<c:choose>
															<c:when test="${not empty historyBean.paymentHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="pmntTbl"
																			class="table table-hover table-bordered ">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="10%"><b>Amount/Payment
																							Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Type/Method</b></th>
																					<th style="color: #004b85" width="10%"><b>Account</b></th>
																					<th style="color: #004b85" width="10%"><b>Processed
																							On/User Id</b></th>
																					<th style="color: #004b85" width="10%"><b>Over
																							paid</b></th>
																					<th style="color: #004b85" width="10%"><b>Total
																							Due</b></th>
																					<th style="color: #004b85" width="10%"><b>Transaction
																							Number</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.paymentHistory}"
																					var="paymentList">
																					<c:forEach items="${paymentList}" var="payment">
																						<tr>
																							<td style="text-transform: uppercase;">$<c:out
																									value="${payment.amount}" />/<c:out
																									value="${payment.paymentDate}" /></td>
																							<td style="text-transform: uppercase;"><c:choose>
																									<c:when test="${payment.paymentSource == 'P'}">
																										<c:out value="PAY-IN-PERSON" />
																									</c:when>
																									<c:when test="${payment.paymentSource == 'M'}">
																										<c:out value="PAY-BY-MAIL" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="PAY-BY-WEB" />
																									</c:otherwise>
																								</c:choose> /<c:out
																									value="${payment.paymentMethod.description}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${payment.account}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${payment.processedOn}" />/<c:out
																									value="${payment.processedBy}" /></td>
																							<td style="text-transform: uppercase;">$<c:out
																									value="${payment.overPaid}" /></td>
																							<td style="text-transform: uppercase;">$<c:out
																									value="${payment.totalDue}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${payment.transaction.id}" /></td>
																							<c:choose>
																								<c:when test="${payment.updatedAt == null}">
																									<td style="text-transform: uppercase;"><c:out
																											value="${payment.createdBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${payment.formattedCreatedAt}" /></td>
																								</c:when>
																								<c:otherwise>
																									<td style="text-transform: uppercase;"><c:out
																											value="${payment.updatedBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${payment.formattedUpdatedAt}" /></td>
																								</c:otherwise>
																							</c:choose>
																						</tr>
																					</c:forEach>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Hearing'}">
														<c:choose>
															<c:when test="${not empty historyBean.hearingHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="hearingTbl"
																			class="table table-hover table-bordered">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="10%"><b>Hearing
																							Officer</b></th>
																					<th style="color: #004b85" width="10%"><b>Hearing
																							Date/<br>Time
																					</b></th>
																					<th style="color: #004b85" width="10%"><b>Hearing
																							Status</b></th>
																					<th style="color: #004b85" width="10%"><b>Disposition
																							Date/<br>Time
																					</b></th>
																					<th style="color: #004b85" width="10%"><b>Disposition
																					</b></th>
																					<th style="color: #004b85" width="10%"><b>Reduction/<br>Total
																							Due
																					</b></th>
																					<th style="color: #004b85" width="10%"><b>Scheduled_At/<br>Scheduled_By
																					</b></th>
																					<th style="color: #004b85" width="10%"><b>Date
																							Mailed </b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.hearingHistory}"
																					var="hearing">
																					<c:if
																						test="${(fn:toUpperCase(hearing.status)=='PENDING')||showHearing}">
																						<tr>
																							<c:choose>
																								<c:when test="${hearing.updatedAt == null}">
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.hearingOfficer}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedHearingDate}" />/<br>
																										<fmt:formatDate type="time" timeStyle="short"
																											pattern="hh:mm a"
																											value="${hearing.hearingTime}" /></td>
																									<td style="text-transform: uppercase;"><c:choose>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																												<c:out value="In-Progress"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																												<c:out value="Pending Review-Updated"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																												<c:out value="Pending Review"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																												<c:out value="Complete"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																												<c:out value="Pending Mail"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																												<c:out value="Needs Rework"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																												<c:out value="Failed to Appear"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'Cancelled')}">
																												<c:out value="Cancelled"></c:out>
																											</c:when>
																											<c:otherwise>
																												<c:out value="Pending"></c:out>
																											</c:otherwise>
																										</c:choose></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedDispDate}" />/<br>
																										<fmt:formatDate type="time" timeStyle="short"
																											pattern="hh:mm a"
																											value="${hearing.dispositionTime}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.disposition}" /></td>
																									<td style="text-transform: uppercase;">$<c:out
																											value="${hearing.reduction}" />/<br>$<c:out
																											value="${hearing.totalDue}" /></td>

																									<td style="text-transform: uppercase;"><c:if
																											test="${not empty hearing.scheduledAt}">
																											<fmt:formatDate type="time" timeStyle="short"
																												pattern="hh:mm a"
																												value="${hearing.scheduledAt}" />/</c:if><br>
																										<c:if test="${not empty hearing.scheduledBy}">
																											<c:out value="${hearing.scheduledBy}" />
																										</c:if></td>
																									<td style="text-transform: uppercase;"><c:choose>
																											<c:when test="${hearing.dateMailed != null}">
																												<c:out
																													value="${hearing.formattedDateMailed}"></c:out>
																											</c:when>
																											<c:otherwise>
																												<c:out value=""></c:out>
																											</c:otherwise>
																										</c:choose></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.createdBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedCreatedAt}" /></td>
																								</c:when>
																								<c:otherwise>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.hearingOfficer}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedHearingDate}" />/<br>
																										<fmt:formatDate type="time" timeStyle="short"
																											pattern="hh:mm a"
																											value="${hearing.hearingTime}" /></td>
																									<td style="text-transform: uppercase;"><c:choose>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PROGRESS')}">
																												<c:out value="In-Progress"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEWUPDATED')}">
																												<c:out value="Pending Review-Updated"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGREVIEW')}">
																												<c:out value="Pending Review"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'COMPLETE')}">
																												<c:out value="Complete"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'PENDINGMAIL')}">
																												<c:out value="Pending Mail"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'REWORK')}">
																												<c:out value="Needs Rework"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'Failed to Appear')}">
																												<c:out value="Failed to Appear"></c:out>
																											</c:when>
																											<c:when
																												test="${fn:containsIgnoreCase(hearing.status,'Cancelled')}">
																												<c:out value="Cancelled"></c:out>
																											</c:when>
																										</c:choose></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedDispDate}" />/<br>
																										<fmt:formatDate type="time" timeStyle="short"
																											pattern="hh:mm a"
																											value="${hearing.dispositionTime}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.disposition}" /></td>
																									<td style="text-transform: uppercase;">$<c:out
																											value="${hearing.reduction}" />/<br>$<c:out
																											value="${hearing.totalDue}" /></td>

																									<td style="text-transform: uppercase;"><c:if
																											test="${not empty hearing.scheduledAt}">
																											<fmt:formatDate type="time" timeStyle="short"
																												pattern="hh:mm a"
																												value="${hearing.scheduledAt}" />/</c:if><br>
																										<c:if test="${not empty hearing.scheduledBy}">
																											<c:out value="${hearing.scheduledBy}" />
																										</c:if></td>
																									<td style="text-transform: uppercase;"><c:choose>
																											<c:when test="${hearing.dateMailed != null}">
																												<c:out
																													value="${hearing.formattedDateMailed}"></c:out>
																											</c:when>
																											<c:otherwise>
																												<c:out value=""></c:out>
																											</c:otherwise>
																										</c:choose></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.updatedBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${hearing.formattedUpdatedAt}" /></td>
																								</c:otherwise>
																							</c:choose>
																						</tr>
																					</c:if>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Suspend'}">
														<c:choose>
															<c:when test="${not empty historyBean.suspendHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="spndTbl"
																			class="table table-hover table-bordered ">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="10%"><b>Suspended</b></th>
																					<th style="color: #004b85" width="10%"><b>Code</b></th>
																					<th style="color: #004b85" width="10%"><b>Suspend
																							Date / Time</b></th>
																					<th style="color: #004b85" width="10%"><b>Processed
																							On</b></th>
																					<th style="color: #004b85" width="10%"><b>Reduction</b></th>
																					<th style="color: #004b85" width="10%"><b>Total
																							Due</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.suspendHistory}"
																					var="suspendList">
																					<c:forEach items="${suspendList}" var="suspend">
																						<tr>
																							<td style="text-transform: uppercase;"><c:out
																									value="${suspend.suspendedCodes.description}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${suspend.suspendedCodes.code}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${suspend.formattedSuspendDate}" /> / <c:out
																									value="${suspend.suspended_time}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${suspend.formattedProcessedOn}" /></td>
																							<td style="text-transform: uppercase;"><c:choose>
																									<c:when test="${not empty suspend.reduction}">
																							&#36;<c:out value="${suspend.reduction}" />
																									</c:when>
																									<c:otherwise>
																							&#36;<c:out value="0.00" />
																									</c:otherwise>
																								</c:choose></td>
																							<td style="text-transform: uppercase;">$<c:out
																									value="${suspend.totalDue}" /></td>
																							<c:choose>
																								<c:when test="${suspend.updatedAt == null}">
																									<td style="text-transform: uppercase;"><c:out
																											value="${suspend.createdBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${suspend.formattedCreatedAt}" /></td>
																								</c:when>
																								<c:otherwise>
																									<td style="text-transform: uppercase;"><c:out
																											value="${suspend.updatedBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${suspend.formattedUpdatedAt}" /></td>
																								</c:otherwise>
																							</c:choose>
																						</tr>
																					</c:forEach>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Correspondence'}">
														<c:choose>
															<c:when test="${not empty historyBean.correspHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="crspTbl"
																			class="table table-hover table-bordered ">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="25%"><b>Correspondence</b></th>
																					<th style="color: #004b85" width="10%"><b>Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Time</b></th>
																					<th style="color: #004b85" width="10%"><b>Letter&nbsp;Sent</b></th>
																					<th style="color: #004b85" width="10%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="15%"><b>Updated
																							Date</b></th>

																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.correspHistory}"
																					var="corresList">
																					<c:forEach items="${corresList}" var="correspond">
																						<tr>
																							<td style="text-transform: uppercase;"><c:out
																									value="${correspond.correspCode.correspDesc}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${correspond.formattedCorresDate}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${correspond.corresp_time}" /></td>
																							<c:if test="${correspond.letterSent==true }">
																								<td style="text-transform: uppercase;"><c:out
																										value="Yes" /></td>
																							</c:if>
																							<c:if test="${correspond.letterSent==false }">
																								<td style="text-transform: uppercase;"><c:out
																										value="No" /></td>
																							</c:if>
																							<c:choose>
																								<c:when test="${correspond.updatedAt == null}">
																									<td style="text-transform: uppercase;"><c:out
																											value="${correspond.createdBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${correspond.formattedCreatedAt}" /></td>

																								</c:when>
																								<c:otherwise>
																									<td style="text-transform: uppercase;"><c:out
																											value="${correspond.updatedBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${correspond.formattedUpdatedAt}" /></td>
																								</c:otherwise>
																							</c:choose>
																						</tr>
																					</c:forEach>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem == 'Notices'}">
														<c:choose>
															<c:when test="${not empty historyBean.noticeHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="noticeTbl"
																			class="table table-hover table-bordered ">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="10%"><b>Notice
																							Type</b></th>
																					<th style="color: #004b85" width="10%"><b>Sent
																							Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Processed
																							Date</b></th>
																					<th style="color: #004b85" width="10%"><b>Mail
																							Sent</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.noticeHistory}"
																					var="noticesList">
																					<c:forEach items="${noticesList}" var="notice">
																						<tr>
																							<td style="text-transform: uppercase;"><c:out
																									value="${notice.noticeType.fullNm}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${notice.formattedSentDate}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="${notice.formattedProcessedDate}" /></td>
																							<td style="text-transform: uppercase;"><c:out
																									value="Yes" /></td>
																							<c:choose>
																								<c:when test="${notice.updatedAt == null}">
																									<td style="text-transform: uppercase;"><c:out
																											value="${notice.createdBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${notice.formattedCreatedAt}" /></td>
																								</c:when>
																								<c:otherwise>
																									<td style="text-transform: uppercase;"><c:out
																											value="${notice.updatedBy}" /></td>
																									<td style="text-transform: uppercase;"><c:out
																											value="${notice.formattedUpdatedAt}" /></td>
																								</c:otherwise>
																							</c:choose>
																						</tr>
																					</c:forEach>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
													<c:when test="${historyItem =='Ipp'}">
														<c:choose>
															<c:when test="${not empty historyBean.ippHistory}">
																<div class="portlet-body">
																	<div class="table-responsive scroller">
																		<table id="ippTbl"
																			class="table table-hover table-bordered ">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #004b85" width="15%"><b>Plan
																							Number</b></th>
																					<th style="color: #004b85" width="15%"><b>Start
																							Date</b></th>
																					<th style="color: #004b85" width="15%"><b>Enroll
																							Amount</b></th>
																					<th style="color: #004b85" width="15%"><b>Down
																							Payment</b></th>
																					<th style="color: #004b85" width="20%"><b>Installment
																							Amount</b></th>
																					<th style="color: #004b85" width="15%"><b>Status</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							By</b></th>
																					<th style="color: #004b85" width="20%"><b>Updated
																							Date</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${historyBean.ippHistory}" var="ipp">
																					<c:choose>
																						<c:when test="${ipp.updatedAt == null}">
																							<tr>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.planNumber}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.startDate}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.enrollAmount}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.downPayment}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.installmentAmount}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.status}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.createdBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.formattedCreatedAt}" /></td>
																							</tr>
																						</c:when>
																						<c:otherwise>
																							<tr>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.planNumber}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.startDate}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.enrollAmount}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.downPayment}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.installmentAmount}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.status}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.updatedBy}" /></td>
																								<td style="text-transform: uppercase;"><c:out
																										value="${ipp.formattedUpdatedAt}" /></td>
																							</tr>
																						</c:otherwise>
																					</c:choose>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>
																</div>
															</c:when>
															<c:otherwise>
																<div class="portlet-body">
																	<table id="allDetailsTbl"
																		class="table table-hover table-bordered ">
																		<thead></thead>
																		<tbody>
																			<tr>
																				<td colspan="4" align="center"><b>No
																						results found</b></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</c:otherwise>
														</c:choose>
													</c:when>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- view all -->
	</div>
</div>

<!-- END CONTENT -->
<!-- END CONTAINER -->
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript"></script>

<script>
	$(document)
			.ready(
					function() {
						$('#historyItem').val('${historyItem}').attr(
								"selected", "selected");
						$("#historyItem").change(function() {
							console.log("a");
							var val = $('#historyItem').val();
							console.log(val);
							if (val != "") {
								$('#historySearchForm').submit();
							}
						});
						var historyItemVal = $('#historyItem').val();
						if (historyItemVal === "All") {

							$('#allDetailsTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2 ],
									"orderable" : false,
								} ],
								"order" : [ [ 3, "desc" ] ]
							});
						} else if (historyItemVal === "PlateEntity") {

							$('#pltEntTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ],
									"orderable" : false,
								} ],
								"scrolly" : false,
								"order" : [ [ 9, "desc" ] ]
							});
						} else if (historyItemVal === "Notes") {
							$('#cmntsTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 3 ],
									"orderable" : false,
								} ],
								"order" : [ [ 2, "desc" ] ]
							});
						} else if (historyItemVal === "Address") {
							$('#adrsTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4 ],
									"orderable" : false,
								} ],
								"order" : [ [ 5, "desc" ] ]
							});
						} else if (historyItemVal === "Penalty") {
							$('#pnltyTbl').DataTable(
									{
										"paging" : false,
										"info" : false,
										"searching" : false,
										"orderClasses" : false,
										"autoWidth" : false,
										"columnDefs" : [ {
											"targets" : [ 0, 1, 2, 3, 4, 5, 6,
													7, 8, 9 ],
											"orderable" : false,
										} ],
										"order" : [ [ 10, "desc" ] ]
									}

							);

						} else if (historyItemVal === "Notices") {
							$('#noticeTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3 ],
									"orderable" : false,
								} ],
								"order" : [ [ 4, "desc" ] ]
							});

						} else if (historyItemVal === "Payment") {
							$('#pmntTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ],
									"orderable" : false,
								} ],
								"order" : [ [ 9, "desc" ] ]
							});

						} else if (historyItemVal === "Hearing") {
							$('#hearingTbl').DataTable(
									{
										"paging" : false,
										"info" : false,
										"searching" : false,
										"orderClasses" : false,
										"autoWidth" : false,
										"columnDefs" : [ {
											"targets" : [ 0, 1, 2, 3, 4, 5, 6,
													7, 8, 9, 10, 11, 12 ],
											"orderable" : false,
										} ],
										"order" : [ [ 13, "desc" ] ]
									});

						} else if (historyItemVal === "CS") {
							$('#csTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ],
									"orderable" : false,
								} ],
								"order" : [ [ 9, "desc" ] ]
							});

						} else if (historyItemVal === "Ipp") {
							$('#ippTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6 ],
									"orderable" : false,
								} ],
								"order" : [ [ 7, "desc" ] ]
							});

						} else if (historyItemVal === "Suspend") {
							$('#spndTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7 ],
									"orderable" : false,
								} ],
								"order" : [ [ 8, "desc" ] ]
							});

						} else if (historyItemVal === "Correspondence") {
							$('#crspTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7 ],
									"orderable" : false,
								} ],
								"order" : [ [ 8, "desc" ] ]
							});

						} else if (historyItemVal === "Misc") {
							$('#citationTbl').DataTable({
								"paging" : false,
								"info" : false,
								"searching" : false,
								"orderClasses" : false,
								"autoWidth" : false,
								"columnDefs" : [ {
									"targets" : [ 0, 1 ],
									"orderable" : false,
								} ],
								"order" : [ [ 2, "desc" ] ]
							});

						} else if (historyItemVal === "Superior") {
							$('#superiorTbl').DataTable(
									{
										"paging" : false,
										"info" : false,
										"searching" : false,
										"orderClasses" : false,
										"autoWidth" : false,
										"columnDefs" : [ {
											"targets" : [ 0, 1, 2, 3, 4, 5, 6,
													7, 8, 9, 10 ],
											"orderable" : false,
										} ],
										"order" : [ [ 11, "desc" ] ]
									});

						}
						$("#printBtn")
								.click(
										function() {

											var val = $('#historyItem').val();

											console.log(val);
											if (val === "All"
													|| val === "Notes") {
												var divToPrint = $('#printarea')
														.clone();
												divToPrint
														.find(
																'.table tr[data-type="internalComment"]')
														.remove();
												var newWin = window.open('',
														'Print-Window');
												newWin.document.open();
												newWin.document
														.write('<html><body onload="window.print()">'
																+ divToPrint
																		.html()
																+ '</body></html>');
												newWin.document.close();
												setTimeout(function() {
													newWin.close();
												}, 10);
											} else {

												var divToPrint = document
														.getElementById('printarea');

												var newWin = window.open('',
														'Print-Window');

												newWin.document.open();

												newWin.document
														.write('<html><body onload="window.print()">'
																+ divToPrint.innerHTML
																+ '</body></html>');

												newWin.document.close();

												setTimeout(function() {
													newWin.close();
												}, 10);

											}

										});
					});
</script>

<style type='text/css'>
@
-webkit-keyframes uil-default-anim { 0% {
	opacity: 1
}

100%
{
opacity




















:










 










0
}
}
@
keyframes uil-default-anim { 0% {
	opacity: 1
}

100%
{
opacity




















:










 










0
}
}
.uil-default-css>div:nth-of-type(1) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.5s;
	animation-delay: -0.5s;
}

.uil-default-css {
	position: relative;
	top: 175px;
	left: 650px;
	background: none;
	width: 200px;
	height: 200px;
	z-index: 999
}

.uil-default-css>div:nth-of-type(2) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.4166666666666667s;
	animation-delay: -0.4166666666666667s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(3) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.33333333333333337s;
	animation-delay: -0.33333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(4) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.25s;
	animation-delay: -0.25s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(5) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.16666666666666669s;
	animation-delay: -0.16666666666666669s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(6) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: -0.08333333333333331s;
	animation-delay: -0.08333333333333331s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(7) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0s;
	animation-delay: 0s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(8) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.08333333333333337s;
	animation-delay: 0.08333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(9) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.16666666666666663s;
	animation-delay: 0.16666666666666663s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(10) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.25s;
	animation-delay: 0.25s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(11) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.33333333333333337s;
	animation-delay: 0.33333333333333337s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}

.uil-default-css>div:nth-of-type(12) {
	-webkit-animation: uil-default-anim 1s linear infinite;
	animation: uil-default-anim 1s linear infinite;
	-webkit-animation-delay: 0.41666666666666663s;
	animation-delay: 0.41666666666666663s;
}

.uil-default-css {
	position: relative;
	background: none;
	width: 200px;
	height: 200px;
}
</style>

<div style="display: none" id="uil-default-css-id"
	class='uil-default-css' style='transform:scale(1);'>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(0deg) translate(0, -60px); transform: rotate(0deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(30deg) translate(0, -60px); transform: rotate(30deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(60deg) translate(0, -60px); transform: rotate(60deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(90deg) translate(0, -60px); transform: rotate(90deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(120deg) translate(0, -60px); transform: rotate(120deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(150deg) translate(0, -60px); transform: rotate(150deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(180deg) translate(0, -60px); transform: rotate(180deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(210deg) translate(0, -60px); transform: rotate(210deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(240deg) translate(0, -60px); transform: rotate(240deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(270deg) translate(0, -60px); transform: rotate(270deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(300deg) translate(0, -60px); transform: rotate(300deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
	<div
		style='top: 80px; left: 93px; width: 14px; height: 40px; background: #00b2ff; -webkit-transform: rotate(330deg) translate(0, -60px); transform: rotate(330deg) translate(0, -60px); border-radius: 10px; position: absolute;'></div>
</div>

<script>
	document.onreadystatechange = function() {
		var state = document.readyState;
		if (state == 'interactive') {
			document.getElementById('uil-default-css-id').style.display = "block";
		}
		if (state == 'complete') {
			document.getElementById('uil-default-css-id').style.display = "none";
		}
	}
</script>

<script>
	function enablePageLoadBar() {
		document.getElementById('uil-default-css-id').style.display = "block";
	}
</script>