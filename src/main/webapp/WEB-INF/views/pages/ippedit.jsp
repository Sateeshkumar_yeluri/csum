<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" id="blockui_violationdetails_data">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div style="text-align: center;">
			<span style="font-size: 16px;">
						<b>License Number:</b><span
						style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
					&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
						Number:</b></span><span style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
		</div>
		<br>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="violationPortlet" class="portlet box green bg-inverse">
						<div class="portlet-title">
							<div class="caption">Installment Payment Plan</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form:form id="violationForm"
								onsubmit="return validateCitDetails()" action="#"
								modelAttribute="ippForm" autocomplete="on"
								enctype="multipart/form-data">
								<div class="form-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-sm">
												<label class="control-label label-sm col-md-3">First
													Name </label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.firstName != null && not empty ippForm.violation.patron.firstName}">
															<form:input class="form-control input-sm"
																path="violation.patron.firstName" id="firstName"
																readonly="true" style="background-color : white; text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.firstName" id="firstName" />
														</c:otherwise>
													</c:choose>
													<span class="help-block"> <span class="alert-danger"
														id="statusError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-sm">
												<label class="control-label label-sm col-md-3">Middle
													Name </label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.middleName != null && not empty ippForm.violation.patron.middleName}">
															<form:input class="form-control input-sm"
																path="violation.patron.middleName" id="middleName"
																readonly="true" style="background-color : white; text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.middleName" id="middleName" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="middleNameError"></span>
													</span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Last
													Name</label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.lastName != null && not empty ippForm.violation.patron.lastName}">
															<form:input class="form-control input-sm"
																path="violation.patron.lastName" id="lastName"
																readonly="true" style="background-color : white; text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.lastName" id="lastName" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="lastNameError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Address
												</label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.address != null && not empty ippForm.violation.patron.address.address}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.address" id="address"
																readonly="true" style="background-color : white; text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.address" id="address" />
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<%-- <div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Address
													1 </label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.addressLine1 != null && not empty ippForm.violation.patron.address.addressLine1}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.addressLine1"
																id="addressLine1" readonly="true"
																style="background-color : white;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.addressLine1"
																id="addressLine1" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="addressLine1Error"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Address
													2 </label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.addressLine2 != null && not empty ippForm.violation.patron.address.addressLine2}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.addressLine2"
																id="addressLine2" readonly="true"
																style="background-color : white;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.addressLine2"
																id="addressLine2" />
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</div>
									</div> --%>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">City
												</label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.city != null && not empty ippForm.violation.patron.address.city}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.city" id="city"
																readonly="true" style="background-color : white;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.city" id="city" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="cityError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">State
												</label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.state != null && not empty ippForm.violation.patron.address.state}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.state" id="state"
																readonly="true" style="background-color : white;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.state" id="state" />
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Zip </label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.patron.address.zip != null && not empty ippForm.violation.patron.address.zip}">
															<form:input class="form-control input-sm"
																path="violation.patron.address.zip" id="zip"
																readonly="true" style="background-color : white;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.patron.address.zip" id="zip" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="zipError"></span>
													</span>
												</div>
											</div>
										</div>
										<%-- <div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">NIXIE
												</label>
												<div class="col-md-9">
													<c:choose>
														<c:when
															test="${ippForm.violation.nixie != null && not empty ippForm.violation.nixie}">
															<form:input class="form-control input-sm"
																path="violation.nixie" id="nixie" readonly="true"
																style="background-color : white; text-transform: uppercase;text-transform: uppercase;" />
														</c:when>
														<c:otherwise>
															<form:input class="form-control input-sm"
																path="violation.nixie" id="nixie" />
														</c:otherwise>
													</c:choose>
													<span class="help-block">&nbsp; <span
														class="alert-danger" id="nixieError"></span>
													</span>
												</div>
											</div>
										</div> --%>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">State
													Plate </label>
												<div class="col-md-9">
															<c:choose>
																<c:when
																	test="${ippForm.violation.plateEntity.licenceNumber != null && not empty ippForm.violation.plateEntity.licenceNumber}">
																	<form:input path="violation.plateEntity.licenceNumber"
																		class="form-control input-sm" id="plate"
																		readonly="true" style="background-color : white;text-transform: uppercase;" />
																</c:when>
																<c:otherwise>
																	<form:input path="violation.plateEntity.licenceNumber"
																		id="plate" class="form-control input-sm" />
																</c:otherwise>
															</c:choose>
												</div>
											</div>
										</div>
									</div>
									<!-- <div class="row">
										
									</div> -->
									<h3 class="form-section"
										style="margin-top: 27px; border-bottom-color: #32c5d2"></h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-sm">
												<label class="control-label label-sm col-md-3">Plan
													Number </label>
												<div class="col-md-9">
													<form:input class="form-control input-sm" path="planNumber"
														id="planNumber" readonly="true"
														style="background-color:white;text-transform: uppercase;"></form:input>
													<span class="help-block"><span class="alert-danger"
														id="planNumberError"></span> </span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-sm">
												<label class="control-label label-sm col-md-3">Plan
													Status </label>
												<div class="col-md-9">
													<form:input class="form-control input-sm" path="status"
														id="status" readonly="true"
														style="background-color:white;text-transform: uppercase;"></form:input>
													<span class="help-block">&nbsp;<%-- <form:errors
															path="status" cssClass="alert-danger" /> --%> <span
														class="alert-danger" id="statusError"></span>
													</span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Original
													Due</label>
												<div class="col-md-9">
													<form:input class="form-control input-sm"
														path="originalDue" id="status" readonly="true"
														style="background-color:white;text-transform: uppercase;"></form:input>
													<span class="help-block">&nbsp;<%-- <form:errors
															path="originalDue" cssClass="alert-danger" /> --%> <span
														class="alert-danger" id="originalDueError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Previous
													Credits </label>
												<div class="col-md-9">
													<form:input path="previousCredits"
														class="form-control input-sm" readonly="true"
														style="background-color:white;text-transform: uppercase;" />
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Enroll
													Amount</label>
												<div class="col-md-9">
													<form:input class="form-control input-sm"
														path="enrollAmount" id="enroll" readonly="true"
														style="background-color:white;text-transform: uppercase;"></form:input>
													<span class="help-block">&nbsp;<%-- <form:errors
															path="enrollAmount" cssClass="alert-danger" /> --%> <span
														class="alert-danger" id="enrollAmountError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Down
													Payment </label>
												<div class="col-md-9">
													<c:choose>
														<c:when test="${ippForm.status == 'PENDING'}">
															<form:input path="downPayment"
																class="form-control input-sm"
																style="background-color:white;text-transform: uppercase;" id="down"
																onchange="downpay()" onfocus="downpay1()" />
														</c:when>
														<c:otherwise>
															<form:input path="downPayment"
																class="form-control input-sm" readonly="true"
																style="background-color:white;" />
														</c:otherwise>
													</c:choose>
													<%-- <form:input path="downPayment"
														class="form-control input-sm"
														style="background-color:white;" id="down"
														onchange="downpay()" /> --%>
													<span class="help-block">&nbsp;<form:errors
															path="downPayment" cssClass="alert-danger" /><span
														class="alert-danger" id="downPaymentError">${errorMessage}</span>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Installment
													Amount </label>
												<div class="col-md-9">
													<c:choose>
														<c:when test="${ippForm.status == 'PENDING'}">
															<form:input path="installmentAmount" id="installment"
																class="form-control input-sm"
																style="background-color:white;text-transform: uppercase;" onchange="instpay()"
																onfocus="instpay1()" />
														</c:when>
														<c:otherwise>
															<form:input path="installmentAmount"
																class="form-control input-sm" readonly="true"
																style="background-color:white;text-transform: uppercase;" />
														</c:otherwise>
													</c:choose>
													<%-- <form:input path="installmentAmount" id="installment"
														class="form-control input-sm"
														style="background-color:white;" onchange="instpay()" /> --%>
													<span class="help-block">&nbsp;<%-- <form:errors
															path="noOfPayments" cssClass="alert-danger" /> --%> <span
														class="alert-danger" id="installmentAmountError"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Number
													of Payments</label>
												<div class="col-md-9">
													<c:choose>
														<c:when test="${ippForm.status == 'PENDING'}">
															<form:input path="noOfPayments"
																class="form-control input-sm"
																style="background-color:white;text-transform: uppercase;" onchange="payments()"
																onfocus="payments1()" />
														</c:when>
														<c:otherwise>
															<form:input path="noOfPayments"
																class="form-control input-sm" readonly="true"
																style="background-color:white;" />
														</c:otherwise>
													</c:choose>
													<%-- <form:input path="noOfPayments"
														class="form-control input-sm"
														style="background-color:white;" onchange="payments()" /> --%>
													<span class="help-block">&nbsp;<%-- <form:errors
															path="noOfPayments" cssClass="alert-danger" /> --%> <span
														class="alert-danger" id="noOfPaymentsError"></span>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label label-sm col-md-3">Start
													On </label>
												<div class="col-md-9">
													<form:input path="startDate" class="form-control input-sm"
														style="background-color:white;text-transform: uppercase;" readonly="true" />
												</div>
											</div>
										</div>
									</div>
									<div class="row" style="padding-top: 30px;">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-10 col-md-10"
													style="padding-left: 20px;">
													<input type="submit" class="btn green btn-sm"
														onclick="return update()"
														formaction="${pageContext.request.contextPath}/ippUpdate/${ippForm.violation.violationId}"
														style="width: 78px; margin: 2px;" value="UPDATE"
														formmethod="post" /> <input class="btn green btn-sm"
														type="submit"
														formaction="${pageContext.request.contextPath}/ippDetails/${ippForm.violation.violationId}"
														class="btn green btn-sm" style="width: 78px; margin: 2px;"
														value="CANCEL" formmethod="get" onclick="cancel()" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${ippForm.noOfPayments}"
		id="originalPayments" /> <input type="hidden"
		value="${ippForm.installmentAmount}" id="originalinstallment" /> <input
		type="hidden" value="${ippForm.downPayment}" id="originaldownPayment" />
</div>
<script>
function downpay() {
	var downPayment = parseFloat(document.getElementById("down").value);
	var installmentAmount = parseFloat(document
			.getElementById("installment").value);
	var enrollAmount = parseFloat(document.getElementById("enroll").value);
	var originaldown = parseFloat(document
			.getElementById("originaldownPayment").value);
	var originalinstallment = parseFloat(document
			.getElementById("originalinstallment").value);
	var originalPayments = document.getElementById("originalPayments").value;
	var due = parseFloat(enrollAmount - downPayment);
	document.getElementById('installmentAmountError').innerHTML = '';
	document.getElementById('noOfPaymentsError').innerHTML = '';
	var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
	if (isNaN(downPayment) == false) {
		if (downPayment > enrollAmount) {
			document.getElementById('downPaymentError').innerHTML = 'Down Payment cannot be greater than Enroll Amount';
		} else if (downPayment < downPayAmt) {
			document.getElementById('downPaymentError').innerHTML = 'Down Payment should be greater than or equals to 30 percent of Enroll Amount';
		} else if (0 == due) {
			document.getElementById('downPaymentError').innerHTML = '';
			document.getElementById("installment").value = 0.00;
			document.getElementById("noOfPayments").value = 0;
		} else {
			document.getElementById('downPaymentError').innerHTML = '';
			document.getElementById("installment").value = (enrollAmount - downPayment) / (2);
			document.getElementById("noOfPayments").value = 3;
		}
	} else if (downPayment < 0 || isNaN(downPayment) == true) {
		document.getElementById('downPaymentError').innerHTML = 'Down Payment should be greater than or equals to 30 percent of Enroll Amount';
	}
}
function instpay() {
	var downPayment = parseFloat(document.getElementById("down").value);
	var enrollAmount = parseFloat(document.getElementById("enroll").value);
	var installmentAmount = parseFloat(document
			.getElementById("installment").value);
	var originalinstallment = parseFloat(document
			.getElementById("originalinstallment").value);
	var originalPayments = parseFloat(document
			.getElementById("originalPayments").value);
	document.getElementById('downPaymentError').innerHTML = '';
	document.getElementById('noOfPaymentsError').innerHTML = '';
	var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
	if (isNaN(installmentAmount) == false && installmentAmount >= 0) {
		if ((downPayment + installmentAmount) > enrollAmount) {
			document.getElementById('installmentAmountError').innerHTML = 'Installment Amount should be less than Enroll Amount minus Downpayment';
		} else if (installmentAmount == 0) {
			document.getElementById('installmentAmountError').innerHTML = '';
			document.getElementById("noOfPayments").value = 1;
			document.getElementById("down").value = enrollAmount;
		} else {
			document.getElementById('installmentAmountError').innerHTML = '';
			if (installmentAmount == originalinstallment) {
				document.getElementById("noOfPayments").value = originalPayments;
			} else {
				document.getElementById("noOfPayments").value = parseFloat(Math
						.floor(((enrollAmount - downPayAmt) / installmentAmount)
								.toFixed(2)) + 1);
			}
			document.getElementById("down").value = downPayAmt
					+ ((enrollAmount - downPayAmt) % installmentAmount);
		}
	} else {
		document.getElementById('installmentAmountError').innerHTML = 'Installment Amount should not be empty or negative';
	}
}
function payments() {
	document.getElementById('installmentAmountError').innerHTML = '';
	document.getElementById('downPaymentError').innerHTML = '';
	var payments = document.getElementById("noOfPayments").value;
	var originalinstallment = parseFloat(document
			.getElementById("originalinstallment").value);
	var originalPayments = parseFloat(document
			.getElementById("originalPayments").value);
	var downPayment = parseFloat(document.getElementById("down").value);
	var enrollAmount = parseFloat(document.getElementById("enroll").value);
	var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
	if (isNaN(payments) == false && payments > 0) {
		if (1 == payments) {
			document.getElementById("installment").value = 0.00;
			document.getElementById("down").value = enrollAmount;
		} else {
			if (downPayment == enrollAmount) {
				document.getElementById("installment").value = ((enrollAmount - downPayAmt) / (payments - 1))
				.toFixed(2);
				document.getElementById("down").value = downPayAmt;
			}else{
				document.getElementById("installment").value = ((enrollAmount - downPayment) / (payments - 1))
				.toFixed(2);
			}
			
		}
	} else {
		document.getElementById("installment").value = 0.00;
		document.getElementById('noOfPaymentsError').innerHTML = 'No of payments should be greater than zero';
	}

}

function payments1() {
	document.getElementById('noOfPaymentsError').innerHTML = '';
}
function downpay1() {
	document.getElementById('downPaymentError').innerHTML = '';
}
function instpay1() {
	document.getElementById('installmentAmountError').innerHTML = '';
}

function cancel() {
	var originaldown = parseFloat(document
			.getElementById("originaldownPayment").value);
	var originalinstallment = parseFloat(document
			.getElementById("originalinstallment").value);
	var originalPayments = parseFloat(document
			.getElementById("originalPayments").value);
	document.getElementById("installment").value = originalinstallment;
	document.getElementById("noOfPayments").value = originalPayments;
	document.getElementById("down").value = originaldown;
}

function update() {
	var downPayment = parseFloat(document.getElementById("down").value);
	var installmentAmount = parseFloat(document
			.getElementById("installment").value);
	var enrollAmount = parseFloat(document.getElementById("enroll").value);
	var downPayAmt = Math.ceil(((enrollAmount * (0.30)).toFixed(2)));
	var payments = document.getElementById("noOfPayments").value;
	if (downPayment > enrollAmount) {
		document.getElementById('downPaymentError').innerHTML = 'Down Payment cannot be greater than Enroll Amount';
		return false;
	} else if (downPayment < downPayAmt || downPayment < 1
			|| isNaN(downPayment) == true) {
		document.getElementById('downPaymentError').innerHTML = 'DownPayment should be greater than or equal to 30 percentage of enrollAmount';
		return false;
	} else if ((downPayment + installmentAmount) > enrollAmount
			|| installmentAmount < 0) {
		document.getElementById('installmentAmountError').innerHTML = 'Installment amount should be less than Enroll Amount minus Downpayment';
		return false;
	} else if (payments < 1) {
		document.getElementById('noOfPaymentsError').innerHTML = 'No of payments should be greater than zero';
		return false;
	}
}
</script>