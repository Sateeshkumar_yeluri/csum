<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message code="lbl.sidemenu.tab.review" /></span></li>
			</ul>
			<div style="margin-top: 10px">
				<a onclick="enablePageLoadBar()"
					style="text-decoration: none; color: #fff;"
					href="${pageContext.request.contextPath}/violationView/${violationId}"><span
					class="btn blue-dark btn-sm"
					style="width: 65px; float: right; margin-right: 10px;">BACK</span></a>
			</div>
			<div style="text-align: center;">
				<span style="font-size: 16px;"> <b>License Number:</b> <span
					style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>
					&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 17px"><b>Violation
							Number:</b></span><span style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
			</div>
		</div>
		<div class="row" style="margin: 0px;">
			<div class="col-md-12 col-sm-12">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="col-md-3 col-sm-3"
							style="padding-left: 0px; padding-right: 0px;">
							<!-- BEGIN Portlet PORTLET-->
							<div class="portlet box blue-dark bg-inverse"
								style="margin: 10px 0 0 10px;">
								<div class="portlet-title">
									<div class="caption">Correspondence</div>
								</div>
								<div class="portlet-body" style="padding-left: 6px;">
									<div class="row">
										<div class="col-md-12">
											<form:form class="form-horizontal"
												action="${pageContext.request.contextPath}/saveCorrespondence/${violationId}"
												method="POST" modelAttribute="reviewProcess">
												<div class="form-actions">
													<div class="tab-content">
														<div class="tab-pane active" id="tab11">
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="control-label label-sm col-md-5">Suspends:</label>
																		<div class="col-md-7">
																			<form:select path="suspends"
																				class="form-control select2" id="suscode">
																				<form:option value="0" label="Select an Option"></form:option>
																				<c:forEach items="${suspendedCodes}"
																					var="suspendedcode">
																					<form:option value="${suspendedcode.id}"
																						label="${suspendedcode.code}-${suspendedcode.fullNm}-${suspendedcode.days}"></form:option>
																				</c:forEach>
																			</form:select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="control-label label-sm col-md-5"
																			style="align: left;">Correspondence:</label>
																		<div class="col-md-7">
																			<form:select path="correspondence"
																				class="form-control select2" id="correscode">
																				<form:option value="0" label="Select an Option"></form:option>
																				<c:forEach items="${correspondenceCodes}"
																					var="correspcode">
																					<form:option value="${correspcode.id}"
																						label="${correspcode.typCd}-${correspcode.fullName}"></form:option>
																				</c:forEach>
																			</form:select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<div class="form-group" style="padding-left: 15px;">
																	<label class="icheckbox_square-blue-dark checked">
																		<form:checkbox id="letterSent" path="letterSent"
																			value="Yes"></form:checkbox><span></span> Letter Sent
																	</label>
																</div>
															</div>
															<%-- <div style="padding-left:6px;">
															<form:input path="letterSent" id="letterSent" type="checkbox" name="letterSent"
																value="yes">Letter Sent
														</form:input>
														</div> --%>
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">

																		<label class="control-label label-sm col-md-5">Comments:</label>
																		<div class="col-md-7">
																			<form:select path="commentsInfoID"
																				class="bs-select form-control" id="notes"
																				onchange="append_Comment()">
																				<form:option value="" label="Select an Option"></form:option>
																				<c:forEach items="${commentDetails}" var="note">
																					<form:option value="${note.comment}"
																						label="${note.comment}"></form:option>
																				</c:forEach>
																			</form:select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="form-group">
																	<div class="col-md-11">
																		<form:textarea name="markdown" path="noteedited"
																			id="noteedit" rows="5" cols="10"
																			style="width:100%;margin-left:20px;text-transform: uppercase;"></form:textarea>
																	</div>
																</div>
															</div>
															<div class="row">
																<div style="margin-top: 20px; margin-left: 140px;">
																	<button type="button" id="myBtn"
																		class="btn blue-dark btn-sm" style="width: 87px;">COMPLETE</button>
																</div>
																<div id="myModal" class="modal">
																	<div class="modal-content" style="width: 20%;">
																		<p style="text-align: center;">Are you sure you
																			want to confirm the details?</p>
																		<div class="modal-body" align="center">
																			<button type="submit" class="btn blue-dark btn-sm"
																				onclick="this.disabled=true;this.form.submit();">CONFIRM</button>
																			<a href="#">
																				<button type="button" id="cancel"
																					class="btn blue-dark btn-sm">CANCEL</button>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
															<%-- <div class="row">
																<div id="recallList">
																	<div class="table-responsive scroller">
																		<table class="table table-hover table-bordered "
																			style="margin-top: 20px; margin-left: 10px;">
																			<thead>
																				<tr style="background-color: #f5f3eb;">
																					<th style="color: #32c5d2"><b>Correspondence
																							Recall List</b></th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${citationCorresp}"
																					var="citationCorresp">
																					<tr class="odd gradeX">
																						<c:if
																							test="${not empty citationCorresp.correspondence.correspCode.fullName}">
																							<c:if
																								test="${citationCorresp.status == 'Sufficient'}">
																								<td style="text-transform: uppercase;"><c:out
																										value="${citationCorresp.correspondence.correspCode.typCd} - ${citationCorresp.correspondence.correspCode.fullName}"></c:out></td>
																								<td style="text-transform: uppercase;"><button
																										type="button" id="sufficientBtn"
																										data-item='{"correspondenceTypeCode":"${citationCorresp.correspondence.correspCode.typCd}","correspondenceId":"${citationCorresp.id}","citationId":"${citationCorresp.citation.citationId}"}'
																										class="btn blue-dark btn-sm"
																										style="width: 105px; padding-left: 5px; padding-right: 5px;">RECALL
																										LETTER</button></td>
																							</c:if>
																						</c:if>
																					</tr>
																				</c:forEach>
																				<div id="sufficientModal" class="modal">
																					<div class="modal-content" style="width: 20%;">
																						<p id="modalConfText" style="text-align: center;">Do
																							you really want to recall letter - ?</p>
																						<div class="modal-body" align="center">
																							<a id="modalConfBtn"
																								href="${pageContext.request.contextPath}/recallCorresp/">
																								<button type="button" class="btn blue-dark btn-sm"
																									onclick="this.disabled=true;">CONFIRM</button>
																							</a> <a href="#">
																								<button type="button" id="sufCancel"
																									class="btn blue-dark btn-sm">CANCEL</button>
																							</a>
																						</div>
																					</div>
																				</div>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div> --%>
														</div>
													</div>
													<span class="help-block">&nbsp;</span>

												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6"
							style="padding-left: 0px; padding-right: 0px;">
							<!-- BEGIN Portlet PORTLET-->
							<div class="portlet box blue-dark bg-inverse"
								style="margin: 10px 0 0 10px;">
								<div class="portlet-title">
									<div class="caption">Ticket Summary</div>
								</div>
								<div class="portlet-body">
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="portlet box blue-dark bg-inverse">
												<div class="portlet-title"
													style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>VIOLATION</b>
													</div>
												</div>
												<div class="portlet-body " style="padding: 6px;">

													<table style="font-size: 13px;">
														<tr>
															<th><b>Violation&nbsp;Number:&nbsp;</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><b><c:out
																			value="${violationId}" /></b></font></td>
														</tr>
														<tr>
															<th><b>Status:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase; color: #FEFDFD; background-color: #F50C2D;">
																	<b><c:out value="${violationForm.status}" /></b>
															</font></td>
														</tr>
														<tr>
															<th><b>Violation:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;">
																	<c:out
																		value="${violationForm.violationCode.description}" />
															</font></td>
														</tr>
														<tr>
															<th><b>Violation Code:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.violationCode.code}" /></font></td>
														</tr>
														<tr>
															<th><b>Issued:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.dateOfViolation}" />&nbsp;<c:out
																		value="${dayOfWeek}" /></font></td>
														</tr>
														<tr>
															<th><b>Issued Time:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.timeOfViolation}" /></font></td>
														</tr>
														<%-- <tr>
															<th><b>Processed:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.processDate}" />&nbsp;<c:out
																		value="${processDay}" /></font></td>
														</tr> --%>
														<tr>
															<th><b>Vehicle:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${vehicle}" /></font></td>
														</tr>
														<tr>
															<th><b>Location:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.locationOfViolation}" /></font></td>
														</tr>
														<tr>
															<th><b></b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="" /></font></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="portlet box blue-dark bg-inverse"
												style="margin-bottom: 20px;">
												<div class="portlet-title"
													style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>VIOLATOR NAME AND ADDRESS</b>
													</div>
												</div>
												<div class="portlet-body " style="padding: 6px;">
													<table style="font-size: 13px;">
														<tr>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${name}" /></font></td>
														</tr>
														<tr>
															<td><font
																style="font-weight: 400; text-transform: uppercase;">
																	<c:out
																		value="${violationForm.patron.address.formattedAddress}" />
															</font></td>
														</tr>
													</table>
												</div>
											</div>
											<div class="portlet box blue-dark bg-inverse"
												style="padding-top: 5px;">
												<div class="portlet-title"
													style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>VIOLATION NOTES</b>
													</div>
												</div>
												<div class="portlet-body" style="padding: 6px;">
													<table style="font-size: 13px;">
														<tr>
															<td style="text-transform: uppercase;"><font
																style="font-weight: 400;"><c:out
																		value="${violationForm.comments[0].formattedCreatedAtNoss}" /></font></td>
														</tr>
													</table>
												</div>
												<div class="portlet-body scroller"
													style="padding: 6px; overflow: scroll; height: 100px;">
													<table style="font-size: 13px;">
														<tr>
															<td style="text-transform: uppercase;"><font
																style="font-weight: 400;"><c:out
																		value="${violationForm.comments[0].comment}" /></font></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="portlet box blue-dark bg-inverse">
												<div class="portlet-title"
													style="padding: 0px 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>ISSUANCE INFORMATION</b>
													</div>
												</div>
												<div class="portlet-body " style="padding: 6px;">
													<table style="font-size: 13px;">
														<tr>
															<th><b>Badge:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${violationForm.employeeNumber}" /></font></td>
														</tr>
														<tr>
															<th><b>Agency:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${Agency}" /></font></td>
														</tr>
														<tr>
															<th><b>Title:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="${title}" /></font></td>
														</tr>
														<tr>
															<th><b>Division:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="" /></font></td>
														</tr>
														<tr>
															<th><b>RD:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:out
																		value="" /></font></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="portlet box blue-dark bg-inverse">
												<div class="portlet-title"
													style="padding: 0 6px; margin-bottom: -3px; height: 27px; min-height: 10px;">
													<div class="caption"
														style="width: -10px; font-size: 12px; padding-top: 2px; padding-bottom: 1px;">
														<b>FINANCIAL</b>
													</div>
												</div>
												<div class="portlet-body " style="padding: 6px;">
													<!--Fine, Penalty 1, Penalty 2, Penalty 3, Penalty 4,Penalty 5, Reduction, Total Due, Unapplied Amt -->
													<table style="font-size: 13px;">
														<tr>
															<th><b>Fine:</b></th>
															<td></td>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:if
																		test="${violationForm.fineAmount!='' && violationForm.fineAmount!=null}">
																		<c:out value="${violationForm.fineAmount}" />
																	</c:if> </font></td>
														</tr>
														<tr>
															<th><b>Total Penalties:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;">
																	<c:choose>
																		<c:when test="${violation.totalPenaltyAmount == '0' }">
																			<td>$<c:out value="0.00" /></td>
																		</c:when>
																		<c:otherwise>
																			<td>$<c:out
																					value="${violation.totalPenaltyAmount}" /></td>
																		</c:otherwise>
																	</c:choose>
															</font></td>
														</tr>
														<tr>
															<th><b>Total Amount Paid:</b></th>
															<td><font
																style="font-weight: 400; text-transform: uppercase;">
																	<c:choose>
																		<c:when test="${totalPaid == '0' }">
																			<td>$<c:out value="0.00" /></td>
																		</c:when>
																		<c:otherwise>
																			<td>$<c:out value="${totalPaid}" /></td>
																		</c:otherwise>
																	</c:choose>
															</font></td>
														</tr>
														<tr>
															<th><b>Amount Paid Date:</b></th>
															<td></td>
															<td><font
																style="font-weight: 400; text-transform: uppercase;"><c:if
																		test="${paidDate!='' && paidDate!=NULL}">
																		<c:out value="${paidDate}" />
																	</c:if> </font></td>
														</tr>
														<tr>
															<th><b>Total Due:</b></th>
															<td></td>
															<td><font
																style="font-weight: 400; text-transform: uppercase; color: #FEFDFD; background-color: #F50C2D;"><c:if
																		test="${violationForm.totalDue!='' && violationForm.totalDue!=NULL}">$<c:out
																			value="${violationForm.totalDue}" />
																	</c:if> </font></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="portlet box blue-dark bg-inverse "
								style="margin: 10px 0 0 10px;">
								<div class="portlet-title">
									<div class="caption">Ticket History</div>
								</div>
								<div class="portlet-body">
									<div class="table-responsive scroller">
										<table id="allDetailsTbl"
											class="table table-hover table-bordered ">
											<thead>
												<tr style="background-color: #f5f3eb;">
													<th style="color: #004b85;" width="9%"><b>Type</b></th>
													<th style="color: #004b85;" width="64%"><b>Data</b></th>
													<th style="color: #004b85;" width="10%"><b>Updated&nbsp;By</b></th>
													<th style="color: #004b85;" width="17%"><b>Updated&nbsp;Date</b></th>
												</tr>
											</thead>
											<tbody>
												<c:if test="${not empty historyBean.penaltyHistory }">
													<c:forEach items="${historyBean.penaltyHistory}"
														var="penalty">
														<tr>
															<td><c:out value="PENALTY" /></td>
															<td style="text-transform: uppercase;">&#36;<c:out
																	value="${penalty.violation.fineAmount}" />, <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty1}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty1}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/<c:out
																			value="${penalty.penaltyCode.formattedpenalty1}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty2}">
																								&#36;<c:out
																			value="${penalty.penaltyCode.penalty2}" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:when>
																	<c:otherwise>
																								&#36;<c:out value="0.00" />/
																								<c:out
																			value="${penalty.penaltyCode.formattedpenalty2}" />&#44;
																							</c:otherwise>
																</c:choose> <c:choose>
																	<c:when
																		test="${not empty penalty.penaltyCode.penalty3}">
																							&#36;<c:out
																			value="${penalty.penaltyCode.penalty3}" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:when>
																	<c:otherwise>
																							&#36;<c:out value="0.00" />/
																							<c:out
																			value="${penalty.penaltyCode.formattedpenalty3}" />&#44;
																							</c:otherwise>
																</c:choose>$<c:out value="${penalty.violation.totalDue}" />&nbsp;
															</td>
															<c:choose>
																<c:when test="${penalty.updatedAt == null}">
																	<td style="text-transform: uppercase;"><c:out
																			value="${penalty.createdBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${penalty.formattedCreatedAt}" /></td>
																</c:when>
																<c:otherwise>
																	<td style="text-transform: uppercase;"><c:out
																			value="${penalty.updatedBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${penalty.formattedUpdatedAt}" /></td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.paymentHistory }">
													<c:forEach items="${historyBean.paymentHistory}"
														var="paymentList">
														<c:forEach items="${paymentList}" var="payment">
															<tr>
																<td><c:out value="PAYMENTS" /></td>
																<td style="text-transform: uppercase;"><c:if
																		test="${not empty payment.amount}">$<c:out
																			value="${payment.amount}" />
																	</c:if> <c:if test="${not empty payment.paymentDate}">/
																		<c:out value="${payment.paymentDate}" />,</c:if> <c:choose>
																		<c:when test="${payment.paymentSource == 'P'}">
																			<c:out value="PAY-IN-PERSON" />
																		</c:when>
																		<c:when test="${payment.paymentSource == 'M'}">
																			<c:out value="PAY-BY-MAIL" />
																		</c:when>
																		<c:otherwise>
																			<c:out value="PAY-BY-WEB" />
																		</c:otherwise>
																	</c:choose> <c:if
																		test="${not empty payment.paymentMethod.description}">/<c:out
																			value="${payment.paymentMethod.description}" />
																	</c:if> <c:if test="${not empty payment.account}">,
																		<c:out value="${payment.account}" />
																	</c:if> <c:if test="${not empty payment.processedOn}">,
																		<c:out value="${payment.processedOn}" />
																	</c:if> <c:if test="${not empty payment.processedBy}">/
																		<c:out value="${payment.processedBy}" />
																	</c:if>,<c:if test="${not empty payment.overPaid}">$<c:out
																			value="${payment.overPaid}" />, </c:if> <c:if
																		test="${not empty payment.totalDue}">$<c:out
																			value="${payment.totalDue}" />
																	</c:if></td>
																<c:choose>
																	<c:when test="${payment.updatedAt == null}">
																		<td style="text-transform: uppercase;"><c:out
																				value="${payment.createdBy}" /></td>
																		<td><c:out value="${payment.formattedCreatedAt}" /></td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-transform: uppercase;"><c:out
																				value="${payment.updatedBy}" /></td>
																		<td><c:out value="${payment.formattedUpdatedAt}" /></td>
																	</c:otherwise>
																</c:choose>
															</tr>
														</c:forEach>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.suspendHistory }">
													<c:forEach items="${historyBean.suspendHistory}"
														var="suspendList">
														<c:forEach items="${suspendList}" var="suspend">
															<tr>
																<td><c:out value="SUSPENDS" /></td>
																<td style="text-transform: uppercase;"><c:if
																		test="${not empty suspend.suspendedCodes.description}">
																		<c:out value="${suspend.suspendedCodes.description}" />
																	</c:if> <c:if test="${not empty suspend.suspendedCodes.code}">,
																		<c:out value="${suspend.suspendedCodes.code}" />
																	</c:if> <c:if test="${not empty suspend.formattedSuspendDate}">,
																		<c:out value="${suspend.formattedSuspendDate}" />
																	</c:if> <c:if test="${not empty suspend.formattedProcessedOn}">,
																		<c:out value="${suspend.formattedProcessedOn}" />
																	</c:if> <c:if test="${not empty suspend.reduction}">,
																		 $<c:out value="${suspend.reduction}" />,
																		</c:if>$<c:out value="${suspend.totalDue}" /></td>
																<c:choose>
																	<c:when test="${suspend.updatedAt == null}">
																		<td style="text-transform: uppercase;"><c:out
																				value="${suspend.createdBy}" /></td>
																		<td><c:out value="${suspend.formattedCreatedAt}" /></td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-transform: uppercase;"><c:out
																				value="${suspend.updatedBy}" /></td>
																		<td><c:out value="${suspend.formattedUpdatedAt}" /></td>
																	</c:otherwise>
																</c:choose>
															</tr>
														</c:forEach>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.correspHistory }">
													<c:forEach items="${historyBean.correspHistory}"
														var="corresList">
														<c:forEach items="${corresList}" var="correspond">
															<tr>
																<td><c:out value="CORRESPONDENCE" /></td>
																<td style="text-transform: uppercase;"><c:if
																		test="${not empty correspond.correspCode.correspDesc}">
																		<c:out value="${correspond.correspCode.correspDesc}" />
																	</c:if> <c:if
																		test="${not empty correspond.formattedCorresDate}">,
																		<c:out value="${correspond.formattedCorresDate}" />
																	</c:if> <c:if test="${not empty correspond.corresp_time}">,
																		<c:out value="${correspond.corresp_time}" />
																	</c:if> <c:if test="${correspond.letterSent==true}">,
																		<c:out value="Yes" />
																	</c:if> <c:if test="${correspond.letterSent==false}">,
																		<c:out value="No" />
																	</c:if></td>
																<c:choose>
																	<c:when test="${correspond.updatedAt == null}">
																		<td style="text-transform: uppercase;"><c:out
																				value="${correspond.createdBy}" /></td>
																		<td><c:out
																				value="${correspond.formattedCreatedAt}" /></td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-transform: uppercase;"><c:out
																				value="${payment.updatedBy}" /></td>
																		<td><c:out value="${payment.formattedUpdatedAt}" /></td>
																	</c:otherwise>
																</c:choose>
															</tr>
														</c:forEach>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.noticeHistory }">
													<c:forEach items="${historyBean.noticeHistory}"
														var="noticesList">
														<c:forEach items="${noticesList}" var="notice">
															<tr>
																<td><c:out value="NOTICES" /></td>
																<td style="text-transform: uppercase;"><c:if
																		test="${not empty notice.noticeType.fullNm}">
																		<c:out value="${notice.noticeType.fullNm}" />
																	</c:if> <c:if test="${not empty notice.formattedSentDate}">, <c:out
																			value="${notice.formattedSentDate}" />
																	</c:if>&nbsp; <c:if
																		test="${not empty notice.formattedProcessedDate}">, <c:out
																			value="${notice.formattedProcessedDate}" />, </c:if>&nbsp;
																	<c:out value="Yes" /></td>
																<c:choose>
																	<c:when test="${notice.updatedAt == null}">
																		<td style="text-transform: uppercase;"><c:out
																				value="${notice.createdBy}" /></td>
																		<td><c:out value="${notice.formattedCreatedAt}" /></td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-transform: uppercase;"><c:out
																				value="${notice.updatedBy}" /></td>
																		<td><c:out value="${notice.formattedUpdatedAt}" /></td>
																	</c:otherwise>
																</c:choose>
															</tr>
														</c:forEach>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.hearingHistory }">
													<c:forEach items="${historyBean.hearingHistory}"
														var="hearing">
														<tr>
															<td><c:out value="HEARINGS" /></td>
															<td style="text-transform: uppercase;"><c:if
																	test="${not empty hearing.hearingOfficer}">
																	<c:out value="${hearing.hearingOfficer}" />
																</c:if> <c:if test="${not empty hearing.formattedHearingDate}">
																	<c:out value="${hearing.formattedHearingDate}" />/<fmt:formatDate
																		type="time" timeStyle="short" pattern="hh:mm a"
																		value="${hearing.hearingTime}" />
																</c:if> <c:if test="${not empty hearing.formattedDispDate}">,
																				<c:out value="${hearing.formattedDispDate}" />
																</c:if> <c:if test="${not empty hearing.dispositionTime}">/
																				<fmt:formatDate type="time" timeStyle="short"
																		pattern="hh:mm a" value="${hearing.dispositionTime}" />
																</c:if> <c:if test="${not empty hearing.disposition}">,
																				<c:out value="${hearing.disposition}" />
																</c:if> <c:if test="${not empty hearing.totalDue}">&#36;
																				<c:out value="${hearing.totalDue}" />
																</c:if> <c:if test="${not empty hearing.scheduledAt}">/
																				<c:out value="${hearing.formattedScheduledAt}" />
																</c:if> <c:if test="${not empty hearing.scheduledBy}">/
																				<c:out value="${hearing.scheduledBy}" />/</c:if> <c:if
																	test="${not empty hearing.status}">,
																				<c:out value="${hearing.status}" />/</c:if> <c:if
																	test="${hearing.dateMailed != null}">/<c:out
																		value="MAIL DATE: "></c:out>
																	<c:out value="${hearing.formattedDateMailed}"></c:out>
																</c:if></td>
															<c:choose>
																<c:when test="${hearing.updatedAt == null}">
																	<td style="text-transform: uppercase;"><c:out
																			value="${hearing.createdBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${hearing.formattedCreatedAt}" /></td>
																</c:when>
																<c:otherwise>
																	<td style="text-transform: uppercase;"><c:out
																			value="${hearing.updatedBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${hearing.formattedUpdatedAt}" /></td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</c:if>
												<c:if test="${not empty historyBean.ippHistory}">
													<c:forEach items="${historyBean.ippHistory}" var="ipp">
														<tr>
															<td><c:out value="IPP" /></td>
															<td style="text-transform: uppercase;"><c:if
																	test="${not empty ipp.planNumber}">
																	<c:out value="${ipp.planNumber}" />
																</c:if> <c:if test="${not empty ipp.startDate}">,
																									<c:out value="${ipp.startDate}" />
																</c:if> <c:if test="${not empty ipp.enrollAmount}">,
																									<c:out value="${ipp.enrollAmount}" />
																</c:if> <c:if test="${not empty ipp.downPayment}">/
																									<c:out value="${ipp.downPayment}" />
																</c:if> <c:if test="${not empty ipp.installmentAmount}">,
																									<c:out value="${ipp.installmentAmount}" />
																</c:if> <c:if test="${not empty ipp.noOfPayments}">/
																									<c:out value="${ipp.noOfPayments}" />
																</c:if> <c:if test="${not empty ipp.status}">,
																									<c:out value="${ipp.status}" />
																</c:if> <c:if test="${not empty ipp.type}">/
																									<c:out value="${ipp.type}" />
																</c:if></td>
															<c:choose>
																<c:when test="${ipp.updatedAt == null}">
																	<td style="text-transform: uppercase;"><c:out
																			value="${ipp.createdBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${ipp.formattedCreatedAt}" /></td>
																</c:when>
																<c:otherwise>
																	<td style="text-transform: uppercase;"><c:out
																			value="${ipp.updatedBy}" /></td>
																	<td style="text-transform: uppercase;"><c:out
																			value="${ipp.formattedUpdatedAt}" /></td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3"
							style="padding-left: 0px; padding-right: 0px;">
							<div class="portlet box blue-dark bg-inverse "
								style="margin: 10px 0 0 10px;">
								<div class="portlet-title">
									<div class="caption">Attachments</div>
								</div>
								<div class="portlet-body" style="overflow-x: scroll;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.modal {
	display: none;
	position: fixed;
	z-index: 1;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	overflow: auto;
	background-color: rgb(0, 0, 0);
	background-color: rgba(0, 0, 0, 0.4);
}

.modal-content {
	background-color: #fefefe;
	margin: 15% auto;
	padding: 20px;
	border: 1px solid #888;
	width: 30%;
	height: 25%;
}
</style>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	$Spelling.SpellCheckAsYouType('noteedit');
	</script>
<script>
	 window.onload = function(e) {
		 $(".select2").select2();
	/*	if(document.getElementById("correscode").value!=""){
			document.getElementById("letterSent").checked = true;
		}		
		if (document.getElementById("optionsRadios34").checked
				|| document.getElementById("optionsRadios35").checked) {
			next_Question2();
		}

		if (document.getElementById("optionsRadios36").checked) {
			submit_Form2();
		} else if (document.getElementById("optionsRadios37").checked) {
			submit_Form2();
		}
		validatecheck();*/
		$('#allDetailsTbl').DataTable({
			"paging" : false,
			"info" : false,
			"searching" : false,
			"orderClasses" : false,			
			"columnDefs" : [ {
				"targets" : [ 0, 1, 2 ],
				"orderable" : false,
			} ],
			"order" : [ [ 3, "desc" ] ]
		});
		set_Comment();
		$('input[type="file"]').change(function(e){
			$("#viewLoadFiles").show();
			document.getElementById('fileTypeError').innerHTML = '';
			document.getElementById('blockui_uploadbtn').style.display = 'block';
	        var files = e.target.files;
	        $("#selectedFilesRow").empty();
	        for (var i = 0; i < files.length; i++)
	        {
	        	var newRowContent = "<tr><td style='text-transform: uppercase;'>"+files[i].name+"</td><td style='text-transform: uppercase;'>"+files[i].name.substr(files[i].name.lastIndexOf('.')+1).toUpperCase()+"</td><td style='text-transform: uppercase;'>"+Math.round(files[i].size/1024)+ " Kb"+"</td></tr>";
	        	$(newRowContent).appendTo($("#upload_files"));
	        	var fileTypeUploaded = files[i].name.substr(files[i].name.lastIndexOf('.')+1).toUpperCase();
	        	var validFileTypes = ['JPEG', 'PDF', 'JPG', 'TIFF', 'TIF','4X6M','PRC'];
	            if(validFileTypes.indexOf(fileTypeUploaded) == -1) {
	            	document.getElementById('blockui_uploadbtn').style.display = 'none';
	            	document.getElementById('fileTypeError').innerHTML = 'Invalid File Uploaded. Allowed PDF,JPG,TIFF type of files';
	            }
	        }
	    });
	}
/*
	function next_Question2() {
		document.getElementById("button1").style.display = 'block';
	}
	function submit_Form2() {
		var initialReview = "${initialReview}";
		if (initialReview == 'True') {
			document.getElementById("button22").style.display = 'none';
		} else {
			document.getElementById("button22").style.display = 'block';
		}
	}
	function validatePeriod() {
		var period = "${period}";
		if (period == 'true') {
			document.getElementById("button1").href = "#tab2";
		} else {
			document.getElementById("button1").href = "#tab6";
		}
	}
	function validatecheck() {
		if (document.getElementById("inlineCheckbox1").checked) {
			document.getElementById("button6").style.display = 'block';
			document.getElementById("button7").style.display = 'none';
		} else {
			document.getElementById("button6").style.display = 'none';
			var initialReview = "${initialReview}";
			if (initialReview == 'True') {
				document.getElementById("button7").style.display = 'none';
			} else {
				document.getElementById("button7").style.display = 'block';
			}
		}
	}*/
	function set_Comment() {
		var selectednote = document.getElementById("notes").value;

		document.getElementById("noteedit").value = selectednote;
	}
	function append_Comment() {
		var selectednote = document.getElementById("noteedit").value;
		var changednote = document.getElementById("notes").value;

		document.getElementById("noteedit").value = selectednote + " "
				+ changednote;
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
$("#myBtn").click(function () {
	$("#myModal").show();
});
$('#recallList').on('click', '#sufficientBtn', function(){
	var correspondenceTypeCode = $(this).data('item').correspondenceTypeCode;	
	var correspondenceId = $(this).data('item').correspondenceId;
	var violationId = $(this).data('item').violationId;
	$('#modalConfText').text('Do you really want to recall letter -'+correspondenceTypeCode+'?')
	$('#modalConfBtn').attr("href", "${pageContext.request.contextPath}/recallCorresp/"+violationId+"/"+correspondenceId);
	$("#sufficientModal").show();
});
$(window).click(function(e) {
	 if (e.target == modal) {
		 $("#myModal").hide();
		 $("#sufficientModal").hide();
	}
});
$("#cancel").click(function () {
	$("#myModal").hide();
});
$("#sufCancel").click(function () {
	$("#sufficientModal").hide();
});
});
 </script>
<style>
#noteedit {
	resize: none;
}
</style>