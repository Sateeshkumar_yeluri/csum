<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.notices.details" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit bordered">
					<div class="row">
						<div class="col-md-12" align="center">
							<span style="font-size: 16px;"><b>License Number:</b><span
								style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
								style="font-size: 17px"><b>Violation Number:</b></span><span
								style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
						</div>
					</div>
					<div class="row" style="margin: 0px;">
						<div class="col-md-12">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-blue sbold uppercase">Notices
										Details Information</span>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table  table-bordered table-hover order-column">
									<thead>
										<tr>
											<th>Violation Number#</th>
											<th>Notice Type</th>
											<th>Sent Date</th>
											<th>Processed Date</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${empty noticesList}">
												<tr>
													<td colspan="4" align="center"><b>No results found</b></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${noticesList}" var="notice">
													<tr class="odd gradeX">
														<!-- <td><label
													class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
														<input type="checkbox" class="checkboxes" value="1" /> <span></span>
												</label></td> -->
														<%-- <td><a
															style="color: blue; text-transform: uppercase;"
															href="<c:url value='../editnotices/${notice.id}' />"><c:out
																	value="${notice.violation.violationId}" /></a></td> --%>
														<td><c:out value="${notice.violation.violationId}" /></td>
														<td style="text-transform: uppercase;"><c:out
																value="${notice.noticeType.fullNm}" /></td>
														<td style="text-transform: uppercase;"><c:out
																value="${notice.formattedSentDate}" /></td>
														<td style="text-transform: uppercase;"><c:out
																value="${notice.formattedProcessedDate}" /></td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>