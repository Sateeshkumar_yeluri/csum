<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content" id="blockui_adminpayment_data"> 
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><span><spring:message code="lbl.page.bar.permit" /></span></li>
			</ul>
		</div>
		<div class="row" style="margin: 0px;">
			<div class="col-md-12 ">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="citationPortlet" class="portlet box blue-dark bg-inverse">
						<div class="portlet-title">
							<div class="caption">Visitor Permit Form</div>
						</div>
						<div class="portlet-body form">
							<c:if test="${not empty errorInfo}">
								<div class="alert alert-danger">
									<span>${errorInfo}</span>
								</div>
							</c:if>
							<div class="form-body">
								<form:form onsubmit="return validateForm();" id="permitPurchaseForm"
									action="${pageContext.request.contextPath}/visitorPermitPurchase"
									modelAttribute="permitForm" method="POST" autocomplete="on">
									<div class="row">
										<%-- <div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-3 control-label label-sm">Are
														you a:<span class="required">*</span></label>
													<div class="col-md-9">
														<div class="mt-radio-inline">
															<label class="mt-radio label-sm"> <form:radiobutton
																	path="patronType" id="optionsRadio1" value="Student" />Student
																<span></span>
															</label><label class="mt-radio label-sm"> <form:radiobutton
																	path="patronType" id="optionsRadio2" value="Employee" />Employee<span></span>
															</label> <label class="mt-radio label-sm"> <form:radiobutton
																	path="patronType" id="optionsRadio3" value="Visitor" />Visitor<span></span>
															</label>
														</div>
														<span class="help-block"><span class="alert-danger"
															id="patronTypeError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Student/Staff
														ID</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" id="csuId"
															placeholder="CSU ID Number" path="csuId"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
										</div> --%>
										<form:input type="hidden" path="patronType" value="Visitor"/>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Permit
														Type<span class="required">*</span></label>
													<div class="col-md-9" id="permitTypeDiv">
														<form:select class="form-control select2" id="permitType"
															path="permitType">
															<form:option value="" data-type=""
																data-value=''>Select an option</form:option>
															<form:option value="Guest Parking Permit" 
																data-value='{"fine":"5"}' data-type='daily'>Guest Parking Permit </form:option>
															<form:option value="Blue Hang Tag Scratcher Permit"
																data-value='{"fine":"60"}' data-type='monthly'>Blue Hang Tag Scratcher Permit </form:option>
															<form:option value="Gold Hang Tag Scratcher Permit"
																data-value='{"fine":"60"}' data-type='monthly'>Gold Hang Tag Scratcher Permit </form:option>
															<form:option value="Disabled Parking Permit"
																data-value='{"fine":"30"}' data-type='monthly'>Disabled Parking Permit </form:option>
														</form:select>
														<span class="help-block">&nbsp;<span class="alert-danger"
															id="permitTypeError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Licence
														Plate Number<span class="required">*</span>
													</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="licenceNumber" placeholder="Vehicle Licence Plate"
															id="licNum" style="text-transform: uppercase;"></form:input>
														<span class="help-block"><span class="alert-danger"
															id="licNumError"></span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Date
														Of Parking<span class="required">*</span>
													</label>
													<div class="col-md-9" style="padding: 0px;">
													<div class="col-md-12" style="padding: 0px;">
													<div class="col-md-9">
														<div class="input-group input-group-sm date date-picker"
															data-date-format="mm/dd/yyyy" data-date-start-date="0d"
															data-date-viewmode="years">
															<form:input path="startDate" class="form-control"
																id="strtDate" />
															<span class="input-group-btn">
																<button class="btn default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
															</span>
														</div>
														</div>
														<div  class="col-md-3">
															<form:input class="form-control input-sm"
															path="noOfMonths" placeholder="Months" readonly="true"
															id="noOfMonths" style="text-transform: uppercase;"></form:input>
														</div>
													</div>
													<span class="help-block">&nbsp;<span class="alert-danger"
															id="strtDateError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" id="timeSlotDiv">
													<label class="control-label label-sm col-md-3">Time
														Slot</label>
													<div class="col-md-9">
														<form:select class="form-control select2" id="timeSlots"
															style="font-size:13px;width:455px;" path="timeSlot">
															<form:option value="">Select an option</form:option>
															<form:option value="07:00:00_17:00:00">7:00AM-5:00PM</form:option>
															<form:option value="17:00:00_07:00:00">5:00PM-7:00AM</form:option>
														</form:select>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">First
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" path="firstName"
															placeholder="First Name" id="fnamePatron"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp; </span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Middle
														Name</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="middleName" placeholder="Middle Name"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp; </span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Last
														Name </label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" path="lastName"
															placeholder="Last Name"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block">&nbsp; </span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
    												<div class="form-group">
													<label class="control-label label-sm col-md-3">Amount<span
														class="required">*</span></label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															placeholder="Amount" path="amount" id="amount"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block"><span class="alert-danger"
															id="amountError"></span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<%-- <div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Payment
														Type</label>
													<div class="col-md-9">
														<form:select class="form-control select" id="paymentType"
															style="font-size:13px;"
															path="permitPayments.paymentType">
															<form:option value="" data-type="card">Select an option</form:option>
															<form:option value="card" data-type="card">Credit Card</form:option>
															<form:option value="payroll" data-type="payroll">Payroll</form:option>
														</form:select>
														<span class="help-block">&nbsp;</span>
													</div>
												</div>
											</div> --%>
											<form:input type="hidden" path="permitPayments.paymentType" value="card" id="paymentType"/>
											<div class="col-md-6" id="cardDiv">
												<!-- <div class="form-group">
													<label class="control-label label-sm col-md-3">Credit
														Card<span class="required">*</span>
													</label>
													<div class="col-md-9" style="padding: 0px;">
														<div class="col-md-6">
															<input class="form-control input-sm"
																placeholder="Credit Card Number" id="number"
																style="text-transform: uppercase;"></input>
														</div>
														<div class="col-md-2">
															<input class="form-control input-sm" placeholder="MM"
																id="mnth" style="text-transform: uppercase;"></input>
														</div>
														<div class="col-md-2">
															<input class="form-control input-sm" placeholder="YY"
																id="year" style="text-transform: uppercase;"></input>
														</div>
														<div class="col-md-2">
															<input class="form-control input-sm" placeholder="CVC"
																id="cvc" style="text-transform: uppercase;"></input>
														</div>
														<span class="help-block"><span class="alert-danger"
															id="numberError"></span></span> <span class="help-block"><span
															class="alert-danger" id="mnthError"></span></span><span
															class="help-block"><span class="alert-danger"
															id="yearError"></span></span> <span class="help-block"><span
															class="alert-danger" id="cvcError"></span></span>
													</div>
												</div> -->
												<div class="form-group form-md-line-input has-success" style="padding: 0px;">
															<label class="col-md-3 control-label"
																for="form_control_1"><b>Credit
																	Card</b></label>
															<div class="col-md-9">
																<div class="form-row">
																	<div id="card-element">
																		<!-- a Stripe Element will be inserted here. -->
																	</div>
																	<!-- Used to display form errors -->
																	<div id="card-errors"></div>
																	<span class="alert-danger" id="clearError">${errorMessage}</span>
																</div>
															</div>
														</div>
														<span class="help-block">&nbsp;</span>
											</div>
										</div>
										<div class="col-md-12" align="center">
											<input type="submit" class="btn blue-dark btn-sm" id="cardPayment"
												value="PROCESS PAYMENT" />
											<button type="button"
												onclick="window.location='${pageContext.request.contextPath}/permitPurchase';"
												class="btn blue-dark btn-sm">CANCEL</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
	$('#amount').keypress(function(e) {
		numericValidation(e);
	});
	$('#number').keypress(function(e) {
		numericValidation(e);
	});
	$('#mnth').keypress(function(e) {
		numericValidation(e);
	});
	$('#year').keypress(function(e) {
		numericValidation(e);
	});
	$('#cvc').keypress(function(e) {
		numericValidation(e);
	});
	$('#noOfMonths').keypress(function(e) {
		numericValidation(e);
	});
	/* $(".mt-radio").change(function(e) {
		if ($("#optionsRadio3").prop('checked')) {
			$("#csuId").val("");
			$("#csuId").attr("readonly", true);
			toggleVistors();
		} else {
			$("#csuId").attr("readonly", false);
			if ($("#optionsRadio1").prop('checked')) {
				toggleStudents();
			}else{
				toggleEmployee();
			}
		}
	}); */
	$(document).ready(function() {
	$("#permitType").change(function(e) {
		var type = $(this).find(':selected').data("type");
		var fine = $(this).find(':selected').data("value").fine;
		var permitType = $(this).val();
		$("#amount").val("$"+fine);
		if(type=='daily'){
			$("#timeSlotDiv").attr("style",'display:block');
		}else{
			$("#timeSlots").val("");
			$("#timeSlotDiv").attr("style",'display:none');
			if(type == 'monthly'){
				$("#noOfMonths").attr("readonly",false);
				$("#noOfMonths").val("1");
			}
		}
		if(permitType == 'Hang Tag Permit'){
			togglePayroll();
		}else if(permitType == 'Blue Hang Tag Scratcher Permit' || permitType == 'Gold Hang Tag Scratcher Permit'){
			toggleCard();
		}else{
			toggleCard();
		}
	});
	$("#strtDate").datepicker({
		format : 'mm/dd/yyyy'
	}).datepicker("setDate", 'today');
	$("#noOfMonths").change(function(){
		var months =$(this).val();
		if(Number(months)<=12 && Number(months)>0){
			$("#strtDateError").text("");
		var fine = $("#permitType").find(':selected').data("value").fine;
		/* alert(fine);
		var fineValue = fine.replace("$","");
		alert(fineValue); */
		var amount = Number(fine)*Number(months);
		$("#amount").val("$"+amount);
		}else if(Number(months)==0){
			$("#strtDateError").text("Number of months can not be zero");
		}else{
			$("#strtDateError").text("Number of months must be less than or equal to 12");
		}
	});
	});
	function togglePayroll(){
		$('#paymentType').empty();
		$('#paymentType').append($("<option></option>").attr("value","").text("Select an Option"));
		$('#paymentType').append($("<option></option>").attr("value","payroll").text("Payroll"));
		$('#cardDiv').attr("style",'display:none');
	}
	function toggleCard(){
		$('#paymentType').empty();
		$('#paymentType').append($("<option></option>").attr("value","").text("Select an Option"));
		$('#paymentType').append($("<option></option>").attr("value","card").text("Credit Card"));
		$('#cardDiv').attr("style",'display:block');
	}function togglePaymentMethods(){
		$('#paymentType').empty();
		$('#paymentType').append($("<option></option>").attr("value","").text("Select an Option"));
		$('#paymentType').append($("<option></option>").attr("value","card").text("Credit Card"));
		$('#paymentType').append($("<option></option>").attr("value","payroll").text("Payroll"));
		$('#cardDiv').attr("style",'display:block');
	}
	/* function toggleVistors() {
		$('#permitType > option').each(function() {
			var obj = $(this).data("value").V;
			if (obj == "Y") {
				$(this).show();
				$(this).prop('disabled', false);
			} else {
				$(this).hide();
				$(this).prop('disabled', true);
			}
		});
	} */
	function toggleVistors() {
		$('#permitType').empty();
		$('#permitType').append($("<option></option>").attr("value","").attr("data-value",'').attr("data-type",'').text("Select an Option"));
		$('#permitType').append($("<option></option>").attr("value","Guest Parking Permit").attr("data-value",'{"fine":"50"}').attr("data-type",'daily').text("Guest Parking Permit"));
		$('#permitType').append($("<option></option>").attr("value","Blue Hang Tag Scratcher Permit").attr("data-value",'{"fine":"60"}').attr("data-type",'monthly').text("Blue Hang Tag Scratcher Permit"));
		$('#permitType').append($("<option></option>").attr("value","Gold Hang Tag Scratcher Permit").attr("data-value",'{"fine":"60"}').attr("data-type",'monthly').text("Gold Hang Tag Scratcher Permit"));
		$('#permitType').append($("<option></option>").attr("value","Disabled Parking Permit").attr("data-value",'{"fine":"20"}').attr("data-type",'semester').text("Disabled Parking Permit"));
	} 
	function toggleEmployee() {
		$('#permitType').empty();
		$('#permitType').append($("<option></option>").attr("value","").attr("data-value",'').attr("data-type",'').text("Select an Option"));
		$('#permitType').append($("<option></option>").attr("value","Single Day Permit").attr("data-value",'{"fine":"5"}').attr("data-type",'daily').text("Single Day Permit"));
		$('#permitType').append($("<option></option>").attr("value","Hang Tag Permit").attr("data-value",'{"fine":"60"}').attr("data-type",'Monthly').text("Hang Tag Permit"));
		$('#permitType').append($("<option></option>").attr("value","Blue Hang Tag Scratcher Permit").attr("data-value",'{"fine":"60"}').attr("data-type",'monthly').text("Blue Hang Tag Scratcher Permit"));
		$('#permitType').append($("<option></option>").attr("value","Gold Hang Tag Scratcher Permit").attr("data-value",'{"fine":"60"}').attr("data-type",'monthly').text("Gold Hang Tag Scratcher Permit"));
		$('#permitType').append($("<option></option>").attr("value","Disabled Parking Permit").attr("data-value",'{"fine":"20"}').attr("data-type",'semester').text("Disabled Parking Permit"));
	}
	function toggleStudents() {
		$('#permitType').empty();
		$('#permitType').append($("<option></option>").attr("value","").attr("data-value",'').attr("data-type",'').text("Select an Option"));
		$('#permitType').append($("<option></option>").attr("value","Semester Decal Permit for MotorCycle").attr("data-value",'{"fine":"20"}').attr("data-type",'semester').text("Semester Decal Permit for MotorCycle"));
		$('#permitType').append($("<option></option>").attr("value","Semester Decal Permit for Auto").attr("data-value",'{"fine":"80"}').attr("data-type",'semester').text("Semester Decal Permit for Auto"));
		$('#permitType').append($("<option></option>").attr("value","Disabled Parking Permit").attr("data-value",'{"fine":"20"}').attr("data-type",'semester').text("Disabled Parking Permit"));
	}
	function validateForm() {
		var strtDate = $("#strtDate").val()
		var amount = $("#amount").val();
		var licNum = $("#licNum").val();
		var cvc = $("#cvc").val();
		var number = $("#number").val();
		var mnth = $("#mnth").val();
		var year = $("#year").val();
		var noOfMonths =$("#noOfMonths").val();
		var permitType = $("#permitType").val();
		/* if (!($("#optionsRadio1").prop("checked")
				|| $("#optionsRadio2").prop("checked") || $("#optionsRadio3")
				.prop("checked"))) {
			$("#patronTypeError").text("Please select Patron Type");
			return false;
		} else {
			$("#patronTypeError").text("");
		} */
		if ($.trim(permitType) == '' || $.trim(permitType).length == 0) {
			$("#permitTypeError").text("Please select Permit Type");
			return false;
		} else {
			$("#permitTypeError").text("");
		}
		if ($.trim(strtDate) == '' || $.trim(strtDate).length == 0) {
			$("#strtDateError").text("Parking Date should not be empty");
			return false;
		} else {
			$("#strtDateError").text("");
		}
		
		if($(this).find(':selected')!='' && $(this).find(':selected').data("type")=='Monthly'){
		if(Number(noOfMonths)<=12 && Number(noOfMonths)>0){
			$("#strtDateError").text("");
		}else if(Number(noOfMonths)==0){
			$("#strtDateError").text("Number of months can not be zero");
			return false;
		}else{
			$("#strtDateError").text("Number of months must be less than or equal to 12");
			return false;
		}
		}
		if ($.trim(amount) == '' || $.trim(amount).length == 0
				|| $.trim(amount) == 0) {
			$("#amountError").text("Amount should not be empty or zero");
			return false;
		} else {
			$("#amountError").text("");
		}
		if ($.trim(licNum) == '' || $.trim(licNum).length == 0) {
			$("#licNumError").text("Licence Number should not be empty");
			return false;
		} else {
			$("#licNumError").text("");
		}
		/* if ($.trim(number) == '' || $.trim(number).length == 0) {
			$("#numberError").text("Credit Card Number should not be empty");
			return false;
		} else {
			$("#numberError").text("");
		}
		if ($.trim(mnth) == '' || $.trim(mnth).length == 0) {
			$("#mnthError").text("Month should not be empty");
			return false;
		} else {
			$("#mnthError").text("");
		}
		if ($.trim(year) == '' || $.trim(year).length == 0) {
			$("#yearError").text("Year should not be empty");
			return false;
		} else {
			$("#yearError").text("");
		}
		if ($.trim(cvc) == '' || $.trim(cvc).length == 0) {
			$("#cvcError").text("CVC should not be empty");
			return false;
		} else {
			$("#cvcError").text("");
		} */
		return true;
	}

	function numericValidation(e) {
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}
	function validateSearchForm() {
		var searchText = $("#ticketNumber").val();
		if (searchText.length == 0) {
			$("#searchTextError").attr('style', "display:block");
			$("#searchTextError").text(
					"Please enter permit Number or License Number");
			return false;
		}
		return true;
	}
</script>

<script src="https://js.stripe.com/v3/"></script>

	<script type="text/javascript">
		//Create a Stripe client
		var stripe = Stripe('${stripePublishableKey}');

		// Create an instance of Elements
		var elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base : {
				color : '#32325d',
				lineHeight : '24px',
				fontFamily : '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing : 'antialiased',
				fontSize : '16px',
				'::placeholder' : {
					color : '#aab7c4'
				}
			},
			invalid : {
				color : '#fa755a',
				iconColor : '#fa755a'
			}
		};

		//Create an instance of the card Element
		var card = elements.create('card', {
			style : style
		});

		// Add an instance of the card Element into the `card-element` <div>
		card.mount('#card-element');

		// Handle real-time validation errors from the card Element.
		card.addEventListener('change', function(event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
				document.getElementById('cardPayment').disabled = false;
			} else {
				displayError.textContent = '';
			}
		});

		// Handle form submission
		var form = document.getElementById('permitPurchaseForm');
		form
				.addEventListener(
						'submit',
						function(event) {
							var paymentType = $("#paymentType").val()
							if(paymentType!=''&& paymentType == 'card'){
							event.preventDefault();
							$(this).find(':input[type=submit]').prop(
									'disabled', true);
							stripe
									.createToken(card)
									.then(
											function(result) {
												var cardAmt = document
														.getElementById('amount').value;
												 if (result.error) {
														// Inform the user if there was an error
														var errorElement = document
																.getElementById('card-errors');
														errorElement.textContent = result.error.message;
														document
																.getElementById('cardPayment').disabled = false;
													} else {
														// Send the token to your server
														stripeTokenHandler(result.token);
														document
																.getElementById('cardPayment').disabled = true;
													}
											});
							}
						});

		function stripeTokenHandler(token) {
			// Insert the token ID into the form so it gets submitted to the server
			var form = document.getElementById('permitPurchaseForm');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'stripeToken');
			hiddenInput.setAttribute('value', token.id);
			form.appendChild(hiddenInput);
			// Submit the form
			form.submit();

			App.blockUI({
				target : '#blockui_adminpayment_data'
			});
		}
	</script>
<style>
.StripeElement {
	background-color: white;
	padding: 8px 12px;
	border-radius: 4px;
	border: 1px solid transparent;
	box-shadow: 0 1px 3px 0 #e6ebf1;
	-webkit-transition: box-shadow 150ms ease;
	transition: box-shadow 150ms ease;
}

.StripeElement--focus {
	box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
	border-color: #fa755a;
}

.StripeElement--webkit-autofill {
	background-color: #fefde5 !important;
}

</style>