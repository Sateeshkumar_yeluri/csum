<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-container">
	<div class="page-content" id="blockui_adminpayment_data">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><span><spring:message code="lbl.page.bar.permit" /></span></li>
			</ul>
		</div>
		<div class="row" style="margin: 0px;">
			<div class="col-md-12 ">
				<div class="tabbable-line boxless tabbable-reversed">
					<div id="citationPortlet" class="portlet box blue-dark bg-inverse">
						<div class="portlet-title">
							<div class="caption">Free Permit Form</div>
						</div>
						<div class="portlet-body form">
							<c:if test="${not empty errorInfo}">
								<div class="alert alert-danger">
									<span>${errorInfo}</span>
								</div>
							</c:if>
							<div class="form-body">
								<form:form onsubmit="return validateForm();"
									id="permitPurchaseForm"
									action="${pageContext.request.contextPath}/saveFreePermit"
									modelAttribute="permitForm" method="POST" autocomplete="on">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Patron
														Type<span class="required">*</span>
													</label>
													<div class="col-md-9" id="patronTypeDiv">
														<form:select class="form-control select2" id="patronType"
															path="patronType">
															<form:option value="" data-type="" data-value=''>Select an option</form:option>
															<form:option value="Student">Student</form:option>
															<form:option value="Employee">Employee</form:option>
															<form:option value="Visitor">Visitor</form:option>
														</form:select>
														<span class="help-block">&nbsp;<span
															class="alert-danger" id="patronTypeError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Student/Staff
														ID</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" readonly="true"
															path="csuId" placeholder="CSU ID NUMBER" id="csuId"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block"><span class="alert-danger"
															id="csuIdError"></span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Permit
														Type<span class="required">*</span>
													</label>
													<div class="col-md-9" id="permitTypeDiv">
														<form:select class="form-control select2" id="permitType"
															path="permitType">
															<form:option value="" data-type="" data-value=''>Select an option</form:option>
															<form:option value="Daily Pass">Daily Pass</form:option>
															<form:option value="Monthly Pass">Monthly Pass</form:option>
															<%-- <form:option value="Guest Parking Permit"
																data-value='{"fine":"5"}' data-type='daily'>Guest Parking Permit </form:option>
															<form:option value="Blue Hang Tag Scratcher Permit"
																data-value='{"fine":"60"}' data-type='monthly'>Blue Hang Tag Scratcher Permit </form:option>
															<form:option value="Gold Hang Tag Scratcher Permit"
																data-value='{"fine":"60"}' data-type='monthly'>Gold Hang Tag Scratcher Permit </form:option>
															<form:option value="Disable Parking Permit"
																data-value='{"fine":"30"}' data-type='monthly'>Disable Parking Permit </form:option> --%>
														</form:select>
														<span class="help-block">&nbsp;<span
															class="alert-danger" id="permitTypeError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Reason</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm" path="comment"
															placeholder="Reason" id="reasonId"
															style="text-transform: uppercase;"></form:input>
														<span class="help-block"><span class="alert-danger"
															id="reasonIdError"></span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Permit
														Start Date<span class="required">*</span>
													</label>
													<div class="col-md-9">
														<div class="input-group input-group-sm date date-picker"
															data-date-format="mm/dd/yyyy" data-date-start-date="0d"
															data-date-viewmode="years">
															<form:input path="startDate" class="form-control"
																id="strtDate" />
															<span class="input-group-btn">
																<button class="btn default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
															</span>
														</div>
														<span class="help-block">&nbsp;<span
															class="alert-danger" id="strtDateError"></span></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label label-sm col-md-3">Licence
														Plate Number<span class="required">*</span>
													</label>
													<div class="col-md-9">
														<form:input class="form-control input-sm"
															path="licenceNumber" placeholder="Vehicle Licence Plate"
															id="licNum" style="text-transform: uppercase;"></form:input>
														<span class="help-block"><span class="alert-danger"
															id="licNumError"></span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12" align="center">
											<input type="submit" class="btn blue-dark btn-sm"
												id="cardPayment" value="SUBMIT" />
											<button type="button"
												onclick="window.location='${pageContext.request.contextPath}/freePermit';"
												class="btn blue-dark btn-sm">CANCEL</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/static/assets/global/plugins/jquery.min.js' />"
	type="text/javascript">
	
</script>

<script>
	$('#amount').keypress(function(e) {
		numericValidation(e);
	});
	$('#number').keypress(function(e) {
		numericValidation(e);
	});
	$('#mnth').keypress(function(e) {
		numericValidation(e);
	});
	$('#year').keypress(function(e) {
		numericValidation(e);
	});
	$('#cvc').keypress(function(e) {
		numericValidation(e);
	});
	$('#noOfMonths').keypress(function(e) {
		numericValidation(e);
	});
	/* $(".mt-radio").change(function(e) {
		if ($("#optionsRadio3").prop('checked')) {
			$("#csuId").val("");
			$("#csuId").attr("readonly", true);
			toggleVistors();
		} else {
			$("#csuId").attr("readonly", false);
			if ($("#optionsRadio1").prop('checked')) {
				toggleStudents();
			}else{
				toggleEmployee();
			}
		}
	}); */
	$(document).ready(function() {
		$("#patronType").change(function(e) {
			var type = $(this).val();
			//alert(type);
			if (type == 'Employee' || type == 'Student') {
				$("#csuId").attr("readOnly", false);
			} else {
				$("#csuId").val("");
				$("#csuId").attr("readOnly", "true");
			}
		});
		$("#strtDate").datepicker({
			format : 'mm/dd/yyyy'
		}).datepicker("setDate", 'today');
	});
	function validateForm() {
		var strtDate = $("#strtDate").val()
		var licNum = $("#licNum").val();
		var permitType = $("#permitType").val();
		if ($.trim(permitType) == '' || $.trim(permitType).length == 0) {
			$("#permitTypeError").text("Please select Permit Type");
			return false;
		} else {
			$("#permitTypeError").text("");
		}
		if ($.trim(strtDate) == '' || $.trim(strtDate).length == 0) {
			$("#strtDateError").text("Parking Date should not be empty");
			return false;
		} else {
			$("#strtDateError").text("");
		}
		if ($.trim(licNum) == '' || $.trim(licNum).length == 0) {
			$("#licNumError").text("Licence Number should not be empty");
			return false;
		} else {
			$("#licNumError").text("");
		}
		return true;
	}

	function numericValidation(e) {
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}
</script>

