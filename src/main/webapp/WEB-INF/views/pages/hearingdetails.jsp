<%@ page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- BEGIN CONTAINER -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
<br /> <br />
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar" style="margin: -70px 0px 10px;">
			<ul class="page-breadcrumb">
				<li><a href="${pageContext.request.contextPath}/violationView/${violationId}"><spring:message
							code="lbl.page.bar.violation.view" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.page.bar.hearing.details" /></span></li>
			</ul>
		</div>
		<%-- <div class="page-bar" style="margin-top: 30px;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/home"><span
						class="title"><spring:message code="lbl.home" /></span></a> <i
					class="fa fa-circle"></i></li>
				<li><span class="title"><spring:message
							code="lbl.page.bar.penalty.details" /></span></li>
			</ul>
		</div> --%>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
		<div class="portlet light portlet-fit bordered">
			<div class="row">
				<div class="col-md-12" align="center">
					<span style="font-size: 16px;"><b>License Number:</b><span
						style="text-transform: uppercase;">&nbsp;${licenseNumber}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
						style="font-size: 17px"><b>Violation Number:</b></span><span
						style="text-transform: uppercase;">&nbsp;${violationId}</span></span>
				</div>
			</div>
			<div class="row" style="margin: 0px;">
				<div class="col-md-12">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue sbold uppercase">Hearing
								Details Information</span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table  table-bordered table-hover order-column">
							<thead>
								<tr>
									<th>Violation Number#</th>
									<th>Hearing Officer</th>
									<th>Hearing<br>Date/Time
									</th>
									<th>Disposition<br>Date/Time
									</th>
									<th>Disposition</th>
									<th>Hearing Type</th>
									<th>Hearing Status</th>
									<th>Total Due</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
								<c:when test="${empty hearing}">
									<tr>
										<td colspan="6" align="center"
											style="text-transform: uppercase;"><b>No results
												found</b></td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<td style="text-transform: uppercase;"><b><c:out
															value="${hearing.violation.violationId}" /> </b></td>
												<td style="text-transform: uppercase;">												
												<c:out value="${hearing.hearingOfficer}" /></td>
												<td><c:out value="${hearing.formattedHearingDate}" />
													<c:if test="${not empty hearing.hearingTime}">
														<fmt:formatDate
															type="time" timeStyle="short" pattern="hh:mm a"
															value="${hearing.hearingTime}" />
													</c:if></td>
												<td><c:out value="${hearing.formattedDispDate}"></c:out><c:if
														test="${not empty hearing.dispositionTime}">/<fmt:formatDate
															type="time" timeStyle="short" pattern="hh:mm a"
															value="${hearing.dispositionTime}" />
													</c:if></td>
												<td style="text-transform: uppercase;"><c:out
															value="${hearing.disposition}" /></td>
												<td><c:out value="${hearing.type}" /></td>
												<td><c:out value="${hearing.status}" /></td>
												<td><c:if test="${not empty hearing.violation.totalDue}">/$<c:out
															value="${hearing.violation.totalDue}" />
													</c:if></td>
									</tr>
								</c:otherwise>
							</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		</div>
	</div>
</div>