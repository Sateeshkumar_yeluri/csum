<!-- BEGIN FOOTER -->
<div class="page-footer navbar-fixed-bottom" style="background-color: #173249;"	>
	<div class="page-footer-inner" style="color:#deb40f;">2018 &copy; Axiom xCell Inc.</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->