<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<!-- <body class="" > -->
<div class="page-header page-header-fixed navbar navbar-fixed-top"
	style="background-color: #173249;">
	<div class="container">
		<!-- BEGIN HEADER -->
		<header class="page-header-inner"
			style="border-bottom-width: 0px; margin-bottom: 0px; padding-bottom: 0px;">
			<!-- <div class="clearfix navbar-fixed-top"> -->

			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="#"> <img
				src="<c:url value='/static/assets/layouts/layout/img/logo.png' />"
				alt="logo" class="logo-default" />
			</a>
			</div>
			<!-- END LOGO -->
			<div class="navbar-fixed-top" id="headerText">
				<c:choose>
					<c:when test="${hearingRequired=='Pending'}">
						<h2 align="center">
							<b><font color="#fff" id="changeFont"><c:out value="Pending"></c:out>&nbsp;<c:out
										value="Cases "></c:out><c:out
										value="${presentDate}"></c:out> <c:out value="${presentTime}"></c:out></font></b>
						</h2>
					</c:when>
					<c:when test="${hearingRequired=='Complete'}">
						<h2 align="center"
							style="margin-top: 10px;">
							<b><font color="#fff"><span id="date"></span></font></b>
						</h2>
					</c:when>
					<c:otherwise>
						<h2 align="center"
							style="margin-top: 10px;">
							<b><font color="#fff"><c:out value="${presentDate}"></c:out>&nbsp;<c:out
										value="${presentTime}"></c:out></font></b></font></b>
						</h2>
					</c:otherwise>
				</c:choose>
				<!-- BEGIN TOPBAR ACTIONS -->
				<div class="navbar-fixed-top">
					<!-- BEGIN USER PROFILE -->
					<ul class="nav navbar-nav pull-right">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
						<li class="dropdown dropdown-user"><a href="javascript:;"
							class="dropdown-toggle" data-toggle="dropdown"
							data-hover="dropdown" data-close-others="true"> <img alt=""
								class="img-circle"
								src="<c:url value='/static/assets/layouts/layout/img/avatar.png ' />" />
								<span class="username username-hide-on-mobile">
									${admin.firstName} ${admin.lastName} </span> <i
								class="fa fa-angle-down"></i>
						</a>
							<ul class="dropdown-menu dropdown-menu-default">
								<li><a
									href="${pageContext.request.contextPath}/adjudicatorLogout">
										<i class="icon-key"></i> Log Out
								</a></li>
							</ul></li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END USER PROFILE -->
				</div>
				<!-- END TOPBAR ACTIONS -->
			</div>
			<!-- </div> -->
		</header>
	</div>
	<!--/container-->

</div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- </body> -->
<style>
@media (min-width:650 x) {
    #changeFont {
        font-size: 30px;   
    }
    #headerText {
        margin-top: 0px; 
    }
}

/* Anything Smaller than Large  */
@media (max-width: 649px) {
    #changeFont {
        font-size: 20px;  
    }
     #headerText {
        margin-top: 35px; 
    }
}

</style>