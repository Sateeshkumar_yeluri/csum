<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top" style="background-color: #173249;">
	<!-- BEGIN HEADER INNER -->
	<div class="container">
	<div class="page-header-inner ">
	<!-- <nav class="navbar navbar-toggleable-md navbar-toggleable-sm"> -->
	 <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon" style="color: #deb40f;">
                                    <span class="fa fa-bars"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="${pageContext.request.contextPath}/home"> <img
				src="<c:url value='/static/assets/layouts/layout/img/logo.png' />"
				alt="logo" class="logo-default" />
			</a>
			<!-- <div class="menu-toggler sidebar-toggler">
				<span></span>
			</div> -->
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<!-- <a href="javascript:;" class="menu-toggler responsive-toggler"
			data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
		</a> -->
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse" id="mega-nav">
			<ul class="nav navbar-nav pull-right" id="navMenu">
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/home"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.home" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/permitSearch"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.permit.search" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/freePermit"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.free.permit" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/violationEntry"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.entry" /></span>
				</a></li>
				<%-- <li class="active"><a
					href="${pageContext.request.contextPath}/citationDashbord"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.citation.search" /></span>
				</a></li> --%>
				<%-- <li class="nav-item  "><a
					href="${pageContext.request.contextPath}/reports" class="nav-link ">
						<span class="title"><spring:message
								code="lbl.topmenu.tab.reports" /></span> <span class="selected"></span>
				</a></li> --%>
				<%-- <li class="nav-item  ">
                                    <a href="javascript:void(0)" class="nav-link ">
									<span class="title"><spring:message code="lbl.topmenu.tab.calendar"/></span>
									<span class="selected"></span>
                                    </a>
                                </li> --%>
				<%-- <li class="nav-item  "><a
					href="${pageContext.request.contextPath}/uploadrecords"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.files" /></span>
				</a> <a href="javascript:void(0)" class="nav-link ">
								<span class="title"><spring:message code="lbl.topmenu.tab.files"/></span>
                             </a></li> --%>
				<li class="dropdown dropdown-user" ><a href="javascript:;" 
				style="padding-top: 3px;padding-bottom: 0px;"
					class="dropdown-toggle" data-toggle="dropdown"
					data-click="dropdown" data-close-others="true"> <img alt=""
						class="img-circle"
						src="<c:url value='/static/assets/layouts/layout/img/avatar.png ' />" />
						<span class="username username-hide-on-mobile" style="color:#deb40f;">
							${admin.firstName} ${admin.lastName} </span> <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li><a href="${pageContext.request.contextPath}/logout">
								<i class="icon-key"></i> Log Out
						</a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
		<!-- </nav> -->
	</div>
	<!-- END HEADER INNER -->
	</div>
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<style>
@media (min-width:650 x) {
    #navMenu {
        background:transparent;       /* transparent red */
    }
}

/* Anything Smaller than Large  */
@media (max-width: 649px) {
    #navMenu {
        background: rgba(23, 50, 73, 1);/* transparent pink */
    }
}

</style>