<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top" style="background-color: #173249;">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner ">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="${pageContext.request.contextPath}/home"> <img
				src="<c:url value='/static/assets/layouts/layout/img/logo.png' />"
				alt="logo" class="logo-default" />
			</a>
			<div class="menu-toggler sidebar-toggler">
				<span></span>
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler"
			data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="active"><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/home"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.home" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/permitSearch"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.permit.search" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/freePermit"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.free.permit" /></span>
				</a></li>
				<li class="nav-item  "><a style="color:#deb40f;"
					href="${pageContext.request.contextPath}/violationEntry"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.violation.entry" /></span>
				</a></li>
				<%-- <li class="active"><a
					href="${pageContext.request.contextPath}/citationDashbord"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.citation.search" /></span>
				</a></li> --%>
				<%-- <li class="nav-item  "><a
					href="${pageContext.request.contextPath}/reports" class="nav-link ">
						<span class="title"><spring:message
								code="lbl.topmenu.tab.reports" /></span> <span class="selected"></span>
				</a></li> --%>
				<%-- <li class="nav-item  ">
                                    <a href="javascript:void(0)" class="nav-link ">
									<span class="title"><spring:message code="lbl.topmenu.tab.calendar"/></span>
									<span class="selected"></span>
                                    </a>
                                </li> --%>
				<%-- <li class="nav-item  "><a
					href="${pageContext.request.contextPath}/uploadrecords"
					class="nav-link "> <span class="title"><spring:message
								code="lbl.topmenu.tab.files" /></span>
				</a> <a href="javascript:void(0)" class="nav-link ">
								<span class="title"><spring:message code="lbl.topmenu.tab.files"/></span>
                             </a></li> --%>
				<li class="dropdown dropdown-user"><a href="javascript:;"
					class="dropdown-toggle" data-toggle="dropdown"
					data-hover="dropdown" data-close-others="true"> <img alt=""
						class="img-circle"
						src="<c:url value='/static/assets/layouts/layout/img/avatar.png ' />" />
						<span class="username username-hide-on-mobile" style="color:#deb40f;">
							${admin.firstName} ${admin.lastName} </span> <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li><a href="${pageContext.request.contextPath}/logout">
								<i class="icon-key"></i> Log Out
						</a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
