package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "DISPOSITION_CODE")
@NamedQueries({
	@NamedQuery(name = "com.csum.model.DispositionCode.findAll", query = "SELECT d FROM DispositionCode d"),
	@NamedQuery(name = "com.csum.model.DispositionCode.findByCode", query = "SELECT d FROM DispositionCode d WHERE d.code = :dispCode"),
})
public class DispositionCode extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6265475350282128944L;

	@Column(name = "CODE")
	private String code;

	@Column(name = "dispclass")
	private String dispclass;

	@Column(name = "FullName")
	private String fullName;

	@Column(name = "Priority")
	private int priority;

	@Column(name = "Rule")
	private String rule;

	@Column(name = "Active")
	private String active;

	@Column(name = "Description")
	private String description;
	
	@Column(name = "DAYS")
	private int days;
	
	@Column(name = "AMOUNT")
	private String amount;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDispclass() {
		return dispclass;
	}

	public void setDispclass(String dispclass) {
		this.dispclass = dispclass;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}


}

