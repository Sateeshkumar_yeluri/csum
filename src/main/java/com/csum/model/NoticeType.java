package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "NOTICE_TYPE")
@NamedQuery(name="com.csum.model.noticeType.findByType", query="SELECT nt FROM NoticeType nt WHERE nt.type = :type")
public class NoticeType extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5331816406910234098L;
	
	@NotEmpty
	@Column(name = "TYPE", nullable = false)
	private int type;

	@NotEmpty
	@Column(name = "FULL_NM", nullable = false)
	private String fullNm;

	@NotEmpty
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getFullNm() {
		return fullNm;
	}

	public void setFullNm(String fullNm) {
		this.fullNm = fullNm;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
