package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "SUSPENDED_CODES")
@NamedQueries({
	@NamedQuery(name="com.csum.model.suspendedCodes.findAll", query="SELECT sc FROM SuspendedCodes sc"),
	@NamedQuery(name="com.csum.model.suspendedCodes.findByCode", query="SELECT sc FROM SuspendedCodes sc WHERE sc.code = :suspCode"),
	@NamedQuery(name="com.csum.model.suspendedCodes.findById", query="SELECT sc FROM SuspendedCodes sc WHERE sc.id = :suspCodeId")
})

public class SuspendedCodes extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5707859988225612900L;
	
	@Column(name = "CODE", nullable = false)
	private int code;

	@Column(name = "FULL_NM", nullable = false)
	private String fullNm;

	@Column(name = "DAYS", nullable = false)
	private int days;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@Column(name = "AMOUNT", nullable = true)
	private String amount;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getFullNm() {
		return fullNm;
	}

	public void setFullNm(String fullNm) {
		this.fullNm = fullNm;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}
