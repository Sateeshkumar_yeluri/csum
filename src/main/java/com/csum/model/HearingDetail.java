package com.csum.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;


@Entity
@Table(name = "HEARING_DETAILS")
@NamedQueries({
@NamedQuery(name = "com.csum.model.Hearing.findByHearing", query = "SELECT h FROM HearingDetail h WHERE h.hearing.id = :hearingId order by h.id desc"),
@NamedQuery(name = "com.csum.model.Hearing.getApprovedDate", query = "SELECT h FROM HearingDetail h WHERE (h.status='PENDINGMAIL' OR h.status='COMPLETE')  AND h.hearing.violation.violationId = :violationId order by h.id asc"),})
public class HearingDetail extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2005459140848554858L;

	@OneToOne
	@JoinColumn(name = "HEARING_ID", nullable = false, updatable = false)
	private Hearing hearing;

	@Column(name = "TRANSLATION")
	private String translation;

	@Column(name = "LANG")
	private String language;

	@Column(name = "HEARING_NOTES")
	private String notes;

	@OneToOne
	@JoinColumn(name = "DECISION")
	private HearingDecision decision;
	
	@ManyToOne
	@JoinColumn(name = "DISPOSITION", updatable = true)
	@NotAudited
	private DispositionCode dispositionCode;
	
	@Column(name = "TOT_DUE", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal totalDue;
	
	private String status;

	public String getTranslation() {
		return translation;
	}

	public Hearing getHearing() {
		return hearing;
	}

	public void setHearing(Hearing hearing) {
		this.hearing = hearing;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
		
	public HearingDecision getDecision() {
		return decision;
	}

	public void setDecision(HearingDecision decision) {
		this.decision = decision;
	}

	public DispositionCode getDispositionCode() {
		return dispositionCode;
	}

	public void setDispositionCode(DispositionCode dispositionCode) {
		this.dispositionCode = dispositionCode;
	}

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
