package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "ROLE")
public class Role extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1332912707585933166L;
	
	@NotEmpty
	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ADMN_TYP_ID", nullable = false, updatable = false)
	private AdminType adminType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AdminType getAdminType() {
		return adminType;
	}

	public void setAdminType(AdminType adminType) {
		this.adminType = adminType;
	}
	
}
