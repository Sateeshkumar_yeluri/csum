package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "REVIEWCOMMENTS")
@NamedQuery(name="com.csum.model.reviewComments.findAllReviewComments", query="SELECT rc FROM ReviewComments rc")
public class ReviewComments extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6087577721566758113L;
	
	@NotNull
	@Column(name = "comment")
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
