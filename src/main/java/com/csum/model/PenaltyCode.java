package com.csum.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;

@Entity
@Table(name = "PNLT_CODE")
@Audited
@AuditTable(value = "PNLT_CODE_HSTRY")
public class PenaltyCode  extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2171367841821215004L;
	
	@Column(name = "STND_FINE")
	private String standardFine;

	@Column(name = "PENALTY1", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal penalty1;

	@Column(name = "PENALTY1DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate penalty1Date;

	@Column(name = "PENALTY2", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal penalty2;

	@Column(name = "PENALTY2DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate penalty2Date;

	@Column(name = "PENALTY3", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal penalty3;

	@Column(name = "PENALTY3DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate penalty3Date;

	public String getStandardFine() {
		return standardFine;
	}

	public void setStandardFine(String standardFine) {
		this.standardFine = standardFine;
	}

	public BigDecimal getPenalty1() {
		return penalty1;
	}

	public void setPenalty1(BigDecimal penalty1) {
		this.penalty1 = penalty1;
	}

	public LocalDate getPenalty1Date() {
		return penalty1Date;
	}

	public void setPenalty1Date(LocalDate penalty1Date) {
		this.penalty1Date = penalty1Date;
	}

	public BigDecimal getPenalty2() {
		return penalty2;
	}

	public void setPenalty2(BigDecimal penalty2) {
		this.penalty2 = penalty2;
	}

	public LocalDate getPenalty2Date() {
		return penalty2Date;
	}

	public void setPenalty2Date(LocalDate penalty2Date) {
		this.penalty2Date = penalty2Date;
	}

	public BigDecimal getPenalty3() {
		return penalty3;
	}

	public void setPenalty3(BigDecimal penalty3) {
		this.penalty3 = penalty3;
	}

	public LocalDate getPenalty3Date() {
		return penalty3Date;
	}

	public void setPenalty3Date(LocalDate penalty3Date) {
		this.penalty3Date = penalty3Date;
	}
	
	public String getFormattedpenalty1() {
		return CSUMDateUtils.getFormattedLocalDate(this.penalty1Date);
	}

	public String getFormattedpenalty2() {
		return CSUMDateUtils.getFormattedLocalDate(this.penalty2Date);
	}

	public String getFormattedpenalty3() {
		return CSUMDateUtils.getFormattedLocalDate(this.penalty3Date);
	}
	
}
