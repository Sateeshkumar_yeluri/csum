package com.csum.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PERMIT_TRANSACTION")
public class PermitTransaction extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2879976959227375120L;
	
	@Column(name = "amount")
	private String amount;

	@Column(name = "transcationId")
	private String transcationId;
	@Column(name = "transcationCode")
	private String transcationCode;
	@Column(name = "statusMessage")
	private String statusMessage;

	@Column(name = "statusResponseCode")
	private String statusResponseCode;

	@Column(name = "status")
	private String status;

	@Column(name = "clientToken", length = 5000)
	private String clientToken;
	@Column(name = "createdDate")
	private Date createdDate;

	@Column(name = "permitId")
	private String permitId;

	@Column(name = "paymentNonce")
	private String paymentNonce;
	
	public String getPermitId() {
		return permitId;
	}

	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTranscationId() {
		return transcationId;
	}

	public void setTranscationId(String string) {
		this.transcationId = string;
	}

	public String getTranscationCode() {
		return transcationCode;
	}

	public void setTranscationCode(String transcationCode) {
		this.transcationCode = transcationCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	
	public String getStatusResponseCode() {
		return statusResponseCode;
	}

	public void setStatusResponseCode(String statusResponseCode) {
		this.statusResponseCode = statusResponseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClientToken() {
		return clientToken;
	}

	public void setClientToken(String clientToken) {
		this.clientToken = clientToken;
	}

	public String getPaymentNonce() {
		return paymentNonce;
	}

	public void setPaymentNonce(String paymentNonce) {
		this.paymentNonce = paymentNonce;
	}

}
