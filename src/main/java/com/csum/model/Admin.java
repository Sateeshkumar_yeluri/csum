package com.csum.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "ADMIN")
@NamedQueries({
@NamedQuery(name = "com.csum.model.Admin.findByUserName", query = "SELECT a FROM Admin a WHERE a.userName = :userName"),
@NamedQuery(name = "com.csum.model.Admin.findAllUsers", query = "SELECT a FROM Admin a"),
})
public class Admin extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5511518125463617310L;
	
	@NotEmpty
	@Column(name = "USER_NAME", unique = true, nullable = false)
	private String userName;

	@NotEmpty
	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@NotEmpty
	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;

	@NotEmpty
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinTable(name = "ADMIN_ROLE", joinColumns = @JoinColumn(name = "ADMIN_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "id"))
	private Role role;

	// ----------------------- Get and Set Methods -----------------------
		
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
