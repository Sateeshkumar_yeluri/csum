package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "PAYMENT_METHOD")
@NamedQuery(name = "com.csum.model.PaymentMethod.findByType", query = "SELECT p FROM PaymentMethod p WHERE p.typCd = :typCd")
public class PaymentMethod extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 22222;// Need to change

	@NotEmpty
	@Column(name = "TYP_CD", nullable = false)
	private int typCd;

	@NotEmpty
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	public int getTypCd() {
		return typCd;
	}

	public void setTypCd(int typCd) {
		this.typCd = typCd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
