package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.search.annotations.IndexedEmbedded;


@Entity
@Table(name = "PATRON")
@Audited
@AuditTable(value = "PATRON_HSTRY")
public class Patron extends AbstractDomain{
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinTable(name = "PATRON_ADDR", joinColumns = @JoinColumn(name = "PATRON_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "ADDR_ID", referencedColumnName = "id"))
	@IndexedEmbedded
	private Address address;	
	
	// ----------------------- Get and Set Methods -----------------------

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	
}
