package com.csum.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;

@Entity
@Table(name = "HEARING")
@Audited
@AuditTable(value = "HEARING_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.hearing.findByViolationId", query="Select h from Hearing h where h.violation.violationId = :violationId"),
@NamedQuery(name="com.csum.model.hearing.findLatestByDate", query="SELECT h FROM Hearing h WHERE h.hearingDate = :hearingDate"),
@NamedQuery(name="com.csum.model.hearing.findAllPending", query="SELECT h FROM Hearing h WHERE (h.hearingDate= :hearingDate OR h.hearingDate='' OR h.hearingDate is NULL) AND h.status ='Pending'  order by hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllPendingByType", query="SELECT h FROM Hearing h WHERE h.hearingDate= :hearingDate AND h.type=:type AND h.status ='Pending' order by hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllPendingBetween", query="SELECT h FROM Hearing h WHERE (h.hearingDate BETWEEN :hearingDate1 AND :hearingDate2  OR h.hearingDate='' OR h.hearingDate is NULL) AND h.status ='Pending'  order by hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllPendingByTypeBetween", query="SELECT h FROM Hearing h WHERE h.hearingDate BETWEEN :hearingDate1 AND :hearingDate2 AND h.type=:type AND h.status ='Pending' order by hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllPendingWritten", query="SELECT h FROM Hearing h WHERE h.type='Written' AND h.status ='Pending' order by hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompleted", query="SELECT h FROM Hearing h WHERE h.status !='Pending' AND h.hearingDate= :hearingDate order by hearingDate asc, hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompletedStatuses", query="SELECT h FROM Hearing h WHERE (h.status = :status1 OR h.status = :status2) AND h.hearingDate= :hearingDate order by  hearingDate asc, hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompletedStatus", query="SELECT h FROM Hearing h WHERE h.status = :status AND h.hearingDate= :hearingDate order by  hearingDate asc, hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompletedBetween", query="SELECT h FROM Hearing h WHERE h.status !='Pending' AND h.hearingDate BETWEEN :hearingDate1 AND :hearingDate2 order by  hearingDate asc, hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompletedBetweenStatuses", query="SELECT h FROM Hearing h WHERE (h.status = :status1 OR h.status = :status2) AND h.hearingDate BETWEEN :hearingDate1 AND :hearingDate2 order by  hearingDate asc, hearingTime asc"),
@NamedQuery(name="com.csum.model.hearing.findAllCompletedBetweenStatus", query="SELECT h FROM Hearing h WHERE h.status = :status AND h.hearingDate BETWEEN :hearingDate1 AND :hearingDate2 order by  hearingDate asc, hearingTime asc"),

})
public class Hearing extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5356689528383171922L;
	
	@OneToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;

	@Column(name = "HEARING_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate hearingDate;

	@Temporal(TemporalType.TIME)
	@Column(name = "HEARING_TIME")
	@DateTimeFormat(pattern = "HH:mm")
	private Date hearingTime;
	
	@Column(name = "HEARING_OFFICER")
	private String hearingOfficer;
	
	@Column(name = "DIPOSITION_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate dispositionDate;

	@Temporal(TemporalType.TIME)
	@Column(name = "DISPOSITION_TIME")
	@DateTimeFormat(pattern = "HH:mm")
	private Date dispositionTime;

	@Column(name = "DISPOSITION")
	private String disposition;
	
	@Column(name = "TOT_DUE", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal totalDue;
	
	@Column(name = "noFineChange", columnDefinition = "TINYINT(1)")
	private boolean noFineChange;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "SCHEDULED_BY")
	private String scheduledBy;

	@Column(name = "SCHEDULED_AT")
	@DateTimeFormat(pattern = "MM/dd/yyyy hh:mm a")
	private Date scheduledAt;
	
	@Column(name = "DATE_MAILED")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate dateMailed;
	
	@Column(name = "IS_RESCHEDULED")
	private boolean rescheduled;
	
	@OneToOne
	@JoinColumn(name = "DECISION")
	@NotAudited
	private HearingDecision decision;
	
	@Transient
	private HearingDetail hearingdetail;
	
	@Transient
	private String requiredDate;

	@Transient
	private String HearingBasis;
	
	@NotAudited
	@OneToMany(mappedBy = "hearing", fetch = FetchType.EAGER)
	private Set<HearingDetail> hearingdetails;
	
	@Column(name = "REDUCTION", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal reduction;

	@Transient
	private String searchType;

	@Transient
	private String searchValue;
	
	@Transient
	private String hearingOfficerName;
	
	public String getHearingOfficerName() {
		return hearingOfficerName;
	}

	public void setHearingOfficerName(String hearingOfficerName) {
		this.hearingOfficerName = hearingOfficerName;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public BigDecimal getReduction() {
		return reduction;
	}

	public void setReduction(BigDecimal reduction) {
		this.reduction = reduction;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getHearingBasis() {
		return HearingBasis;
	}

	public void setHearingBasis(String hearingBasis) {
		HearingBasis = hearingBasis;
	}

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public LocalDate getHearingDate() {
		return hearingDate;
	}

	public void setHearingDate(LocalDate hearingDate) {
		this.hearingDate = hearingDate;
	}

	public Date getHearingTime() {
		return hearingTime;
	}

	public void setHearingTime(Date hearingTime) {
		this.hearingTime = hearingTime;
	}

	public String getHearingOfficer() {
		return hearingOfficer;
	}

	public void setHearingOfficer(String hearingOfficer) {
		this.hearingOfficer = hearingOfficer;
	}

	public LocalDate getDispositionDate() {
		return dispositionDate;
	}

	public void setDispositionDate(LocalDate dispositionDate) {
		this.dispositionDate = dispositionDate;
	}

	public Date getDispositionTime() {
		return dispositionTime;
	}

	public void setDispositionTime(Date dispositionTime) {
		this.dispositionTime = dispositionTime;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(String scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public Date getScheduledAt() {
		return scheduledAt;
	}

	public void setScheduledAt(Date scheduledAt) {
		this.scheduledAt = scheduledAt;
	}

	public LocalDate getDateMailed() {
		return dateMailed;
	}

	public void setDateMailed(LocalDate dateMailed) {
		this.dateMailed = dateMailed;
	}
	
	public boolean isNoFineChange() {
		return noFineChange;
	}

	public void setNoFineChange(boolean noFineChange) {
		this.noFineChange = noFineChange;
	}

	public boolean isRescheduled() {
		return rescheduled;
	}

	public void setRescheduled(boolean rescheduled) {
		this.rescheduled = rescheduled;
	}

	public HearingDecision getDecision() {
		return decision;
	}

	public void setDecision(HearingDecision decision) {
		this.decision = decision;
	}

	public HearingDetail getHearingdetail() {
		return hearingdetail;
	}

	public void setHearingdetail(HearingDetail hearingdetail) {
		this.hearingdetail = hearingdetail;
	}

	public Set<HearingDetail> getHearingdetails() {
		return hearingdetails;
	}

	public void setHearingdetails(Set<HearingDetail> hearingdetails) {
		this.hearingdetails = hearingdetails;
	}

	public String getFormattedHearingDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.hearingDate);
	}
	
	public String getFormattedDispDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.dispositionDate);
	}
	
	public String getFormattedDateMailed() {
		return CSUMDateUtils.getFormattedLocalDate(this.dateMailed);
	}
	
	public String getFormattedScheduledAt() {
		return CSUMDateUtils.getFormatedDateTime(this.scheduledAt);
	}
	
}
