package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "INSPECTOR")
//@NamedQuery(name="com.csum.model.violation.findByInspectorEmployeeId", query="select i from Inspector i where employeeId = :employeeId")
@NamedQuery(name="com.csum.model.violation.findByInspectorUserName", query="select i from Inspector i where username = :username")
public class Inspector extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -149956113820438751L;
	
	@NotNull
	@Column(name = "employee_id", nullable = true)
	private long employeeId;

	@Column(name = "username", nullable = true)
	private String username;

	@Column(name = "password", nullable = true)
	private String password;

	@Column(name = "last_name", nullable = true)
	private String lastName;

	@Column(name = "first_name", nullable = true)
	private String firstName;

	@Column(name = "title", nullable = true)
	private String title;
	
	@Column(name = "gender", nullable = true)
	private String gender;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
}
