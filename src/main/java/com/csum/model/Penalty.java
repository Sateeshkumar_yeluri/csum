package com.csum.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;


@Entity
@Table(name = "PNLT")
@Audited
@AuditTable(value = "PNLT_HSTRY")
@NamedQuery(name="com.csum.model.penalty.findByViolationId",query="Select p from Penalty p where p.violation.violationId = :violationId")
public class Penalty extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8806170319267900259L;
	
	@Column(name = "TOT_DUE", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal totalDue;
	
	@OneToOne
	@JoinColumn(name = "VLN_ID", nullable = false)
	private Violation violation;
	
	@ManyToOne
	@JoinColumn(name = "PNLT_CODE")
	private PenaltyCode penaltyCode;

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public PenaltyCode getPenaltyCode() {
		return penaltyCode;
	}

	public void setPenaltyCode(PenaltyCode penaltyCode) {
		this.penaltyCode = penaltyCode;
	}
}
