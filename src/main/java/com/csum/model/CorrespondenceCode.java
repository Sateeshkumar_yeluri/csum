package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "CORRESP_CODE")
@NamedQueries({
@NamedQuery(name="com.csum.model.correspondenceCodes.findAll",query="SELECT cc FROM CorrespondenceCode cc"),
@NamedQuery(name="com.csum.model.correspondenceCodes.findById", query="SELECT cc FROM CorrespondenceCode cc WHERE cc.id=:correspCodeId")
})
public class CorrespondenceCode extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3922972457509783487L;
	
	@NotNull
	@Column(name = "TYP_CD", nullable = false)
	private long typCd;

	@NotEmpty
	@Column(name = "CLS", nullable = true)
	private String cls;

	@NotEmpty
	@Column(name = "FULL_NAME", nullable = false)
	private String fullName;
	
	@NotEmpty
	@Column(name = "CORRESP_DESC", nullable = false)
	private String correspDesc;

	public long getTypCd() {
		return typCd;
	}

	public void setTypCd(long typCd) {
		this.typCd = typCd;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCorrespDesc() {
		return correspDesc;
	}

	public void setCorrespDesc(String correspDesc) {
		this.correspDesc = correspDesc;
	}

}
