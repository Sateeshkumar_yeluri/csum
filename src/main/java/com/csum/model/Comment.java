package com.csum.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

@Entity
@Indexed
@Table(name = "COMMENT")
@NamedQuery(name="com.csum.model.comment.findCommentByMode", query="SELECT c FROM Comment c WHERE c.violation.violationId = :violationId AND  c.mode=:mode ORDER BY c.createdAt DESC")
public class Comment extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3941815931567481218L;
	
	@ManyToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "COMMENT")
	@Field
	@Analyzer(definition = "customanalyzer")
	private String comment;

	@Column(name = "COMMENT_TYPE")
	private String commentType;
	
	private String mode;

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public String getComment() {
		return comment;
	}	

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentType() {
		return commentType;
	}

	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}	

}
