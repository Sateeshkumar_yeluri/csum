package com.csum.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.csum.utils.CSUMDateUtils;


@Entity
@Table(name = "PAYMENT")
@Audited
@AuditTable(value = "PAYMENT_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.Payment.findByTransactionId", query="SELECT p FROM Payment p WHERE p.transaction.id = :txId"),
@NamedQuery(name="com.csum.model.payment.findByViolationId", query="SELECT p FROM Payment p WHERE p.violation.violationId = :violationId"),
@NamedQuery(name="com.csum.model.payment.findLatestByViolationId", query="SELECT p FROM Payment p WHERE p.violation.violationId = :violationId  ORDER BY p.id DESC"),
@NamedQuery(name="com.csum.model.payment.findByPlanNumber", query="SELECT p FROM Payment p WHERE p.installmentPaymentPlan.planNumber = :planNumber")
})
public class Payment extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8381473465116391878L;
	
	@OneToOne
	@JoinColumn(name = "VIOLATION", nullable = false)
	private Violation violation;

	public BigDecimal amount;

	public String paymentDate;

	private String account;

	private String processedBy;

	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate processedOn;
	
	private int methodId;

	private String chequeFileName;

	@Transient
	private String chequeFileType;
	
	private String chequeNumber;

	@Lob
	@Column(name = "chequeFile", length = 10000)
	private byte[] chequeFile;

	@Transient
	private List<MultipartFile> uploadedCheque;
	
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean ipp;
	
	@Column(name = "IPP_TYPE", nullable = true, updatable = true)
	private String ippType;
	
	@ManyToOne
	@JoinColumn(name = "IPP_ID", nullable = true, updatable = true)
	@NotAudited
	private Ipp installmentPaymentPlan;

	@ManyToOne
	@JoinColumn(name = "txId")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Transaction transaction;

	private String paymentSource;
	
	@ManyToOne
	@JoinColumn(name = "PAYMNT_METHOD_ID", nullable = false, updatable = true)
	@NotAudited
	private PaymentMethod paymentMethod;
	
	public BigDecimal totalDue;
	
	private BigDecimal overPaid;
	
	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public LocalDate getProcessedOn() {
		return processedOn;
	}

	public void setProcessedOn(LocalDate processedOn) {
		this.processedOn = processedOn;
	}

	public int getMethodId() {
		return methodId;
	}

	public void setMethodId(int methodId) {
		this.methodId = methodId;
	}

	public String getChequeFileName() {
		return chequeFileName;
	}

	public void setChequeFileName(String chequeFileName) {
		this.chequeFileName = chequeFileName;
	}

	public String getChequeFileType() {
		return chequeFileType;
	}

	public void setChequeFileType(String chequeFileType) {
		this.chequeFileType = chequeFileType;
	}

	public byte[] getChequeFile() {
		return chequeFile;
	}

	public void setChequeFile(byte[] chequeFile) {
		this.chequeFile = chequeFile;
	}

	public List<MultipartFile> getUploadedCheque() {
		return uploadedCheque;
	}

	public void setUploadedCheque(List<MultipartFile> uploadedCheque) {
		this.uploadedCheque = uploadedCheque;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(String paymentSource) {
		this.paymentSource = paymentSource;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public BigDecimal getOverPaid() {
		return overPaid;
	}

	public void setOverPaid(BigDecimal overPaid) {
		this.overPaid = overPaid;
	}

	public boolean isIpp() {
		return ipp;
	}

	public void setIpp(boolean ipp) {
		this.ipp = ipp;
	}

	public String getIppType() {
		return ippType;
	}

	public void setIppType(String ippType) {
		this.ippType = ippType;
	}

	public Ipp getInstallmentPaymentPlan() {
		return installmentPaymentPlan;
	}

	public void setInstallmentPaymentPlan(Ipp installmentPaymentPlan) {
		this.installmentPaymentPlan = installmentPaymentPlan;
	}
	
	public String getFormattedProcessedOn() {
		return CSUMDateUtils.getFormattedLocalDate(this.processedOn);
	}
	
}
