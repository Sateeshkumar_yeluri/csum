package com.csum.model;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "HEARING_SCHEDULER")
@NamedQuery(name = "com.csum.model.HearingScheduler.findAll", query = "SELECT s FROM HearingScheduler s")
public class HearingScheduler extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2005459140848554858L;

	private String value;
	
	private String description;
	
	private String type;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

