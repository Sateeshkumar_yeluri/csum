package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "ADMIN_TYPE")
public class AdminType extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9143674658027849703L;
	
	@NotEmpty
	@Column(name = "TYPE", unique = true, nullable = false)
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
