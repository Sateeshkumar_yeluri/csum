package com.csum.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

@Entity
@Table(name = "PERMIT_PAYMENT")
@Audited
@AuditTable(value = "PERMIT_PAYMENT_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.PermitPayments.findByTransactionId", query="SELECT p FROM PermitPayments p WHERE p.permitTransaction.id = :txId"),
})
public class PermitPayments extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1668497885267864044L;
	
	@Column(name="PAYMENT_TYPE")
	private String paymentType;
	
	@Column(name="AMOUNT")
	private String amount;
	
	@Column(name="PAYMENT_DATE")
	private LocalDate paymentDate;
	
	@OneToOne
	@JoinColumn(name = "txId")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private PermitTransaction permitTransaction;

	public PermitTransaction getPermitTransaction() {
		return permitTransaction;
	}

	public void setPermitTransaction(PermitTransaction permitTransaction) {
		this.permitTransaction = permitTransaction;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

}
