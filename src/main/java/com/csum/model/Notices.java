package com.csum.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;


@Entity
@Table(name = "NOTICES")
@Audited
@AuditTable(value = "NOTICES_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.notices.findByViolationId",query="SELECT n FROM Notices n where n.violation.violationId = :violationId"),
@NamedQuery(name="com.csum.model.notices.findLatestByViolationId",query="SELECT n FROM Notices n where n.violation.violationId = :violationId")
})
public class Notices extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3903153388176028292L;
	
	@ManyToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;
	
	@ManyToOne
	@JoinColumn(name = "NOTICE_TYPE", nullable = true, updatable = true)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private NoticeType noticeType;
	
	@Column(name = "SENT_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate sentDate;

	@Column(name = "PROCESSED_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate processedDate;

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public NoticeType getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(NoticeType noticeType) {
		this.noticeType = noticeType;
	}

	public LocalDate getSentDate() {
		return sentDate;
	}

	public void setSentDate(LocalDate sentDate) {
		this.sentDate = sentDate;
	}

	public LocalDate getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(LocalDate processedDate) {
		this.processedDate = processedDate;
	}
	
	public String getFormattedSentDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.sentDate);
	}

	public String getFormattedProcessedDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.processedDate);
	}
	
}
