package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "HEARING_DECISION")
@NamedQueries({
@NamedQuery(name = "com.csum.model.HearingDecision.findAll", query = "SELECT hd FROM HearingDecision hd WHERE hd.isDeletedInd='N'"),
@NamedQuery(name = "com.csum.model.HearingDecision.findById", query = "SELECT hd FROM HearingDecision hd WHERE hd.isDeletedInd='N' AND hd.id=:id"),
})
public class HearingDecision extends AbstractDomain{
	

	private static final long serialVersionUID = 34874519863416434L;
	
	@Column(name = "Code")
	private String code;

	@Column(name = "Description")
	private String description;
	
	@Column(name="isdeleted_ind")
	private char isDeletedInd;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public char getIsDeletedInd() {
		return isDeletedInd;
	}

	public void setIsDeletedInd(char isDeletedInd) {
		this.isDeletedInd = isDeletedInd;
	}	
	
}

