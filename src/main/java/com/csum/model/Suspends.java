package com.csum.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;


@Entity
@Table(name = "SUSPENDS")
@Audited
@AuditTable(value = "SUSPENDS_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.suspends.findByViolationId", query="SELECT s FROM Suspends s WHERE s.violation.violationId = :violationId"),
@NamedQuery(name="com.csum.model.suspends.findLatestByViolationId", query="SELECT s FROM Suspends s WHERE s.violation.violationId = :violationId order by id desc")
})
public class Suspends extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7429432381911922854L;
	
	@ManyToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;
	
	@ManyToOne
	@JoinColumn(name = "SUSPENDED_CODES_ID", nullable = false, updatable = true)
	@NotAudited
	private SuspendedCodes suspendedCodes;
	
	@Column(name = "SUSPENDED_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate suspended_date;

	@Column(name = "SUSPENDED_TIME")
	private String suspended_time;
	
	@Column(name = "TOT_DUE", columnDefinition = "DECIMAL(7,2)", nullable = true)
	private BigDecimal totalDue;
	
	@Column(name = "REDUCTION", columnDefinition = "DECIMAL(7,2)", nullable = true)
	private BigDecimal reduction;
	
	@Column(name = "MODE")
	private String mode;
	
	@Column(name = "PROCESSED_ON")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate processedOn;

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public SuspendedCodes getSuspendedCodes() {
		return suspendedCodes;
	}

	public void setSuspendedCodes(SuspendedCodes suspendedCodes) {
		this.suspendedCodes = suspendedCodes;
	}

	public LocalDate getSuspended_date() {
		return suspended_date;
	}

	public void setSuspended_date(LocalDate suspended_date) {
		this.suspended_date = suspended_date;
	}

	public String getSuspended_time() {
		return suspended_time;
	}

	public void setSuspended_time(String suspended_time) {
		this.suspended_time = suspended_time;
	}

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public BigDecimal getReduction() {
		return reduction;
	}

	public void setReduction(BigDecimal reduction) {
		this.reduction = reduction;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public LocalDate getProcessedOn() {
		return processedOn;
	}

	public void setProcessedOn(LocalDate processedOn) {
		this.processedOn = processedOn;
	}
	
	public String getFormattedSuspendDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.suspended_date);
	}
	
	public String getFormattedProcessedOn() {
		return CSUMDateUtils.getFormattedLocalDate(this.processedOn);
	}

	public String getFormattedSuspended_time() {
		return CSUMDateUtils.getFormattedLocalTime(this.suspended_time);
	}
}
