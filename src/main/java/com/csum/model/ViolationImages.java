package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table(name = "VIOLATION_IMAGES")
@NamedQueries({
@NamedQuery(name="com.csum.model.violationImages.findByViolationId", query="SELECT i FROM ViolationImages i WHERE i.violation.violationId =:violationId and  parentFileId = 0 "),
@NamedQuery(name="com.csum.model.violationImages.findByParentId", query="SELECT i FROM ViolationImages i WHERE i.parentFileId =:parentFileId"),
@NamedQuery(name="com.csum.model.violationImages.findByImageId", query="SELECT i FROM ViolationImages i WHERE i.id =:imageId"),
@NamedQuery(name="com.csum.model.violationImages.findByType", query="SELECT i FROM ViolationImages i WHERE i.type =:type and i.violation.violationId =:violationId and  parentFileId = 0 "),
})
public class ViolationImages extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "VLN_ID")
	private Violation violation;

	@Column(name = "IMG_NAME")
	private String imageName;

	@Column(name = "IMG_TYPE")
	private String imageType;

	@Lob
	@Column(name = "IMG", length = 100000000)
	private byte[] image;

	@Column(name = "UPD_DATE")
	private String uploadedDate;

	@Column(name = "parentfile_id")
	private long parentFileId;

	private String type;

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public long getParentFileId() {
		return parentFileId;
	}

	public void setParentFileId(long parentFileId) {
		this.parentFileId = parentFileId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
