package com.csum.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;


@Entity
@Table(name = "CORRESPONDENCE")
@Audited
@AuditTable(value = "CORRESPONDENCE_HSTRY")
@NamedQueries({
@NamedQuery(name="com.csum.model.correspondence.findByViolationId", query="SELECT c FROM Correspondence c WHERE c.violation.violationId = :violationId"),
@NamedQuery(name="com.csum.model.correspondence.findLatestByViolationId", query="SELECT c FROM Correspondence c WHERE c.violation.violationId = :violationId order by id desc")
})
public class Correspondence extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6773445317628012031L;
	
	@ManyToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;
	
	@ManyToOne
	@JoinColumn(name = "CORRESPONDENCE")
	@NotAudited
	private CorrespondenceCode correspCode;
	
	@Column(name = "letterSent", columnDefinition = "TINYINT(1)")
	private boolean letterSent;
	
	@Column(name = "CORRESP_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate corresp_date;
	
	@Column(name = "CORRESP_TIME", nullable = true)
	private String corresp_time;
	
	private String mode;

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public CorrespondenceCode getCorrespCode() {
		return correspCode;
	}

	public void setCorrespCode(CorrespondenceCode correspCode) {
		this.correspCode = correspCode;
	}

	public boolean isLetterSent() {
		return letterSent;
	}

	public void setLetterSent(boolean letterSent) {
		this.letterSent = letterSent;
	}

	public LocalDate getCorresp_date() {
		return corresp_date;
	}

	public void setCorresp_date(LocalDate corresp_date) {
		this.corresp_date = corresp_date;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getCorresp_time() {
		return corresp_time;
	}

	public void setCorresp_time(String corresp_time) {
		this.corresp_time = corresp_time;
	}	

	public String getFormattedCorresDate() {
		return CSUMDateUtils.getFormattedLocalDate(this.corresp_date);
		
	}
	public String getFormattedCorresp_time() {
		return CSUMDateUtils.getFormattedLocalTime(this.corresp_time);
		
	}
}
