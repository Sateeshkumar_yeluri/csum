package com.csum.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.util.StringUtils;

@Entity
@Table(name="ADDRESS")
@Audited
@AuditTable(value="ADDRESS_HSTRY")
public class Address extends AbstractDomain{
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "STATE")
	private String state;
	
	@Column(name = "ZIP")
	private String zip;
	
	@Column(name = "ADDRESS")
	private String address;

	// ----------------------- Get and Set Methods -----------------------
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getFormattedAddress() {
		StringBuilder formattedAddress = new StringBuilder("");
		List<String> addressValues = new ArrayList<String>();
		if (!StringUtils.isEmpty(this.address)) {
			addressValues.add(this.address);
		}
		if (!StringUtils.isEmpty(this.city)) {
			addressValues.add(this.city);
		}
		if (!StringUtils.isEmpty(this.state)) {
			addressValues.add(this.state);
		}
		if (!StringUtils.isEmpty(this.zip)) {
			addressValues.add(this.zip);
		}
		String listString = String.join(", ", addressValues);
		formattedAddress.append(listString);
		return formattedAddress.toString();
	}
		
}
