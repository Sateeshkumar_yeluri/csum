package com.csum.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.format.annotation.DateTimeFormat;

import com.csum.utils.CSUMDateUtils;

@Entity
@Audited
@AuditTable(value = "PERMIT_HSTRY")
@Table(name = "PERMIT")
@NamedQueries({
		@NamedQuery(name = "com.csum.model.permit.findByPermitId", query = "Select p from Permit p where p.permitId=:permitId"),
		@NamedQuery(name = "com.csum.model.permit.findValidPermit", query = "Select p from Permit p where p.licenceNumber = :licenceNumber AND p.parkingSlot = :parkingSlot "
				+ "AND (:scannedDateTime BETWEEN  STR_TO_DATE(CONCAT(p.startDate,' ',p.startTime ),'%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(CONCAT(p.endDate,' ',p.endTime ),'%Y-%m-%d %H:%i:%s' )) ") })

public class Permit extends AbstractDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8918524430798382718L;

	@Column(name = "PERMIT_ID")
	private String permitId;

	@Column(name = "PATRON_TYPE")
	private String patronType;

	@Column(name = "PERMIT_TYPE")
	private String permitType;

	@Column(name = "Amount")
	private String amount;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "LIC_NUM")
	private String licenceNumber;

	@Column(name = "CSU_ID")
	private String csuId;

	@Column(name = "PARKING_SLOT")
	private String parkingSlot;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinTable(name = "PRMT_PYMNT", joinColumns = @JoinColumn(name = "PERMIT_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "PERMIT_PAYMENT_ID", referencedColumnName = "id"))
	@IndexedEmbedded
	private PermitPayments permitPayments;

	@Transient
	private String timeSlot;

	@Column(name = "START_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate startDate;

	@Column(name = "END_DATE")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private LocalDate endDate;

	@Column(name = "START_TIME")
	@DateTimeFormat(pattern = "hh:mm a")
	private LocalTime startTime;

	@Column(name = "END_TIME")
	@DateTimeFormat(pattern = "hh:mm a")
	private LocalTime endTime;
	
	@Transient
	private String noOfMonths;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "COMMENT")
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNoOfMonths() {
		return noOfMonths;
	}

	public void setNoOfMonths(String noOfMonths) {
		this.noOfMonths = noOfMonths;
	}

	public String getPermitId() {
		return permitId;
	}

	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getPatronType() {
		return patronType;
	}

	public void setPatronType(String patronType) {
		this.patronType = patronType;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public String getCsuId() {
		return csuId;
	}

	public void setCsuId(String csuId) {
		this.csuId = csuId;
	}

	public PermitPayments getPermitPayments() {
		return permitPayments;
	}

	public void setPermitPayments(PermitPayments permitPayments) {
		this.permitPayments = permitPayments;
	}

	public String getParkingSlot() {
		return parkingSlot;
	}

	public void setParkingSlot(String parkingSlot) {
		this.parkingSlot = parkingSlot;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	
	public String getFormattedStartDate(){
		return CSUMDateUtils.getFormattedLocalDate(this.startDate);
		
	}
	
	public String getFormattedEndDate(){
		return CSUMDateUtils.getFormattedLocalDate(this.endDate);
		
	}
	
	public String getFormattedStartTime(){
		return CSUMDateUtils.getFormattedLocalTime(this.startTime);		
	}
	
	public String getFormattedEndTime(){
		return CSUMDateUtils.getFormattedLocalTime(this.endTime);
	}
}
