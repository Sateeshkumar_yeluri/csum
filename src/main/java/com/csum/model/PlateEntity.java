package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "PLATE_ENT")
@Audited
@AuditTable(value = "PLATE_ENT_HSTRY")
public class PlateEntity extends AbstractDomain{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1273491044349258840L;

	@Column(name = "VIN")
	private String vinNumber;
	
	@Column(name = "LIC_NUM")
	private String licenceNumber;
	
	@Column(name = "VHCL_MAKE")
	private String vehicleMake;
	
	@Column(name = "VHCL_MODEL")
	private String vehicleModel;
	
	@Column(name = "VHCL_COLOR")
	private String vehicleColor;
	
	@Column(name = "BODY_TYPE")
	private String bodyType;
	
	@Column(name = "VHCL_NUM")
	private String vehicleNumber;
	
	@Column(name = "STATE")
	private String state;

	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "YEAR")
	private String year;
	

	// ----------------------- Get and Set Methods -----------------------

	public String getVinNumber() {
		return vinNumber;
	}

	public void setVinNumber(String vinNumber) {
		this.vinNumber = vinNumber;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleColor() {
		return vehicleColor;
	}

	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}

	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
}
