package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "USER")
@NamedQuery(name = "com.csum.model.User.findByUserName", query = "SELECT u FROM User u WHERE u.userName = :userName")
public class User extends AbstractDomain{
	
	private static final long serialVersionUID = 293076841770771982L;
	
	@NotEmpty
	@Column(name = "USER_NAME", unique = true, nullable = false)
	private String userName;

	@NotEmpty
	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@NotEmpty
	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;

	@NotEmpty
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;
	
	@NotEmpty
	@Column(name = "ROLE", nullable = false)
	private String role;
	
	@NotEmpty
	@Column(name = "CSU_ID", nullable = false)
	private String csuId;

	// ----------------------- Get and Set Methods -----------------------
	
	public String getUserName() {
		return userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCsuId() {
		return csuId;
	}

	public void setCsuId(String csuId) {
		this.csuId = csuId;
	}
	
	
}
