package com.csum.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "VLN_CODE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQuery(name="com.csum.model.violationcode.findAll", query="Select v from ViolationCode v where v.isDeleted='N'")
public class ViolationCode extends AbstractDomain{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "CODE", unique = true, nullable = false)
	private String code;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "STND_FINE")
	private BigDecimal standardFine;

	@Column(name = "ONE")
	private BigDecimal one;

	@Column(name = "TWO")
	private BigDecimal two;

	@Column(name = "THREE")
	private BigDecimal three;
	
	@Column(name = "IS_DELETED")
	private char isDeleted;	

	// ----------------------- Get and Set Methods -----------------------

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getStandardFine() {
		return standardFine;
	}

	public void setStandardFine(BigDecimal standardFine) {
		this.standardFine = standardFine;
	}

	public BigDecimal getOne() {
		return one;
	}

	public void setOne(BigDecimal one) {
		this.one = one;
	}

	public BigDecimal getTwo() {
		return two;
	}

	public void setTwo(BigDecimal two) {
		this.two = two;
	}

	public BigDecimal getThree() {
		return three;
	}

	public void setThree(BigDecimal three) {
		this.three = three;
	}

	public char getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(char isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}
