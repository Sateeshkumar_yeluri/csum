package com.csum.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;




@Entity
@Audited
@AuditTable(value = "VIOLATION_HSTRY")
@Table(name = "VIOLATION")
@Indexed
@AnalyzerDef(name = "customanalyzer", tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = {
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
				@Parameter(name = "language", value = "English") }) })
@NamedQuery(name="com.csum.model.violation.findByViolationId", query="select v from Violation v where violationId = :violationId")
public class Violation extends AbstractDomain {
	
	private static final long serialVersionUID = 6579307032772985827L;
	
	@Column(name="VIOLATION_ID")
	private String violationId;
	
	@Column(name="VLN_LOCATION")
	private String locationOfViolation;
	
	@Column(name = "VLN_DATE")
	private String dateOfViolation;

	@Column(name = "VLN_TIME")
	private String timeOfViolation;
	
	@Column(name = "ISS_DATE")
	private String issueDate;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "ISS_TIME")
	private Date issueTime;
	
	@Column(name = "COMMENTS")
	private String Comments;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "TOT_DUE", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal totalDue;
	
	@Column(name = "FINE_AMNT", columnDefinition = "DECIMAL(7,2)")
	private BigDecimal fineAmount;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinTable(name = "VLN_PATRON", joinColumns = @JoinColumn(name = "VLN_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "PAT_ID", referencedColumnName = "id"))
	@IndexedEmbedded
	private Patron patron;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "VLN_CODE_ID", nullable = true)
	private ViolationCode violationCode;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinTable(name = "VLN_PLATE_ENT", joinColumns = @JoinColumn(name = "VLN_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "PLATE_ENT_ID", referencedColumnName = "id"))
	@IndexedEmbedded
	private PlateEntity plateEntity;
	
	@Column(name = "EMP_N0")
	private String employeeNumber;
	
	@Column(name = "ISS_OFFICER")
	private String issuingOfficer;
	
	@Column(name = "PROCESSDATE")
	private String processDate;

	@Column(name = "SUSPENDPROCESSDATE")
	private String suspendProcessDate;
	
	// ----------------------- Get and Set Methods -----------------------

	public String getViolationId() {
		return violationId;
	}

	public void setViolationId(String violationId) {
		this.violationId = violationId;
	}
	
	public String getLocationOfViolation() {
		return locationOfViolation;
	}

	public void setLocationOfViolation(String locationOfViolation) {
		this.locationOfViolation = locationOfViolation;
	}

	public String getDateOfViolation() {
		return dateOfViolation;
	}

	public void setDateOfViolation(String dateOfViolation) {
		this.dateOfViolation = dateOfViolation;
	}

	public String getTimeOfViolation() {
		return timeOfViolation;
	}

	public void setTimeOfViolation(String timeOfViolation) {
		this.timeOfViolation = timeOfViolation;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public Date getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(Date issueTime) {
		this.issueTime = issueTime;
	}

	public String getComments() {
		return Comments;
	}

	public void setComments(String comments) {
		Comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	public BigDecimal getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public Patron getPatron() {
		return patron;
	}

	public void setPatron(Patron patron) {
		this.patron = patron;
	}

	public ViolationCode getViolationCode() {
		return violationCode;
	}

	public void setViolationCode(ViolationCode violationCode) {
		this.violationCode = violationCode;
	}

	public PlateEntity getPlateEntity() {
		return plateEntity;
	}

	public void setPlateEntity(PlateEntity plateEntity) {
		this.plateEntity = plateEntity;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getIssuingOfficer() {
		return issuingOfficer;
	}

	public void setIssuingOfficer(String issuingOfficer) {
		this.issuingOfficer = issuingOfficer;
	}

	public String getProcessDate() {
		return processDate;
	}

	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}

	public String getSuspendProcessDate() {
		return suspendProcessDate;
	}

	public void setSuspendProcessDate(String suspendProcessDate) {
		this.suspendProcessDate = suspendProcessDate;
	}
		
}
