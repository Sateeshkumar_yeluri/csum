package com.csum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "HOLIDAY_LIST")
@NamedQueries({
@NamedQuery(name = "com.csum.model.holidays.findAll", query = "SELECT h FROM Holidays h")
})
public class Holidays extends AbstractDomain{
		
	private static final long serialVersionUID = -3374964181865329638L;
	
	@Column(name = "HOLIDAY_DATE")
	private String holidayDate;
	
	@Column(name = "DESCRIPTION")
	private String description;

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
