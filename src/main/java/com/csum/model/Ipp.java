package com.csum.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;


@Entity
@Table(name = "IPP")
@Audited
@AuditTable(value = "IPP_HSTRY")
@NamedQueries({
	@NamedQuery(name="com.csum.model.ipp.findByViolationId", query="SELECT i FROM Ipp i WHERE i.violation.violationId = :violationId"),
	@NamedQuery(name="com.csum.model.ipp.findIppByPlanNumber", query="SELECT i FROM Ipp i WHERE i.planNumber = :planNumber")
})
public class Ipp extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -692385963594305512L;

	@OneToOne
	@JoinTable(name = "IPP_VLN", joinColumns = @JoinColumn(name = "IPP_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "VLN_ID", referencedColumnName = "id"))
	private Violation violation;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long planNumber;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "IPP_STATUS")
	private String status;
	
	@Column(name = "ORIGINAL_DUE")
	private BigDecimal originalDue;

	@Column(name = "PREV_CREDITS")
	private BigDecimal previousCredits;

	@Column(name = "ENROLL_AMOUNT")
	private BigDecimal enrollAmount;

	@Column(name = "DOWN_PAYMENT")
	private BigDecimal downPayment;

	@Column(name = "UNENROLL_AMOUNT")
	private BigDecimal unenrollAmount;

	@Column(name = "INSTALL_AMOUNT")
	private BigDecimal installmentAmount;

	@Column(name = "PAYMENTS")
	private int noOfPayments;

	@Column(name = "START_DATE")
	private String startDate;
	
	@Column(name = "InstallmentDate_1")
	private String installmentDate1;
	
	@Column(name = "InstallmentDate_2")
	private String installmentDate2;
	
	@Column(name = "InstallmentDate_3")
	private String installmentDate3;
	
	public String getInstallmentDate1() {
		return installmentDate1;
	}

	public void setInstallmentDate1(String installmentDate1) {
		this.installmentDate1 = installmentDate1;
	}

	public String getInstallmentDate2() {
		return installmentDate2;
	}

	public void setInstallmentDate2(String installmentDate2) {
		this.installmentDate2 = installmentDate2;
	}
	
	public String getInstallmentDate3() {
		return installmentDate3;
	}

	public void setInstallmentDate3(String installmentDate3) {
		this.installmentDate3 = installmentDate3;
	}

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public long getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(long planNumber) {
		this.planNumber = planNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

		public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getOriginalDue() {
		return originalDue;
	}

	public void setOriginalDue(BigDecimal originalDue) {
		this.originalDue = originalDue;
	}

	public BigDecimal getPreviousCredits() {
		return previousCredits;
	}

	public void setPreviousCredits(BigDecimal previousCredits) {
		this.previousCredits = previousCredits;
	}

	public BigDecimal getEnrollAmount() {
		return enrollAmount;
	}

	public void setEnrollAmount(BigDecimal enrollAmount) {
		this.enrollAmount = enrollAmount;
	}

	public BigDecimal getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(BigDecimal downPayment) {
		this.downPayment = downPayment;
	}

	public BigDecimal getUnenrollAmount() {
		return unenrollAmount;
	}

	public void setUnenrollAmount(BigDecimal unenrollAmount) {
		this.unenrollAmount = unenrollAmount;
	}

	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public int getNoOfPayments() {
		return noOfPayments;
	}

	public void setNoOfPayments(int noOfPayments) {
		this.noOfPayments = noOfPayments;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
