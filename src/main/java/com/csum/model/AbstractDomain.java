package com.csum.model;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.DocumentId;

import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;

@Cacheable(true)
@MappedSuperclass
@Audited
@AuditOverride
public abstract class AbstractDomain implements java.io.Serializable {
	
	private static final long serialVersionUID = -7028311952645680890L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@DocumentId
	private Long id;
	
	@Transient
	private String user;

	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@Column(name = "updated_at")
	private LocalDateTime updatedAt;

	@Column(name = "updated_by", length = 20)
	private String updatedBy;

	@Column(name = "created_by", length = 20)
	private String createdBy;
	
	// ----------------------- Logic Methods -----------------------

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(this.getClass().getSimpleName());
			sb.append("@").append(hashCode());
			sb.append("[");
			toFieldString(sb, this.getClass().getSuperclass().getDeclaredFields());
			toFieldString(sb, this.getClass().getDeclaredFields());
			sb.append("]");
			return sb.toString();
		}

		// ----------------------- Helper Methods -----------------------

		protected void toFieldString(StringBuilder sb, Field[] fields) {
			for (Field field : fields) {
				if (field.isAnnotationPresent(javax.persistence.Id.class)
						|| field.isAnnotationPresent(javax.persistence.Column.class)) {

					try {
						String name = field.getName();
						field.setAccessible(true);
						Object value = field.get(this);
						sb.append(name).append("='").append(value).append("', ");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		// ----------------------- Get and Set Methods -----------------------
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public LocalDateTime getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(LocalDateTime createdAt) {
			this.createdAt = createdAt;
		}

		public LocalDateTime getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(LocalDateTime updatedAt) {
			this.updatedAt = updatedAt;
		}
		
		@PreUpdate
		public void onPreUpdate() {
			this.updatedAt = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID));
			this.updatedBy = getUser();
		}

		@PrePersist
		public void onPrePersist() {
			this.createdAt = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID));
			this.createdBy = getUser();
		}
		
		public String getFormattedUpdatedAt() {
			return CSUMDateUtils.getFormattedLocalDateTime(this.updatedAt);
		}

		public String getFormattedCreatedAt() {
			return CSUMDateUtils.getFormattedLocalDateTime(this.createdAt);
		}

}
