package com.csum.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;


@Entity
@Table(name = "REVIEWPROCESS")
@NamedQuery(name="com.csum.model.reviewProcess.findByViolationId", query="SELECT rp FROM ReviewProcess rp WHERE rp.violation.violationId = :violationId")
public class ReviewProcess extends AbstractDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7963671231732348781L;
	
	public String question1;
	public String question2;
	public String ignoreTime;
	@Transient
	public Long suspends;
	@Transient
	public Long correspondence;
	@Transient
	public String commentsInfoID;
	@Transient
	public String noteedited;

	@Transient
	private List<MultipartFile> files;
	@Transient
	private String fileType;
	
	@Transient
	private boolean letterSent;
	
	private boolean status;
	
	@OneToOne
	@JoinColumn(name = "VLN_ID", nullable = false, updatable = false)
	private Violation violation;

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getNoteedited() {
		return noteedited;
	}

	public void setNoteedited(String noteedited) {
		this.noteedited = noteedited;
	}

	public Long getSuspends() {
		return suspends;
	}

	public void setSuspends(Long suspends) {
		this.suspends = suspends;
	}

	public Long getCorrespondence() {
		return correspondence;
	}

	public void setCorrespondence(Long correspondence) {
		this.correspondence = correspondence;
	}

	public String getCommentsInfoID() {
		return commentsInfoID;
	}

	public void setCommentsInfoID(String commentsInfoID) {
		this.commentsInfoID = commentsInfoID;
	}

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getIgnoreTime() {
		return ignoreTime;
	}

	public void setIgnoreTime(String ignoreTime) {
		this.ignoreTime = ignoreTime;
	}

	public boolean isLetterSent() {
		return letterSent;
	}

	public void setLetterSent(boolean letterSent) {
		this.letterSent = letterSent;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
