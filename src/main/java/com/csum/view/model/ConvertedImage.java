package com.csum.view.model;

public class ConvertedImage {

	private long fId;
	private String fName;

	public ConvertedImage() {
	}

	public ConvertedImage(long fId, String fName) {
		super();
		this.fId = fId;
		this.fName = fName;
	}

	public long getfId() {
		return fId;
	}

	public void setfId(long fId) {
		this.fId = fId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {	
		this.fName = fName;
	}

}
