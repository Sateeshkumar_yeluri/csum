package com.csum.view.model;

import java.time.LocalDateTime;

public class HearingHistoryView {
	private String description;
	private LocalDateTime hstryDateTime;
	private String dateTimeInString;
	private String type;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getHstryDateTime() {
		return hstryDateTime;
	}
	public void setHstryDateTime(LocalDateTime hstryDateTime) {
		this.hstryDateTime = hstryDateTime;
	}
	public String getDateTimeInString() {
		return dateTimeInString;
	}
	public void setDateTimeInString(String dateTimeInString) {
		this.dateTimeInString = dateTimeInString;
	}
	
}
