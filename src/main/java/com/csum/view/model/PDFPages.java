package com.csum.view.model;

public class PDFPages {

	private long pageId;
	private String pageName;
	private byte[] pageContent;

	public long getPageId() {
		return pageId;
	}

	public void setPageId(long pageId) {
		this.pageId = pageId;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public byte[] getPageContent() {
		return pageContent;
	}

	public void setPageContent(byte[] pageContent) {
		this.pageContent = pageContent;
	}

	@Override
	public String toString() {
		return "PDFPages [pageId=" + pageId + ", pageName=" + pageName + "]";
	}
}
