package com.csum.view.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class UploadForm {

	private String violationId;
	
	private List<MultipartFile> files;

	private String fileType;

	public String getViolationId() {
		return violationId;
	}

	public void setViolationId(String violationId) {
		this.violationId = violationId;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
