package com.csum.view.model;

import java.util.List;

import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.Hearing;
import com.csum.model.Ipp;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;

public class HistoryBean {

	private String historyItem;
	
	private List<Comment> commentHistory;

	private List<List<Correspondence>> correspHistory;

	private List<Hearing> hearingHistory;

	private List<List<Notices>> noticeHistory;

	private List<List<Payment>> paymentHistory;
	
	private List<Penalty> penaltyHistory;

	private List<PlateEntity> plateHistory;
	
	private List<List<Suspends>> suspendHistory;
	
	private List<Ipp> ippHistory;

	public List<Comment> getCommentHistory() {
		return commentHistory;
	}

	public void setCommentHistory(List<Comment> commentHistory) {
		this.commentHistory = commentHistory;
	}

	public List<List<Correspondence>> getCorrespHistory() {
		return correspHistory;
	}

	public void setCorrespHistory(List<List<Correspondence>> correspHistory) {
		this.correspHistory = correspHistory;
	}

	public List<Hearing> getHearingHistory() {
		return hearingHistory;
	}

	public void setHearingHistory(List<Hearing> hearingHistory) {
		this.hearingHistory = hearingHistory;
	}

	public List<List<Notices>> getNoticeHistory() {
		return noticeHistory;
	}

	public void setNoticeHistory(List<List<Notices>> noticeHistory) {
		this.noticeHistory = noticeHistory;
	}

	public List<List<Payment>> getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(List<List<Payment>> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	public List<Penalty> getPenaltyHistory() {
		return penaltyHistory;
	}

	public void setPenaltyHistory(List<Penalty> penaltyHistory) {
		this.penaltyHistory = penaltyHistory;
	}	

	public List<PlateEntity> getPlateHistory() {
		return plateHistory;
	}

	public void setPlateHistory(List<PlateEntity> plateHistory) {
		this.plateHistory = plateHistory;
	}

	public List<List<Suspends>> getSuspendHistory() {
		return suspendHistory;
	}

	public void setSuspendHistory(List<List<Suspends>> suspendHistory) {
		this.suspendHistory = suspendHistory;
	}

	public String getHistoryItem() {
		return historyItem;
	}

	public void setHistoryItem(String historyItem) {
		this.historyItem = historyItem;
	}

	public List<Ipp> getIppHistory() {
		return ippHistory;
	}

	public void setIppHistory(List<Ipp> ippHistory) {
		this.ippHistory = ippHistory;
	}
	
}
