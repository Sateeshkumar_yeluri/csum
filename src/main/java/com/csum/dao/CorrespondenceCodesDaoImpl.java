package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.CorrespondenceCode;

@Repository("correspondenceCodesDao")
public class CorrespondenceCodesDaoImpl extends AbstractDao<Integer, CorrespondenceCode>
		implements CorrespondenceCodesDao {

	public static final Logger logger = LogManager.getLogger(CorrespondenceCodesDaoImpl.class);

	@Override
	public List<CorrespondenceCode> findAll() {
		List<CorrespondenceCode> correspondenceCodes;
		try {
			correspondenceCodes = entityManager
					.createNamedQuery("com.csum.model.correspondenceCodes.findAll", CorrespondenceCode.class)
					.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return correspondenceCodes;
	}

	@Override
	public CorrespondenceCode findById(Long correspCodeId) {
		 CorrespondenceCode correspondenceCode;
		try{
			correspondenceCode = (CorrespondenceCode) entityManager.createNamedQuery("com.csum.model.correspondenceCodes.findById",CorrespondenceCode.class).setParameter("correspCodeId",correspCodeId).getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
		return correspondenceCode;
	}
}
