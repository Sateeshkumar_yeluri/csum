package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.ViolationImages;

@Repository("violationImagesDao")
public class ViolationImagesDaoImpl extends AbstractDao<Integer, ViolationImages>implements ViolationImagesDao{
	
	public static final Logger logger = LogManager.getLogger(ViolationImagesDaoImpl.class);

	@Override
	public void save(ViolationImages images) {
		try{
			entityManager.persist(images);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public List<ViolationImages> findByViolationId(String violationId) {
		List<ViolationImages> images;
		try{
			images = entityManager.createNamedQuery("com.csum.model.violationImages.findByViolationId",ViolationImages.class).setParameter("violationId", violationId).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return images;
	}

	@Override
	public List<ViolationImages> findByParentId(long parentId) {
		List<ViolationImages> images;
		try{
			images = entityManager.createNamedQuery("com.csum.model.violationImages.findByParentId",ViolationImages.class).setParameter("parentFileId", parentId).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return images;
	}

	@Override
	public ViolationImages findByImageId(long imageId) {
		ViolationImages image;
		try{
			image = entityManager.createNamedQuery("com.csum.model.violationImages.findByImageId",ViolationImages.class).setParameter("imageId", imageId).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return image;
	}

	@Override
	public void updateImage(ViolationImages vlnImage) {
		try{
			ViolationImages entity = findByImageId(vlnImage.getId());
			entity.setImage(vlnImage.getImage());
			entity.setImageName(vlnImage.getImageName());
			entity.setImageType(vlnImage.getImageType());
			entity.setParentFileId(vlnImage.getParentFileId());
			entity.setUploadedDate(vlnImage.getUploadedDate());
			entity.setType(vlnImage.getType());
			entity.setUser(vlnImage.getUser());
			entity.setViolation(vlnImage.getViolation());
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public void delete(long id) {
		try{
			ViolationImages images = findByImageId(id);
			entityManager.remove(images);
			int res = entityManager.createQuery("DELETE FROM ViolationImages i  WHERE i.parentFileId = :parentId").setParameter("parentId", id).executeUpdate();
			
		}catch (Exception e) {
			logger.error(e, e);
		}
		
	}

	@Override
	public List<ViolationImages> findByType(String violationId, String type) {
		List<ViolationImages> image;
		try{
			image = entityManager.createNamedQuery("com.csum.model.violationImages.findByType",ViolationImages.class).setParameter("type", type).setParameter("violationId", violationId).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return image;
	}

}
