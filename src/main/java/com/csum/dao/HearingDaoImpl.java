package com.csum.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Hearing;

@Repository("hearingDao")
public class HearingDaoImpl extends AbstractDao<Integer, Hearing> implements HearingDao {

	public static final Logger logger = LogManager.getLogger(HearingDaoImpl.class);

	@Override
	public Hearing findByViolationId(String violationId) {
		Hearing hearing;
		try {
			hearing = entityManager.createNamedQuery("com.csum.model.hearing.findByViolationId", Hearing.class)
					.setParameter("violationId", violationId).getSingleResult();
			return hearing;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Hearing> findLatestByDate(LocalDate date) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findLatestByDate", Hearing.class)
					.setParameter("hearingDate", date).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public void save(Hearing hearingForm) {
		logger.debug("Saving hearing information");
		try {
			entityManager.persist(hearingForm);
			logger.debug("Hearing information saved");
		} catch (Exception e) {
			logger.error(e, e);
		}

	}

	@Override
	public void updateHearing(Hearing hearingForm) {
		logger.debug("Updating hearing information");
		try {
			Hearing entity = findById(hearingForm.getId());
			entity.setDisposition(hearingForm.getDisposition());
			entity.setDispositionDate(hearingForm.getDispositionDate());
			entity.setDispositionTime(hearingForm.getDispositionTime());
			entity.setHearingDate(hearingForm.getHearingDate());
			entity.setHearingTime(hearingForm.getHearingTime());
			entity.setHearingOfficer(hearingForm.getHearingOfficer());
			entity.setReduction(hearingForm.getReduction());
			entity.setTotalDue(hearingForm.getTotalDue());
			entity.setUser(hearingForm.getUser());
			entity.setStatus(hearingForm.getStatus());
			entity.setType(hearingForm.getType());
			entity.setNoFineChange(hearingForm.isNoFineChange());
			entity.setDecision(hearingForm.getDecision());
			entity.setRescheduled(hearingForm.isRescheduled());
			entity.setScheduledAt(hearingForm.getScheduledAt());
			entity.setScheduledBy(hearingForm.getScheduledBy());
			entity.setDateMailed(hearingForm.getDateMailed());
			logger.debug("Hearing information updated");
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

	@Override
	public Hearing findById(Long hearingId) {
		try {
			return entityManager.find(Hearing.class, hearingId);
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate curdate) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findAllPending", Hearing.class)
					.setParameter("hearingDate", curdate).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate curDate, String hearingType) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findAllPendingByType", Hearing.class)
					.setParameter("hearingDate", curDate).setParameter("type", hearingType).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findAllPendingBetween", Hearing.class)
					.setParameter("hearingDate1", date1).setParameter("hearingDate2", date2).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2, String hearingType) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllPendingByTypeBetween", Hearing.class)
					.setParameter("hearingDate1", date1).setParameter("hearingDate2", date2)
					.setParameter("type", hearingType).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllWrittenHearingsList() {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findAllPendingWritten", Hearing.class)
					.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(LocalDate curDate) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager.createNamedQuery("com.csum.model.hearing.findAllCompleted", Hearing.class)
					.setParameter("hearingDate", curDate).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate curDate) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllCompletedStatuses", Hearing.class)
					.setParameter("status1", hearingStatus1).setParameter("status2", hearingStatus2)
					.setParameter("hearingDate", curDate).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllCompletedStatus", Hearing.class)
					.setParameter("status", hearingStatus).setParameter("hearingDate", date).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(LocalDate date1, LocalDate date2) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllCompletedBetween", Hearing.class)
					.setParameter("hearingDate1", date1).setParameter("hearingDate2", date2).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate date1,
			LocalDate date2) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllCompletedBetweenStatuses", Hearing.class)
					.setParameter("hearingDate1", date1).setParameter("hearingDate2", date2)
					.setParameter("status1", hearingStatus1).setParameter("status2", hearingStatus2).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date1, LocalDate date2) {
		List<Hearing> hearingsList;
		try {
			hearingsList = entityManager
					.createNamedQuery("com.csum.model.hearing.findAllCompletedBetweenStatus", Hearing.class)
					.setParameter("hearingDate1", date1).setParameter("hearingDate2", date2)
					.setParameter("status", hearingStatus).getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return hearingsList;
	}

	@Override
	public void delete(Long hearingId) {
		try {
			Hearing entity = findById(hearingId);
			entityManager.remove(entity);
		} catch (NoResultException e) {
			logger.error(e, e);
		}		
	}

}
