package com.csum.dao;

import java.util.List;

import com.csum.model.ViolationImages;

public interface ViolationImagesDao {
	
	void save(ViolationImages images);
	
	List<ViolationImages> findByViolationId(String violationId);
	
	List<ViolationImages> findByParentId(long parentId);

	ViolationImages findByImageId(long imageId);
	
	void updateImage(ViolationImages vlnImage);
	
	void delete(long id);
	
	List<ViolationImages> findByType(String violationId, String type);
}
