package com.csum.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.csum.model.SuspendedCodes;
import com.csum.model.Suspends;
import com.csum.model.Violation;
import com.csum.utils.CSUMDateUtils;

@Repository("suspendsDao")
public class SuspendsDaoImpl extends AbstractDao<Integer, Suspends> implements SuspendsDao{
	
	public static final Logger logger = LogManager.getLogger(SuspendsDaoImpl.class);
	
	@Autowired
	private SuspendedCodesDao suspendedCodesDao;
	
	@Autowired
	private ViolationDao violationDao;

	@Override
	public void save(Suspends suspend) {
		try{
			if(suspend!=null && suspend.getSuspendedCodes()!=null){
				SuspendedCodes suspendedCodes =  suspendedCodesDao.findById(suspend.getSuspendedCodes().getId());
				Violation violation = violationDao.findByViolationId(suspend.getViolation().getViolationId());
				if(suspendedCodes!=null && suspendedCodes.getDays()>0){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(new Date());	
					calendar.add(Calendar.DATE, suspend.getSuspendedCodes().getDays());
					violation.setSuspendProcessDate(CSUMDateUtils.getDateInString(calendar.getTime()));
					suspend.setViolation(violation);
				}
			}			
			entityManager.persist(suspend);
		}catch(Exception e){
			logger.error(e,e);
		}		
	}

	@Override
	public List<Suspends> findByViolationId(String violationId) {
		List<Suspends> suspends;
		try{
			suspends = entityManager.createNamedQuery("com.csum.model.suspends.findByViolationId",Suspends.class).setParameter("violationId", violationId).getResultList();			
		}catch(NoResultException e){
			return null;
		}
		return suspends;
	}

	@Override
	public Suspends findLatestByViolationId(String violationId) {
		Suspends suspend;
		try{
			suspend = entityManager.createNamedQuery("com.csum.model.suspends.findLatestByViolationId",Suspends.class).setParameter("violationId", violationId).setMaxResults(1).getSingleResult();			
		}catch(NoResultException e){
			return null;
		}
		return suspend;
	}

}
