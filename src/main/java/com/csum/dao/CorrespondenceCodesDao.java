package com.csum.dao;

import java.util.List;

import com.csum.model.CorrespondenceCode;

public interface CorrespondenceCodesDao {
	
	List<CorrespondenceCode> findAll();
	
	CorrespondenceCode findById(Long correspCodeId);

}
