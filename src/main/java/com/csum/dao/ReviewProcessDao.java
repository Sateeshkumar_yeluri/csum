package com.csum.dao;

import java.util.List;

import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;

public interface ReviewProcessDao {
	
	List<ReviewComments> findAllReviewComments();
	
	ReviewProcess findByViolationId(String violationId);
	
	void save(ReviewProcess reviewProcess);

	void update(ReviewProcess reviewProcess);
}
