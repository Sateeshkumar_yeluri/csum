package com.csum.dao;

import java.util.List;

import com.csum.model.DispositionCode;

public interface DispositionCodeDao {
	
	List<DispositionCode> findAll();
	
	DispositionCode findByCode(String dispCode);

}
