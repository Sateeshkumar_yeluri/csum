package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.SuspendedCodes;

@Repository("suspendedCodesDao")
public class SuspendedCodesDaoImpl extends AbstractDao<Integer, SuspendedCodes> implements SuspendedCodesDao{
	
	public static final Logger logger = LogManager.getLogger(SuspendedCodesDaoImpl.class);

	@Override
	public List<SuspendedCodes> findAll() {
		List<SuspendedCodes> suspendedCodes;
		try{
			suspendedCodes = entityManager.createNamedQuery("com.csum.model.suspendedCodes.findAll",SuspendedCodes.class).getResultList();
			return suspendedCodes;
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	public SuspendedCodes findByCode(int suspCode) {
		SuspendedCodes suspendedCodes;
		try{
			suspendedCodes = (SuspendedCodes) entityManager.createNamedQuery("com.csum.model.suspendedCodes.findByCode",SuspendedCodes.class).setParameter("suspCode", suspCode).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return suspendedCodes;
	}

	@Override
	public SuspendedCodes findById(Long suspCodeId) {	
		SuspendedCodes suspendedCodes;
		try{
			suspendedCodes = (SuspendedCodes) entityManager.createNamedQuery("com.csum.model.suspendedCodes.findById",SuspendedCodes.class).setParameter("suspCodeId", suspCodeId).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return suspendedCodes;
	}

}
