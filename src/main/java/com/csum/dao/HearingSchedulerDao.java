package com.csum.dao;

import java.util.List;

import com.csum.model.HearingScheduler;

public interface HearingSchedulerDao {

	List<HearingScheduler> findAll();

}
