package com.csum.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.Hearing;
import com.csum.model.Ipp;
import com.csum.model.NoticeType;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.PaymentMethod;
import com.csum.model.Penalty;
import com.csum.model.PenaltyCode;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;
import com.csum.model.Transaction;
import com.csum.model.Violation;

@Repository("historyDao")
public class HistoryDaoImpl extends AbstractDao<Integer, Violation> implements HistoryDao {

	public static final Logger logger = LogManager.getLogger(HistoryDaoImpl.class);

	@Autowired
	private ViolationDao violationDao;

	@Autowired
	private PenaltyDao penaltyDao;

	@Autowired
	private PaymentDetailsDao paymentDetailsDao;

	@Autowired
	private NoticesDao noticesDao;

	@Autowired
	private HearingDao hearingDao;

	@Autowired
	private SuspendsDao suspendsDao;

	@Autowired
	private CorrespondenceDao correspondenceDao;

	@Autowired
	private PaymentMethodDao paymentMethodDao;

	@Autowired
	private TransactionDao transactionDao;

	@Autowired
	private NoticeTypeDao noticeTypeDao;
	
	@Autowired
	private IppDao ippDao;
	
	@Autowired
	private ViolationImagesDao violationImagesDao;

	@Override
	public List<Penalty> getAllPenaltyHistory(String violationId) {
		List<Penalty> penaltyWithCodeHistory = new ArrayList<Penalty>();
		List<Penalty> penaltyHistory = new ArrayList<Penalty>();
		Penalty penalty = penaltyDao.findByViolationId(violationId);
		if (penalty != null) {
			Long penaltyId = penalty.getId();
			AuditReader reader = AuditReaderFactory.get(entityManager);
			AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Penalty.class, true, false)
					.add(AuditEntity.id().eq(penaltyId));
			query.addOrder(AuditEntity.revisionNumber().desc());
			penaltyHistory = query.getResultList();
			List<PenaltyCode> penaltyCodeHistory = new ArrayList<PenaltyCode>();
			for (Penalty penalty2 : penaltyHistory) {
				Long penaltyCodeId = penalty2.getPenaltyCode().getId();
				AuditQuery penaltyCodeQuery = (AuditQuery) reader.createQuery()
						.forRevisionsOfEntity(PenaltyCode.class, true, false).add(AuditEntity.id().eq(penaltyCodeId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				penaltyCodeHistory = penaltyCodeQuery.getResultList();
				logger.debug("penalty code history" + penaltyCodeHistory);
				for (PenaltyCode penaltyCode : penaltyCodeHistory) {
					Penalty penalty3 = clonePenalty(penalty2);
					penalty3.setPenaltyCode(penaltyCode);
					if (penaltyCode.getUpdatedBy() != null) {
						penalty3.setUpdatedBy(penaltyCode.getUpdatedBy());
						penalty3.setUpdatedAt(penaltyCode.getUpdatedAt());
					}
					Violation violation = violationDao.findByViolationId(violationId);
					penalty3.setViolation(violation);
					penaltyWithCodeHistory.add(penalty3);
				}
			}
			logger.debug("Penalty History" + penaltyWithCodeHistory);
		}
		return penaltyWithCodeHistory;
	}

	public Penalty clonePenalty(Penalty object) {
		Penalty penalty = new Penalty();
		penalty.setViolation(object.getViolation());
		penalty.setCreatedAt(object.getCreatedAt());
		penalty.setCreatedBy(object.getCreatedBy());
		penalty.setId(object.getId());
		penalty.setTotalDue(object.getTotalDue());
		penalty.setUpdatedAt(object.getUpdatedAt());
		penalty.setUpdatedBy(object.getUpdatedBy());
		penalty.setUser(object.getUser());
		return penalty;

	}

	@Override
	public List<List<Payment>> getAllPaymentHistory(String violationId) {
		List<List<Payment>> paymentDetailsHistory = new ArrayList<List<Payment>>();
		List<Payment> paymentlist = paymentDetailsDao.findByViolationId(violationId);
		Transaction tx = null;
		if (paymentlist != null) {
			List<Payment> paymentHistory = null;
			for (Payment payment : paymentlist) {
				Long paymentId = payment.getId();
				AuditReader reader = AuditReaderFactory.get(entityManager);
				AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Payment.class, true, false)
						.add(AuditEntity.id().eq(paymentId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				paymentHistory = query.getResultList();
				if (paymentHistory != null) {
					for (Payment paymentvalue : paymentHistory) {
						int id = paymentvalue.getMethodId();
						PaymentMethod paymentMethod = new PaymentMethod();
						paymentMethod = paymentMethodDao.findByType(id);
						paymentvalue.setPaymentMethod(paymentMethod);
						if (paymentvalue.getTransaction() != null) {
							tx = transactionDao.findById(paymentvalue.getTransaction().getId());
						}
						paymentvalue.setTransaction(tx);
					}
				}
				paymentDetailsHistory.add(paymentHistory);
			}
		}
		//logger.debug("Payment History" + paymentDetailsHistory);
		return paymentDetailsHistory;
	}

	@Override
	public List<Hearing> getAllHearingHistory(String violationId) {
		List<Hearing> hearingDetailsHistory = new ArrayList<Hearing>();
		Hearing hearing = hearingDao.findByViolationId(violationId);
		if (hearing != null) {
			Long hearingId = hearing.getId();
			AuditReader reader = AuditReaderFactory.get(entityManager);
			AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Hearing.class, true, false)
					.add(AuditEntity.id().eq(hearingId));
			query.addOrder(AuditEntity.revisionNumber().desc());
			hearingDetailsHistory = query.getResultList();
		}
		//logger.debug("Hearing History" + hearingDetailsHistory);
		return hearingDetailsHistory;
	}

	@Override
	public List<List<Suspends>> getAllSuspendsHistory(String violationId) {
		List<List<Suspends>> suspendDetailsHistory = new ArrayList<List<Suspends>>();
		List<Suspends> suspendModifiedHistory = new ArrayList<Suspends>();
		List<Suspends> suspendlist = suspendsDao.findByViolationId(violationId);
		if (suspendlist != null) {
			List<Suspends> suspendHistory = null;
			for (Suspends suspend : suspendlist) {
				Long suspendId = suspend.getId();
				AuditReader reader = AuditReaderFactory.get(entityManager);
				AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Suspends.class, true, false)
						.add(AuditEntity.id().eq(suspendId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				suspendHistory = query.getResultList();
				if (suspendHistory != null) {
					for (Suspends suspendValue : suspendHistory) {
						/*
						 * int code = suspendValue.getCode(); SuspendedCodes
						 * suspendCode = suspendedCodeDao.findById(code);
						 */
						suspendValue.setSuspendedCodes(suspend.getSuspendedCodes());
							suspendModifiedHistory.add(suspendValue);
					}
				}
				suspendDetailsHistory.add(suspendModifiedHistory);
			}
		}
		//logger.debug("Suspend History" + suspendDetailsHistory);
		return suspendDetailsHistory;
	}

	@Override
	public List<List<Correspondence>> getAllCorrespHistory(String violationId) {
		List<List<Correspondence>> correspondenceDetailsHistory = new ArrayList<List<Correspondence>>();
		List<Correspondence> corresModifiedHistory = new ArrayList<Correspondence>();
		List<Correspondence> correslist = correspondenceDao.findByViolationId(violationId);
		if (correslist != null) {
			List<Correspondence> corresHistory = null;
			for (Correspondence corres : correslist) {
				Long corresId = corres.getId();
				AuditReader reader = AuditReaderFactory.get(entityManager);
				AuditQuery query = (AuditQuery) reader.createQuery()
						.forRevisionsOfEntity(Correspondence.class, true, false).add(AuditEntity.id().eq(corresId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				corresHistory = query.getResultList();
				if (corresHistory != null) {
					for (Correspondence corresValue : corresHistory) {
						/*
						 * long typCode = corresValue.getTypCode();
						 * CorrespondenceCode correspCode =
						 * correspondenceCodeDao.findById(typCode);
						 */
						corresValue.setCorrespCode(corres.getCorrespCode());
							corresModifiedHistory.add(corresValue);
					}
				}
				correspondenceDetailsHistory.add(corresModifiedHistory);
			}
		}
		//logger.debug("Correspondence History" + correspondenceDetailsHistory);
		return correspondenceDetailsHistory;
	}

	@Override
	public List<List<Notices>> getAllNoticesHistory(String violationId) {
		List<List<Notices>> noticeDetailsHistory = new ArrayList<List<Notices>>();
		List<Notices> noticeslist = noticesDao.findByViolationId(violationId);
		if (noticeslist != null) {
			List<Notices> noticeHistory = null;
			for (Notices notice : noticeslist) {
				Long noticeId = notice.getId();
				AuditReader reader = AuditReaderFactory.get(entityManager);
				AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Notices.class, true, false)
						.add(AuditEntity.id().eq(noticeId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				noticeHistory = query.getResultList();
				if (noticeHistory != null) {
					for (Notices noticeValue : noticeHistory) {
						int type = noticeValue.getNoticeType().getType();
						if (type != 0) {
							NoticeType noticeType = noticeTypeDao.findByType(type);
							noticeValue.setNoticeType(noticeType);
						}
					}
				}
				noticeDetailsHistory.add(noticeHistory);
			}
		}
		//logger.debug("Notice History" + noticeDetailsHistory);
		return noticeDetailsHistory;
	}

	@Override
	public List<PlateEntity> getAllPlateHistory(String violationId) {
		List<PlateEntity> plateEntityHistory = new ArrayList<PlateEntity>();
		List<PlateEntity> plateHistory = new ArrayList<PlateEntity>();
		Violation violation = violationDao.findByViolationId(violationId);
		if (violation != null) {
			Long plateEntityId = violation.getPlateEntity().getId();
			AuditReader reader = AuditReaderFactory.get(entityManager);
			AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(PlateEntity.class, true, false)
					.add(AuditEntity.id().eq(plateEntityId));
			query.addOrder(AuditEntity.revisionNumber().desc());
			plateEntityHistory = query.getResultList();
			// logger.debug("plate entity History" + plateEntityHistory);
		}
		if (plateEntityHistory.size() == 1 && StringUtils.isEmpty(plateEntityHistory.get(0).getCreatedBy())) {
			return null;
		}
		for (PlateEntity plate : plateEntityHistory) {
			if (!StringUtils.isEmpty(plate.getLicenceNumber()) || !StringUtils.isEmpty(plate.getVinNumber())
					|| !StringUtils.isEmpty(plate.getState()) || !StringUtils.isEmpty(plate.getMonth())
					|| !StringUtils.isEmpty(plate.getYear()) || !StringUtils.isEmpty(plate.getBodyType())
					|| !StringUtils.isEmpty(plate.getVehicleMake()) || !StringUtils.isEmpty(plate.getVehicleModel())
					|| !StringUtils.isEmpty(plate.getVehicleColor())) {
				plateHistory.add(plate);
			}
		}
		return plateHistory;
	}

	@Override
	public List<Ipp> getAllIppHistory(String violationId) {
		try {
			Ipp ipp = ippDao.findByViolationId(violationId);
			List<Ipp> ippHistory = null;
			if (ipp != null) {
				Long ippId = ipp.getId();
				AuditReader reader = AuditReaderFactory.get(entityManager);
				AuditQuery query = (AuditQuery) reader.createQuery().forRevisionsOfEntity(Ipp.class, true, false)
						.add(AuditEntity.id().eq(ippId));
				query.addOrder(AuditEntity.revisionNumber().desc());
				ippHistory = query.getResultList();
				logger.debug("citation History" + ippHistory);
			}
			return ippHistory;
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public void deleteAll(String createdBy) {
		try{
			List<Long> violationIds = entityManager.createQuery("SELECT v.id FROM Violation v where v.createdBy = :createdBy").setParameter("createdBy", createdBy).getResultList();
			logger.debug("violationIds>>: "+violationIds);
			if(violationIds!=null && violationIds.size()>0){
			int vlnImgRes = entityManager.createNativeQuery("DELETE FROM VIOLATION_IMAGES where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnImgRes >>"+vlnImgRes);
			int cmntRes = entityManager.createNativeQuery("DELETE FROM COMMENT where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("cmntRes >>"+cmntRes);
			int suspHisRes = entityManager.createNativeQuery("DELETE FROM SUSPENDS_HSTRY where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("suspHisRes >>"+suspHisRes);
			int suspRes = entityManager.createNativeQuery("DELETE FROM SUSPENDS where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("suspRes >>"+suspRes);
			int correspHisRes = entityManager.createNativeQuery("DELETE FROM CORRESPONDENCE_HSTRY where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("correspHisRes >>"+correspHisRes);
			int correspRes = entityManager.createNativeQuery("DELETE FROM CORRESPONDENCE where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("correspRes >>"+correspRes);
			int noticesHisRes = entityManager.createNativeQuery("DELETE FROM NOTICES_HSTRY where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("noticesHisRes >>"+noticesHisRes);
			int noticesRes = entityManager.createNativeQuery("DELETE FROM NOTICES where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("noticesRes >>"+noticesRes);
			int pymntHisRes = entityManager.createNativeQuery("DELETE FROM PAYMENT_HSTRY where VIOLATION in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pymntHisRes >>"+pymntHisRes);
			int pymntRes = entityManager.createNativeQuery("DELETE FROM PAYMENT where VIOLATION in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pymntRes >>"+pymntRes);
			int tranRes = entityManager.createNativeQuery("DELETE FROM TRANSACTION where violationId in (SELECT VIOLATION_ID from VIOLATION where id in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("tranRes >>"+tranRes);
			int ippHisRes = entityManager.createNativeQuery("DELETE FROM IPP_HSTRY where ID in (SELECT IPP_ID FROM IPP_VLN WHERE VLN_ID IN (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ippHisRes >>"+ippHisRes);
			int ippRes = entityManager.createNativeQuery("DELETE FROM IPP where ID in (SELECT IPP_ID FROM IPP_VLN WHERE VLN_ID IN (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ippRes >>"+ippRes);
			int ippVlnHisRes = entityManager.createNativeQuery("DELETE FROM IPP_VLN_AUD WHERE VLN_ID IN (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ippVlnHisRes >>"+ippVlnHisRes);
			int ippVlnRes = entityManager.createNativeQuery("DELETE FROM IPP_VLN WHERE VLN_ID IN (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ippVlnRes >>"+ippVlnRes);
			int pnltyCdHisRes = entityManager.createNativeQuery("DELETE FROM PNLT_CODE_HSTRY where id in (SELECT PNLT_CODE FROM PNLT where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pnltyCdHisRes >>"+pnltyCdHisRes);
			int pnltyCdRes = entityManager.createNativeQuery("DELETE FROM PNLT_CODE where id in (SELECT PNLT_CODE FROM PNLT where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pnltyCdRes >>"+pnltyCdRes);
			int pnltyHisRes = entityManager.createNativeQuery("DELETE FROM PNLT_HSTRY where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pnltyHisRes >>"+pnltyHisRes);
			int pnltyRes = entityManager.createNativeQuery("DELETE FROM PNLT where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pnltyRes >>"+pnltyRes);
			int hrngDtlsRes = entityManager.createNativeQuery("DELETE FROM HEARING_DETAILS where HEARING_ID in (SELECT id FROM HEARING where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("hrngDtlsRes >>"+hrngDtlsRes);
			int hrngHisRes = entityManager.createNativeQuery("DELETE FROM HEARING_HSTRY where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("hrngHisRes >>"+hrngHisRes);
			int hrngRes = entityManager.createNativeQuery("DELETE FROM HEARING where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("hrngRes >>"+hrngRes);
			int pltEntHisRes = entityManager.createNativeQuery("DELETE FROM PLATE_ENT_HSTRY where id in (SELECT PLATE_ENT_ID FROM VLN_PLATE_ENT where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pltEntHisRes >>"+pltEntHisRes);
			int pltEntRes = entityManager.createNativeQuery("DELETE FROM PLATE_ENT where id in (SELECT PLATE_ENT_ID FROM VLN_PLATE_ENT where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("pltEntRes >>"+pltEntRes);
			int vlnPltEntHisRes = entityManager.createNativeQuery("DELETE FROM VLN_PLATE_ENT_AUD where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnPltEntHisRes >>"+vlnPltEntHisRes);
			int vlnPltEntRes = entityManager.createNativeQuery("DELETE FROM VLN_PLATE_ENT where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnPltEntRes >>"+vlnPltEntRes);
			int adrsHisRes = entityManager.createNativeQuery("DELETE FROM ADDRESS_HSTRY where id in (SELECT ADDR_ID FROM PATRON_ADDR where PATRON_ID in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids)))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("adrsHisRes >>"+adrsHisRes);
			int adrsRes = entityManager.createNativeQuery("DELETE FROM ADDRESS where id in (SELECT ADDR_ID FROM PATRON_ADDR where PATRON_ID in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids)))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("adrsRes >>"+adrsRes);
			int ptrnadrsHisRes = entityManager.createNativeQuery("DELETE FROM PATRON_ADDR_AUD where PATRON_ID in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ptrnadrsHisRes >>"+ptrnadrsHisRes);
			int ptrnadrsRes = entityManager.createNativeQuery("DELETE FROM PATRON_ADDR where PATRON_ID in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ptrnadrsRes >>"+ptrnadrsRes);
			int ptrnHisRes = entityManager.createNativeQuery("DELETE FROM PATRON_HSTRY where id in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ptrnHisRes >>"+ptrnHisRes);
			int ptrnRes = entityManager.createNativeQuery("DELETE FROM PATRON where id in (SELECT PAT_ID FROM VLN_PATRON where VLN_ID in (:ids))").setParameter("ids", violationIds).executeUpdate();
			logger.debug("ptrnRes >>"+ptrnRes);
			int vlnptrnHisRes = entityManager.createNativeQuery("DELETE FROM VLN_PATRON_AUD where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnptrnHisRes >>"+vlnptrnHisRes);
			int vlnptrnRes = entityManager.createNativeQuery("DELETE FROM VLN_PATRON where VLN_ID in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnptrnRes >>"+vlnptrnRes);
			int vlnHisRes = entityManager.createNativeQuery("DELETE FROM VIOLATION_HSTRY where id in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnHisRes >>"+vlnHisRes);
			int vlnRes = entityManager.createNativeQuery("DELETE FROM VIOLATION where id in (:ids)").setParameter("ids", violationIds).executeUpdate();
			logger.debug("vlnRes >>"+vlnRes);	
			}
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public Comment findCommentByMode(String violationId, String mode) {
		Comment comment = null;
		try{
			comment = entityManager.createNamedQuery("com.csum.model.comment.findCommentByMode",Comment.class).setParameter("mode", mode).setParameter("violationId", violationId).setMaxResults(1).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return comment;
	}

}
