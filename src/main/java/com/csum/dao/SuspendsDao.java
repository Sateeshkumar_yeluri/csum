package com.csum.dao;

import java.util.List;

import com.csum.model.Suspends;

public interface SuspendsDao {
	
	void save(Suspends suspend);

	List<Suspends> findByViolationId(String violationId);
	
	Suspends findLatestByViolationId(String violationId);
}
