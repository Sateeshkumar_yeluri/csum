package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Notices;

@Repository("noticesDao")
public class NoticesDaoImpl extends AbstractDao<Integer, Notices> implements NoticesDao{
	
	public static final Logger logger = LogManager.getLogger(NoticesDaoImpl.class);

	@Override
	public List<Notices> findByViolationId(String violationId) {
		List<Notices> noticesList;
		try{
			noticesList = entityManager.createNamedQuery("com.csum.model.notices.findByViolationId",Notices.class).setParameter("violationId", violationId).getResultList();
			return noticesList;
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	public Notices findLatestByViolationId(String violationId) {
		Notices notices;
		try{
			notices = entityManager.createNamedQuery("com.csum.model.notices.findLatestByViolationId",Notices.class).setParameter("violationId", violationId).setMaxResults(1).getSingleResult();
			return notices;
		}catch(NoResultException e){
			return null;
		}
	}

}
