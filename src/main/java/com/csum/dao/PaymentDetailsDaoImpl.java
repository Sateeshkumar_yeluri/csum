package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.csum.model.Payment;
import com.csum.model.PaymentMethod;
import com.csum.model.Violation;
import com.csum.service.ViolationService;
import com.csum.dao.PaymentMethodDao;

@Repository("paymentDetailsDao")
public class PaymentDetailsDaoImpl extends AbstractDao<Integer, Payment> implements PaymentDetailsDao {

	public static final Logger logger = LogManager.getLogger(PaymentDetailsDaoImpl.class);
	
	@Autowired
	private PaymentMethodDao paymentMethodDao;

	@Autowired
	private ViolationService violationService;
	
	@Override
	public Payment findByTransactionId(Long id) {
		try {
			return (Payment) entityManager.createNamedQuery("com.csum.model.Payment.findByTransactionId",Payment.class).setParameter("txId", id).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void save(Payment payment) {
		try{
		PaymentMethod paymentMethodObj = paymentMethodDao.findByType(payment.getPaymentMethod().getTypCd());
		Violation violation = violationService.findByViolationId(payment.getViolation().getViolationId());
		payment.setPaymentMethod(paymentMethodObj);
		payment.setViolation(violation);
		entityManager.persist(payment);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public List<Payment> findByViolationId(String violationId) {
		List<Payment> paymentsList = null;
		try{
			paymentsList=entityManager.createNamedQuery("com.csum.model.payment.findByViolationId",Payment.class).setParameter("violationId", violationId).getResultList();
			return paymentsList;
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	public Payment findLatestByViolationId(String violationId) {
		Payment payment;
		try{
			payment = entityManager.createNamedQuery("com.csum.model.payment.findLatestByViolationId",Payment.class).setParameter("violationId", violationId).setMaxResults(1).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return payment;
	}

	
	
	@Override
	public void updatePaymentDetails(Payment payment) {
		try{
			Payment entity = findById(payment.getId());
			entity.setAccount(payment.getAccount());
			entity.setAmount(payment.getAmount());
			if (payment.getChequeFile() != null && payment.getChequeFile().length > 0) {
			entity.setChequeFile(payment.getChequeFile());
			entity.setChequeFileName(payment.getChequeFileName());
			entity.setChequeFileType(payment.getChequeFileType());
			}
			entity.setUser(payment.getUser());
			entity.setMethodId(payment.getMethodId());
			entity.setOverPaid(payment.getOverPaid());
			entity.setPaymentDate(payment.getPaymentDate());
			PaymentMethod method = paymentMethodDao.findByType(payment.getPaymentMethod().getTypCd());
			if (method != null) {
				entity.setPaymentMethod(method);
			}
			entity.setPaymentSource(payment.getPaymentSource());
			entity.setProcessedBy(payment.getProcessedBy());
			entity.setProcessedOn(payment.getProcessedOn());
			entity.setTotalDue(payment.getTotalDue());
			Violation violation= entity.getViolation();
			violation.setTotalDue(payment.getViolation().getTotalDue());
			violation.setUser(payment.getUser());
			entity.setViolation(violation);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	private Payment findById(Long paymentId) {
		Payment payment;
		try{
			payment = entityManager.find(Payment.class, paymentId);
		}catch(NoResultException e){
			return null;
		}
		return payment;
	}

	@Override
	public List<Payment> findByPlanNumber(long planNumber) {
		List<Payment> payments;
		try{
			payments = entityManager.createNamedQuery("com.csum.model.payment.findByPlanNumber",Payment.class).setParameter("planNumber", planNumber).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return payments;
	}
}
