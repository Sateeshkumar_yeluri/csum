package com.csum.dao;

import com.csum.model.PermitTransaction;

public interface PermitTransactionDao {
	
	void save(PermitTransaction transaction);

}
