package com.csum.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.PermitTransaction;

@Repository("PermitTransactionDao")
public class PermitTransactionDaoImpl extends AbstractDao<Integer, PermitTransaction> implements PermitTransactionDao{
	
	public static final Logger logger = LogManager.getLogger(PermitTransactionDaoImpl.class);

	@Override
	public void save(PermitTransaction transaction) {
		try{
			persist(transaction);
		}catch(Exception e){
			logger.error(e,e);
		}
	}
	
	
}
    