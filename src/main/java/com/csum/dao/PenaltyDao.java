package com.csum.dao;

import com.csum.model.Penalty;

public interface PenaltyDao {

	Penalty findByViolationId(String violationId);
}
