 package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.NoticeType;

@Repository("noticeTypeDao")
public class NoticeTypeDaoImpl extends AbstractDao<Integer, NoticeType> implements NoticeTypeDao{
	
	public static final Logger logger = LogManager.getLogger(NoticeTypeDaoImpl.class);

	@Override
	public NoticeType findByType(int type) {
		NoticeType noticeType;
		try{
			noticeType = entityManager.createNamedQuery("com.csum.model.noticeType.findByType",NoticeType.class).setParameter("type", type).getSingleResult();
		}catch(NoResultException e){
		return null;
		}
		return noticeType;
	}

}
