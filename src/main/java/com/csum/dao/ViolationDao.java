package com.csum.dao;

import java.util.List;

import com.csum.model.Comment;
import com.csum.model.Violation;
import com.csum.model.ViolationSearch;

public interface ViolationDao {
	
	Violation findByViolationId(String violationId);
	
	void saveViolation(Violation violationForm);
	
	List<Violation> findBySearchCriteria(ViolationSearch violationSearchForm);

	void updateViolation(Violation violation);
	
	void addComment(Comment comment);
}
