package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Correspondence;

@Repository("correspondenceDao")
public class CorrespondenceDaoImpl extends AbstractDao<Integer, Correspondence> implements CorrespondenceDao{
	
	public static final Logger logger = LogManager.getLogger(CorrespondenceDaoImpl.class);

	@Override
	public void save(Correspondence corresp) {
		try{
			entityManager.persist(corresp);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public List<Correspondence> findByViolationId(String violationId) {
		List<Correspondence> correspondences;
		try{
			correspondences = entityManager.createNamedQuery("com.csum.model.correspondence.findByViolationId",Correspondence.class).setParameter("violationId", violationId).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return correspondences;
	}

	@Override
	public Correspondence findLatestByViolationId(String violationId) {
		Correspondence correspondence;
		try{
			correspondence = entityManager.createNamedQuery("com.csum.model.correspondence.findLatestByViolationId",Correspondence.class).setParameter("violationId", violationId).setMaxResults(1).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return correspondence;
	}
	
}
