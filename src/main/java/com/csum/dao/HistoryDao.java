package com.csum.dao;

import java.util.List;

import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.Hearing;
import com.csum.model.Ipp;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;

public interface HistoryDao {

	List<Penalty> getAllPenaltyHistory(String violationId);

	List<List<Payment>> getAllPaymentHistory(String violationId);

	List<Hearing> getAllHearingHistory(String violationId);

	List<List<Suspends>> getAllSuspendsHistory(String violationId);

	List<List<Correspondence>> getAllCorrespHistory(String violationId);

	List<List<Notices>> getAllNoticesHistory(String violationId);

	List<PlateEntity> getAllPlateHistory(String violationId);
	
	void deleteAll(String createdBy);
	
	List<Ipp> getAllIppHistory(String violationId);
	
	Comment findCommentByMode(String violationId, String mode);

}
