package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Payment;
import com.csum.model.PermitPayments;

@Repository("permitPaymentsDao")
public class PermitPaymentsDaoImpl extends AbstractDao<Integer, PermitPayments> implements PermitPaymentsDao{
	
	public static final Logger logger = LogManager.getLogger();

	@Override
	public void savePermitPayment(PermitPayments permitPayments) {
		try{
			entityManager.persist(permitPayments);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public PermitPayments findByTransactionId(Long id) {
		try {
			return (PermitPayments) entityManager.createNamedQuery("com.csum.model.PermitPayments.findByTransactionId",PermitPayments.class).setParameter("txId", id).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
