package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.HearingScheduler;

@Repository
public class HearingSchedulerDaoImpl extends AbstractDao<Integer, HearingScheduler> implements HearingSchedulerDao {

	public static final Logger logger = LogManager.getLogger(HearingSchedulerDaoImpl.class);

	@Override
	public List<HearingScheduler> findAll() {
		try {
			return entityManager.createNamedQuery("com.csum.model.HearingScheduler.findAll", HearingScheduler.class)
					.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}
