package com.csum.dao;

import java.util.List;

import com.csum.model.Admin;

public interface AdminDao {

	Admin findByUserName(String username);
	
	List<Admin> findAllUsers();
}
