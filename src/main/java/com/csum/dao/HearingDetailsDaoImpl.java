package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Hearing;
import com.csum.model.HearingDetail;

@Repository("hearingDetailsDao")
public class HearingDetailsDaoImpl extends AbstractDao<Integer, HearingDetail> implements HearingDetailsDao {

	public static final Logger logger = LogManager.getLogger(HearingDetailsDaoImpl.class);

	@Override
	public HearingDetail findByHearing(Long hearingId) {
		HearingDetail hearingDetail;
		try{
			hearingDetail = entityManager.createNamedQuery("com.csum.model.Hearing.findByHearing",HearingDetail.class).setParameter("hearingId", hearingId).setMaxResults(1).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return hearingDetail;
	}

	@Override
	public void save(HearingDetail hearingForm) {
		try{
			Hearing hearing = entityManager.find(Hearing.class, hearingForm.getHearing().getId());
			hearing.setStatus(hearingForm.getStatus());
			hearing.setNoFineChange(hearingForm.getHearing().isNoFineChange());
			hearing.setReduction(hearingForm.getHearing().getReduction());
			hearing.setHearingOfficer(hearingForm.getHearing().getHearingOfficer());
			hearing.setDispositionDate(hearingForm.getHearing().getDispositionDate());
			hearing.setTotalDue(hearingForm.getHearing().getTotalDue());
			hearing.setDisposition(hearingForm.getHearing().getDisposition());
			hearing.setDispositionTime(hearingForm.getHearing().getDispositionTime());
			hearing.setUser(hearingForm.getUser());
			hearing.setDecision(hearingForm.getDecision());
			hearing.setDateMailed(hearingForm.getHearing().getDateMailed());
			hearingForm.setHearing(hearing);
			hearingForm.setTotalDue(hearing.getTotalDue());
			entityManager.persist(hearingForm);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public void deleteByHearingId(Long hearingId) {
		try {
			int res = getEntityManager().createQuery("delete from HearingDetail h where h.hearing.id=:hearing")
					.setParameter("hearing", hearingId).executeUpdate();			
		}catch(Exception e){
			logger.error(e, e);
		}
		
	}
}
