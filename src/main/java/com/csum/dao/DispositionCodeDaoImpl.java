package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.DispositionCode;

@Repository("dispositionCodeDao")
public class DispositionCodeDaoImpl extends AbstractDao<Integer, DispositionCode> implements DispositionCodeDao{

	public static final Logger logger = LogManager.getLogger();

	@Override
	public List<DispositionCode> findAll() {
		List<DispositionCode> dispositionCodes;
		try{
			dispositionCodes = entityManager.createNamedQuery("com.csum.model.DispositionCode.findAll",DispositionCode.class).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return dispositionCodes;
	}

	@Override
	public DispositionCode findByCode(String dispCode) {
		DispositionCode dispositionCode;
		try{
			dispositionCode = entityManager.createNamedQuery("com.csum.model.DispositionCode.findByCode",DispositionCode.class).setParameter("dispCode", dispCode).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return dispositionCode;
	}
}
