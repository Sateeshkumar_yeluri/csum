package com.csum.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Address;
import com.csum.model.Comment;
import com.csum.model.Patron;
import com.csum.model.PlateEntity;
import com.csum.model.Violation;
import com.csum.model.ViolationSearch;
import com.csum.utils.CSUMConstants;

@Repository("violationDao")
public class ViolationDaoImpl extends AbstractDao<Integer, Violation> implements ViolationDao {

	public static final Logger logger = LogManager.getLogger(ViolationDaoImpl.class);

	@Override
	public Violation findByViolationId(String violationId) {
		try {
			Violation violation = entityManager
					.createNamedQuery("com.csum.model.violation.findByViolationId", Violation.class)
					.setParameter("violationId", violationId).getSingleResult();
			return violation;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void saveViolation(Violation violationForm) {
		try {
			logger.debug("Violation saving started >> " + violationForm.getViolationId());
			Patron patron = new Patron();
			Address address = new Address();
			PlateEntity plateEntity = new PlateEntity();
			if (violationForm.getPatron() != null) {
				patron = violationForm.getPatron();
				if (patron.getAddress() != null) {
					address = patron.getAddress();
				} else {
					patron.setAddress(address);
				}
			} else {
				patron.setAddress(address);
				violationForm.setPatron(patron);
			}
			if (violationForm.getPlateEntity() != null) {
				plateEntity = violationForm.getPlateEntity();
			} else {
				violationForm.setPlateEntity(plateEntity);
			}
			address.setUser(violationForm.getUser());
			patron.setUser(violationForm.getUser());
			plateEntity.setUser(violationForm.getUser());
			entityManager.persist(address);
			entityManager.persist(patron);
			entityManager.persist(plateEntity);
			entityManager.persist(violationForm);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

	@Override
	public List<Violation> findBySearchCriteria(ViolationSearch violationSearchForm) {
		List<Violation> violationList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Violation> criteriaQuery = criteriaBuilder.createQuery(Violation.class);
			Root<Violation> violation = criteriaQuery.from(Violation.class);
			criteriaQuery.select(violation);
			TypedQuery<Violation> typedQuery = null;
			List<Predicate> predicates = new ArrayList<>();
			if (violationSearchForm != null && violationSearchForm.getSearchType() != null) {				
				if (!violationSearchForm.getSearchType().equalsIgnoreCase("SL") && !violationSearchForm.getSearchType().equalsIgnoreCase("Latest")) {
					if (violationSearchForm.getSearchType().equalsIgnoreCase("VN")) {
					String violationId = violationSearchForm.getTicketNumber();
					predicates.add(criteriaBuilder.equal(violation.get("violationId"), violationId));
					if (predicates.isEmpty()) {
						criteriaQuery = null;
					} else {
						criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
					}
				} else if (violationSearchForm.getSearchType().equalsIgnoreCase("LN")) {
					String licenceNumber = violationSearchForm.getTicketNumber();
					Join<Violation, PlateEntity> plateEntity = violation.join("plateEntity");
					predicates.add(criteriaBuilder.equal(plateEntity.get("licenceNumber"), licenceNumber));
					if (predicates.isEmpty()) {
						criteriaQuery = null;
					} else {
						criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
					}
				} else if (violationSearchForm.getSearchType().equalsIgnoreCase("LName")) {
					Join<Violation, Patron> patron = violation.join("patron");
					String lastName = violationSearchForm.getLastName();
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(patron.get("lastName")),
							lastName.toUpperCase().trim() + CSUMConstants.CSUM_LIKE_SEARCH_SUFFIX));
					if (predicates.isEmpty()) {
						criteriaQuery = null;
					} else {
						criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
					}
				} else if (violationSearchForm.getSearchType().equalsIgnoreCase("VN&LName")) {
					String violationId = violationSearchForm.getTicketNumber();
					Join<Violation, Patron> patron = violation.join("patron");
					String lastName = violationSearchForm.getLastName();
					predicates.add(criteriaBuilder.and(
							criteriaBuilder.like(criteriaBuilder.upper(patron.get("lastName")),
									lastName.toUpperCase().trim() + CSUMConstants.CSUM_LIKE_SEARCH_SUFFIX),
							criteriaBuilder.equal(violation.get("violationId"), violationId)));
					if (predicates.isEmpty()) {
						criteriaQuery = null;
					} else {
						criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
					}
				}
				}
				if (criteriaQuery != null) {
					criteriaQuery.orderBy(criteriaBuilder.desc(violation.get("id")));
					typedQuery = entityManager.createQuery(criteriaQuery);
					if(violationSearchForm.getSearchType().equalsIgnoreCase("Latest")){
						typedQuery.setMaxResults(10);
					}
					violationList = typedQuery.getResultList();
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return violationList;
	}

	@Override
	public void updateViolation(Violation violation) {
		logger.debug("updating the citation information:");
		try {
			Violation entity = findByViolationId(violation.getViolationId());
			if (entity != null) {
				entity.setViolationId(violation.getViolationId());
				entity.setLocationOfViolation(violation.getLocationOfViolation());
				entity.setDateOfViolation(violation.getDateOfViolation());
				entity.setTimeOfViolation(violation.getTimeOfViolation());
				entity.setIssueDate(violation.getIssueDate());
				entity.setIssueTime(violation.getIssueTime());
				entity.setComments(violation.getComments());
				entity.setStatus(violation.getStatus());
				entity.setTotalDue(violation.getTotalDue());
				entity.setFineAmount(violation.getFineAmount());
				entity.setEmployeeNumber(violation.getEmployeeNumber());
				entity.setIssuingOfficer(violation.getIssuingOfficer());
				entity.setViolationCode(violation.getViolationCode());
				entity.setProcessDate(violation.getProcessDate());
				entity.setSuspendProcessDate(violation.getSuspendProcessDate());
				entity.setUser(violation.getUser());
				if (violation.getPatron() != null) {
					if (entity.getPatron() == null) {
						Patron patron = new Patron();
						patron.setFirstName(violation.getPatron().getFirstName());
						patron.setLastName(violation.getPatron().getLastName());
						patron.setMiddleName(violation.getPatron().getMiddleName());
						entity.setPatron(patron);
					} else {
						entity.getPatron().setFirstName(violation.getPatron().getFirstName());
						entity.getPatron().setLastName(violation.getPatron().getLastName());
						entity.getPatron().setMiddleName(violation.getPatron().getMiddleName());
					}
					if (violation.getPatron().getAddress() != null) {
						if (entity.getPatron().getAddress() == null) {
							Address address = new Address();
							address.setAddress(violation.getPatron().getAddress().getAddress());
							address.setCity(violation.getPatron().getAddress().getCity());
							address.setZip(violation.getPatron().getAddress().getZip());
							address.setState(violation.getPatron().getAddress().getState());
							violation.getPatron().setAddress(address);
						} else {
							entity.getPatron().getAddress().setAddress(violation.getPatron().getAddress().getAddress());
							entity.getPatron().getAddress().setCity(violation.getPatron().getAddress().getCity());
							entity.getPatron().getAddress().setZip(violation.getPatron().getAddress().getZip());
							entity.getPatron().getAddress().setState(violation.getPatron().getAddress().getState());
						}
					}
				}
				if (violation.getPlateEntity() != null) {
					if (entity.getPlateEntity() == null) {
						PlateEntity plateEntity = new PlateEntity();
						plateEntity.setVinNumber(violation.getPlateEntity().getVinNumber());
						plateEntity.setLicenceNumber(violation.getPlateEntity().getLicenceNumber());
						plateEntity.setVehicleColor(violation.getPlateEntity().getVehicleColor());
						plateEntity.setVehicleMake(violation.getPlateEntity().getVehicleMake());
						plateEntity.setVehicleModel(violation.getPlateEntity().getVehicleModel());
						plateEntity.setVehicleNumber(violation.getPlateEntity().getVehicleNumber());
						plateEntity.setBodyType(violation.getPlateEntity().getBodyType());
						plateEntity.setState(violation.getPlateEntity().getState());
						plateEntity.setMonth(violation.getPlateEntity().getMonth());
						plateEntity.setYear(violation.getPlateEntity().getYear());
					} else {
						entity.getPlateEntity().setVinNumber(violation.getPlateEntity().getVinNumber());
						entity.getPlateEntity().setLicenceNumber(violation.getPlateEntity().getLicenceNumber());
						entity.getPlateEntity().setVehicleColor(violation.getPlateEntity().getVehicleColor());
						entity.getPlateEntity().setVehicleMake(violation.getPlateEntity().getVehicleMake());
						entity.getPlateEntity().setVehicleModel(violation.getPlateEntity().getVehicleModel());
						entity.getPlateEntity().setVehicleNumber(violation.getPlateEntity().getVehicleNumber());
						entity.getPlateEntity().setBodyType(violation.getPlateEntity().getBodyType());
						entity.getPlateEntity().setState(violation.getPlateEntity().getState());
						entity.getPlateEntity().setMonth(violation.getPlateEntity().getMonth());
						entity.getPlateEntity().setYear(violation.getPlateEntity().getYear());
					}
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

	@Override
	public void addComment(Comment comment) {
		try {
			entityManager.persist(comment);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

}
