package com.csum.dao;

import java.util.List;

import com.csum.model.HearingDecision;

public interface HearingDecisionDao {
	
	List<HearingDecision> findAll();
	
	HearingDecision findById(Long id);

}
