package com.csum.dao;

import java.util.List;

import com.csum.model.Correspondence;

public interface CorrespondenceDao {
	
	void save(Correspondence corresp);

	List<Correspondence> findByViolationId(String violationId);
	
	Correspondence findLatestByViolationId(String violationId);

}
