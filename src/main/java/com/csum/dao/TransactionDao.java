package com.csum.dao;

import com.csum.model.Transaction;

public interface TransactionDao {

	void save(Transaction transcation);

	Transaction findById(Long id);
}
