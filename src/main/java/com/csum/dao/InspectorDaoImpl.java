package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Inspector;

@Repository("inspectorDao")
public class InspectorDaoImpl extends AbstractDao<Integer, Inspector> implements InspectorDao{
	
	public static final Logger logger = LogManager.getLogger(InspectorDaoImpl.class);

	@Override
	public Inspector findByUserName(String username) {
		try{
			Inspector inspector = entityManager.createNamedQuery("com.csum.model.violation.findByInspectorUserName",Inspector.class).setParameter("username", username).getSingleResult();
				return inspector;
			}catch(NoResultException e){
				return null;
			}
	}
	
	/*@Override
	public Inspector findByEmployeeId(String employeeId)
	{
		try{
			long employeId = Long.parseLong(employeeId);
			Inspector inspector = entityManager.createNamedQuery("com.csum.model.violation.findByInspectorEmployeeId",Inspector.class).setParameter("employeeId", employeId).getSingleResult();
				return inspector;
			}catch(NoResultException e){
				return null;
			}
	}*/

}
