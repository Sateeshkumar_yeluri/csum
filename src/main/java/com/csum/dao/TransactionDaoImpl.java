package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Transaction;

@Repository("transactionDao")
public class TransactionDaoImpl extends AbstractDao<Integer,Transaction> implements TransactionDao{
	
	public static final Logger logger =LogManager.getLogger(TransactionDaoImpl.class);

	@Override
	public void save(Transaction transcation) {
		try{
			persist(transcation);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	@Override
	public Transaction findById(Long id) {
		Transaction transaction;
		try{
			transaction = entityManager.createNamedQuery("com.csum.model.transaction.findById",Transaction.class).setParameter("id",id).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return transaction;
	}

}
