package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao{
	
	private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);

	public User findByUserName(String userName) {
		try {
			User user = (User) entityManager.createNamedQuery("com.csum.model.User.findByUserName", User.class)
					.setParameter("userName", userName).getSingleResult();
			return user;
		} catch (NoResultException ex) {
			return null;
		}
	}

}
