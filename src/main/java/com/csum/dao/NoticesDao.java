package com.csum.dao;

import java.util.List;

import com.csum.model.Notices;

public interface NoticesDao {
	
	List<Notices> findByViolationId(String violationId);
	
	Notices findLatestByViolationId(String violationId);

}
