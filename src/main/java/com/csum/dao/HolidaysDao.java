package com.csum.dao;

import java.util.ArrayList;

import com.csum.model.Holidays;

public interface HolidaysDao {
	
	ArrayList<Holidays> findAll();

}
