package com.csum.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Permit;
import com.csum.model.ViolationSearch;
import com.csum.utils.CSUMConstants;

@Repository("permitDao")
public class PermitDaoImpl extends AbstractDao<Integer, Permit> implements PermitDao {

	public static final Logger logger = LogManager.getLogger(PermitDaoImpl.class);

	@Override
	public void savePermit(Permit permit) {
		try {
			entityManager.persist(permit);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

	@Override
	public Permit findByPermitId(String permitId) {
		try {
			Permit permit = entityManager.createNamedQuery("com.csum.model.permit.findByPermitId", Permit.class)
					.setParameter("permitId", permitId).getSingleResult();
			return permit;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Permit> findBySearchCriteria(ViolationSearch permitSearchForm) {
		List<Permit> permitList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Permit> criteriaQuery = criteriaBuilder.createQuery(Permit.class);
			Root<Permit> permit = criteriaQuery.from(Permit.class);
			criteriaQuery.select(permit);
			TypedQuery<Permit> typedQuery = null;
			List<Predicate> predicates = new ArrayList<>();
			if (permitSearchForm != null && permitSearchForm.getSearchType() != null) {
				if (!permitSearchForm.getSearchType().equalsIgnoreCase("SL")
						&& !permitSearchForm.getSearchType().equalsIgnoreCase("Latest")) {
					if (permitSearchForm.getSearchType().equalsIgnoreCase("PN")) {
						String permitId = permitSearchForm.getTicketNumber();
						predicates.add(criteriaBuilder.equal(permit.get("permitId"), permitId));
						if (predicates.isEmpty()) {
							criteriaQuery = null;
						} else {
							criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
						}
					} else if (permitSearchForm.getSearchType().equalsIgnoreCase("LN")) {
						String licNum = permitSearchForm.getTicketNumber();
						predicates.add(criteriaBuilder.like(criteriaBuilder.upper(permit.get("licenceNumber")),
								CSUMConstants.CSUM_LIKE_SEARCH_SUFFIX + licNum.toUpperCase().trim()
										+ CSUMConstants.CSUM_LIKE_SEARCH_SUFFIX));/*
																					 * predicates
																					 * .
																					 * add
																					 * (
																					 * criteriaBuilder
																					 * .
																					 * like
																					 * (
																					 * criteriaBuilder
																					 * .
																					 * upper
																					 * (
																					 * permit
																					 * .
																					 * get
																					 * (
																					 * "licenceNumber"
																					 * )
																					 * )
																					 * ,
																					 * licNum
																					 * .
																					 * toUpperCase
																					 * (
																					 * )
																					 * .
																					 * trim
																					 * (
																					 * )
																					 * +
																					 * CSUMConstants
																					 * .
																					 * CSUM_LIKE_SEARCH_SUFFIX
																					 * )
																					 * )
																					 * ;
																					 * predicates
																					 * .
																					 * add
																					 * (
																					 * criteriaBuilder
																					 * .
																					 * like
																					 * (
																					 * criteriaBuilder
																					 * .
																					 * upper
																					 * (
																					 * permit
																					 * .
																					 * get
																					 * (
																					 * "licenceNumber"
																					 * )
																					 * )
																					 * ,
																					 * CSUMConstants
																					 * .
																					 * CSUM_LIKE_SEARCH_SUFFIX
																					 * +
																					 * licNum
																					 * .
																					 * toUpperCase
																					 * (
																					 * )
																					 * .
																					 * trim
																					 * (
																					 * )
																					 * )
																					 * )
																					 * ;
																					 */
						if (predicates.isEmpty()) {
							criteriaQuery = null;
						} else {
							criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[] {})));
						}
					}
				}
				if (criteriaQuery != null) {
					criteriaQuery.orderBy(criteriaBuilder.desc(permit.get("id")));
					typedQuery = entityManager.createQuery(criteriaQuery);
					if (permitSearchForm.getSearchType().equalsIgnoreCase("Latest")) {
						typedQuery.setFirstResult(0);
						typedQuery.setMaxResults(10);
					}
					permitList = typedQuery.getResultList();
				}
			}
			return permitList;
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * @Override public Permit findValidPermit(String licenceNumber, String
	 * parkingSlot, LocalDate scannedDate, LocalTime scannedTime) { try{ Permit
	 * permit = (Permit)
	 * entityManager.createNamedQuery("com.csum.model.permit.findValidPermit",
	 * Permit.class).setParameter("licenceNumber",
	 * licenceNumber).setParameter("parkingSlot",
	 * parkingSlot).setParameter("scannedDate",
	 * scannedDate).setParameter("scannedTime", scannedTime); return permit;
	 * }catch(NoResultException e){ return null; } }
	 */

	@Override
	public Permit findValidPermit(String licenceNumber, String parkingSlot, LocalDateTime scannedDateTime) {
		try {
			Permit permit = (Permit) entityManager
					.createNamedQuery("com.csum.model.permit.findValidPermit", Permit.class)
					.setParameter("licenceNumber", licenceNumber).setParameter("parkingSlot", parkingSlot)
					.setParameter("scannedDateTime", scannedDateTime).setMaxResults(1).getSingleResult();
			return permit;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void deleteAllPermitData(String createdBy) {
		try {
			int prmtpymntHisRes = entityManager.createNativeQuery("DELETE FROM PRMT_PYMNT_AUD WHERE PERMIT_ID IN (SELECT ID FROM PERMIT WHERE created_by = :createdBy)").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("prmtpymntHisRes >> "+prmtpymntHisRes);
			int prmtpymntRes = entityManager.createNativeQuery("DELETE FROM PRMT_PYMNT WHERE PERMIT_ID IN (SELECT ID FROM PERMIT WHERE created_by = :createdBy)").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("prmtpymntRes >> "+prmtpymntRes);
			int pymntHisRes = entityManager.createNativeQuery("DELETE FROM PERMIT_PAYMENT_HSTRY WHERE created_by = :createdBy").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("pymntHisRes >> "+pymntHisRes);
			int pymntRes = entityManager.createNativeQuery("DELETE FROM PERMIT_PAYMENT WHERE created_by = :createdBy").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("pymntRes >> "+pymntRes);
			int prmtHisRes = entityManager.createNativeQuery("DELETE FROM PERMIT_HSTRY WHERE created_by = :createdBy").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("prmtHisRes >> "+prmtHisRes);
			int prmtRes = entityManager.createNativeQuery("DELETE FROM PERMIT WHERE created_by = :createdBy").setParameter("createdBy", createdBy).executeUpdate();
			logger.debug("prmtRes >> "+prmtRes);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

}
