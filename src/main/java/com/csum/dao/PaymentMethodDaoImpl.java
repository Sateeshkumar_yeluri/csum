package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.PaymentMethod;

@Repository("paymentMethodDao")
public class PaymentMethodDaoImpl extends AbstractDao<Integer, PaymentMethod> implements PaymentMethodDao{
	
	public static final Logger logger = LogManager.getLogger(PaymentMethodDaoImpl.class);

	@Override
	public PaymentMethod findByType(int type) {
		try{
			PaymentMethod paymentMethod = entityManager.createNamedQuery("com.csum.model.PaymentMethod.findByType",PaymentMethod.class).setParameter("typCd", type).getSingleResult();
			return paymentMethod;
			
		}catch(NoResultException e){
			return null;			
		}		
	}

}
