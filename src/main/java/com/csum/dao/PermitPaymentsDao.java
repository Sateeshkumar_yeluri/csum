package com.csum.dao;

import com.csum.model.PermitPayments;

public interface PermitPaymentsDao {
	
	void savePermitPayment(PermitPayments permitPayments);

	PermitPayments findByTransactionId(Long id);
}
