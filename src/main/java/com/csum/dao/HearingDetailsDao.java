package com.csum.dao;

import com.csum.model.HearingDetail;

public interface HearingDetailsDao {
	
	HearingDetail findByHearing(Long hearingId);
	
	void save(HearingDetail hearingForm);

	void deleteByHearingId(Long hearingId);
}
