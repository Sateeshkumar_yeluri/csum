package com.csum.dao;

import java.util.List;

import com.csum.model.SuspendedCodes;

public interface SuspendedCodesDao {
	
	List<SuspendedCodes> findAll();
	
	SuspendedCodes findByCode(int suspCode);

	SuspendedCodes findById(Long suspCodeId);
}
