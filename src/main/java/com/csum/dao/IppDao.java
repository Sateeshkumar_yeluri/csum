package com.csum.dao;

import com.csum.model.Ipp;

public interface IppDao {
	
	Ipp findByViolationId(String violationId);
	
	void save(Ipp ippObj);

	Ipp findIppByPlanNumber(Long planNumber);

	void updateIpp(Ipp ipp);
	
	void deleteById(Long id);
}
