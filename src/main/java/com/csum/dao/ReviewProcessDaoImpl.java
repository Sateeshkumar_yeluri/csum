package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;

@Repository("reviewProcessDao")
public class ReviewProcessDaoImpl extends AbstractDao<Integer, ReviewProcess> implements ReviewProcessDao{
	
	public static final Logger logger = Logger.getLogger(ReviewProcessDaoImpl.class);

	@Override
	public List<ReviewComments> findAllReviewComments() {
		 List<ReviewComments> comments;
		try{
			comments = entityManager.createNamedQuery("com.csum.model.reviewComments.findAllReviewComments",ReviewComments.class).getResultList();
			return comments;
		}catch(NoResultException e){
			return null;
		}
	}

	@Override
	public ReviewProcess findByViolationId(String violationId) {
		ReviewProcess reviewProcess;
		try{
			reviewProcess = entityManager.createNamedQuery("com.csum.model.reviewProcess.findByViolationId",ReviewProcess.class).setParameter("violationId", violationId).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return reviewProcess;
	}
	
	@Override
	public void save(ReviewProcess reviewProcess){
		try{
			entityManager.persist(reviewProcess);			
		}catch(Exception e){
			logger.error(e,e);
		}
	}
	
	@Override
	public void update(ReviewProcess reviewProcess){
		try{
			ReviewProcess entity = findById(reviewProcess.getId());
			entity.setViolation(reviewProcess.getViolation());
			entity.setQuestion1(reviewProcess.getQuestion1());
			entity.setQuestion2(reviewProcess.getQuestion2());
			entity.setIgnoreTime(reviewProcess.getIgnoreTime());
			entity.setUser(reviewProcess.getUser());
			entity.setStatus(reviewProcess.isStatus());
		}catch(Exception e){
			logger.error(e,e);
		}		
	}
	
	public ReviewProcess findById(Long reviewId) {
		try {
			return getEntityManager().find(ReviewProcess.class, reviewId);
		} catch (NoResultException ex) {
			return null;
		}
	}
	

}
