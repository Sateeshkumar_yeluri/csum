package com.csum.dao;

import java.util.List;

import com.csum.model.ViolationCode;

public interface ViolationCodeDao {
	
	List<ViolationCode> findAll();
	
	ViolationCode findByCode(String code);

}
