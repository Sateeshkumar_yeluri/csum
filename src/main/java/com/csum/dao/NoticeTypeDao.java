package com.csum.dao;

import com.csum.model.NoticeType;

public interface NoticeTypeDao {

	NoticeType findByType(int type);

}
