package com.csum.dao;

import com.csum.model.PaymentMethod;

public interface PaymentMethodDao {
	
	PaymentMethod findByType(int type);

}
