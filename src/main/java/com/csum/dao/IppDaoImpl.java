package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Hearing;
import com.csum.model.Ipp;

@Repository("ippDao")
public class IppDaoImpl extends AbstractDao<Integer, Ipp> implements IppDao {

	public static final Logger logger = LogManager.getLogger(IppDaoImpl.class);

	@Override
	public Ipp findByViolationId(String violationId) {
		Ipp ipp;
		try {
			ipp = entityManager.createNamedQuery("com.csum.model.ipp.findByViolationId", Ipp.class)
					.setParameter("violationId", violationId).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return ipp;
	}

	@Override
	public void save(Ipp ippObj) {
		logger.debug("saving the ipp information:" + ippObj.getPlanNumber());
		try {
			getEntityManager().persist(ippObj);
		} catch (Exception e) {
			logger.debug(e, e);
		}
	}

	@Override
	public Ipp findIppByPlanNumber(Long planNumber) {
		Ipp ipp;
		try {
			ipp = entityManager.createNamedQuery("com.csum.model.ipp.findIppByPlanNumber", Ipp.class)
					.setParameter("planNumber", planNumber).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return ipp;
	}

	@Override
	public void updateIpp(Ipp ipp) {
		try {
			logger.debug("updating the ipp information:");
			Ipp entity = findIppByPlanNumber(ipp.getPlanNumber());
			if (entity != null) {
				entity.setDownPayment(ipp.getDownPayment());
				entity.setEnrollAmount(ipp.getEnrollAmount());
				entity.setInstallmentAmount(ipp.getInstallmentAmount());
				entity.setNoOfPayments(ipp.getNoOfPayments());
				entity.setOriginalDue(ipp.getOriginalDue());
				entity.setPreviousCredits(ipp.getPreviousCredits());
				entity.setStartDate(ipp.getStartDate());
				entity.setStatus(ipp.getStatus());
				entity.setUnenrollAmount(ipp.getUnenrollAmount());
				entity.setUser(ipp.getUser());
				entity.setViolation(ipp.getViolation());
			}
		} catch (Exception e) {
			logger.debug(e, e);
		}

	}

	@Override
	public void deleteById(Long id) {
		try {
			Ipp entity = findById(id);
			entityManager.remove(entity);
		} catch (NoResultException e) {
			logger.error(e, e);
		}
	}

	private Ipp findById(Long id) {
		try {
			return entityManager.find(Ipp.class, id);
		} catch (NoResultException ex) {
			return null;
		}
	}

}
