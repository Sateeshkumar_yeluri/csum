package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.HearingDecision;

@Repository("hearingDecisionDao")
public class HearingDecisionDaoImpl extends AbstractDao<Integer, HearingDecision> implements HearingDecisionDao{

	public static final Logger logger =LogManager.getLogger(HearingDecisionDaoImpl.class);

	@Override
	public List<HearingDecision> findAll() {
		List<HearingDecision>  hearingDecisions;
		try{
			hearingDecisions = entityManager.createNamedQuery("com.csum.model.HearingDecision.findAll",HearingDecision.class).getResultList();
		}catch(NoResultException e){
			return null;
		}
		return hearingDecisions;
	}

	@Override
	public HearingDecision findById(Long id) {
		HearingDecision  hearingDecision;
		try{
			hearingDecision = entityManager.createNamedQuery("com.csum.model.HearingDecision.findById",HearingDecision.class).setParameter("id", id).getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return hearingDecision;		
	} 
}
