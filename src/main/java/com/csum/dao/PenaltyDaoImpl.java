package com.csum.dao;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Penalty;

@Repository("penaltyDao")
public class PenaltyDaoImpl extends AbstractDao<Integer,Penalty> implements PenaltyDao{
	
	public static final Logger logger = LogManager.getLogger(PenaltyDaoImpl.class);

	@Override
	public Penalty findByViolationId(String violationId) {
		try{
			Penalty penalty = entityManager.createNamedQuery("com.csum.model.penalty.findByViolationId",Penalty.class).setParameter("violationId", violationId).getSingleResult();
			return penalty;
		}catch(NoResultException e){
			return null;
		}
	}

}
