package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.ViolationCode;

@Repository("ViolationCodeDao")
public class ViolationCodeDaoImpl extends AbstractDao<Integer, ViolationCode> implements ViolationCodeDao{
	
	public static final Logger logger = LogManager.getLogger(ViolationCodeDaoImpl.class);

	@Override
	public List<ViolationCode> findAll() {
		try{
			List<ViolationCode> violationCodes = entityManager.createNamedQuery("com.csum.model.violationcode.findAll",ViolationCode.class).getResultList();
			return violationCodes;
		}catch(NoResultException e){
			return null;
		}
	}
	
	@Override
	public ViolationCode findByCode(String code) {
		return entityManager.createQuery("SELECT v FROM ViolationCode v where v.code = :code", ViolationCode.class).setParameter("code", code)
				.getSingleResult();
	}

}
