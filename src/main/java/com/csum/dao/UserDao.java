package com.csum.dao;

import com.csum.model.User;

public interface UserDao {

	User findByUserName(String userName);

}
