package com.csum.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Admin;

@Repository("adminDao")
public class AdminDaoImpl extends AbstractDao<Integer, Admin> implements AdminDao{
	
	public static final Logger logger=LogManager.getLogger(AdminDaoImpl.class);
	
	public Admin findByUserName(String userName) {
		try {
			Admin admin =  entityManager.createNamedQuery("com.csum.model.Admin.findByUserName", Admin.class)
					.setParameter("userName", userName).getSingleResult();
			return admin;
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<Admin> findAllUsers() {
		 List<Admin> admins;
		try {
			admins =  entityManager.createNamedQuery("com.csum.model.Admin.findAllUsers", Admin.class)
					.getResultList();
			return admins;
		} catch (NoResultException ex) {
			return null;
		}
	}
}
