package com.csum.dao;

import java.util.ArrayList;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csum.model.Holidays;

@Repository("holidaysDao")
public class HolidaysDaoImpl extends AbstractDao<Integer, Holidays> implements HolidaysDao{
	
	public static final Logger logger = LogManager.getLogger(HolidaysDaoImpl.class);

	@Override
	public ArrayList<Holidays> findAll() {
		ArrayList<Holidays> holidaysList;
		try{
			holidaysList = (ArrayList<Holidays>) entityManager.createNamedQuery("com.csum.model.holidays.findAll",Holidays.class).getResultList();
		}catch(NoResultException e){
		return null;
		}
		return holidaysList;
	}
}
