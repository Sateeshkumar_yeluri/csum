package com.csum.dao;

import com.csum.model.Inspector;

public interface InspectorDao {
	
	//Inspector findByEmployeeId(String employeeId);
	
	Inspector findByUserName(String username);

}
