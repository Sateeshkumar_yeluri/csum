package com.csum.controller;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Address;
import com.csum.model.Admin;
import com.csum.model.Ipp;
import com.csum.model.Patron;
import com.csum.model.Payment;
import com.csum.model.PlateEntity;
import com.csum.model.Violation;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.PaymentDetailsService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMDateUtils;

@Controller
public class InstallmentPaymentController {

	public static final Logger logger = LogManager.getLogger(InstallmentPaymentController.class);

	@Autowired
	private IppService ippService;

	@Autowired
	private HearingService hearingService;

	@Autowired
	private ViolationService violationService;

	@Autowired
	private PaymentDetailsService paymentDetailsService;

	@GetMapping(value = { "/ippDetails/{violationId}" })
	public String IppDetails(ModelMap model, @PathVariable String violationId, @ModelAttribute("ippForm") Ipp ipp) {
		try {
			Ipp ippObj = ippService.findByViolationId(violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			Violation violation = violationService.findByViolationId(violationId);
			List<Payment> payments = paymentDetailsService.findByViolationId(violationId);
			if (violation.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
			}
			BigDecimal paidToDate = new BigDecimal(0.00);
			if (payments != null && payments.size() > 0) {
				for (Payment p : payments) {
					paidToDate = paidToDate.add(p.getAmount());
				}
			}
			model.addAttribute("paidToDate", paidToDate);
			model.addAttribute("violationId", violationId);
			if (ippObj == null && !StringUtils.isEmpty(violation.getFineAmount())
					&& violation.getFineAmount() != null) {
				Ipp ippForm = ippService.recalculate(violationId, ipp);
				ippForm.setStatus("New");
				model.addAttribute("ippForm", ippForm);
				model.addAttribute("violation", violation);
			} else {
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ippObj.getPlanNumber());
				BigDecimal downPayAmt = new BigDecimal(0.00);
				BigDecimal installmentAmt = new BigDecimal(0.00);
				int remainPayemnts = ippObj.getNoOfPayments();
				BigDecimal remainAmt = new BigDecimal(0.00);
				BigDecimal instAmount = new BigDecimal(0.00);
				String buttonText = null;
				double number = 0;
				if (ippObj.getInstallmentAmount().compareTo(new BigDecimal(0.00)) != 1
						&& ippObj.getNoOfPayments() < 2) {
					instAmount = violation.getTotalDue();
					remainPayemnts = 0;
					buttonText = "Pay Installment 1";
					model.addAttribute("instAmount", instAmount);
					model.addAttribute("remainPayemnts", remainPayemnts);
					model.addAttribute("buttonText", buttonText);
					model.addAttribute("ippForm", ippObj);
					ippObj.setViolation(violation);
					return "ippDetails";
				} else {
					if (ippPayments != null && ippPayments.size() > 0) {
						for (Payment payment : ippPayments) {
							if (payment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								downPayAmt = downPayAmt.add(payment.getAmount());
							} else if (!payment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								installmentAmt = installmentAmt.add(payment.getAmount());
							}
						}
					}
					/*
					 * List<String> obj = new ArrayList<String>();
					 * obj.add(ippObj.getInstallmentAmount()+"("+ippObj.
					 * getInstallmentDate2()+")");
					 * obj.add(ippObj.getInstallmentAmount()+"("+ippObj.
					 * getInstallmentDate3()+")");
					 * model.addAttribute("amountList",obj );
					 */
					number = Math.ceil(((installmentAmt.add(downPayAmt.subtract(ippObj.getDownPayment())))
							.divide(ippObj.getInstallmentAmount(), new MathContext(4))).doubleValue());
					int num = (int) number + 1;
					Payment ippPay = paymentDetailsService.findLatestByViolationId(violationId);
					if (ippPay != null && ippPay.getIppType() != null) {
						if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							remainAmt = downPayAmt.subtract(ippObj.getDownPayment());
						} else {
							remainAmt = (downPayAmt.subtract(ippObj.getDownPayment())).add(installmentAmt);
						}
						if (ippPay.getIppType() != null && ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")
								&& downPayAmt.compareTo(ippObj.getDownPayment()) == -1) {
							remainPayemnts = ippObj.getNoOfPayments();
						} else if ((remainAmt.remainder(ippObj.getInstallmentAmount()))
								.compareTo(new BigDecimal(0.00)) == 0) {
							remainPayemnts = ippObj.getNoOfPayments() - num;
						} else {
							remainPayemnts = ippObj.getNoOfPayments() - num + 1;
						}
					}
					if (ippPayments != null && downPayAmt.compareTo(ippObj.getDownPayment()) == -1) {
						buttonText = "Pay Installment 1";
					} else if ((remainAmt.remainder(ippObj.getInstallmentAmount()))
							.compareTo(new BigDecimal(0.00)) == 0) {
						int i = num + 1;
						buttonText = "Pay Installment " + i;
					} else {
						buttonText = "Pay Installment " + num;
					}
					if (ippObj.getStatus().equalsIgnoreCase("FULFILLED")) {
						remainPayemnts = 0;
					}
					if (ippPayments != null && downPayAmt.compareTo(ippObj.getDownPayment()) != -1) {
						if (violation.getTotalDue().compareTo(ippObj.getInstallmentAmount()) != -1) {
							if ((remainAmt.remainder(ippObj.getInstallmentAmount()))
									.compareTo(new BigDecimal(0.00)) == 0) {
								instAmount = ippObj.getInstallmentAmount();
							} else {
								instAmount = ippObj.getInstallmentAmount().multiply(new BigDecimal(number))
										.subtract(remainAmt);
							}
						} else {
							instAmount = violation.getTotalDue();
						}
					} else {
						instAmount = ippObj.getDownPayment().subtract(downPayAmt);
					}
				}
				model.addAttribute("instAmount", instAmount);
				model.addAttribute("remainPayemnts", remainPayemnts);
				model.addAttribute("buttonText", buttonText);
				model.addAttribute("ippForm", ippObj);
				ippObj.setViolation(violation);
				model.addAttribute("paidToDate", paidToDate);
				if (violation.getPlateEntity() != null) {
					model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
				}
				return "ippDetails";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "installmentpaymentplan";
	}

	@PostMapping(value = "/createIpp/{violationId}")
	public String createIpp(@ModelAttribute("ippForm") Ipp ippForm, BindingResult bindingResult, Model model,
			HttpSession session, @PathVariable String violationId) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			Violation violation = violationService.findByViolationId(violationId);
			Ipp ipp = ippService.findByViolationId(violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			if (ipp != null) {
				ipp.setViolation(violation);
				model.addAttribute("ippForm", ipp);
				return "ippDetails";
			} else if (violation != null && violation.getFineAmount().compareTo(BigDecimal.ZERO) != 0) {
				BigDecimal total = new BigDecimal(0.00);
				BigDecimal due = new BigDecimal(0.00);
				List<Payment> paymentList = paymentDetailsService.findByViolationId(violationId);
				if (paymentList != null && paymentList.size() > 0) {
					for (Payment payment : paymentList) {
						total = payment.getAmount();
						due = due.add(total);
					}
				}
				BigDecimal paidToDate = due;
				Ipp ippObj = ippService.recalculate(violationId, ippForm);
				ippObj.setStatus("PENDING");
				ippObj.setUser(user.getUserName());
				ippService.save(ippObj);
				LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(ippObj.getStartDate()).plusDays(21);
				violation.setSuspendProcessDate(CSUMDateUtils.getFormattedLocalDate(localDate));
				violation.setUser(user.getUserName());
				violationService.updateViolation(violation);
				// sendIPPLetters(violation);
				model.addAttribute("paidToDate", paidToDate);
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ippObj.getPlanNumber());
				String buttonText = null;
				if (ippPayments != null && ippPayments.size() > 0) {
					buttonText = "Installment";
				} else {
					buttonText = "Down Payment";
				}
				model.addAttribute("buttonText", buttonText);
				model.addAttribute("violationId", violationId);
				model.addAttribute("ippForm", ippObj);
				model.addAttribute("violation", violation);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../ippDetails/" + violationId;
	}

	/*
	 * private void sendIPPLetters(Violation violation) { try { Properties prop
	 * = new Properties(); InputStream inputStream = (InputStream)
	 * InstallmentPaymentController.class.getClassLoader()
	 * .getResourceAsStream("application.properties"); prop.load(inputStream);
	 * String parkingCode = prop.getProperty("violation.ipp.confirmation");
	 * Correspondence corres = new Correspondence(); CorrespondenceCode
	 * correspondence = new CorrespondenceCode(); correspondence =
	 * correspondenceCodeService.findById(Long.parseLong(parkingCode));
	 * corres.setCorresp_date( new
	 * Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).
	 * toLocalDate()); Calendar c = Calendar.getInstance(); c.setTime(new
	 * Date()); c.add(Calendar.DATE, 1);
	 * corres.setCorresp_time(CSUMDateUtils.getFormattedLocalTime( new
	 * Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).
	 * toLocalTime())); corres.setMode(prop.getProperty("violation.ipp.mode"));
	 * corres.setCorrespCode(correspondence); corres.setUser("SYSTEM");
	 * corres.setLetterSent(true); corres.setLetterSent(false);
	 * correspondenceService.save(corres); } catch (Exception e) {
	 * logger.error(e, e); } }
	 */

	@GetMapping(value = { "/recalculate/{violationId}" })
	public String recalculateIppPayment(Model model, @PathVariable String violationId,
			@ModelAttribute("ippForm") Ipp ippForm, HttpSession session) {
		try {
			model.addAttribute("violationId", violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			Violation violation = violationService.findByViolationId(violationId);
			Ipp ippObj = ippService.recalculate(violationId, ippForm);
			model.addAttribute("ippForm", ippObj);
			if (violation != null && violation.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "ippRecalculate";
	}

	@PostMapping(value = { "/cancelPlan/{planNumber}" })
	public String cancelPlan(ModelMap model, @PathVariable Long planNumber, HttpSession session) {
		Ipp ipp = null;
		String violationId = "";
		try {
			Admin user = (Admin) session.getAttribute("admin");
			ipp = ippService.findIppByPlanNumber(planNumber);
			if (ipp != null) {
				if(ipp.getViolation()!=null){
					violationId = ipp.getViolation().getViolationId();
				boolean ippFlag = ippService.showIpp(ipp.getViolation().getViolationId());
				model.addAttribute("ippFlag", ippFlag);
				boolean hearingSchedulerFlag = hearingService.showHearingScheduler(ipp.getViolation().getViolationId());
				model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
				}
				ipp.setStatus("CANCELED");
				ipp.setUser(user.getUserName());
				ippService.updateIpp(ipp);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../violationView/" +violationId;
	}
	
	@GetMapping(value = "/ippedit/{violationId}")
	public String editIpp(Model model, HttpSession session, @PathVariable String violationId) {
		BigDecimal paidToDate = new BigDecimal(0.00);
		Violation violation = null;
		Ipp ipp = null;
		try{
		if (violationId != null) {
			model.addAttribute("violationId", violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			violation = violationService.findByViolationId(violationId);
			ipp = ippService.findByViolationId(violationId);
		}
		if (violation != null ){
			if (violation.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
			}
			if(ipp != null && !ipp.getOriginalDue().equals(0.00)) {		
			BigDecimal total = new BigDecimal(0.00);
			BigDecimal due = new BigDecimal(0.00);
			List<Payment> paymentList = paymentDetailsService.findByViolationId(violationId);
			if (paymentList != null && paymentList.size() > 0) {
				for (Payment payment : paymentList) {
					if (payment.isIpp() == true) {
						total = payment.getAmount();
						due = due.add(total);
					}
				}
			}
			paidToDate = due;
			model.addAttribute("paidToDate", paidToDate);
		}
		}
		model.addAttribute("ippForm", ipp);
		}catch(Exception e){
			logger.error(e,e);
		}
		return "ippEdit";
	}

	@PostMapping(value = { "/ippUpdate/{violationId}" })
	public String UpdateIpp(ModelMap model, @ModelAttribute("ippForm") Ipp ippForm, @PathVariable String violationId,
			HttpSession session) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null && ippForm != null) {
				if (violation.getPatron() == null && ippForm.getViolation() != null
						&& ippForm.getViolation().getPatron() != null) {
					Patron patron = new Patron();
					violation.setPatron(patron);
				}
				if (violation.getPatron().getAddress() == null && ippForm.getViolation().getPatron().getAddress() != null) {
					Address address = new Address();
					violation.getPatron().setAddress(address);
				}
				if (violation.getPlateEntity() == null && ippForm.getViolation() != null
						&& ippForm.getViolation().getPlateEntity() != null) {
					PlateEntity plateEntity = new PlateEntity();
					violation.setPlateEntity(plateEntity);
				}
			}
			if (ippForm != null && ippForm.getViolation() != null && ippForm.getViolation().getPatron() != null
					&& violation.getPatron() != null) {
				if (ippForm.getViolation().getPatron().getFirstName() != null
						&& !ippForm.getViolation().getPatron().getFirstName().equals("")) {
					violation.getPatron().setFirstName(ippForm.getViolation().getPatron().getFirstName());
				}
				if (ippForm.getViolation().getPatron().getLastName() != null
						&& !ippForm.getViolation().getPatron().getLastName().equals("")) {
					violation.getPatron().setLastName(ippForm.getViolation().getPatron().getLastName());
				}
				if (ippForm.getViolation().getPatron().getAddress().getAddress() != null
						&& !ippForm.getViolation().getPatron().getAddress().getAddress().trim().equals("")) {
					violation.getPatron().getAddress()
							.setAddress(ippForm.getViolation().getPatron().getAddress().getAddress());
				}
				if (ippForm.getViolation().getPatron().getAddress().getCity() != null
						&& !ippForm.getViolation().getPatron().getAddress().getCity().trim().equals("")) {
					violation.getPatron().getAddress().setCity(ippForm.getViolation().getPatron().getAddress().getCity());
				}
				if (ippForm.getViolation().getPatron().getAddress().getZip() != null
						&& !ippForm.getViolation().getPatron().getAddress().getZip().trim().equals("")) {
					violation.getPatron().getAddress().setZip(ippForm.getViolation().getPatron().getAddress().getZip());
				}
				if (ippForm.getViolation().getPatron().getAddress().getState() != null
						&& !ippForm.getViolation().getPatron().getAddress().getState().trim().equals("")) {
					violation.getPatron().getAddress()
							.setState(ippForm.getViolation().getPatron().getAddress().getState());
				}
			}
			violationService.updateViolation(violation);
			if (ippForm != null && ippForm.getViolation() != null) {
				if (ippForm.getViolation().getPlateEntity().getLicenceNumber() != null) {
					violation.getPlateEntity()
							.setLicenceNumber(ippForm.getViolation().getPlateEntity().getLicenceNumber());
				}
				

			}
			BigDecimal paidToDate = new BigDecimal(0.00);
			Ipp ipp = null;
			if (violationId != null) {
				ipp = ippService.findByViolationId(violationId);
			}
			if (violation != null && ippForm != null && !ippForm.getOriginalDue().equals(0.00)) {
				BigDecimal total = new BigDecimal(0.00);
				BigDecimal due = new BigDecimal(0.00);
				List<Payment> paymentList = paymentDetailsService.findByViolationId(violationId);
				if (paymentList != null && paymentList.size() > 0) {
					for (Payment payment : paymentList) {
						if (payment.isIpp() == true) {
							total = payment.getAmount();
							due = due.add(total);
						}
					}
				}
				paidToDate = due;
				model.addAttribute("paidToDate", paidToDate);
			}
			if (ipp != null && violation != null && ippForm != null) {
				ipp.setInstallmentAmount(ippForm.getInstallmentAmount());
				ipp.setDownPayment(ippForm.getDownPayment());
				ipp.setNoOfPayments(ippForm.getNoOfPayments());
			}
			ipp.setUser(user.getUserName());
			violation.setUser(user.getUserName());
			violationService.updateViolation(violation);
			ipp.setViolation(violation);
			ippService.updateIpp(ipp);
		} catch (Exception e) {
			logger.error(e, e);
			e.printStackTrace();
		}
		return "redirect:../ippDetails/" + violationId;
	}
}
