package com.csum.controller;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Admin;
import com.csum.model.Permit;
import com.csum.model.PermitPayments;
import com.csum.model.PermitTransaction;
import com.csum.model.User;
import com.csum.model.ViolationSearch;
import com.csum.service.PermitPaymentsService;
import com.csum.service.PermitService;
import com.csum.service.PermitTransactionService;
import com.csum.utils.CSUMConstants;
import com.stripe.Stripe;
import com.stripe.model.Charge;

@Controller
public class PermitController {

	public static final Logger logger = LogManager.getLogger(PermitController.class);

	@Autowired
	private PermitService permitService;

	@Autowired
	private PermitPaymentsService permitPaymentsService;

	@Autowired
	private PermitTransactionService permitTransactionService;

	@GetMapping(value = { "/permitPurchase" })
	public String purchasePermit(ModelMap model) {
		Permit permit = new Permit();
		String stripePublishableKey = "";
		Properties prop = new Properties();
		try {
			InputStream inputStream = ConsumerStripeController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);

			if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
				stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
			} else {
				stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		model.addAttribute("permitForm", permit);
		model.addAttribute("permitSearchForm", new ViolationSearch());
		return "permitPurchase";
	}

	@GetMapping(value = { "/visitorPermitPurchase" })
	public String purchaseVisitorPermit(ModelMap model) {
		Permit permit = new Permit();
		String stripePublishableKey = "";
		Properties prop = new Properties();
		try {
			InputStream inputStream = ConsumerStripeController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);

			if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
				stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
			} else {
				stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		model.addAttribute("permitForm", permit);
		return "visitorPermitPurchase";
	}

	@PostMapping(value = { "/permitPurchase" })
	public String savePermit(ModelMap model, @ModelAttribute("permitForm") Permit permit, HttpSession session,
			HttpServletRequest req, HttpServletResponse res) throws IOException {
		String token = null;
		String status = null;
		String transcationId = null;
		String transcationStatus = null;
		String statusMessage = null;
		String balanceTxId = null;
		String transactionCode = null;
		PermitPayments permitPayments = new PermitPayments();
		Properties prop = new Properties();
		InputStream inputStream = ConsumerStripeController.class.getClassLoader()
				.getResourceAsStream("application.properties");

		prop.load(inputStream);
		try {
			User user = (User) session.getAttribute("user");
			String amount = permit.getAmount();
			permit.setAmount(amount.replace("$", ""));
			if (permit.getPermitPayments() != null) {
				permit.setType("P");
				permitPayments = permit.getPermitPayments();
				if (permit.getPatronType() != null && permit.getPatronType().trim().length() > 0) {
					if (permit.getPatronType().equalsIgnoreCase("Student")) {
						permit.setParkingSlot("LOT E");
					} else if (permit.getPatronType().equalsIgnoreCase("Employee")) {
						permit.setParkingSlot("LOT A");
					} else if (permit.getPatronType().equalsIgnoreCase("Visitor")) {
						permit.setParkingSlot("LOT V");
					}
				}

				if (permit.getStartDate() != null) {
					LocalDate startDate = permit.getStartDate();
					String startTime = "";
					if (permit.getTimeSlot() != null && permit.getTimeSlot().trim().length() > 0) {
						String timeSlot = permit.getTimeSlot();
						String[] times = timeSlot.split("_");
						startTime = times[0];
						permit.setStartTime(LocalTime.parse(times[0]));
						permit.setEndTime(LocalTime.parse(times[1]));
					} else {
						startTime = "00:00:00";
						permit.setStartTime(LocalTime.parse(startTime));
						permit.setEndTime(LocalTime.parse("23:59:59"));
					}
					if (permit.getPermitType() != null && permit.getPermitType().contains("Semester")) {
						permit.setEndDate(startDate.plusMonths(6));
					} else {
						if (permit.getNoOfMonths() != null && permit.getNoOfMonths().trim().length() > 0) {
							permit.setEndDate(startDate.plusMonths(Long.parseLong(permit.getNoOfMonths())));
						} else {
							if (startTime.startsWith("17")) {
								permit.setEndDate(startDate.plusDays(1));
							} else {
								permit.setEndDate(startDate);
							}
						}
					}
				}
				if (user != null) {
					permit.setUser(user.getUserName());
				}
				if (permit.getLicenceNumber() != null) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddYYYYHHmm");
					String currentDate = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).format(formatter);
					String permitId = "CSUM" + permit.getLicenceNumber() + currentDate;
					permit.setPermitId(permitId.toUpperCase());
				}
				if (permitPayments.getPaymentType() != null
						&& permitPayments.getPaymentType().equalsIgnoreCase("card")) {
					token = req.getParameter("stripeToken");
					BigDecimal due = new BigDecimal(permit.getAmount());
					if (token != null && !token.isEmpty()) {
						// logger.debug("processConsumerCardPayment::
						// dueAmount:: " + dueAmount);
						try {
							long amountInCents = 0;
							amountInCents = due.multiply(new BigDecimal("100")).longValue();
							if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
								Stripe.apiKey = prop.getProperty("violation.stripe.prodapikey");
							} else {
								Stripe.apiKey = prop.getProperty("violation.stripe.testapikey");
							}
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("amount", amountInCents);
							params.put("currency", "usd");
							String name = "";
							if (permit.getLastName() != null && permit.getLastName().trim().length() > 0) {
								name += permit.getLastName();
							}
							if (permit.getFirstName() != null && permit.getFirstName().trim().length() > 0) {
								if (name.trim().length() > 0) {
									name += " " + permit.getFirstName();
								} else {
									name += permit.getFirstName();
								}
							}
							if (name != null && name.length() > 0) {
								params.put("description", "Permit $ " + due + " purchased by " + name);
							} else {
								params.put("description", "Permit details not available");
							}
							params.put("source", token);
							Charge charge = null;
							try {
								charge = Charge.create(params);
							} catch (Exception e) {
								logger.debug(e, e);
								model.addAttribute("errorMessage",
										"Payment Server Error. Check card statement before repeating the payment");
							}
							if (charge != null) {
								/*
								 * logger.debug(charge+" "+charge.getStatus( ) +
								 * " " + charge.getBalanceTransaction() + " " +
								 * charge.getFailureMessage() + " " +
								 * charge.getId() + " " + charge.getInvoice() +
								 * " " + charge.getPaid());
								 */
								status = charge.getStatus();
								statusMessage = charge.getDescription();
								transcationId = charge.getId();
								transcationStatus = charge.getPaid() + "";
								balanceTxId = charge.getBalanceTransaction();
								transactionCode = charge.getFailureCode();
							} else {
								model.addAttribute("errorMessage",
										"Payment Failed. Check card statement before repeating the payment");
							}
						} catch (Exception e) {
							logger.debug(e, e);
							model.addAttribute("errorMessage",
									"Internal Server Error. Check card statement before repeating the payment");
						}
						PermitTransaction transaction = new PermitTransaction();
						transaction.setStatus(status);
						transaction.setAmount("$" + permit.getAmount());
						transaction.setCreatedDate(new Date());
						transaction.setStatusResponseCode(transcationStatus);
						transaction.setTranscationId(transcationId);
						transaction.setPermitId(permit.getPermitId());
						transaction.setStatusMessage(statusMessage);
						transaction.setClientToken(token);
						transaction.setPaymentNonce(balanceTxId);
						transaction.setTranscationCode(transactionCode);
						permitTransactionService.save(transaction);
						if ((transcationStatus != null && transcationStatus.equalsIgnoreCase("true"))
								&& (status != null && status.equalsIgnoreCase("succeeded"))) {
							permitPayments.setPermitTransaction(transaction);
							permitPayments.setAmount(permit.getAmount());
							permitPayments.setPaymentDate(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
							permitPaymentsService.savePermitPayment(permitPayments);
							model.addAttribute("TxResultStatus", "Citation payment success");
							model.addAttribute("errorMessagePayment",
									"Permit Purchase is successful. Please find the below acknowledgement");
							PermitPayments payment = permitPaymentsService.findByTransactionId(transaction.getId());
							model.addAttribute("payedInfo", payment);
							permit.setPermitPayments(permitPayments);
							permitService.savePermit(permit);
							Permit permitInfo = permitService.findByPermitId(permit.getPermitId());
							model.addAttribute("permitInfo", permitInfo);
							return "permitInfo";
						} else {
							model.addAttribute("errorMessage",
									"Payment Server Error. Check card statement before repeating the payment");
						}
					} else {
						model.addAttribute("errorMessage", "Internal Server Error. Payment Token Error.");
					}
				} else {
					permitPayments.setAmount(permit.getAmount());
					permitPayments.setPaymentDate(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
					permitPaymentsService.savePermitPayment(permitPayments);
					permit.setPermitPayments(permitPayments);
					permitService.savePermit(permit);
					Permit permitInfo = permitService.findByPermitId(permit.getPermitId());
					model.addAttribute("permitInfo", permitInfo);
					return "permitInfo";
				}
			}

			/*
			 * if(permit.getStartDate()!=null){ LocalDateTime startDate =
			 * permit.getStartDate(); String[] time =
			 * permit.getTimeSlot().split("_"); String[] times =
			 * time[0].split(":"); LocalDateTime fromDate =
			 * LocalDateTime.of(startDate.getYear(), startDate.getMonth(),
			 * startDate.getDayOfMonth(), Integer.parseInt(times[0]),
			 * Integer.parseInt(times[1]), Integer.parseInt(times[2]));
			 * 
			 * }
			 */
		} catch (Exception e) {
			logger.error(e, e);
		}
		String stripePublishableKey = "";
		if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
			stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
		} else {
			stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
		}
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		model.addAttribute("permitSearchForm", new ViolationSearch());
		return "permitPurchase";
	}

	@PostMapping(value = { "/visitorPermitPurchase" })
	public String saveVisitorPermit(ModelMap model, @ModelAttribute("permitForm") Permit permit, HttpSession session,
			HttpServletRequest req, HttpServletResponse res) throws IOException {
		String token = null;
		String status = null;
		String transcationId = null;
		String transcationStatus = null;
		String statusMessage = null;
		String balanceTxId = null;
		String transactionCode = null;
		PermitPayments permitPayments = new PermitPayments();
		Properties prop = new Properties();
		InputStream inputStream = ConsumerStripeController.class.getClassLoader()
				.getResourceAsStream("application.properties");
		prop.load(inputStream);
		try {
			String amount = permit.getAmount();
			permit.setAmount(amount.replace("$", ""));
			if (permit.getPermitPayments() != null) {
				permitPayments = permit.getPermitPayments();
				permit.setParkingSlot("LOT V");
				permit.setType("P");
				if (permit.getStartDate() != null) {
					LocalDate startDate = permit.getStartDate();
					String startTime = "";
					if (permit.getTimeSlot() != null && permit.getTimeSlot().trim().length() > 0) {
						String timeSlot = permit.getTimeSlot();
						String[] times = timeSlot.split("_");
						startTime = times[0];
						permit.setStartTime(LocalTime.parse(times[0]));
						permit.setEndTime(LocalTime.parse(times[1]));
					} else {
						startTime = "00:00:00";
						permit.setStartTime(LocalTime.parse(startTime));
						permit.setEndTime(LocalTime.parse("23:59:59"));
					}
					if (permit.getNoOfMonths() != null && permit.getNoOfMonths().trim().length() > 0) {
						permit.setEndDate(startDate.plusMonths(Long.parseLong(permit.getNoOfMonths())));
					} else {
						if (startTime.startsWith("17")) {
							permit.setEndDate(startDate.plusDays(1));
						} else {
							permit.setEndDate(startDate);
						}
					}
				}
				if (permit.getLicenceNumber() != null) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddYYYYHHmm");
					String currentDate = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).format(formatter);
					String permitId = "CSUM" + permit.getLicenceNumber() + currentDate;
					permit.setPermitId(permitId.toUpperCase());
				}
				if (permitPayments.getPaymentType() != null
						&& permitPayments.getPaymentType().equalsIgnoreCase("card")) {
					token = req.getParameter("stripeToken");
					BigDecimal due = new BigDecimal(permit.getAmount());
					if (token != null && !token.isEmpty()) {
						// logger.debug("processConsumerCardPayment::
						// dueAmount:: " + dueAmount);
						try {
							long amountInCents = 0;
							amountInCents = due.multiply(new BigDecimal("100")).longValue();
							if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
								Stripe.apiKey = prop.getProperty("violation.stripe.prodapikey");
							} else {
								Stripe.apiKey = prop.getProperty("violation.stripe.testapikey");
							}
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("amount", amountInCents);
							params.put("currency", "usd");
							String name = "";
							if (permit.getLastName() != null && permit.getLastName().trim().length() > 0) {
								name += permit.getLastName();
							}
							if (permit.getFirstName() != null && permit.getFirstName().trim().length() > 0) {
								if (name.trim().length() > 0) {
									name += " " + permit.getFirstName();
								} else {
									name += permit.getFirstName();
								}
							}
							if (name != null && name.length() > 0) {
								params.put("description", "Permit $ " + due + " purchased by " + name);
							} else {
								params.put("description", "Permit details not available");
							}
							params.put("source", token);
							Charge charge = null;
							try {
								charge = Charge.create(params);
							} catch (Exception e) {
								logger.debug(e, e);
								model.addAttribute("errorMessage",
										"Payment Server Error. Check card statement before repeating the payment");
							}
							if (charge != null) {
								/*
								 * logger.debug(charge+" "+charge.getStatus( ) +
								 * " " + charge.getBalanceTransaction() + " " +
								 * charge.getFailureMessage() + " " +
								 * charge.getId() + " " + charge.getInvoice() +
								 * " " + charge.getPaid());
								 */
								status = charge.getStatus();
								statusMessage = charge.getDescription();
								transcationId = charge.getId();
								transcationStatus = charge.getPaid() + "";
								balanceTxId = charge.getBalanceTransaction();
								transactionCode = charge.getFailureCode();
							} else {
								model.addAttribute("errorMessage",
										"Payment Failed. Check card statement before repeating the payment");
							}
						} catch (Exception e) {
							logger.debug(e, e);
							model.addAttribute("errorMessage",
									"Internal Server Error. Check card statement before repeating the payment");
						}
						PermitTransaction transaction = new PermitTransaction();
						transaction.setStatus(status);
						transaction.setAmount("$" + permit.getAmount());
						transaction.setCreatedDate(new Date());
						transaction.setStatusResponseCode(transcationStatus);
						transaction.setTranscationId(transcationId);
						transaction.setPermitId(permit.getPermitId());
						transaction.setStatusMessage(statusMessage);
						transaction.setClientToken(token);
						transaction.setPaymentNonce(balanceTxId);
						transaction.setTranscationCode(transactionCode);
						permitTransactionService.save(transaction);
						if ((transcationStatus != null && transcationStatus.equalsIgnoreCase("true"))
								&& (status != null && status.equalsIgnoreCase("succeeded"))) {
							permitPayments.setPermitTransaction(transaction);
							permitPayments.setAmount(permit.getAmount());
							permitPayments.setPaymentDate(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
							permitPaymentsService.savePermitPayment(permitPayments);
							model.addAttribute("TxResultStatus", "Citation payment success");
							model.addAttribute("errorMessagePayment",
									"Permit Purchase is successful. Please find the below acknowledgement");
							PermitPayments payment = permitPaymentsService.findByTransactionId(transaction.getId());
							model.addAttribute("payedInfo", payment);
							permit.setPermitPayments(permitPayments);
							permitService.savePermit(permit);
							Permit permitInfo = permitService.findByPermitId(permit.getPermitId());
							model.addAttribute("permitInfo", permitInfo);
							return "visitorPermitInfo";
						} else {
							model.addAttribute("errorMessage",
									"Payment Server Error. Check card statement before repeating the payment");
						}
					} else {
						model.addAttribute("errorMessage", "Internal Server Error. Payment Token Error.");
					}
				}
			}

			/*
			 * if(permit.getStartDate()!=null){ LocalDateTime startDate =
			 * permit.getStartDate(); String[] time =
			 * permit.getTimeSlot().split("_"); String[] times =
			 * time[0].split(":"); LocalDateTime fromDate =
			 * LocalDateTime.of(startDate.getYear(), startDate.getMonth(),
			 * startDate.getDayOfMonth(), Integer.parseInt(times[0]),
			 * Integer.parseInt(times[1]), Integer.parseInt(times[2]));
			 * 
			 * }
			 */
		} catch (Exception e) {
			logger.error(e, e);
		}
		String stripePublishableKey = "";
		if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
			stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
		} else {
			stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
		}
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		model.addAttribute("permitSearchForm", new ViolationSearch());
		return "visitorPermitPurchase";
	}

	@GetMapping(value = { "/freePermit" })
	public String freePermit(ModelMap model, HttpSession session) {
		Permit permit = new Permit();
		model.addAttribute("permitForm", permit);
		return "freePermit";
	}

	@PostMapping(value = { "/saveFreePermit" })
	public String saveFreePermit(ModelMap model, HttpSession session, @ModelAttribute("permitForm") Permit permit) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			if (user != null) {
				permit.setUser(user.getUserName());
			}
			permit.setParkingSlot("LOT F");
			if (permit.getStartDate() != null) {
				permit.setStartTime(LocalTime.parse("00:00:00"));
				permit.setEndTime(LocalTime.parse("23:59:59"));
				if(permit.getPermitType()!=null && permit.getPermitType().equalsIgnoreCase("Monthly Pass")){
					permit.setEndDate(permit.getStartDate().plusMonths(1));
				}else{
					permit.setEndDate(permit.getStartDate().plusDays(1));
				}
			}
			if (permit.getLicenceNumber() != null) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddYYYYHHmm");
				String currentDate = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).format(formatter);
				String permitId = "CSUM" + permit.getLicenceNumber() + currentDate;
				permit.setPermitId(permitId.toUpperCase());
			}
			System.out.print(permit.getType());
			permitService.savePermit(permit);
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("permitInfo", permit);
		return "freePermitInfo";
	}
	
	@PostMapping(value = { "/samplePermitData" })
	public String InsertSampleData(ModelMap model) {
		logger.debug("Inserting Sample Permit Data");
		String user = "System";
		try{
			permitService.deleteAllPermitData(user);//need to implement
			insertPermit("1A23456","Student","Semester Decal Permit for MotorCycle","20.00","CSUS0001","LOT A","P",user,"card",6,null);
			insertPermit("2B34567","Student","Semester Decal Permit for Auto","80.00","CSUS0002","LOT A","P",user,"card",6,null);
			insertPermit("3C45678","Student","Disabled Parking Permit","20.00","CSUS0003","LOT D","P",user,"card",6,null);
			insertPermit("4D56789","Employee","Hang Tag Permit","120.00","CSUE0001","LOT E","P",user,"payroll",2,null);
			insertPermit("5E67890","Employee","Blue Hang Tag Scratcher Permit","180.00","CSUE0002","LOT E","P",user,"card",3,null);
			insertPermit("6F78901","Employee","Gold Hang Tag Scratcher Permit","360.00","CSUE0003","LOT E","P",user,"payroll",6,null);
			insertPermit("7G89012","Employee","Single Day Permit","5.00","CSUE0004","LOT E","P",user,"card",0,"17:00:00_07:00:00");
			insertPermit("8H90123","Employee","Disabled Parking Permit","20.00","CSUE0005","LOT D","P",user,"card",6,null);
			insertPermit("9I01234","Visitor","Gold Hang Tag Scratcher Permit","60.00",null,"LOT O","P",user,"card",1,null);
			insertPermit("1J23456","Visitor","Blue Hang Tag Scratcher Permit","240.00",null,"LOT O","P",user,"card",4,null);
			insertPermit("2K34567","Visitor","Guest Parking Permit","50.00",null,"LOT O","P",user,"card",0,"07:00:00_17:00:00");
			insertPermit("3L45678","Visitor","Disabled Parking Permit","20.00",null,"LOT D","P",user,"card",6,null);
		}catch(Exception e){
			logger.error(e,e);
		}		
		return "redirect:/permitSearch";
	}
	

	private void insertPermit(String licenceNumber, String patronType, String permitType, String amount, String csuId, String parkingSlot, String type, String user, String paymentType, int noOfMonths, String timeSlot) {
		Permit permit = new Permit();
		PermitPayments permitPayments = new PermitPayments();
		try{
			String permitId="";
			LocalDate currDate = LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).plusDays(1);
			if (licenceNumber != null) {
				permit.setLicenceNumber(licenceNumber);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddYYYYHHmm");
				String currentDate = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).format(formatter);
				permitId= "CSUM" + permit.getLicenceNumber() + currentDate;
				permit.setPermitId(permitId.toUpperCase());
			}
			permit.setStartDate(currDate);
			if(timeSlot!=null){
				String[] times = timeSlot.split("_");
				permit.setStartTime(LocalTime.parse(times[0]));
				permit.setEndTime(LocalTime.parse(times[1]));
			}else{
			permit.setStartTime(LocalTime.of(0, 0, 0));
			permit.setEndTime(LocalTime.of(23, 59,59));
			}
			if(noOfMonths>0){
				permit.setEndDate(currDate.plusMonths(noOfMonths));
			}else{
				permit.setEndDate(currDate.plusDays(1));
			}
			permit.setAmount(amount);
			permit.setCsuId(csuId);
			permit.setParkingSlot(parkingSlot);
			permit.setPatronType(patronType);
			permitPayments = insertPermitPayment(paymentType, amount, user, permitId);
			permit.setPermitPayments(permitPayments);
			permit.setPermitType(permitType);
			permit.setType(type);
			permit.setUser(user);
			permitService.savePermit(permit);
		}catch(Exception e){
			logger.error(e,e);
		}
	}

	private PermitPayments insertPermitPayment(String paymentType, String amount, String user, String permitId) {
		PermitPayments permitPayments = new PermitPayments();
		try{
			permitPayments.setAmount(amount);
			permitPayments.setPaymentType(paymentType);
			permitPayments.setPaymentDate(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
			if(paymentType.equalsIgnoreCase("Card")){
				PermitTransaction transaction = new PermitTransaction();
				transaction.setStatus("suceeded");
				transaction.setAmount("$" +amount);
				transaction.setCreatedDate(new Date());
				transaction.setStatusResponseCode("false");
				transaction.setTranscationId("SampleData");
				transaction.setPermitId(permitId);
				permitTransactionService.save(transaction);
				permitPayments.setPermitTransaction(transaction);
			}
			permitPayments.setUser(user);
			permitPaymentsService.savePermitPayment(permitPayments);
		}catch(Exception e){
			logger.error(e,e);
		}
		return permitPayments;
	}
}
