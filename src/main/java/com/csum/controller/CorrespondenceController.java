package com.csum.controller;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Address;
import com.csum.model.Admin;
import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.CorrespondenceCode;
import com.csum.model.Hearing;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;
import com.csum.model.SuspendedCodes;
import com.csum.model.Suspends;
import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.service.CorrespondenceCodesService;
import com.csum.service.CorrespondenceService;
import com.csum.service.HistoryService;
import com.csum.service.ReviewProcessService;
import com.csum.service.SuspendedCodesService;
import com.csum.service.SuspendsService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.csum.view.model.HistoryBean;

@Controller
public class CorrespondenceController {

	public static final Logger logger = LogManager.getLogger(CorrespondenceController.class);

	@Autowired
	private ViolationService violationService;

	@Autowired
	private SuspendedCodesService suspendedCodesService;

	@Autowired
	private CorrespondenceCodesService correspondenceCodesService;

	@Autowired
	private ViolationImagesService violationImagesService;

	@Autowired
	private SuspendsService suspendsService;

	@Autowired
	private CorrespondenceService correspondenceService;

	@Autowired
	private ReviewProcessService reviewProcessService;

	@Autowired
	private HistoryService historyService;

	@GetMapping(value = { "/addCorrespondence/{violationId}" })
	public String addCorrespondence(ModelMap model, HttpSession httpSession, @PathVariable String violationId)
			throws IOException, ParseException {
		Violation violation;
		String licenseNumber = "";
		List<ReviewComments> commentDetails = new ArrayList<ReviewComments>();
		Properties prop = new Properties();
		List<SuspendedCodes> suspendedCodes;
		List<CorrespondenceCode> correspondenceCodes;
		ReviewProcess reviewProcess;
		List<ViolationImages> images;
		boolean period = false;
		String weekDay = "";
		String name = "";
		HistoryBean historyBean = new HistoryBean();
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				if (violation.getDateOfViolation() != null && !violation.getDateOfViolation().equalsIgnoreCase("")) {
					weekDay = CSUMDateUtils.getDayOfWeek(violation.getDateOfViolation());
				}
				if (violation.getPlateEntity() != null && violation.getPlateEntity().getLicenceNumber() != null) {
					licenseNumber = violation.getPlateEntity().getLicenceNumber();
				}
				InputStream inputStream = ViolationController.class.getClassLoader()
						.getResourceAsStream("application.properties");
				prop.load(inputStream);
				if (violation.getPatron() != null) {
					if (violation.getPatron().getFirstName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = violation.getPatron().getFirstName() + " ";
					}
					if (violation.getPatron().getMiddleName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = name + violation.getPatron().getMiddleName() + " ";
					}
					if (violation.getPatron().getLastName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {

						name = name + violation.getPatron().getLastName();
					}
				}
			}
			commentDetails = reviewProcessService.findAllReviewComments();
			suspendedCodes = suspendedCodesService.findAll();
			correspondenceCodes = correspondenceCodesService.findAll();
			reviewProcess = new ReviewProcess();
			reviewProcess.setLetterSent(true);
			images = violationImagesService.findByViolationId(violationId);
			historyBean = historyService.getRequiredHistory("All", violationId);
			model.addAttribute("historyBean",historyBean);
			model.addAttribute("images", images);
			model.addAttribute("reviewProcess", reviewProcess);
			model.addAttribute("correspondenceCodes", correspondenceCodes);
			model.addAttribute("suspendedCodes", suspendedCodes);
			model.addAttribute("commentDetails", commentDetails);
			model.addAttribute("name", name);
			model.addAttribute("period", period);
			model.addAttribute("licenseNumber", licenseNumber);
			model.addAttribute("dayOfWeek", weekDay);
			model.addAttribute("violationForm", violation);
			model.addAttribute("violationId", violationId);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "correspondence";
	}

	@PostMapping(value = { "/saveCorrespondence/{violationId}" })
	public String correspondenceInfo(@ModelAttribute("reviewProcess") ReviewProcess reviewProcess, ModelMap model,
			HttpSession httpSession, @PathVariable String violationId) throws IOException, ParseException {
		Violation violation;
		Properties prop = new Properties();
		Comment comment = new Comment();
		Suspends suspend = new Suspends();
		Correspondence corres = new Correspondence();
		SuspendedCodes suspendCode = new SuspendedCodes();
		CorrespondenceCode correspCode = new CorrespondenceCode();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			InputStream inputStream = CorrespondenceController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			violation = violationService.findByViolationId(violationId);
			reviewProcess.setViolation(violation);
			reviewProcess.setUser(user.getUserName());
			if (reviewProcess.getSuspends() != null && reviewProcess.getSuspends() != 0) {
				suspendCode = suspendedCodesService.findById(reviewProcess.getSuspends());
				if (suspendCode.getDescription() != null && suspendCode.getDescription() != "") {
					suspend.setSuspendedCodes(suspendCode);
					suspend.setUser(user.getUserName());
					suspend.setProcessedOn(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					suspend.setTotalDue(reviewProcess.getViolation().getTotalDue());
					suspend.setSuspended_date(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					suspend.setSuspended_time(CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime()));
					if (suspendCode.getAmount() != null && suspendCode.getAmount() != "") {
						BigDecimal newFine = new BigDecimal(suspendCode.getAmount());
						BigDecimal due = violation.getTotalDue().subtract(violation.getFineAmount()).add(newFine);
						violation.setTotalDue(due);
						violation.setFineAmount(newFine);
						violationService.updateViolation(violation);					
					}
					if (suspendCode.getCode() == 150 || suspendCode.getCode() == 850 || suspendCode.getCode() == 851
							|| suspendCode.getCode() == 381 || suspendCode.getCode() == 681) {
						violation.setStatus("CLOSED");
						violation.setTotalDue(new BigDecimal(0.00));
						violation.setUser(user.getUserName());
						violationService.updateViolation(violation);
					}
					suspend.setViolation(violation);
					suspend.setMode(prop.getProperty("violation.corresp.mode"));
					suspendsService.save(suspend);
				}
			}
			if (reviewProcess.getCorrespondence() != null && reviewProcess.getCorrespondence() != 0) {
				correspCode = correspondenceCodesService.findById(reviewProcess.getCorrespondence());
				if (correspCode.getCorrespDesc() != null && correspCode.getCorrespDesc() != "") {
					corres.setViolation(violation);
					corres.setCorresp_date(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					Calendar c = Calendar.getInstance();
					c.setTime(new Date());
					c.add(Calendar.DATE, 1);
					corres.setCorresp_time(CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime()));
					corres.setCorrespCode(correspCode);
					corres.setUser(user.getUserName());
					corres.setLetterSent(reviewProcess.isLetterSent());
					corres.setMode(prop.getProperty("violation.corresp.mode"));
					correspondenceService.save(corres);
				}
			}
			if (reviewProcess.getNoteedited() != null && reviewProcess.getNoteedited() != "") {
				comment.setViolation(violation);
				comment.setComment(reviewProcess.getNoteedited());
				comment.setCommentType("I");
				comment.setMode(prop.getProperty("violation.corresp.mode"));
				comment.setUser(user.getUserName());
				violationService.addComment(comment);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../violationView/"+violationId;
	}

}
