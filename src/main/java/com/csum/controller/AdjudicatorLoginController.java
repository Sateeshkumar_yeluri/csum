package com.csum.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.csum.model.Admin;
import com.csum.model.Hearing;
import com.csum.model.User;
import com.csum.service.AdminService;
import com.csum.service.HearingService;
import com.csum.service.UserService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.csum.view.model.LoginBean;

@Controller
public class AdjudicatorLoginController {

	public static final Logger logger = LogManager.getLogger(AdjudicatorLoginController.class);

	@Autowired
	private AdminService adminService;

	@Autowired
	private HearingService hearingService;

	@GetMapping("/adjudicatorLogin")
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, LoginBean loginBean) {
		ModelAndView model = new ModelAndView();
		// LoginBean loginBean = new LoginBean();
		model.addObject("adjudicatorLoginBean", loginBean);
		return model;
	}

	@PostMapping("/adjudicatorLogin")
	public String executeLogin(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("adjudicatorLoginBean") LoginBean loginBean, Model model,
			RedirectAttributes redirectAttributes) {

		logger.debug("logged in user name" + loginBean.getUsername());
		try {
			Admin user = adminService.findByUserName(loginBean.getUsername());
			if (user != null && loginBean.getPassword().equals(user.getPassword())) {
				request.getSession().setAttribute("admin", user);
				return "redirect:/adjudicatorDashboardSupervisor/Pending/All";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("resultMessage", "Invalid Credentials");
		return "adjudicatorLogin";
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@GetMapping(value = "/adjudicatorLogout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/adjudicatorLogin?logout";
	}

}
