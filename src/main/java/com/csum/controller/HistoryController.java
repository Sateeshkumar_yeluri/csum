package com.csum.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.csum.model.Violation;
import com.csum.service.HearingService;
import com.csum.service.HistoryService;
import com.csum.service.IppService;
import com.csum.service.ViolationService;
import com.csum.view.model.HistoryBean;

@Controller
public class HistoryController {
	
	public static final Logger logger = LogManager.getLogger(HistoryController.class);
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private IppService ippService;
	
	@GetMapping(value = { "/history/{violationId}" })
	public String getHistory(@PathVariable String violationId,
			@RequestParam(required = false, defaultValue = "All") String historyItem, Model model) {
		String licenseNumber="";
		Violation violation;
		HistoryBean historyBean = null;
		try{
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			violation = violationService.findByViolationId(violationId);
			if(violation!=null){
				if(violation.getPlateEntity()!=null && violation.getPlateEntity().getLicenceNumber()!=null){
					licenseNumber = violation.getPlateEntity().getLicenceNumber();
				}
				historyBean = historyService.getRequiredHistory(historyItem, violationId);
			}			
		}catch(Exception e){
			logger.error(e,e);
		}
		model.addAttribute("licenseNumber",licenseNumber);
		model.addAttribute("historyBean",historyBean);
		model.addAttribute("historyItem", historyItem);	
		model.addAttribute("violationId",violationId);
		return "history";
	}
	

}
