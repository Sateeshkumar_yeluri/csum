package com.csum.controller;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import com.csum.model.Admin;
import com.csum.model.Ipp;
import com.csum.model.Payment;
import com.csum.model.PaymentMethod;
import com.csum.model.Transaction;
import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.PaymentDetailsService;
import com.csum.service.PaymentMethodService;
import com.csum.service.TransactionService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;


@Controller
public class PaymentDetailsController {
	
	public static final Logger logger = LogManager.getLogger(PaymentDetailsController.class);
	
	private static long paymentTransactionId = 333445;
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private ViolationImagesService violationImagesService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private PaymentDetailsService paymentDetailsService; 
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private IppService ippService;
	
	@PostMapping(value = { "/checkPayment/{violationId}" })
	public String checkPayment(@ModelAttribute("paymentForm") Payment paymentForm, @PathVariable String violationId,
			HttpServletRequest req, HttpServletResponse res, Model model, BindingResult bindingResult) {
		// logger.debug(paymentForm + " cheque::" +
		// paymentForm.getUploadedCheque());
		Admin user = (Admin) req.getSession().getAttribute("admin");
		Violation violationdetails = violationService.findByViolationId(violationId);
		try {
			for (MultipartFile uploadedCheque : paymentForm.getUploadedCheque()) {
				/*
				 * logger.debug("uploadedCheque.getName()- " +
				 * uploadedCheque.getName() + " uploadedCheque.getSize()- " +
				 * uploadedCheque.getSize());
				 */
				/*
				 * paymentForm.setChequeFileName(uploadedCheque.
				 * getOriginalFilename());
				 * paymentForm.setChequeFile(uploadedCheque.getBytes());
				 */
				if (uploadedCheque != null && uploadedCheque.getSize() > 0) {
					ViolationImages images = new ViolationImages();
					String fileName = uploadedCheque.getOriginalFilename();
					String fileType = fileName.substring(fileName.lastIndexOf('.') + 1);
					// logger.debug("File name >> " + fileName + " " +
					// fileName.substring(fileName.lastIndexOf('.')));
					images.setImageName(fileName.substring(0, fileName.lastIndexOf('.')));
					images.setViolation(violationdetails);
					images.setImage(uploadedCheque.getBytes());
					images.setImageType(fileType.toUpperCase());
					String uploadedDate = CSUMDateUtils.getDateInString(new Date());
					// logger.debug("uploadedDate >> " + uploadedDate);
					images.setUploadedDate(uploadedDate);
					images.setType("Checks");
					if(user!=null){
					images.setUser(user.getUserName());
					}
					violationImagesService.save(images);
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
			model.addAttribute("errorMessageCheque", "Unable to upload file. Try again");
		}
		if (paymentForm.getAmount() == null) {
			paymentForm.setAmount(new BigDecimal("0"));
		}
		if (paymentForm.getAmount().compareTo(new BigDecimal("0")) > 0 && violationId != null) {
			try {
				BigDecimal due = paymentForm.getAmount();
				Transaction tx = new Transaction();
				tx.setCreatedDate(new Date());
				tx.setAmount(paymentForm.getAmount() + "");
				tx.setStatus("Success");
				tx.setStatusMessage("Citation Payment by cheque is successful");
				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyyyHHmmSS");
				String currentTimeStamp = DATE_FORMAT.format(new Timestamp(new Date().getTime()));
				tx.setTranscationId(paymentTransactionId + currentTimeStamp + "TX");
				tx.setViolationId(violationId);
				transactionService.save(tx);
				// logger.debug("save payment:: paymentForm:: " +
				// paymentForm + " txId:: " + tx.getId());
				paymentForm.setTransaction(tx);
				savePayment(violationdetails, violationdetails.getTotalDue(), user, due, tx, paymentForm.getPaymentSource(), 5);
				Violation violationInfo = violationService.findByViolationId(violationId);
				model.addAttribute("violationInfo", violationInfo);
				model.addAttribute("errorMessagePayment",
						"Citation payment is successful. Please find the below acknowledgement");
				Payment payment = paymentDetailsService.findByTransactionId(tx.getId());
				// logger.debug("Cheque Payment Receipt Info:: " + payment);
				model.addAttribute("payedInfo", payment);
			} catch (Exception e) {
				logger.error(e, e);
				model.addAttribute("unactiveOncheckPay", "active");
				/*
				 * String token = getBraintreeClientToken();
				 * model.addAttribute("ClientToken", token);
				 */
				String stripePublishableKey = getStripeKey();
				model.addAttribute("stripePublishableKey", stripePublishableKey);
				model.addAttribute("errorMessageCheque", "Server error.");
				if (violationdetails.getPlateEntity() != null) {
					model.addAttribute("licenseNumber", violationdetails.getPlateEntity().getLicenceNumber());
				}
				return "payment";
			}
			return "violationInfo";
		} else {
			model.addAttribute("unactiveOncheckPay", "active");
			/*
			 * String token = getBraintreeClientToken();
			 * model.addAttribute("ClientToken", token);
			 */
			String stripePublishableKey = getStripeKey();
			model.addAttribute("stripePublishableKey", stripePublishableKey);
			model.addAttribute("violationId", violationId);
			model.addAttribute("violation", violationdetails);
			model.addAttribute("totalDue", violationdetails.getTotalDue());
			model.addAttribute("paymentForm", new Payment());
			model.addAttribute("errorMessageCheque", "Amount should not be NULL or empty");
			if (violationdetails.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violationdetails.getPlateEntity().getLicenceNumber());
			}
			return "payment";
		}
	}

	private String getStripeKey() {
		String stripePublishableKey = "";
		Properties prop = new Properties();
		try {
			InputStream inputStream = PaymentDetailsController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
				stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
			} else {
				stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
			}

		} catch (Exception e) {
			logger.error(e, e);
		}
		return stripePublishableKey;

	}
	
	public void savePayment(Violation violation, BigDecimal totalDue, Admin user, BigDecimal amount,
			Transaction tx, String paymentSource, int type) {
		BigDecimal amountPaid;
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod = paymentMethodService.findByType(type);
		amountPaid = amount;
		BigDecimal overPaid = new BigDecimal(0.00);
		BigDecimal totalTotalDue = totalDue;
		Payment payment = new Payment();
		if (amountPaid.signum() == 1) {
			amount = totalDue.subtract(amountPaid);
			if (amount.signum() == 1) {
				totalDue = amount;
			} else if (amount.signum() == -1) {
				overPaid = amountPaid.subtract(totalDue);
				totalDue = new BigDecimal(0.00);
			} else {
				totalDue = new BigDecimal(0.00);
			}
			payment.setAmount(amountPaid);
			violation.setTotalDue(totalDue);
			payment.setTotalDue(totalDue);
			payment.setAccount("PAY-BY-WEB");
			payment.setViolation(violation);
			Ipp ipp = ippService.findByViolationId(violation.getViolationId());
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
					&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
				payment.setIpp(true);
			} else {
				payment.setIpp(false);
			}
			payment.setOverPaid(overPaid);
			payment.setPaymentDate(CSUMDateUtils.getDateInString(new Date()));
			payment.setPaymentMethod(paymentMethod);
			payment.setMethodId(paymentMethod.getTypCd());
			if (user != null && user.getUserName() != null && user.getUserName() != "") {
				payment.setProcessedBy(user.getUserName());
				payment.setUser(user.getUserName());
			} else {
				payment.setProcessedBy("Consumer");
				payment.setUser("Consumer");
			}
			payment.setProcessedOn(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
			payment.setTransaction(tx);
			payment.setPaymentSource(paymentSource);
			boolean fullPayFlag = false;
			if (totalDue.compareTo(new BigDecimal("0.00")) == 0 && overPaid.compareTo(new BigDecimal("0.00")) == 0
					&& violation.getViolationCode() != null && !violation.getStatus().equalsIgnoreCase("PAID")) {
				violation.setStatus("PAID");
				fullPayFlag = true;
			} else if (totalDue.compareTo(new BigDecimal("0.00")) == 0
					&& overPaid.compareTo(new BigDecimal("0.00")) == 1 && violation.getViolationCode() != null) {
				violation.setStatus("OVERPAID");
				fullPayFlag = true;
			}
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
					&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
				payment.setViolation(violation);
				payment.setInstallmentPaymentPlan(ipp);
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
				BigDecimal downPayAmt = new BigDecimal(0.00);
				BigDecimal installmentAmt = new BigDecimal(0.00);
				if (ippPayments != null && ippPayments.size() > 0) {
					for (Payment downPayment : ippPayments) {
						if (downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							downPayAmt = downPayAmt.add(downPayment.getAmount());
						} else if (!downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							installmentAmt = installmentAmt.add(downPayment.getAmount());
						}
					}
				}
				BigDecimal downAndInstPay = ipp.getDownPayment().add(ipp.getInstallmentAmount());
				if (!(installmentAmt.add(amountPaid)).equals(new BigDecimal(0.00))
                        && downAndInstPay.compareTo(installmentAmt.add(ipp.getDownPayment()).add(amountPaid)) != 1) {
					/*int daysElapsed = violation.getElapsedDays();
					LocalDate prevSusProcDate=CSUMDateUtils.convert(violation.getSuspendProcessDate());
					if(prevSusProcDate.isBefore(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)))){
						daysElapsed += (int)  ChronoUnit.DAYS.between(prevSusProcDate,LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
					}
					violation.setElapsedDays(daysElapsed);*/
					LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(ipp.getStartDate()).plusDays(81);
					LocalDate processDate1 = CSUMDateUtils.getFormattedLocalDate(violation.getSuspendProcessDate());
					String stringLocalDate = CSUMDateUtils.getFormattedLocalDate(localDate);
					if (!localDate.equals(processDate1)) {
						violation.setSuspendProcessDate(stringLocalDate);
					}
				}
				if (ippPayments != null && downPayAmt.compareTo(ipp.getDownPayment()) != -1) {
					if (amountPaid.compareTo(totalTotalDue) == -1) {
						int num = 0;
						BigDecimal amt = downPayAmt.subtract(ipp.getDownPayment());
						if (!installmentAmt.equals(new BigDecimal(0.00))) {
							double number = Math.ceil(((installmentAmt.add(amt))
									.divide(ipp.getInstallmentAmount(), new MathContext(4))).doubleValue());
							num = (int) number;
						}
						BigDecimal remainAmt = new BigDecimal(0.00);
						Payment ippPay = paymentDetailsService.findLatestByViolationId(violation.getViolationId());
						if (ippPay != null && ippPay.getIppType() != null) {
							if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = downPayAmt.subtract(ipp.getDownPayment());
							} else if (!ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = (downPayAmt.subtract(ipp.getDownPayment())).add(installmentAmt);
							}
						}
						if ((remainAmt.remainder(ipp.getInstallmentAmount()))
								.compareTo(new BigDecimal(0.00)) == 0) {
							int i = num + 1;
							payment.setIppType("Installment : " + i);
						} else {
							payment.setIppType("Installment : " + num);
						}
					} else {
						payment.setIppType("FullPayment");
						ipp.setStatus("FULFILLED");
					}
				} else if (fullPayFlag) {
					payment.setIppType("FullPayment");
					ipp.setStatus("FULFILLED");
				} else {
					payment.setIppType("DownPayment");
					if ((downPayAmt.add(amount)).compareTo(ipp.getDownPayment()) == -1) {
						ipp.setStatus("PENDING");
					} else {
						ipp.setStatus("ACTIVE");
						String startDate = ipp.getStartDate();
						/*int daysElapsed = violation.getElapsedDays();
						LocalDate prevSusProcDate=CSUMDateUtils.getFormattedLocalDate(violation.getSuspendProcessDate());
						if(prevSusProcDate.isBefore(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)))){
							daysElapsed += (int)  ChronoUnit.DAYS.between(prevSusProcDate,LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
						}
						violation.setElapsedDays(daysElapsed);*/
						LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(startDate).plusDays(51);
						violation.setSuspendProcessDate(CSUMDateUtils.getFormattedLocalDate(localDate)); 
					}
				}
				payment.setUser(user.getUserName());
					ipp.setUser(user.getUserName());
					ippService.updateIpp(ipp);	
			}
			violation.setTotalDue(totalDue);
			if (user != null) {
				violation.setUser(user.getUserName());
			}
			violationService.updateViolation(violation);
			payment.setViolation(violation);
			paymentDetailsService.save(payment);
		}
	}
	
	@PostMapping(value = { "/cashPayment/{violationId}/{type}" })
	public String cashPayment(@PathVariable String violationId, @PathVariable String type, HttpServletRequest req, HttpServletResponse res, Model model,
			@ModelAttribute("paymentForm") Payment paymentDetailsForm, BindingResult bindingResult) {
		// logger.debug(paymentDetailsForm);
		//BigDecimal amountPaid = paymentDetailsForm.getAmount();
		if (paymentDetailsForm.getAmount() == null) {
			paymentDetailsForm.setAmount(new BigDecimal("0"));
		}		
		Violation violationDetails = violationService.findByViolationId(violationId);

		if (paymentDetailsForm.getAmount().compareTo(new BigDecimal("0")) > 0 && violationId != null) {
			try {
				BigDecimal due = paymentDetailsForm.getAmount();
				Admin user = (Admin) req.getSession().getAttribute("admin");
				Transaction tx = new Transaction();
				tx.setCreatedDate(new Date());
				tx.setAmount(paymentDetailsForm.getAmount() + "");
				tx.setStatus("Success");
				if(type != null){
					if(type.equalsIgnoreCase("cash")){
						tx.setStatusMessage("Citation Payment by Cash is successful");
					}else if(type.equalsIgnoreCase("dmv")){
						tx.setStatusMessage("Citation Payment by Dmv is successful");
					}
				}
				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyyyHHmmSS");
				String currentTimeStamp = DATE_FORMAT.format(new Timestamp(new Date().getTime()));
				tx.setTranscationId(paymentTransactionId + currentTimeStamp + "TX");
				tx.setViolationId(violationId);
				transactionService.save(tx);
				// logger.debug("save cashPayment:: paymentDetailsForm:: " +
				// paymentDetailsForm);
				paymentDetailsForm.setTransaction(tx);
				if(type != null){
					if(type.equalsIgnoreCase("cash")){
						savePayment(violationDetails, violationDetails.getTotalDue(), user, due, tx, paymentDetailsForm.getPaymentSource(), 1);
					}else if(type.equalsIgnoreCase("dmv")){
						savePayment(violationDetails, violationDetails.getTotalDue(), user, due, tx, paymentDetailsForm.getPaymentSource(), 12);
					}
				}
				Violation violationInfo = violationService.findByViolationId(violationId);
				model.addAttribute("violationInfo", violationInfo);
				model.addAttribute("errorMessagePayment",
						"Violation payment is successful. Please find the below acknowledgement");
				Payment payment = paymentDetailsService.findByTransactionId(tx.getId());
				// logger.debug("Cheque Payment Receipt Info:: " + payment);
				model.addAttribute("payedInfo", payment);
			} catch (Exception e) {
				logger.error(e, e);
				if(type != null){
					if(type.equalsIgnoreCase("cash")){
						model.addAttribute("unactiveOncashPay", "active");
					}else if(type.equalsIgnoreCase("dmv")){
						model.addAttribute("unactiveOndmvPay", "active");
					}
				}
				/*
				 * String token = getBraintreeClientToken();
				 * model.addAttribute("ClientToken", token);
				 */
				String stripePublishableKey = getStripeKey();
				model.addAttribute("stripePublishableKey", stripePublishableKey);
				model.addAttribute("errorMessageCheque", "Server error.");
				if (violationDetails.getPlateEntity() != null) {
					model.addAttribute("licenseNumber", violationDetails.getPlateEntity().getLicenceNumber());
				}
				return "payment";
			}
			return "violationInfo";
		} else {
			if(type != null){
				if(type.equalsIgnoreCase("cash")){
					model.addAttribute("unactiveOncashPay", "active");
					model.addAttribute("errorMessageCash", "Amount should not be NULL or Empty");
				}else if(type.equalsIgnoreCase("dmv")){
					model.addAttribute("unactiveOndmvPay", "active");
					model.addAttribute("errorMessageDmv", "Amount should not be NULL or Empty");
				}
			}
			/*
			 * String token = getBraintreeClientToken();
			 * model.addAttribute("ClientToken", token);
			 */
			String stripePublishableKey = getStripeKey();
			model.addAttribute("stripePublishableKey", stripePublishableKey);
			model.addAttribute("violationId", violationId);
			model.addAttribute("citation", violationDetails);
			model.addAttribute("totalDue", violationDetails.getTotalDue());
			model.addAttribute("paymentForm", new Payment());
			if (violationDetails.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violationDetails.getPlateEntity().getLicenceNumber());
			}
			return "payment";
		}
	}
	
	@GetMapping(value={"/paymentDetails/{violationId}"})
	public String getPaymentDetails(@PathVariable String violationId, ModelMap model){
		String licenseNumber="";
		List<Payment> paymentList = new ArrayList<Payment>();
		try{
		Violation violation= violationService.findByViolationId(violationId);
		if(violation!=null && violation.getPlateEntity()!=null && violation.getPlateEntity().getLicenceNumber()!=null){
			licenseNumber = violation.getPlateEntity().getLicenceNumber();
		}
		paymentList = paymentDetailsService.findByViolationId(violationId);
		model.addAttribute("paymentList",paymentList);
		model.addAttribute("licenseNumber",licenseNumber);
		model.addAttribute("violationId",violationId);
		}catch(Exception e){
			logger.error(e,e);
		}
		boolean ippFlag = ippService.showIpp(violationId);
		model.addAttribute("ippFlag", ippFlag);
		boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		return "paymentDetails";
		
	}
}
