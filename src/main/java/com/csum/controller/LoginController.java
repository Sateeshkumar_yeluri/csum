package com.csum.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csum.model.Admin;
import com.csum.model.ViolationSearch;
import com.csum.service.AdminService;
import com.csum.view.model.LoginBean;

@Controller
public class LoginController {
	
	private static Logger logger = LogManager.getLogger(LoginController.class);
	
	@Autowired
	private AdminService adminService;
	
	@GetMapping("/login")
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, LoginBean loginBean) {
		ModelAndView model = new ModelAndView();
		// LoginBean loginBean = new LoginBean();
		model.addObject("loginBean", loginBean);
		return model;
	}
	
	@PostMapping("/login")
	public String executeLogin(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("loginBean") LoginBean loginBean, Model model) {
		try{
		Admin user = adminService.findByUserName(loginBean.getUsername());
		if (loginBean != null && user != null && user.getPassword() != null && user.getPassword() != null
				&& user.getPassword().equals(loginBean.getPassword())) {
			logger.debug("successfully logged in");
			request.getSession().setAttribute("admin", user);
			model.addAttribute("violationSearchForm",new ViolationSearch());
			return "redirect:/home";
		}
		model.addAttribute("resultMessage", "Invalid Credentials");
		}catch(Exception e){
			logger.error(e,e);
		}
		return "login";
	}
	
	@GetMapping(value = "/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/login?logout";
	}

}
