package com.csum.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.csum.model.Notices;
import com.csum.model.Violation;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.NoticesService;
import com.csum.service.ViolationService;

@Controller
public class NoticesController {
	
	public static final Logger logger = LogManager.getLogger();
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private NoticesService noticesService;	

	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private IppService ippService;
	
	@GetMapping(value = { "/noticesDetails/{violationId}" })
	public String getNoticeDetails(@PathVariable String violationId, ModelMap model) {
		String licenseNumber="";
		List<Notices> noticesList;
		try{
			Violation violation= violationService.findByViolationId(violationId);
			if(violation!=null && violation.getPlateEntity()!=null && violation.getPlateEntity().getLicenceNumber()!=null){
				licenseNumber = violation.getPlateEntity().getLicenceNumber();
			}
			noticesList = noticesService.findByViolationId(violationId);
			model.addAttribute("noticesList",noticesList);
			model.addAttribute("licenseNumber",licenseNumber);
			model.addAttribute("violationId",violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		}catch(Exception e){
			logger.error(e,e);
		}		
		return "noticesDetails";
	}

}
