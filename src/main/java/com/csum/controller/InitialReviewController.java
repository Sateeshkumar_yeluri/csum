package com.csum.controller;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.csum.model.Address;
import com.csum.model.Admin;
import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.CorrespondenceCode;
import com.csum.model.Hearing;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;
import com.csum.model.SuspendedCodes;
import com.csum.model.Suspends;
import com.csum.model.User;
import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.service.CorrespondenceCodesService;
import com.csum.service.CorrespondenceService;
import com.csum.service.HistoryService;
import com.csum.service.ReviewProcessService;
import com.csum.service.SuspendedCodesService;
import com.csum.service.SuspendsService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.csum.view.model.HistoryBean;

@Controller
public class InitialReviewController {
	
	public static final Logger logger = LogManager.getLogger();
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired 
	private ReviewProcessService reviewProcessService;
	
	@Autowired
	private SuspendedCodesService suspendedCodesService;
	
	@Autowired
	private CorrespondenceCodesService correspondenceCodesService;
	
	@Autowired
	private ViolationImagesService violationImagesService;
	
	@Autowired
	private SuspendsService suspendsService;
	
	@Autowired
	private CorrespondenceService  correspondenceService;
	
	@Autowired
	private HistoryService historyService;
	
	@GetMapping(value={"/initialReview/{violationId}"})
	public String initialReview(ModelMap model, HttpSession httpSession, @PathVariable String violationId){
		Violation violation;
		String licenseNumber="";
		List<ReviewComments> commentDetails = new ArrayList<ReviewComments>();
		Properties prop = new Properties();
		List<SuspendedCodes> suspendedCodes;
		List<CorrespondenceCode> correspondenceCodes;
		ReviewProcess reviewProcess;
		List<ViolationImages> images ;
		boolean period = false;
		String reviewLimit;
		String weekDay = "";
		String name="";
		HistoryBean historyBean;
		try{
			Admin user = (Admin) httpSession.getAttribute("admin");
			violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				if (violation.getDateOfViolation() != null && !violation.getDateOfViolation().equalsIgnoreCase("")) {
					weekDay = CSUMDateUtils.getDayOfWeek(violation.getDateOfViolation());
				}
				if (violation.getPlateEntity() != null && violation.getPlateEntity().getLicenceNumber() != null) {
					licenseNumber = violation.getPlateEntity().getLicenceNumber();
				}
				InputStream inputStream =ViolationController.class.getClassLoader()
						.getResourceAsStream("application.properties");
				prop.load(inputStream);
				reviewLimit = prop.getProperty("violation.initialreview.value");
				period = validateForInitialReview(violation, reviewLimit);
				if(violation.getPatron()!=null){				
					if (violation.getPatron().getFirstName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = violation.getPatron().getFirstName() + " ";
					}
					if (violation.getPatron().getMiddleName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = name + violation.getPatron().getMiddleName() + " ";
					}
					if (violation.getPatron().getLastName() != null && violation.getPatron().getFirstName().trim().length() > 0) {

						name = name + violation.getPatron().getLastName();
					}
				}
			}
			commentDetails = reviewProcessService.findAllReviewComments();
			suspendedCodes = suspendedCodesService.findAll();
			correspondenceCodes = correspondenceCodesService.findAll();
			reviewProcess = reviewProcessService.findByViolationId(violationId);
			if(reviewProcess==null){
				reviewProcess = new ReviewProcess();
			}
			images = violationImagesService.findByViolationId(violationId);
			historyBean = historyService.getRequiredHistory("All", violationId);
			model.addAttribute("historyBean",historyBean);
			model.addAttribute("images",images);
			model.addAttribute("reviewProcess",reviewProcess);
			model.addAttribute("correspondenceCodes",correspondenceCodes);
			model.addAttribute("suspendedCodes",suspendedCodes);
			model.addAttribute("commentDetails",commentDetails);
			model.addAttribute("name", name);
			model.addAttribute("period",period);
			model.addAttribute("licenseNumber",licenseNumber);
			model.addAttribute("dayOfWeek", weekDay);
			model.addAttribute("violationForm",violation);
			model.addAttribute("violationId",violationId);
		}catch(Exception e){
			logger.debug(e,e);
		}
		return "initialReview";
	}
	
	@PostMapping(value = { "/initialReview/{violationId}" })
	public String initialReview(@ModelAttribute("reviewProcess") ReviewProcess reviewProcess, ModelMap model,
			HttpSession httpSession, @PathVariable String violationId) throws IOException, ParseException {
		Violation violation;
		try{
			User user = (User) httpSession.getAttribute("user");
			violation = violationService.findByViolationId(violationId);
			reviewProcess.setViolation(violation);
			reviewProcess.setUser(user.getUserName());
			ReviewProcess reviewProcessDetails = reviewProcessService.findByViolationId(violationId);
			if(reviewProcessDetails!=null){
				reviewProcess.setId(reviewProcessDetails.getId());
				reviewProcessService.update(reviewProcess);
			}else{
				reviewProcessService.save(reviewProcess);
			}			
		}catch(Exception e){
			logger.error(e,e);
		}
		return "redirect:/initialReview/" + violationId;
	}
	
	@PostMapping(value = { "/reviewResults/{violationId}" })
	public String reviewResults(@ModelAttribute("reviewProcess") ReviewProcess reviewProcess, ModelMap model,
			HttpSession httpSession, @PathVariable String violationId, RedirectAttributes redirectAttributes)
			throws IOException, ParseException {
		Violation violation;
		boolean period = false;
		Properties prop = new Properties();
		String reviewLimit;
		ReviewProcess reviewProcessDetails;
		boolean reviewStatus;
		SuspendedCodes suspendCode = new SuspendedCodes();
		Suspends suspend = new Suspends();
		CorrespondenceCode correspCode = new CorrespondenceCode();
		Correspondence corresp = new Correspondence();
		Comment comment = new Comment();
		try{
			Admin user = (Admin) httpSession.getAttribute("admin");
			violation = violationService.findByViolationId(violationId);
			if(violation!=null){
				reviewProcess.setViolation(violation);
				InputStream inputStream =ViolationController.class.getClassLoader()
						.getResourceAsStream("application.properties");
				prop.load(inputStream);
				reviewLimit = prop.getProperty("violation.initialreview.value");
				period = validateForInitialReview(violation, reviewLimit);
			}
			reviewProcess.setUser(user.getUserName());
			reviewProcessDetails = reviewProcessService.findByViolationId(violationId);
			if((reviewProcess.getCorrespondence() != null && reviewProcess.getCorrespondence() != 0)
					|| (reviewProcess.getSuspends() != null && reviewProcess.getSuspends() != 0)){
				reviewStatus = true;
			}else{
				reviewStatus = false;
			}
			if(reviewProcessDetails!=null){	
				reviewProcessDetails.setStatus(reviewStatus);
				reviewProcessService.update(reviewProcessDetails);
			}else{
				reviewProcess.setStatus(reviewStatus);	
				reviewProcessService.save(reviewProcess);
			}
			if(reviewProcessDetails != null && ((!period && reviewProcessDetails.ignoreTime != null) || (period))
					&& (reviewProcessDetails.question1 != null
					&& (reviewProcessDetails.question1.equalsIgnoreCase("Person")
							|| reviewProcessDetails.question1.equalsIgnoreCase("Mail")
							|| reviewProcessDetails.question1.equalsIgnoreCase("Phone")))){
				suspendCode = suspendedCodesService.findByCode(220);
				suspend.setSuspendedCodes(suspendCode);
				suspend.setUser(user.getUserName());
				suspend.setProcessedOn(
						new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
				suspend.setTotalDue(reviewProcess.getViolation().getTotalDue());
				suspend.setSuspended_date(
						new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
				suspend.setSuspended_time(CSUMDateUtils.getFormattedLocalTime(
						new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime()));
				if (suspendCode.getAmount() != null && suspendCode.getAmount() != "") {
					BigDecimal newFine = new BigDecimal(suspendCode.getAmount());
					BigDecimal due = violation.getTotalDue().subtract(violation.getFineAmount()).add(newFine);
					violation.setTotalDue(due);
					violation.setFineAmount(newFine);
					violationService.updateViolation(violation);					
				}
				suspend.setViolation(violation);
				suspend.setMode("IR");
				suspendsService.save(suspend);	
				suspend = new Suspends();
				suspendCode = new SuspendedCodes();
			}
			if (reviewProcess.getSuspends() != null && reviewProcess.getSuspends() != 0) {
				suspendCode = suspendedCodesService.findById(reviewProcess.getSuspends());
				if(suspendCode.getDescription() != null && suspendCode.getDescription() != ""){
					suspend.setSuspendedCodes(suspendCode);
					suspend.setUser(user.getUserName());
					suspend.setProcessedOn(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					suspend.setTotalDue(reviewProcess.getViolation().getTotalDue());
					suspend.setSuspended_date(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					suspend.setSuspended_time(CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime()));
					if (suspendCode.getAmount() != null && suspendCode.getAmount() != "") {
						BigDecimal newFine = new BigDecimal(suspendCode.getAmount());
						BigDecimal due = violation.getTotalDue().subtract(violation.getFineAmount()).add(newFine);
						violation.setTotalDue(due);
						violation.setFineAmount(newFine);
						violationService.updateViolation(violation);					
					}
					suspend.setViolation(violation);
					suspend.setMode("IR");
					suspendsService.save(suspend);
				}
			}
			if (reviewProcess.getCorrespondence() != null && reviewProcess.getCorrespondence() != 0) {
				correspCode = correspondenceCodesService.findById(reviewProcess.getCorrespondence());
				if (correspCode.getCorrespDesc() != null && correspCode.getCorrespDesc() != "") {
					corresp.setViolation(violation);
					corresp.setCorresp_date(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					corresp.setCorresp_time(CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime()));
					corresp.setUser(user.getUserName());
					corresp.setCorrespCode(correspCode);
					corresp.setLetterSent(reviewProcess.isLetterSent());
					corresp.setMode("IR");
					correspondenceService.save(corresp);
				}
			}
			if (reviewProcess.getCommentsInfoID() != null && reviewProcess.getCommentsInfoID().trim().length()>0) {
				comment.setViolation(violation);
				comment.setUser(user.getUserName());
				comment.setComment(reviewProcess.getCommentsInfoID());
				comment.setMode("IR");
				comment.setCommentType("I");
				violationService.addComment(comment);
			}else if (reviewProcess.getNoteedited() != null && reviewProcess.getNoteedited() != "") {
				comment.setViolation(violation);
				comment.setComment(reviewProcess.getNoteedited());
				comment.setCommentType("I");
				comment.setMode("IR");
				comment.setUser(user.getUserName());
				violationService.addComment(comment);
			}
			
			
		}catch(Exception e){
			logger.error(e,e);
		}		
		return "redirect:/initialReview/" + violationId;
	}
		
	private boolean validateForInitialReview(Violation violation, String reviewLimit) {
		boolean Value = false;
		try {
			String issueDate = violation.getDateOfViolation();
			if (issueDate != null && issueDate != "") {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf.parse(issueDate));
				cal.add(Calendar.DATE, Integer.parseInt(reviewLimit));
				Date sent1 = cal.getTime();
				Date current = new Date();
				int count1 = sent1.compareTo(current);
				if (count1 > 0) {
					Value = true;
				}
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
		return Value;
	}

	
	
	

}
