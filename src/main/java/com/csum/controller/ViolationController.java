package com.csum.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.csum.model.Violation;
import com.csum.model.ViolationCode;
import com.csum.service.CorrespondenceService;
import com.csum.service.HearingService;
import com.csum.service.HistoryService;
import com.csum.service.IppService;
import com.csum.service.NoticesService;
import com.csum.service.PaymentDetailsService;
import com.csum.service.PenaltyService;
import com.csum.service.SuspendsService;
import com.csum.service.ViolationCodeService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.csum.view.model.LoginBean;
import com.csum.model.Address;
import com.csum.model.Admin;
import com.csum.model.Correspondence;
import com.csum.model.Hearing;
import com.csum.model.Notices;
import com.csum.model.Patron;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;

@Controller
public class ViolationController {

	public static final Logger logger = LogManager.getLogger(ViolationController.class);

	@Autowired
	private ViolationService violationService;

	@Autowired
	private ViolationCodeService violationCodeService;

	@Autowired
	private HearingService hearingService;

	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private IppService ippService;
	
	@Autowired
	private PaymentDetailsService paymentDetailsService;
	
	@Autowired
	private CorrespondenceService correspondenceService;
	
	@Autowired
	private SuspendsService suspendsService;
	
	@Autowired
	private NoticesService noticesService;
	
	@Autowired
	private PenaltyService penaltyService;
	
	@GetMapping(value = { "/violationEntry" })
	public String enterViolation(ModelMap model) {
		Violation violation = new Violation();
		List<ViolationCode> violationCodes = violationCodeService.findAll();
		model.addAttribute("violationCodes", violationCodes);
		model.addAttribute("violationForm", violation);
		return "violationEntry";
	}

	@PostMapping(value = { "/saveViolation" })
	public String saveViolation(@ModelAttribute("violationForm") Violation violationForm, Model model,
			HttpSession session) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			model.addAttribute("enteredViolationId", violationForm.getViolationId());
			Violation violationCheck = violationService.findByViolationId(violationForm.getViolationId());
			if (violationCheck != null) {
				model.addAttribute("errorInfo",
						"Violation with number" + violationForm.getViolationId() + " is already exists.");
				return "violationEntry";
			} else {
				String curDate = CSUMDateUtils.getDateInString(new Date());
				Violation violation = new Violation();
				violation.setIssueDate(curDate);
				violation.setIssueTime(new Date());
				violation.setProcessDate(curDate);
				violation.setSuspendProcessDate(curDate);
				if (user != null) {
					violationForm.setUser(user.getUserName());
				}
				violationService.saveViolation(violationForm);
				logger.debug("Violation saved >> " + violationForm.getViolationId());
				Violation violationInfo = violationService.findByViolationId(violationForm.getViolationId());
				model.addAttribute("violationInfo", violationInfo);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "violationInfo";
	}

	@GetMapping(value = { "/violationView/{violationId}" })
	public String violationView(@PathVariable String violationId, ModelMap model) {
		try {
			BigDecimal totalPaid = new BigDecimal(0.00);
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				model.addAttribute("violationId", violation.getViolationId());
				String formattedTime = violation.getTimeOfViolation();
				if (formattedTime != null && formattedTime != "" && formattedTime.length() == 7) {
					formattedTime = "0" + formattedTime;
					violation.setTimeOfViolation(formattedTime);
				}
				model.addAttribute("violation", violation);
				if (violation.getDateOfViolation() != null && !violation.getDateOfViolation().equalsIgnoreCase("")) {
					/*
					 * Properties prop = new Properties(); InputStream
					 * inputStream = ViolationController.class.getClassLoader()
					 * .getResourceAsStream("application.properties");
					 * prop.load(inputStream);
					 */
					String weekDay = CSUMDateUtils.getDayOfWeek(violation.getDateOfViolation());
					model.addAttribute("dayOfWeek", weekDay);
				}
				String name = "";
				if (violation.getPatron() != null) {
					if (violation.getPatron().getFirstName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = violation.getPatron().getFirstName() + " ";
					}
					if (violation.getPatron().getMiddleName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {
						name = name + violation.getPatron().getMiddleName() + " ";
					}
					if (violation.getPatron().getLastName() != null
							&& violation.getPatron().getFirstName().trim().length() > 0) {

						name = name + violation.getPatron().getLastName();
					}
					model.addAttribute("name", name);
				}
				List<Payment> paymentsList = paymentDetailsService.findByViolationId(violationId);
				if (paymentsList.size() > 0 && paymentsList != null) {
					for (Payment p : paymentsList) {
						if (p.getAmount() != null) {
							totalPaid = totalPaid.add(p.getAmount());
						}

					}
				}
				model.addAttribute("totalPaid", totalPaid);
				Correspondence correspondenceDetails = correspondenceService.findLatestByViolationId(violationId);
				if (correspondenceDetails != null && correspondenceDetails.getCorresp_time() != null
						&& correspondenceDetails.getCorresp_time() != ""
						&& correspondenceDetails.getCorresp_time().length() == 7) {
					correspondenceDetails.setCorresp_time("0" + correspondenceDetails.getCorresp_time());
				}
				model.addAttribute("correspondenceDetails", correspondenceDetails);
				Payment paymentDetails = paymentDetailsService.findLatestByViolationId(violationId);
				model.addAttribute("paymentDetails", paymentDetails);
				Hearing hearingDetails = hearingService.findByViolationId(violationId);
				model.addAttribute("hearingDetails", hearingDetails);
				Suspends suspendDetails = suspendsService.findLatestByViolationId(violationId);
				if (suspendDetails != null && suspendDetails.getSuspended_time() != null
						&& suspendDetails.getSuspended_time() != "" && suspendDetails.getSuspended_time().length() == 7) {
					suspendDetails.setSuspended_time("0" + suspendDetails.getSuspended_time());
				}
				model.addAttribute("suspendDetails", suspendDetails);
				Penalty penaltyDetails = penaltyService.findByViolationId(violationId);
				model.addAttribute("penalty", penaltyDetails);
				Notices noticeDetails = noticesService.findLatestByViolationId(violationId);
				model.addAttribute("noticeDetails", noticeDetails);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		boolean ippFlag = ippService.showIpp(violationId);
		model.addAttribute("ippFlag", ippFlag);
		return "violationView";
	}

	@GetMapping(value = { "/violationDetails/{violationId}" })
	public String editViolation(@PathVariable String violationId, ModelMap model) {
		try {
			if (violationId != null) {
				Violation violation = violationService.findByViolationId(violationId);
				if (violation != null) {
					List<ViolationCode> violationCodes = violationCodeService.findAll();
					model.addAttribute("violationCodes", violationCodes);
				}
				model.addAttribute("violationForm", violation);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "violationEntry";
	}

	@PostMapping(value = { "/updateViolation/{violationId}" })
	public String updateViolation(@ModelAttribute("violationForm") Violation violation, ModelMap model,
			@PathVariable String violationId, HttpSession session) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			Violation prevViolation = violationService.findByViolationId(violationId);
			if(prevViolation!=null){
				violation.setIssueDate(prevViolation.getIssueDate());
				violation.setIssueTime(prevViolation.getIssueTime());
				violation.setProcessDate(prevViolation.getProcessDate());
				violation.setSuspendProcessDate(prevViolation.getSuspendProcessDate());
			}
			if (violation.getTotalDue() != null && violation.getTotalDue().compareTo(BigDecimal.ZERO) == 0
					&& !violation.getStatus().equals("VOID") && !violation.getStatus().equals("OVERPAID")) {
				violation.setStatus("PAID");
			} else if (violation.getTotalDue() != null && violation.getTotalDue().compareTo(BigDecimal.ZERO) == -1) {
				violation.setStatus("OVERPAID");
				violation.setTotalDue(BigDecimal.ZERO);
			}
			if (violation.getStatus() != null && violation.getStatus().equals("VOID")) {
				violation.setTotalDue(new BigDecimal(0.00));
			}
			if (user != null) {
				violation.setUser(user.getUserName());
			}
			violationService.updateViolation(violation);
			Violation violationInfo = violationService.findByViolationId(violationId);
			model.addAttribute("violationInfo", violationInfo);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "violationInfo";
	}

	@PostMapping(value = { "/sampleData" })
	public String InsertSampleData(ModelMap model) {
		logger.debug("Inserting Sample Data");
		String user = "System";
		try {
			historyService.deleteAll(user);
			insertViolation("LOT A", "Pending", "CVC 21113 (A)-1.1", "1ABC234", "12345678901234", "SUV", "FORD", "ECOSPORT","BLU", user);
			insertViolation("LOT B", "Pending", "CVC 21113 (A)-1.2", "2BCD345", "23456789012345", "SUV", "BMW", "BMW X1","WHI", user);
			insertViolation("LOT C", "Pending", "CVC 21113 (A)-1.3", "3CDE456", "34567890123456", "TRUCK", "CHEV", "SILVERADO","MAR", user);
			insertViolation("LOT D", "Pending", "CVC 21113 (A)-4.1", "4DEF567", "45678901234567", "SUV", "MER", "Mercedes Benz GLS","GRY", user);
			insertViolation("LOT E", "Pending", "CVC 21113 (A)-4.4", "5EFG678", "56789012345678", "TRUCK", "CADI", "ESCALADE","BLK", user);
			insertViolation("LOT F", "Pending", "CVC 21113 (A)-4.5", "6FGH789", "67890123456789", "SUV", "FORD", "ECOSPORT","BLU", user);
			insertViolation("LOT G", "Pending", "CVC 22507.8 (C)", "7GHI890", "78901234567890", "SUV", "BMW", "BMW X1","WHI", user);
			insertViolation("LOT I", "Pending", "CVC 22500 (F)", "8HIJ901", "89012345678901", "TRUCK", "CHEV", "SILVERADO","MAR", user);
			insertViolation("LOT M", "Pending", "CVC 22500.1", "9IJK012", "90123456789012", "SUV", "MER", "Mercedes Benz GLS","GRY", user);
			insertViolation("LOT O", "Pending", "CVC 22514", "1JKL23", "10123456789012", "TRUCK", "CADI", "ESCALADE","BLK", user);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:/home";
	}
	
	private void insertViolation(String locationOfViolation, String status, String violationCode, String licenceNumber,
			String vin, String bodyType, String vhclMake, String vhclModel, String vhclColor, String user) {
		Violation violation = new Violation();
		ViolationCode vlnCode;
		BigDecimal fineAmount;
		PlateEntity plateEntity = new PlateEntity();
		try {
			String curDate = CSUMDateUtils.getDateInString(new Date());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			long violationId = calendar.getTimeInMillis();
			violation.setViolationId(violationId + "");
			violation.setLocationOfViolation(locationOfViolation);
			violation.setDateOfViolation(
					CSUMDateUtils.getFormattedLocalDate(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))));
			violation.setTimeOfViolation(
					CSUMDateUtils.getFormattedLocalTime(LocalTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))));
			violation.setIssueDate(curDate);
			violation.setProcessDate(curDate);
			violation.setSuspendProcessDate(curDate);
			violation.setIssueTime(new Date());
			violation.setStatus(status);
			vlnCode = violationCodeService.findByCode(violationCode);
			if (vlnCode != null) {
				fineAmount = vlnCode.getStandardFine();
				violation.setFineAmount(fineAmount);
				violation.setTotalDue(fineAmount);
				violation.setViolationCode(vlnCode);
			}
			violation.setPatron(new Patron());
			plateEntity.setLicenceNumber(licenceNumber);
			plateEntity.setVinNumber(vin);
			plateEntity.setBodyType(bodyType);
			plateEntity.setVehicleMake(vhclMake);
			plateEntity.setVehicleModel(vhclModel);
			plateEntity.setVehicleColor(vhclColor);
			violation.setPlateEntity(plateEntity);
			violation.setUser(user);
			violationService.saveViolation(violation);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
	
	@GetMapping(value = { "/addressEdit/{violationId}/{hearingId}" })
	public String showAddressDetails(@PathVariable String violationId, @PathVariable Long hearingId, ModelMap model,
			HttpSession httpSession, RedirectAttributes redirectAttributes) {
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			if (user != null) {
				Violation violation = violationService.findByViolationId(violationId);
				//List<States> states = statesService.findAll();
				model.addAttribute("addressDetailsForm", violation);
				model.addAttribute("violationId", violationId);
				model.addAttribute("hearingId", hearingId);
				//model.addAttribute("states", states);
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:../../adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "addressEdit";
	}
	
	@PostMapping(value = { "/addressEdit/{violationId}/{hearingId}" })
	public String editAddressDetails(@PathVariable String violationId, @PathVariable Long hearingId, ModelMap model,
			@ModelAttribute("addressDetailsForm") Violation addressDetailsForm, HttpSession httpSession,
			RedirectAttributes redirectAttributes) {
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			if (user != null) {
				Violation violation = violationService.findByViolationId(violationId);
				if (violation != null && addressDetailsForm != null) {
					if (violation.getPatron() != null) {
						if (violation.getPatron().getAddress() != null) {
							violation.getPatron().getAddress()
									.setAddress(addressDetailsForm.getPatron().getAddress().getAddress());
							violation.getPatron().getAddress()
									.setCity(addressDetailsForm.getPatron().getAddress().getCity());
							violation.getPatron().getAddress()
									.setState(addressDetailsForm.getPatron().getAddress().getState());
							violation.getPatron().getAddress()
									.setZip(addressDetailsForm.getPatron().getAddress().getZip());
						} else {
							Address address = new Address();
							address.setAddress(addressDetailsForm.getPatron().getAddress().getAddress());
							address.setCity(addressDetailsForm.getPatron().getAddress().getCity());
							address.setState(addressDetailsForm.getPatron().getAddress().getState());
							address.setZip(addressDetailsForm.getPatron().getAddress().getZip());
							violation.getPatron().setAddress(address);
						}
						violation.getPatron().setFirstName(addressDetailsForm.getPatron().getFirstName());
						violation.getPatron().setLastName(addressDetailsForm.getPatron().getLastName());
					} else {
						Patron patron = new Patron();
						Address address = new Address();
						address.setAddress(addressDetailsForm.getPatron().getAddress().getAddress());
						address.setCity(addressDetailsForm.getPatron().getAddress().getCity());
						address.setState(addressDetailsForm.getPatron().getAddress().getState());
						address.setZip(addressDetailsForm.getPatron().getAddress().getZip());
						patron.setFirstName(addressDetailsForm.getPatron().getFirstName());
						patron.setLastName(addressDetailsForm.getPatron().getLastName());
						patron.setAddress(address);
						violation.setPatron(patron);
					}
					violation.setUser(user.getUserName());
					violationService.updateViolation(violation);
					/*if (hearingId > 0) {
						FormLetter formLetter = formLetterService.findByHearingId(hearingId);
						Hearing hearing = hearingDetailsService.findById(hearingId);
						if (hearing != null && !hearing.getStatus().equalsIgnoreCase("COMPLETE")) {
							if (formLetter != null && violation != null && violation.getPatron() != null) {
								saveLetterDetails(hearingId, user, false, true);
							}
						}
					}*/
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:../../adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:/hearingsEdit/" + violationId + "/" + hearingId;
	}

}
