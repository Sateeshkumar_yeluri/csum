package com.csum.controller;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.csum.model.Admin;
import com.csum.model.Ipp;
import com.csum.model.Payment;
import com.csum.model.PaymentMethod;
import com.csum.model.Transaction;
import com.csum.model.User;
import com.csum.model.Violation;
import com.csum.service.IppService;
import com.csum.service.PaymentDetailsService;
import com.csum.service.PaymentMethodService;
import com.csum.service.TransactionService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.stripe.Stripe;
import com.stripe.model.Charge;

@Controller
public class ConsumerStripeController {

	public static final Logger logger = LogManager.getLogger(ConsumerStripeController.class);

	@Autowired
	private ViolationService violationService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private PaymentDetailsService paymentDetailsService;

	@Autowired
	private PaymentMethodService paymentMethodService;
	
	@Autowired
	private IppService ippService;

	@RequestMapping(value = { "/payment/{violationId}" }, method = RequestMethod.GET)
	public String adminStripePayment(ModelMap model, HttpServletRequest req, HttpServletResponse res,
			@PathVariable String violationId) throws ServletException, IOException {
		Properties prop = new Properties();
		InputStream inputStream = ConsumerStripeController.class.getClassLoader()
				.getResourceAsStream("application.properties");
		prop.load(inputStream);
		BigDecimal totalDue = new BigDecimal(0.00);
		BigDecimal instAmount = BigDecimal.ZERO;
		boolean ippFlag = false;
		try {
			Payment payment = new Payment();
			String stripePublishableKey = "";
			if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
				stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
			} else {
				stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
			}
			model.addAttribute("stripePublishableKey", stripePublishableKey);
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null){
				if(violation.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
				}
				totalDue = violation.getTotalDue();
			}
			model.addAttribute("violationId", violationId);
			Ipp ipp = ippService.findByViolationId(violationId);
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")) {
				ippFlag = true;
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
				// List<Payment> allPayments =
				// paymentDetailsService.findById(citationId);
				Payment ippPay = paymentDetailsService.findLatestByViolationId(violationId);
				BigDecimal downPayAmt = new BigDecimal(0.00);
				BigDecimal installmentAmt = new BigDecimal(0.00);
				if (ippPayments != null && ippPayments.size() > 0) {
					for (Payment downPayment : ippPayments) {
						if (downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							downPayAmt = downPayAmt.add(downPayment.getAmount());
						} else if (!downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							installmentAmt = installmentAmt.add(downPayment.getAmount());
						}
					}
				}
				BigDecimal remainAmt = new BigDecimal(0.00);
				double number = 1;
				if (ippPayments != null && downPayAmt.compareTo(ipp.getDownPayment()) != -1) {
					if (totalDue.compareTo(ipp.getInstallmentAmount()) != -1) {
						if (ippPay != null && ippPay.getIppType() != null) {
							if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = downPayAmt.subtract(ipp.getDownPayment());
							} else if (!ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = (downPayAmt.subtract(ipp.getDownPayment())).add(installmentAmt);
							}
						}
						if (installmentAmt.compareTo(new BigDecimal(0.00)) != 0) {
							number = Math.ceil(((installmentAmt.add(downPayAmt.subtract(ipp.getDownPayment())))
									.divide(ipp.getInstallmentAmount(), new MathContext(4))).doubleValue());
						}
						if ((remainAmt.remainder(ipp.getInstallmentAmount()))
								.compareTo(new BigDecimal(0.00)) == 0) {
							instAmount = ipp.getInstallmentAmount();
						} else {
							instAmount = ipp.getInstallmentAmount().multiply(new BigDecimal(number))
									.subtract(remainAmt);
						}
					} else {
						instAmount = totalDue;
					}
				} else {
					instAmount = ipp.getDownPayment().subtract(downPayAmt);
				}
				model.addAttribute("instAmount", instAmount);
			}	
			payment.setIpp(ippFlag);
			model.addAttribute("paymentForm", payment);
			model.addAttribute("totalDue", totalDue);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "payment";
	}

	@RequestMapping(value = { "/checkout/{violationId}" }, method = RequestMethod.POST)
	public String adminCheckout(ModelMap model, HttpServletRequest req, HttpServletResponse res,
			@PathVariable String violationId, @ModelAttribute("paymentForm") Payment paymentForm,
			BindingResult bindingResult) throws ServletException, IOException {
		Admin user = (Admin) req.getSession().getAttribute("admin");
		// logger.debug("processConsumerCardPayment:: " + citationId);
		BigDecimal totalDue = null;
		String token = null;
		String status = null;
		String transcationId = null;
		String transcationStatus = null;
		String statusMessage = null;
		String balanceTxId = null;
		String transactionCode = null;
		Properties prop = new Properties();
		InputStream inputStream = ConsumerStripeController.class.getClassLoader()
				.getResourceAsStream("application.properties");
		if (violationId != null) {
			Violation violation = violationService.findByViolationId(violationId);
			try {
				prop.load(inputStream);
				if (violation != null) {
					if (paymentForm.getAmount() != null && paymentForm.getAmount().compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal due = paymentForm.getAmount();
						token = req.getParameter("stripeToken");
						BigDecimal dueAmount = violation.getTotalDue();
						if (token != null && !token.isEmpty()) {
							// logger.debug("processConsumerCardPayment::
							// dueAmount:: " + dueAmount);
							try {
								long amountInCents = 0;
								amountInCents = due.multiply(new BigDecimal("100")).longValue();
								if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
									Stripe.apiKey = prop.getProperty("violation.stripe.prodapikey");
								} else {
									Stripe.apiKey = prop.getProperty("violation.stripe.testapikey");
								}
								Map<String, Object> params = new HashMap<String, Object>();
								params.put("amount", amountInCents);
								params.put("currency", "usd");
								if (violation.getPatron() != null) {
									params.put("description",
											"Violation $ " + due + " payed by " + violation.getPatron().getLastName()
													+ " " + violation.getPatron().getFirstName());
								} else {
									params.put("description", "Citation details not available");
								}
								params.put("source", token);
								Charge charge = null;
								try {
									charge = Charge.create(params);
								} catch (Exception e) {
									logger.debug(e, e);
									model.addAttribute("errorMessage",
											"Payment Server Error. Check card statement before repeating the payment");
								}
								if (charge != null) {
									/*
									 * logger.debug(charge+" "+charge.getStatus(
									 * ) + " " + charge.getBalanceTransaction()
									 * + " " + charge.getFailureMessage() + " "
									 * + charge.getId() + " " +
									 * charge.getInvoice() + " " +
									 * charge.getPaid());
									 */
									status = charge.getStatus();
									statusMessage = charge.getDescription();
									transcationId = charge.getId();
									transcationStatus = charge.getPaid() + "";
									balanceTxId = charge.getBalanceTransaction();
									transactionCode = charge.getFailureCode();
								} else {
									model.addAttribute("errorMessage",
											"Payment Failed. Check card statement before repeating the payment");
								}
							} catch (Exception e) {
								logger.debug(e, e);
								model.addAttribute("errorMessage",
										"Internal Server Error. Check card statement before repeating the payment");
							}
							Transaction transcation = new Transaction();
							transcation.setStatus(status);
							transcation.setAmount("$" + dueAmount);
							transcation.setCreatedDate(new Date());
							transcation.setStatusResponseCode(transcationStatus);
							transcation.setTranscationId(transcationId);
							transcation.setViolationId(violationId);
							transcation.setStatusMessage(statusMessage);
							transcation.setClientToken(token);
							transcation.setPaymentNonce(balanceTxId);
							transcation.setTranscationCode(transactionCode);
							transactionService.save(transcation);
							if ((transcationStatus != null && transcationStatus.equalsIgnoreCase("true"))
									&& (status != null && status.equalsIgnoreCase("succeeded"))) {
								savePayment(violation, violation.getTotalDue(), user, due, transcation,
										paymentForm.getPaymentSource());
								model.addAttribute("TxResultStatus", "Citation payment success");
								model.addAttribute("errorMessagePayment",
										"Citation payment is successful. Please find the below acknowledgement");
								Payment payment = paymentDetailsService.findByTransactionId(transcation.getId());
								model.addAttribute("payedInfo", payment);
								Violation violationInfo = violationService.findByViolationId(violationId);
								model.addAttribute("violationInfo", violationInfo);
								return "violationInfo";
							} else {
								model.addAttribute("errorMessage",
										"Payment Server Error. Check card statement before repeating the payment");
							}
						} else {
							model.addAttribute("errorMessage", "Internal Server Error. Payment Token Error.");
						}
					} else {
						model.addAttribute("errorMessage", "Amount should not be NULL or Empty");
					}
				} else {
					model.addAttribute("errorMessage", "Invalid violation Id");
				}
				if (violation.getPlateEntity() != null) {
					model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
				}
			} catch (Exception e) {
				logger.error(e, e);
				model.addAttribute("errorMessage",
						"Internal Server Error. Check card statement before repeating the payment");
				model.addAttribute("lastName", violation.getPatron().getLastName());
			}
		}
		String stripePublishableKey = "";
		if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
			stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
		} else {
			stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
		}
		model.addAttribute("violationId", violationId);
		model.addAttribute("totalDue", totalDue);
		model.addAttribute("paymentForm", paymentForm);
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		return "payment";
	}

	public void savePayment(Violation violation, BigDecimal totalDue, Admin user, BigDecimal amount, Transaction tx,
			String paymentSource) {
		BigDecimal amountPaid;
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod = paymentMethodService.findByType(2);
		amountPaid = amount;
		BigDecimal overPaid = new BigDecimal(0.00);
		BigDecimal totalTotalDue = totalDue;
		Payment payment = new Payment();
		if (amountPaid.signum() == 1) {
			amount = totalDue.subtract(amountPaid);
			if (amount.signum() == 1) {
				totalDue = amount;
			} else if (amount.signum() == -1) {
				overPaid = amountPaid.subtract(totalDue);
				totalDue = new BigDecimal(0.00);
			} else {
				totalDue = new BigDecimal(0.00);
			}
			payment.setAmount(amountPaid);
			violation.setTotalDue(totalDue);
			payment.setTotalDue(totalDue);
			payment.setAccount("PAY-BY-WEB");
			payment.setViolation(violation);
			Ipp ipp = ippService.findByViolationId(violation.getViolationId());
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
					&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
				payment.setIpp(true);
			} else {
				payment.setIpp(false);
			}
			payment.setOverPaid(overPaid);
			payment.setPaymentDate(CSUMDateUtils.getDateInString(new Date()));
			payment.setPaymentMethod(paymentMethod);
			payment.setMethodId(paymentMethod.getTypCd());
			if (user != null && user.getUserName() != null && user.getUserName() != "") {
				payment.setProcessedBy(user.getUserName());
				payment.setUser(user.getUserName());
			} else {
				payment.setProcessedBy("Consumer");
				payment.setUser("Consumer");
			}
			payment.setProcessedOn(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
			payment.setTransaction(tx);
			payment.setPaymentSource(paymentSource);
			boolean fullPayFlag = false;
			if (totalDue.compareTo(new BigDecimal("0.00")) == 0 && overPaid.compareTo(new BigDecimal("0.00")) == 0
					&& violation.getViolationCode() != null && !violation.getStatus().equalsIgnoreCase("PAID")) {
				violation.setStatus("PAID");
				fullPayFlag = true;
			} else if (totalDue.compareTo(new BigDecimal("0.00")) == 0
					&& overPaid.compareTo(new BigDecimal("0.00")) == 1 && violation.getViolationCode() != null) {
				violation.setStatus("OVERPAID");
				fullPayFlag = true;
			}
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
					&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
				payment.setViolation(violation);
				payment.setInstallmentPaymentPlan(ipp);
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
				BigDecimal downPayAmt = new BigDecimal(0.00);
				BigDecimal installmentAmt = new BigDecimal(0.00);
				if (ippPayments != null && ippPayments.size() > 0) {
					for (Payment downPayment : ippPayments) {
						if (downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							downPayAmt = downPayAmt.add(downPayment.getAmount());
						} else if (!downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							installmentAmt = installmentAmt.add(downPayment.getAmount());
						}
					}
				}
				BigDecimal downAndInstPay = ipp.getDownPayment().add(ipp.getInstallmentAmount());
				if (!(installmentAmt.add(amountPaid)).equals(new BigDecimal(0.00))
                        && downAndInstPay.compareTo(installmentAmt.add(ipp.getDownPayment()).add(amountPaid)) != 1) {
					/*int daysElapsed = violation.getElapsedDays();
					LocalDate prevSusProcDate=CSUMDateUtils.convert(violation.getSuspendProcessDate());
					if(prevSusProcDate.isBefore(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)))){
						daysElapsed += (int)  ChronoUnit.DAYS.between(prevSusProcDate,LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
					}
					violation.setElapsedDays(daysElapsed);*/
					LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(ipp.getStartDate()).plusDays(81);
					LocalDate processDate1 = CSUMDateUtils.getFormattedLocalDate(violation.getSuspendProcessDate());
					String stringLocalDate = CSUMDateUtils.getFormattedLocalDate(localDate);
					if (!localDate.equals(processDate1)) {
						violation.setSuspendProcessDate(stringLocalDate);
					}
				}
				if (ippPayments != null && downPayAmt.compareTo(ipp.getDownPayment()) != -1) {
					if (amountPaid.compareTo(totalTotalDue) == -1) {
						int num = 0;
						BigDecimal amt = downPayAmt.subtract(ipp.getDownPayment());
						if (!installmentAmt.equals(new BigDecimal(0.00))) {
							double number = Math.ceil(((installmentAmt.add(amt))
									.divide(ipp.getInstallmentAmount(), new MathContext(4))).doubleValue());
							num = (int) number;
						}
						BigDecimal remainAmt = new BigDecimal(0.00);
						Payment ippPay = paymentDetailsService.findLatestByViolationId(violation.getViolationId());
						if (ippPay != null && ippPay.getIppType() != null) {
							if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = downPayAmt.subtract(ipp.getDownPayment());
							} else if (!ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = (downPayAmt.subtract(ipp.getDownPayment())).add(installmentAmt);
							}
						}
						if ((remainAmt.remainder(ipp.getInstallmentAmount()))
								.compareTo(new BigDecimal(0.00)) == 0) {
							int i = num + 1;
							payment.setIppType("Installment : " + i);
						} else {
							payment.setIppType("Installment : " + num);
						}
					} else {
						payment.setIppType("FullPayment");
						ipp.setStatus("FULFILLED");
					}
				} else if (fullPayFlag) {
					payment.setIppType("FullPayment");
					ipp.setStatus("FULFILLED");
				} else {
					payment.setIppType("DownPayment");
					if ((downPayAmt.add(amount)).compareTo(ipp.getDownPayment()) == -1) {
						ipp.setStatus("PENDING");
					} else {
						ipp.setStatus("ACTIVE");
						String startDate = ipp.getStartDate();
						/*int daysElapsed = violation.getElapsedDays();
						LocalDate prevSusProcDate=CSUMDateUtils.getFormattedLocalDate(violation.getSuspendProcessDate());
						if(prevSusProcDate.isBefore(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)))){
							daysElapsed += (int)  ChronoUnit.DAYS.between(prevSusProcDate,LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
						}
						violation.setElapsedDays(daysElapsed);*/
						LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(startDate).plusDays(51);
						violation.setSuspendProcessDate(CSUMDateUtils.getFormattedLocalDate(localDate)); 
					}
				}
				payment.setUser(user.getUserName());
					ipp.setUser(user.getUserName());
					ippService.updateIpp(ipp);	
			}
			violation.setTotalDue(totalDue);
			if (user != null) {
				violation.setUser(user.getUserName());
			}
			violationService.updateViolation(violation);
			payment.setViolation(violation);
			paymentDetailsService.save(payment);
		}
	}

	@GetMapping(value = { "/consumerPayment/{violationId}" })
	public String consumerPayment(HttpServletRequest req, HttpServletResponse res, ModelMap model,
			@PathVariable String violationId) {
		Properties prop = new Properties();
		Payment payment = new Payment();
		String stripePublishableKey = "";
		String licenseNumber = "";
		BigDecimal totalDue = null;
		BigDecimal instAmount = BigDecimal.ZERO;
		boolean ippFlag = false;
		try {
			InputStream inputStream = ConsumerStripeController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
				stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
			} else {
				stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
			}
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				if (violation.getPlateEntity() != null && violation.getPlateEntity().getLicenceNumber() != null) {
					licenseNumber = violation.getPlateEntity().getLicenceNumber();
				}
				totalDue = violation.getTotalDue();
			}
			model.addAttribute("totalDue", totalDue);
			model.addAttribute("licenseNumber", licenseNumber);
			model.addAttribute("stripePublishableKey", stripePublishableKey);
			model.addAttribute("paymentForm", payment);
			model.addAttribute("violationId", violationId);
			Ipp ipp = ippService.findByViolationId(violationId);
			if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")) {
				ippFlag = true;
				List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
				// List<Payment> allPayments =
				// paymentDetailsService.findById(citationId);
				Payment ippPay = paymentDetailsService.findLatestByViolationId(violationId);
				BigDecimal downPayAmt = new BigDecimal(0.00);
				BigDecimal installmentAmt = new BigDecimal(0.00);
				if (ippPayments != null && ippPayments.size() > 0) {
					for (Payment downPayment : ippPayments) {
						if (downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							downPayAmt = downPayAmt.add(downPayment.getAmount());
						} else if (!downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
							installmentAmt = installmentAmt.add(downPayment.getAmount());
						}
					}
				}
				BigDecimal remainAmt = new BigDecimal(0.00);
				double number = 1;
				if (ippPayments != null && downPayAmt.compareTo(ipp.getDownPayment()) != -1) {
					if (totalDue.compareTo(ipp.getInstallmentAmount()) != -1) {
						if (ippPay != null && ippPay.getIppType() != null) {
							if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = downPayAmt.subtract(ipp.getDownPayment());
							} else if (!ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
								remainAmt = (downPayAmt.subtract(ipp.getDownPayment())).add(installmentAmt);
							}
						}
						if (installmentAmt.compareTo(new BigDecimal(0.00)) != 0) {
							number = Math.ceil(((installmentAmt.add(downPayAmt.subtract(ipp.getDownPayment())))
									.divide(ipp.getInstallmentAmount(), new MathContext(4))).doubleValue());
						}
						if ((remainAmt.remainder(ipp.getInstallmentAmount()))
								.compareTo(new BigDecimal(0.00)) == 0) {
							instAmount = ipp.getInstallmentAmount();
						} else {
							instAmount = ipp.getInstallmentAmount().multiply(new BigDecimal(number))
									.subtract(remainAmt);
						}
					} else {
						instAmount = totalDue;
					}
				} else {
					instAmount = ipp.getDownPayment().subtract(downPayAmt);
				}
				model.addAttribute("instAmount", instAmount);
			}	
			payment.setIpp(ippFlag);
			model.addAttribute("paymentForm", payment);
			model.addAttribute("totalDue", totalDue);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "consumerPayment";
	}

	@PostMapping(value = { "/consumerCheckout/{violationId}" })
	public String consumerCheckout(HttpServletRequest req, HttpServletResponse res, ModelMap model,
			@PathVariable String violationId, @ModelAttribute("paymentForm") Payment paymentForm) {
		Properties prop = new Properties();
		BigDecimal totalDue = BigDecimal.ZERO;
		String token = null;
		String status = null;
		long amountInCents = 0;
		String transcationId = null;
		String transcationStatus = null;
		String statusMessage = null;
		String balanceTxId = null;
		String transactionCode = null;
		BigDecimal amountPaid;
		try {
			InputStream inputStream = ConsumerStripeController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			Violation consumer = violationService.findByViolationId(violationId);
			if (consumer != null) {
				if (paymentForm.getAmount() != null && paymentForm.getAmount().compareTo(BigDecimal.ZERO) > 0) {
					totalDue = consumer.getTotalDue();
					amountPaid = paymentForm.getAmount();
					if(totalDue.compareTo(amountPaid)>=0){
						token = req.getParameter("stripeToken");
						if (token != null && !token.isEmpty()) {
							try {
							amountInCents = totalDue.multiply(new BigDecimal("100")).longValue();
							if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
								Stripe.apiKey = prop.getProperty("violation.stripe.prodapikey");
							} else {
								Stripe.apiKey = prop.getProperty("violation.stripe.testapikey");
							}
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("amount", amountInCents);
							params.put("currency", "usd");
							if (consumer.getPatron() != null) {
								String firstName = "", lastName = "";
								if (consumer.getPatron().getFirstName() != null) {
									firstName = consumer.getPatron().getFirstName();
								}
								if (consumer.getPatron().getLastName() != null) {
									lastName = consumer.getPatron().getLastName();
								}
								params.put("description",
										"Violation $ " + totalDue + " payed by " + lastName + " " + firstName);
							} else {
								params.put("description", "Consumer details not available");
							}
							params.put("source", token);
							Charge charge = null;
							try {
								charge = Charge.create(params);
							} catch (Exception e) {
								logger.debug(e, e);
								model.addAttribute("errorMessage",
										"Payment Server Error. Check card statement before repeating the payment");
							}
							if (charge != null) {
								status = charge.getStatus();
								statusMessage = charge.getDescription();
								transcationId = charge.getId();
								transcationStatus = charge.getPaid() + "";
								balanceTxId = charge.getBalanceTransaction();
								transactionCode = charge.getFailureCode();
							} else {
								model.addAttribute("errorMessage",
										"Payment Failed. Check card statement before repeating the payment");
							}	
							} catch (Exception e) {
								logger.debug(e, e);
								model.addAttribute("errorMessage",
										"Internal Server Error. Check card statement before repeating the payment");
							}
							Transaction transaction = new Transaction();
							transaction.setStatus(status);
							transaction.setAmount("$" + paymentForm.getAmount());
							transaction.setCreatedDate(new Date());
							transaction.setStatusResponseCode(transcationStatus);
							transaction.setTranscationId(transcationId);
							transaction.setViolationId(violationId);
							transaction.setStatusMessage(statusMessage);
							transaction.setClientToken(token);
							transaction.setPaymentNonce(balanceTxId);
							transaction.setTranscationCode(transactionCode);
							transactionService.save(transaction);
							if ((transcationStatus != null && transcationStatus.equalsIgnoreCase("true"))
									&& (status != null && status.equalsIgnoreCase("succeeded"))) {
								saveConsumerPayment(consumer, prop.getProperty("violation.consumer.user"), amountPaid, transaction, paymentForm.getPaymentSource());
								model.addAttribute("TxResultStatus", "Citation payment success");
								model.addAttribute("errorMessagePayment",
										"Citation payment is successful. Please find the below acknowledgement");
								Payment payment = paymentDetailsService.findByTransactionId(transaction.getId());
								if (consumer.getPlateEntity() != null) {
									model.addAttribute("licenseNumber", consumer.getPlateEntity().getLicenceNumber());
								}
								model.addAttribute("payedInfo", payment);
								model.addAttribute("consumer", consumer);
								return "consumerPaymentInfo";
							} else {
								model.addAttribute("errorMessage",
										"Payment Server Error. Check card statement before repeating the payment");
							}
						} else {
							model.addAttribute("errorMessage", "Internal Server Error. Payment Token Error.");
						}
					} else {
						model.addAttribute("errorMessage", "Amount should be less than or equal to Total Due");
					}
				} else {
					model.addAttribute("errorMessage", "Amount should not be NULL or Empty");
				}
			} else {
				model.addAttribute("errorMessage", "Invalid violation Id");
			}
		} catch (Exception e) {
			logger.error(e, e);
			model.addAttribute("errorMessage",
					"Internal Server Error. Check card statement before repeating the payment");
		}
		String stripePublishableKey = "";
		if (prop.getProperty("violation.stripe.production.isenable").equalsIgnoreCase("true")) {
			stripePublishableKey = prop.getProperty("violation.stripe.prodpublishablekey");
		} else {
			stripePublishableKey = prop.getProperty("violation.stripe.testpublishablekey");
		}
		model.addAttribute("paymentForm", paymentForm);
		model.addAttribute("totalDue", totalDue);
		model.addAttribute("violationId", violationId);
		model.addAttribute("stripePublishableKey", stripePublishableKey);
		return "consumerPayment";
	}

	private void saveConsumerPayment(Violation consumer, String user, BigDecimal amount, Transaction tx,
			String paymentSource) {
		BigDecimal totalDue, amountPaid, overPaid;
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod = paymentMethodService.findByType(10);
		amountPaid = amount;
		overPaid = new BigDecimal(0.00);
		totalDue = new BigDecimal(0.00);
		BigDecimal total = new BigDecimal(0.00);
		boolean fullPayFlag = false;
				Payment payment = new Payment();
				total = consumer.getTotalDue();
				if (amountPaid.signum() == 1 && total.compareTo(new BigDecimal(0.00)) == 1) {
					amountPaid = amountPaid.subtract(total);
					if (amountPaid.signum() == 1) {
						totalDue = new BigDecimal(0.00);
						amount = total;
					} else {
						totalDue = total.subtract(total.add(amountPaid));
						amount = total.subtract(totalDue);
					}
					/*
					 * if (isless.signum() != -1) { totalDue=isless;
					 * payment.setTotalDue(totalDue);
					 * payment.setOverPaid(overPaid); } else{
					 * consumer.setStatus(Status.OVERPAID);
					 * overPaid=amountPaid.subtract(total);
					 * payment.setTotalDue(totalDue);
					 * payment.setOverPaid(overPaid); }
					 */
					payment.setAccount("PAY-BY-WEB");
					payment.setAmount(amount);
					payment.setTotalDue(totalDue);
					payment.setOverPaid(overPaid);
					payment.setViolation(consumer);
					Ipp ipp = ippService.findByViolationId(consumer.getViolationId());
					if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
							&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
						payment.setIpp(true);
					} else {
						payment.setIpp(false);
					}
					payment.setPaymentDate(CSUMDateUtils.getDateInString(new Date()));
					payment.setPaymentMethod(paymentMethod);
					payment.setMethodId(paymentMethod.getTypCd());
					payment.setProcessedBy("CONSUMER");
					payment.setUser("CONSUMER");
					payment.setProcessedOn(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
					payment.setTransaction(tx);
					payment.setPaymentSource(paymentSource);
					if (totalDue.compareTo(new BigDecimal("0.00")) == 0
							&& overPaid.compareTo(new BigDecimal("0.00")) == 0) {
						consumer.setStatus("PAID");
						fullPayFlag = true;
					}
					if (ipp != null && !ipp.getStatus().equalsIgnoreCase("Canceled")
							&& !ipp.getStatus().equalsIgnoreCase("FULFILLED")) {
						payment.setViolation(consumer);
						payment.setInstallmentPaymentPlan(ipp);
						List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
						BigDecimal downPayAmt = new BigDecimal(0.00);
						BigDecimal installmentAmt = new BigDecimal(0.00);
						if (ippPayments != null && ippPayments.size() > 0) {
							for (Payment downPayment : ippPayments) {
								if (downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
									downPayAmt = downPayAmt.add(downPayment.getAmount());
								} else if (!downPayment.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
									installmentAmt = installmentAmt.add(downPayment.getAmount());
								}
							}
						}
						BigDecimal downAndInstPay = ipp.getDownPayment().add(ipp.getInstallmentAmount());
						if (!(installmentAmt.add(amount)).equals(new BigDecimal(0.00))
                                && downAndInstPay.compareTo(installmentAmt.add(ipp.getDownPayment()).add(amount)) != 1) {
							/*int daysElapsed = consumer.getElapsedDays();
							LocalDate prevSusProcDate=CSUMDateUtils.convert(consumer.getSuspendProcessDate());
							if(prevSusProcDate.isBefore(LocalDate.now(ZoneId.of(EcitationConstants.ECIT_PST_ZONE_ID)))){
								daysElapsed += (int)  ChronoUnit.DAYS.between(prevSusProcDate,LocalDate.now(ZoneId.of(EcitationConstants.ECIT_PST_ZONE_ID)));
							}
							consumer.setElapsedDays(daysElapsed);*/
							LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(ipp.getStartDate()).plusDays(81);
							LocalDate processDate1 = CSUMDateUtils.getFormattedLocalDate(consumer.getSuspendProcessDate());
							String stringLocalDate = CSUMDateUtils.getFormattedLocalDate(localDate);
							if (!localDate.equals(processDate1)) {
								consumer.setSuspendProcessDate(stringLocalDate);
							}
						}
						Violation cit = violationService.findByViolationId(consumer.getViolationId());
						if (ippPayments != null && downPayAmt.compareTo(ipp.getDownPayment()) != -1) {
							if (amount.compareTo(cit.getTotalDue()) != 0) {
								int num = 0;
								BigDecimal amt = downPayAmt.subtract(ipp.getDownPayment());
								if (!installmentAmt.equals(new BigDecimal(0.00))) {
									double number = Math.ceil(((installmentAmt.add(amt))
											.divide(ipp.getInstallmentAmount(), new MathContext(4))).doubleValue());
									num = (int) number;
								}
								BigDecimal remainAmt = new BigDecimal(0.00);
								Payment ippPay = paymentDetailsService.findLatestByViolationId(consumer.getViolationId());
								if (ippPay != null && ippPay.getIppType() != null) {
									if (ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
										remainAmt = downPayAmt.subtract(ipp.getDownPayment());
									} else if (!ippPay.getIppType().equalsIgnoreCase("DOWNPAYMENT")) {
										remainAmt = (downPayAmt.subtract(ipp.getDownPayment())).add(installmentAmt);
									}
								}
								if ((remainAmt.remainder(ipp.getInstallmentAmount()))
										.compareTo(new BigDecimal(0.00)) == 0) {
									int i = num + 1;
									payment.setIppType("Installment : " + i);
								} else {
									payment.setIppType("Installment : " + num);
								}
							} else {
								payment.setIppType("FullPayment");
								ipp.setStatus("FULFILLED");
							}
						} else if (fullPayFlag) {
							payment.setIppType("FullPayment");
							ipp.setStatus("FULFILLED");
						} else {
							payment.setIppType("DownPayment");
							if ((downPayAmt.add(amount)).compareTo(ipp.getDownPayment()) == -1) {
								ipp.setStatus("PENDING");
							} else {
								ipp.setStatus("ACTIVE");
								String startDate = ipp.getStartDate();
								LocalDate localDate = CSUMDateUtils.getFormattedLocalDate(startDate).plusDays(51);
								consumer.setSuspendProcessDate(CSUMDateUtils.getFormattedLocalDate(localDate)); 
							}
						}
						ipp.setUser("Consumer");
						ippService.updateIpp(ipp);
					}
					consumer.setTotalDue(totalDue);
					consumer.setUser("Consumer");
					violationService.updateViolation(consumer);
					paymentDetailsService.save(payment);
		}
		/*BigDecimal amountPaid = amount;
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod = paymentMethodService.findByType(10);
		Payment payment = new Payment();
		BigDecimal totalDue;
		BigDecimal overPaid = new BigDecimal(0.00);
		try{
			if (amountPaid.signum() == 1) {
				totalDue = consumer.getTotalDue();
				amount = totalDue.subtract(amountPaid);
				if (amount.signum() == 1) {
					totalDue = amount;
				} else if (amount.signum() == -1) {
					overPaid = amountPaid.subtract(totalDue);
					totalDue = new BigDecimal(0.00);
				} else {
					totalDue = new BigDecimal(0.00);
				}
				payment.setAmount(amountPaid);
				consumer.setTotalDue(totalDue);
				payment.setOverPaid(overPaid);
				payment.setTotalDue(totalDue);
				payment.setAccount("PAY-BY-WEB");
				payment.setPaymentDate(CSUMDateUtils.getDateInString(new Date()));
				payment.setPaymentMethod(paymentMethod);
				payment.setMethodId(paymentMethod.getTypCd());
				payment.setUser(user);
				payment.setProcessedBy(user);
				payment.setProcessedOn(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
				payment.setTransaction(tx);
				payment.setPaymentSource(paymentSource);
				if (totalDue.compareTo(new BigDecimal("0.00")) == 0 && overPaid.compareTo(new BigDecimal("0.00")) == 0
						&& consumer.getViolationCode() != null && !consumer.getStatus().equalsIgnoreCase("PAID")) {
					consumer.setStatus("PAID");
				} else if (totalDue.compareTo(new BigDecimal("0.00")) == 0
						&& overPaid.compareTo(new BigDecimal("0.00")) == 1 && consumer.getViolationCode() != null) {
					consumer.setStatus("OVERPAID");
				}
				consumer.setTotalDue(totalDue);
				consumer.setUser(user);
				violationService.updateViolation(consumer);
				payment.setViolation(consumer);
				paymentDetailsService.save(payment);
			}			
		}catch(Exception e){
			logger.error(e,e);
		}*/
	}

}
