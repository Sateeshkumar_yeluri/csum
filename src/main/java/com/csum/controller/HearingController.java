package com.csum.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.csum.model.Admin;
import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.DispositionCode;
import com.csum.model.Hearing;
import com.csum.model.HearingDecision;
import com.csum.model.HearingDetail;
import com.csum.model.HearingScheduler;
import com.csum.model.Holidays;
import com.csum.model.Ipp;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.PaymentMethod;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;
import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.model.ViolationSearch;
import com.csum.service.AdminService;
import com.csum.service.DispositionCodeService;
import com.csum.service.HearingDecisionService;
import com.csum.service.HearingDetailsService;
import com.csum.service.HearingSchedulerService;
import com.csum.service.HearingService;
import com.csum.service.HistoryService;
import com.csum.service.HolidaysService;
import com.csum.service.IppService;
import com.csum.service.PaymentDetailsService;
import com.csum.service.PaymentMethodService;
import com.csum.service.PenaltyService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMConstants;
import com.csum.utils.CSUMDateUtils;
import com.csum.view.model.HearingHistoryView;
import com.csum.view.model.HistoryBean;
import com.csum.view.model.LoginBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;

@Controller
public class HearingController {

	public static final Logger logger = LogManager.getLogger(HearingController.class);

	@Autowired
	private ViolationService violationService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private HearingService hearingService;

	@Autowired
	private HearingSchedulerService hearingSchedulerService;

	@Autowired
	private HolidaysService holidaysService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private PaymentDetailsService paymentDetailsService;

	@Autowired
	private PenaltyService penaltyService;

	@Autowired
	private ViolationImagesService violationImagesService;

	@Autowired
	private HearingDetailsService hearingDetailsService;

	@Autowired
	private HearingDecisionService hearingDecisionService;

	@Autowired
	private DispositionCodeService dispositionCodeService;

	@Autowired
	private PaymentMethodService paymentMethodService;

	@Autowired
	private IppService ippService;

	@GetMapping(value = { "/hearingDetails/{violationId}" })
	public String getHearingDetails(@PathVariable String violationId, ModelMap model) {
		String licenseNumber = "";
		Hearing hearing;
		try {
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null && violation.getPlateEntity() != null
					&& violation.getPlateEntity().getLicenceNumber() != null) {
				licenseNumber = violation.getPlateEntity().getLicenceNumber();
			}
			hearing = hearingService.findByViolationId(violationId);
			model.addAttribute("hearing", hearing);
			model.addAttribute("licenseNumber", licenseNumber);
			model.addAttribute("violationId", violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "hearingDetails";
	}

	@GetMapping(value = { "/hearingEntry/{violationId}" })
	public String hearingEntry(@PathVariable String violationId, ModelMap model) {
		Hearing hearing = new Hearing();
		List<HearingScheduler> hearingSchedulers;
		List<HearingScheduler> availableSchedules = new ArrayList<>();
		Violation violation;
		ArrayList<String> holidays = new ArrayList<>();
		List<Holidays> holidaysList = new ArrayList<>();
		try {
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			violation = violationService.findByViolationId(violationId);
			hearing = hearingService.findByViolationId(violationId);
			hearingSchedulers = hearingSchedulerService.findAll();
			holidaysList = holidaysService.findAll();
			if (holidaysList != null && holidaysList.size() > 0) {
				for (Holidays holiday : holidaysList) {
					if (holiday != null && holiday.getHolidayDate() != null
							&& holiday.getHolidayDate().trim().length() > 0) {
						holidays.add("'" + holiday.getHolidayDate() + "'");
					}
				}
			}
			logger.debug("holidays >>> " + holidays);
			if (hearing == null) {
				availableSchedules = getAvailableSchedulers(
						CSUMDateUtils.getFormattedLocalDate(
								new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate()),
						model);
				hearing = new Hearing();
			} else {
				availableSchedules = getAvailableSchedulers(
						CSUMDateUtils.getFormattedLocalDate(hearing.getHearingDate()), model);
				Date hearingTime = hearing.getHearingTime();
				if (hearingTime != null) {
					if (hearingSchedulers != null && hearingSchedulers.size() > 0) {
						for (HearingScheduler hearingScheduler : hearingSchedulers) {
							if (hearingScheduler != null && hearingScheduler.getDescription() != null) {
								String times[] = hearingScheduler.getDescription().split("-");
								if (times != null && times.length > 0) {
									SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
									SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm:ss");
									Calendar cal1 = Calendar.getInstance(), cal2 = Calendar.getInstance(),
											cal3 = Calendar.getInstance();
									cal1.setTime(date24Format.parse(date24Format.format(date12Format.parse(times[0]))));
									cal2.add(Calendar.DATE, 1);
									cal2.setTime(date24Format.parse(date24Format.format(date12Format.parse(times[1]))));
									cal2.add(Calendar.DATE, 1);
									cal3.setTime(hearing.getHearingTime());
									cal3.add(Calendar.DATE, 1);
									Date time = cal3.getTime();
									if (time.after(cal1.getTime()) && time.before(cal2.getTime())) {
										hearing.setHearingTime(
												date24Format.parse(date24Format.format(date12Format.parse(times[0]))));
										// System.out.println(hearing.getHearingTime());
										// System.out.println(hearingScheduler.getValue());
										break;
									}
								}
							}
						}
					}
				}
			}
			if (violation != null) {
				hearing.setViolation(violation);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		if (!model.containsAttribute("hearingDetailsForm")) {
			model.addAttribute("hearingDetailsForm", hearing);
		}
		model.addAttribute("holidays", holidays);
		model.addAttribute("hearingSchedulers", availableSchedules);
		return "hearingEntry";
	}

	private List<HearingScheduler> getAvailableSchedulers(String hearingDate, ModelMap model) {
		List<HearingScheduler> hearingSchedulers = hearingSchedulerService.findAll();
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			if (hearingDate != null && hearingDate.trim().length() > 0) {
				LocalDate date = LocalDate.parse(hearingDate, formatter);
				List<Hearing> hearings = hearingService.findLatestByDate(date);
				List<HearingScheduler> schedulers = new ArrayList<>();
				if ((hearings != null && hearings.size() > 0)
						|| (hearingSchedulers != null && hearingSchedulers.size() > 0)) {
					if (hearings != null && hearings.size() > 0) {
						for (Hearing hearing : hearings) {
							for (HearingScheduler hearingScheduler : hearingSchedulers) {
								if (hearing.getHearingTime() != null
										&& hearing.getHearingTime().toString().contains(hearingScheduler.getValue())) {
									if (!schedulers.contains(hearingScheduler)) {
										schedulers.add(hearingScheduler);
									}
								}
							}
						}
					}
					if (hearingSchedulers != null && hearingSchedulers.size() > 0) {
						for (HearingScheduler hearingScheduler : hearingSchedulers) {
							if (hearingScheduler != null && !schedulers.contains(hearingScheduler)) {
								String times[] = hearingScheduler.getDescription().split("-");
								if (times != null && times.length > 0) {
									SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
									SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
									SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm:ss");
									Calendar cal1 = Calendar.getInstance(), cal2 = Calendar.getInstance();
									cal1.setTime(date24Format.parse(date24Format.format(date12Format.parse(times[0]))));
									cal2.setTime(dateFormat.parse(hearingDate));
									cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
									cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
									cal2.set(Calendar.SECOND, cal1.get(Calendar.SECOND));
									// System.out.println(new Date() + "" +
									// cal2.getTime());
									if (new Date().after(cal2.getTime())) {
										schedulers.add(hearingScheduler);
									}
								}
							}
						}
					}
					if (hearingSchedulers != null && hearingSchedulers.size() > 0) {
						hearingSchedulers.removeAll(schedulers);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return hearingSchedulers;
	}

	@GetMapping(value = { "/getSchedulers" })
	@ResponseBody
	private String getSchedulers(@RequestParam String hearingDate, ModelMap model) {
		try {
			List<HearingScheduler> availableSchedules = new ArrayList<>();
			availableSchedules = getAvailableSchedulers(hearingDate, model);
			Gson gson = new Gson();
			String jsonObject = gson.toJson(availableSchedules);
			return jsonObject;
		} catch (Exception e) {
			logger.error(e, e);
		}
		return null;
	}

	@PostMapping(value = { "/hearingDetails/add/{violationId}" })
	public String saveHearing(@PathVariable String violationId, Model model,
			@ModelAttribute("hearingDetailsForm") Hearing hearingForm, BindingResult bindingResult, HttpSession session,
			RedirectAttributes redirectAttributes, ModelMap modelmap) throws IOException {
		boolean isAvail = true;
		Admin user = (Admin) session.getAttribute("admin");
		Violation violation = violationService.findByViolationId(violationId);
		Properties prop = new Properties();
		InputStream inputStream = ViolationController.class.getClassLoader()
				.getResourceAsStream("application.properties");
		prop.load(inputStream);
		try {
			if (user != null) {
				LocalDateTime localTime = LocalDateTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID));
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				hearingForm.setScheduledAt(format.parse(CSUMDateUtils.getFormattedLocalDateTime(localTime)));
				hearingForm.setScheduledBy(user.getUserName());
				List<Hearing> hearings = hearingService.findLatestByDate(hearingForm.getHearingDate());
				if (hearings != null && hearings.size() > 0) {
					for (Hearing hearing : hearings) {
						if (hearing.getHearingTime() != null
								&& hearing.getHearingTime().equals(hearingForm.getHearingTime())
								&& LocalTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))
										.isAfter(hearing.getHearingTime().toInstant()
												.atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime())
								&& LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))
										.isAfter(hearing.getHearingDate())) {
							isAvail = false;
						}
					}
				}
				hearingForm.setViolation(violation);
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("loginBean", new LoginBean());
				return "redirect:/login";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		if (isAvail) {
			hearingForm.setStatus("PENDING");
			if (hearingForm.getId() != null) {
				Hearing hearing = hearingService.findByViolationId(violationId);
				hearing.setUser(user.getUserName());
				hearing.setStatus("Cancelled");
				hearingService.updateHearing(hearing);
				hearingForm.setRescheduled(true);
				hearingForm.setUser(user.getUserName());
				hearingForm.setTotalDue(violation.getTotalDue());
				hearingService.updateHearing(hearingForm);
			} else {
				hearingForm.setRescheduled(false);
				hearingForm.setUser(user.getUserName());
				hearingForm.setTotalDue(violation.getTotalDue());
				hearingForm.setViolation(violation);
				hearingService.save(hearingForm);
			}
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
			return "redirect:../" + hearingForm.getViolation().getViolationId();
		} else {
			String hearingSchedulersInString = getSchedulers(
					CSUMDateUtils.getFormattedLocalDate(hearingForm.getHearingDate()), modelmap);
			if (hearingSchedulersInString != null && hearingSchedulersInString.trim().length() > 0) {
				ObjectMapper mapper = new ObjectMapper();
				List<HearingScheduler> participantJsonList = mapper.readValue(hearingSchedulersInString,
						new TypeReference<List<HearingScheduler>>() {
						});
				model.addAttribute("hearingSchedulers", participantJsonList);
				model.addAttribute("errorInfoEx", "Already hearing slot is booked in selected Timings.");
			} else {
				model.addAttribute("errorInfoEx",
						"No hearing slots are available for selected date please select another date.");
			}
			model.addAttribute("hearingDetailsForm", hearingForm);
			return "hearingEntry";
		}

	}

	public List<Hearing> setHearingAllDetails(List<Hearing> hearingList) {
		// logger.debug("SetHearingAllDetails:: " + hearingList);
		for (Hearing hearing : hearingList) {
			/*
			 * HearingDetail hearingDetails =
			 * hearingDetailsAllService.findByHearing(hearing.getId()); if
			 * (hearingDetails != null) {
			 * hearing.setHearingdetail(hearingDetails); }
			 */
			if (hearing.getHearingOfficer() != null && hearing.getHearingOfficer() != "") {
				Admin user = adminService.findByUserName(hearing.getHearingOfficer());
				if (user != null) {
					String name = user.getFirstName() + " " + user.getLastName();
					hearing.setHearingOfficerName(name);
				}
			}
		}
		return hearingList;
	}

	@GetMapping(value = { "/adjudicatorDashboardSupervisor/{hearingRequired}/{checkbox}" })
	public String adjudicatorDashboardSupervisor(@PathVariable String hearingRequired, @PathVariable String checkbox,
			ModelMap model, RedirectAttributes redirectAttributes,
			@ModelAttribute("hearingDetailsForm") Hearing hearingForm, HttpSession httpSession) {
		// logger.debug("AdjudicatorDashboardSupervisor:: " + hearingForm);
		model.addAttribute("hearingRequired", hearingRequired);
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			if (user != null) {
				if (hearingRequired != null && hearingRequired.equalsIgnoreCase("Search")) {
					if (hearingForm.getSearchType() != null && hearingForm.getSearchValue() != null) {
						String searchType = hearingForm.getSearchType(), searchValue = hearingForm.getSearchValue();
						ViolationSearch searchForm = new ViolationSearch();
						searchForm.setTicketNumber(searchValue);
						searchForm.setSearchType(searchType);
						List<Violation> violationList = null;
						violationList = violationService.findBySearchCriteria(searchForm);
						/*
						 * if (violationList != null && violationList.size() >
						 * 0) { for (Violation violation : violationList) { if
						 * (violation != null && violation.getViolationId() !=
						 * null) { Hearing hearing =
						 * hearingService.findByViolationId(violation.
						 * getViolationId()); if (hearing != null) {
						 * violation.getHearings().add(hearing); } } } }
						 */
						model.addAttribute("violationList", violationList);
						model.addAttribute("activeOnSearch", "active");
					}
				} else {
					List<Hearing> hearingList = null;
					LocalDate date = new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))
							.toLocalDate();
					String presentTime = CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime());
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
					boolean flag;
					String presentDate = "";
					LocalDate date1 = null, date2 = null;
					model.addAttribute("checkbox", checkbox);
					if (hearingForm != null) {
						if (hearingForm.getRequiredDate() != null && hearingForm.getRequiredDate() != "") {
							if (hearingForm.getHearingBasis() != null
									&& hearingForm.getHearingBasis().equalsIgnoreCase("Monthly")) {
								flag = true;
								Calendar cal = Calendar.getInstance();
								String requiredDate[] = hearingForm.getRequiredDate().split(",");
								if (requiredDate.length > 1) {
									Date date3 = new SimpleDateFormat("MMM", Locale.ENGLISH)
											.parse(requiredDate[0].trim());
									cal.setTime(date3);
									int year = Integer.parseInt(requiredDate[1].trim());
									cal.set(Calendar.YEAR, year);
									cal.set(Calendar.DATE, 1);
									date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									int lastDate = cal.getActualMaximum(Calendar.DATE);
									cal.add(Calendar.DATE, lastDate - 1);
									date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									presentDate = requiredDate[0].trim() + ", " + requiredDate[1].trim();

								} else {
									SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
									cal.setTime(sdf.parse(requiredDate[0].trim()));
									int number = 0;
									number = -cal.get(Calendar.DATE) + 1;
									cal.add(Calendar.DATE, number);
									date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									int lastDate = cal.getActualMaximum(Calendar.DATE);
									cal.add(Calendar.DATE, lastDate - 1);
									date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									int num = cal.get(Calendar.MONTH);
									DateFormatSymbols dfs = new DateFormatSymbols();
									String[] months = dfs.getMonths();
									presentDate = months[num] + ", " + cal.get(Calendar.YEAR);
								}

							} else if (hearingForm.getHearingBasis() != null
									&& hearingForm.getHearingBasis().equalsIgnoreCase("Annually")) {
								flag = true;
								Calendar cal = Calendar.getInstance();
								String requiredDate[] = hearingForm.getRequiredDate().split("-");
								if (requiredDate.length > 1) {
									date1 = LocalDate.parse(requiredDate[0].trim(), formatter);
									date2 = LocalDate.parse(requiredDate[1].trim(), formatter);
									presentDate = CSUMDateUtils.getFormattedLocalDate(date1) + "-"
											+ CSUMDateUtils.getFormattedLocalDate(date2);
								} else if (requiredDate.length > 0) {
									SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
									cal.setTime(sdf.parse(requiredDate[0].trim()));
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(sdf.parse(CSUMDateUtils.getFormattedLocalDate(date)));
									if (cal.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)) {
										cal.set(Calendar.DATE, 31);
										cal.set(Calendar.MONTH, 11);
									} else {
										cal.setTime(sdf.parse(CSUMDateUtils.getFormattedLocalDate(date)));
									}
									date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									cal.set(Calendar.DATE, 1);
									cal.set(Calendar.MONTH, 0);
									date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									presentDate = CSUMDateUtils.getFormattedLocalDate(date1) + "-"
											+ CSUMDateUtils.getFormattedLocalDate(date2);
									;
								}
							} else if (hearingForm.getHearingBasis() != null
									&& hearingForm.getHearingBasis().equalsIgnoreCase("Weekly")) {
								flag = true;
								String requiredDate[] = hearingForm.getRequiredDate().split("-");
								if (requiredDate.length > 1) {
									date1 = LocalDate.parse(requiredDate[0].trim(), formatter);
									date2 = LocalDate.parse(requiredDate[1].trim(), formatter);
									presentDate = CSUMDateUtils.getFormattedLocalDate(date1) + "-"
											+ CSUMDateUtils.getFormattedLocalDate(date2);
								} else {
									SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
									Calendar cal = Calendar.getInstance();
									cal.setTime(sdf.parse(requiredDate[0].trim()));
									int number = 0;
									number = -cal.get(Calendar.DAY_OF_WEEK) + 1;
									cal.add(Calendar.DATE, number);
									date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									cal.add(Calendar.DATE, 6);
									date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									presentDate = CSUMDateUtils.getFormattedLocalDate(date1) + "-"
											+ CSUMDateUtils.getFormattedLocalDate(date2);
								}
							} else {
								flag = false;
								date = LocalDate.parse(hearingForm.getRequiredDate(), formatter);
								presentDate = CSUMDateUtils.getFormattedLocalDate(date);
							}
						} else {
							flag = false;
							presentDate = CSUMDateUtils.getFormattedLocalDate(date);
						}

					} else {
						flag = false;
						presentDate = CSUMDateUtils.getFormattedLocalDate(date);
					}
					model.addAttribute("presentDate", presentDate);
					hearingForm.setRequiredDate(presentDate);
					model.addAttribute("presentTime", presentTime);
					if (hearingRequired.equalsIgnoreCase("Pending")) {
						if (flag == false) {
							switch (checkbox) {
							case "All":
								hearingList = hearingService.findAllPendingHearingsList(date);
								break;
							case "InPerson":
								hearingList = hearingService.findAllPendingHearingsList(date, "PERSON");
								break;
							case "WalkIn":
								hearingList = hearingService.findAllPendingHearingsList(date, "WALKIN");
								break;
							case "Written":
								hearingList = hearingService.findAllWrittenHearingsList();
								break;
							}
						} else {
							switch (checkbox) {
							case "All":
								hearingList = hearingService.findAllPendingHearingsList(date1, date2);
								break;
							case "InPerson":
								hearingList = hearingService.findAllPendingHearingsList(date1, date2, "PERSON");
								break;
							case "WalkIn":
								hearingList = hearingService.findAllPendingHearingsList(date1, date2, "WALKIN");
								break;
							case "Written":
								hearingList = hearingService.findAllWrittenHearingsList();
								break;
							}
						}
						model.addAttribute("activeOnPending", "active");
					} else {
						if (flag == false) {
							switch (checkbox) {
							case "All":
								hearingList = hearingService.findAllCompletedHearingsList(date);
								break;
							case "InProgress":
								hearingList = hearingService.findAllCompletedHearingsList("Progress", "Rework", date);
								break;
							case "PendingReview":
								hearingList = hearingService.findAllCompletedHearingsList("PENDINGREVIEW",
										"PENDINGREVIEWUPDATED", date);
								break;
							case "PendingMail":
								hearingList = hearingService.findAllCompletedHearingsList("PendingMail", date);
								break;
							case "FTA":
								hearingList = hearingService.findAllCompletedHearingsList("Failed to Appear", date);
								break;
							case "Complete":
								hearingList = hearingService.findAllCompletedHearingsList("Complete", date);
								break;
							}
						} else {
							switch (checkbox) {
							case "All":
								hearingList = hearingService.findAllCompletedHearingsList(date1, date2);
								break;
							case "InProgress":
								hearingList = hearingService.findAllCompletedHearingsList("Progress", "Rework", date1,
										date2);
								break;
							case "PendingReview":
								hearingList = hearingService.findAllCompletedHearingsList("PendingReview",
										"PENDINGREVIEWUPDATED", date1, date2);
								break;
							case "PendingMail":
								hearingList = hearingService.findAllCompletedHearingsList("PendingMail", date1, date2);
								break;
							case "FTA":
								hearingList = hearingService.findAllCompletedHearingsList("Failed to Appear", date1,
										date2);
								break;
							case "Complete":
								hearingList = hearingService.findAllCompletedHearingsList("Complete", date1, date2);
								break;
							}
						}
						model.addAttribute("activeOnComplete", "active");
					}
					if (hearingList != null && hearingList.size() > 0) {
						model.addAttribute("hearingList", setHearingAllDetails(hearingList));
					}
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
			List<Admin> userList = adminService.findAllUsers();
			model.addAttribute("userList", userList);
			model.addAttribute("hearingForm", hearingForm);
		} catch (Exception e) {
			logger.error(e, e);
			e.printStackTrace();
		}
		return "adjudicatorDashboardSupervisor";
	}

	@PostMapping(value = { "/assignHearing/{hearingId}/{user}" })
	public String assignHearing(@PathVariable Long hearingId, @PathVariable String user, HttpSession httpSession,
			ModelMap model, RedirectAttributes redirectAttributes) {
		try {
			Admin userSession = (Admin) httpSession.getAttribute("admin");
			if (userSession != null) {
				Hearing hearing = hearingService.findById(hearingId);
				if (hearing != null) {
					if ((hearing.getHearingOfficer() == null || hearing.getHearingOfficer() == "")
							|| !user.equalsIgnoreCase("Unassigned")) {
						hearing.setHearingOfficer(user);
					} else if (user.equalsIgnoreCase("Unassigned") && hearing.getHearingOfficer() != null
							&& hearing.getHearingOfficer() != "") {
						// hearingDetailsAllService.deleteByHearingId(hearing.getId());
						hearing.setHearingOfficer("");
						hearing.setStatus("PENDING");
					}
					hearing.setUser(userSession.getUserName());
					hearingService.updateHearing(hearing);
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:/adjudicatorDashboardSupervisor/Pending/All";
	}

	@PostMapping(value = { "/cancelHearing/{hearingId}/{violationId}" })
	private String cancelHearing(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String violationId, @PathVariable Long hearingId, Model model, HttpSession session,
			RedirectAttributes redirectAttributes) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			if (user != null) {
				Hearing hearing = hearingService.findById(hearingId);
				if (hearing != null) {
					hearingService.delete(hearingId);
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:/adjudicatorDashboardSupervisor/Pending/All";
	}

	@GetMapping(value = { "/hearingsEdit/{violationId}/{hearingId}" })
	public String editHearingDetails(@PathVariable String violationId, @PathVariable Long hearingId, ModelMap model,
			HttpSession httpSession, RedirectAttributes redirectAttributes,
			@ModelAttribute("hearingDetail") HearingDetail letterHearingDetail,
			@RequestParam(required = false, name = "noteErrorMsg") String noteErrorMsg) {
		HistoryBean historyBean = null;
		Violation violation;
		List<ViolationImages> images = null;
		List<ViolationImages> checks = null;
		List<ViolationImages> corresp = null;
		try {
			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			Admin user = (Admin) httpSession.getAttribute("admin");
			if (user != null) {
				violation = violationService.findByViolationId(violationId);
				if (violation != null) {
					LocalDate date = new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID))
							.toLocalDate();
					historyBean = historyService.getRequiredHistory("All", violationId);
					model.addAttribute("historyBean", historyBean);
					if (violation.getPatron() != null) {
						if (violation.getPatron().getLastName() != null
								&& violation.getPatron().getFirstName() != null) {
							model.addAttribute("lastName",
									violation.getPatron().getFirstName() + " " + violation.getPatron().getLastName());
						} else if (violation.getPatron().getLastName() != null) {
							model.addAttribute("lastName", violation.getPatron().getLastName());
						} else if (violation.getPatron().getFirstName() != null) {
							model.addAttribute("lastName", violation.getPatron().getFirstName());
						}
						if (violation.getPatron().getAddress() != null) {
							String address1 = null;
							if (violation.getPatron().getAddress().getAddress() != null) {
								address1 = violation.getPatron().getAddress().getAddress();
							}
							String address2 = null;
							if (violation.getPatron().getAddress().getCity() != null) {
								address2 = violation.getPatron().getAddress().getCity();
							}
							if (violation.getPatron().getAddress().getState() != null) {
								if (address2 != null) {
									address2 = address2 + " " + violation.getPatron().getAddress().getState();
								} else {
									address2 = violation.getPatron().getAddress().getState();
								}
							}
							if (violation.getPatron().getAddress().getZip() != null) {
								if (address2 != null) {
									address2 = address2 + " " + violation.getPatron().getAddress().getZip();
								} else {
									address2 = violation.getPatron().getAddress().getZip();
								}
							}
							model.addAttribute("address1", address1);
							model.addAttribute("address2", address2);
						}
					}
					images = violationImagesService.findByType(violationId, "Attachments");
					checks = violationImagesService.findByType(violationId, "Checks");
					corresp = violationImagesService.findByType(violationId, "Correspondence");
					List<DispositionCode> dispCodes = dispositionCodeService.findAll();
					DateTimeFormatter formatters = DateTimeFormatter.ofPattern("MMM d, uuuu");
					String presentDate = date.format(formatters);
					String presentTime = CSUMDateUtils.getFormattedLocalTime(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalTime());
					model.addAttribute("presentDate", presentDate);
					model.addAttribute("presentTime", presentTime);
					List<Admin> userList = adminService.findAllUsers();
					model.addAttribute("userList", userList);
					model.addAttribute("dispCodes", dispCodes);
					model.addAttribute("images", images);
					model.addAttribute("checks", checks);
					model.addAttribute("corresp", corresp);
					model.addAttribute("violation", violation);
					model.addAttribute("violationId", violationId);
					if (hearingId == 0) {
						Hearing hearing = hearingService.findByViolationId(violationId);
						if (hearing != null) {
							hearingId = hearing.getId();
						}
					}
					if (hearingId != null && hearingId > 0) {
						HearingDetail hearingDetail = hearingDetailsService.findByHearing(hearingId);
						// List<HearingDetail> hearingDetails =
						// hearingDetailsService.findByHearingAll(hearingId);
						Hearing hearing = hearingService.findByViolationId(violationId);
						if (hearingDetail == null) {
							hearingDetail = new HearingDetail();
							if (hearing != null) {
								hearingDetail.setHearing(hearing);
							}
						}
						if (letterHearingDetail != null && letterHearingDetail.getDecision() != null) {
							hearing.setTotalDue(letterHearingDetail.getHearing().getTotalDue());
							hearing.setReduction(letterHearingDetail.getHearing().getReduction());
							hearing.setNoFineChange(letterHearingDetail.getHearing().isNoFineChange());
							HearingDecision decision = hearingDecisionService
									.findById(letterHearingDetail.getDecision().getId());
							letterHearingDetail.setDecision(decision);
							letterHearingDetail.setHearing(hearing);
							letterHearingDetail.setStatus(hearing.getStatus());
							if (noteErrorMsg != null) {
								model.addAttribute("noteErrorMsg", noteErrorMsg);
							}
							model.addAttribute("hearingDetailsForm", letterHearingDetail);

						} else {
							if (hearing != null && (hearing.getReduction() == null || hearing.getTotalDue() == null)) {
								hearing.setTotalDue(violation.getTotalDue());
								hearing.setReduction(BigDecimal.ZERO);
							}
							hearingDetail.setHearing(hearing);
							model.addAttribute("hearingDetailsForm", hearingDetail);
						}
						/*
						 * FormLetter formLetter =
						 * formLetterService.findByHearingId(hearingId); if
						 * (formLetter != null) { List<ViolationImages>
						 * violationImages =
						 * violationImagesService.findByName(violationId,
						 * formLetter.getLetterDescription() + "_" +
						 * formLetter.getViolationNumber()); if (violationImages
						 * != null && violationImages.size() > 0) { if
						 * (violationImages.get(0) != null &&
						 * violationImages.get(0).getId() != null) {
						 * model.addAttribute("viewImageId",
						 * violationImages.get(0).getId());
						 * model.addAttribute("isLetterEdited", "Y"); } else {
						 * model.addAttribute("isLetterEdited", "N"); } } }
						 */
						List<Hearing> hearingList = new ArrayList<>();
						List<Hearing> hearings = new ArrayList<>();
						Date time = CSUMDateUtils.getFormattedDate(date.format(formatter2));
						hearingList = hearingService.findAllPendingHearingsList(date);
						if (hearing != null && hearing.getType() != null
								&& !hearing.getType().equalsIgnoreCase("WRITTEN") && hearingList != null
								&& hearingList.size() > 0) {
							for (Hearing hearingdetails : hearingList) {
								if (hearingdetails != null && hearingdetails.getHearingTime() != null && time != null
										&& time.after(hearing.getHearingTime()) && hearings.size() < 2) {
									hearings.add(hearingdetails);
								}
							}
						}
						model.addAttribute("hearingList", hearings);
						List<HearingDecision> decisions = hearingDecisionService.findAll();
						model.addAttribute("decisions", decisions);
					}
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		BigDecimal totalPaid = new BigDecimal(0.00);
		List<Payment> paymentsList = paymentDetailsService.findByViolationId(violationId);
		if (paymentsList.size() > 0 && paymentsList != null) {
			for (Payment p : paymentsList) {
				if (p.getAmount() != null) {
					totalPaid = totalPaid.add(p.getAmount());
				}

			}
		}
		model.addAttribute("totalPaid", totalPaid);
		Penalty penaltyDetails = penaltyService.findByViolationId(violationId);
		model.addAttribute("penaltyDetails", penaltyDetails);
		// Payment paymentDetails =
		// paymentDetailsService.findLatestById(violationId);
		// logger.debug("Payment Details Latest :" + paymentDetails);
		// model.addAttribute("paymentDetails", paymentDetails);
		if (hearingId != null && hearingId > 0) {
			return "activeCase";
		} else {
			return "appealCase";
		}
	}

	@PostMapping(value = { "/updateHearingStatus/{hearingId}" })
	private String updateHearingStatus(@PathVariable String hearingId, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			if (user != null) {
				Hearing hearing = hearingService.findById(Long.parseLong(hearingId));
				if (hearing != null) {
					hearing.setStatus("PROGRESS");
					hearing.setHearingOfficer(user.getUserName());
					hearing.setHearingDate(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(CSUMConstants.CSUM_PST_ZONE_ID));
					hearing.setHearingTime(calendar.getTime());
					hearing.setUser(user.getUserName());
					hearingService.updateHearing(hearing);
				}
				return "redirect:../hearingsEdit/" + hearing.getViolation().getViolationId() + "/" + hearingId;
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../hearingsEdit/" + 0 + "/" + hearingId;
	}

	@PostMapping(value = { "/hearingFTAppear/{violationId}/{hearingId}" })
	public String updateHearingFTAppear(@PathVariable String violationId, @PathVariable Long hearingId, ModelMap model,
			HttpSession httpSession, RedirectAttributes redirectAttributes) {
		Admin user = (Admin) httpSession.getAttribute("admin");
		Violation violation = new Violation();
		try {
			if (user != null) {
				Properties prop = new Properties();
				InputStream inputStream = HearingController.class.getClassLoader()
						.getResourceAsStream("application.properties");
				prop.load(inputStream);
				Hearing hearing = hearingService.findById(hearingId);
				if (hearing != null) {
					hearing.setStatus("Failed to Appear");
					hearing.setHearingOfficer(user.getUserName());
					hearing.setUser(user.getUserName());
					if (hearing.getViolation() != null) {
						violation = hearing.getViolation();
					} else {
						violation = violationService.findByViolationId(violationId);
					}
					if (violation != null) {
						DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						String dispCode = prop.getProperty("violation.hearing.failtoappear");
						DispositionCode dispositionCode = dispositionCodeService.findByCode(dispCode);
						if (dispositionCode != null && dispositionCode.getDays() != 0) {
							SimpleDateFormat sdfnew = new SimpleDateFormat("MM/dd/yyyy");
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(new Date());
							calendar.add(Calendar.DATE, dispositionCode.getDays());
							violation.setSuspendProcessDate(sdfnew.format(calendar.getTime()));
							violation.setUser(user.getUserName());
							violationService.updateViolation(violation);
						}
						hearing.setDisposition(dispCode);
						hearing.setDispositionDate(
								new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
						hearing.setDispositionTime(sdf.parse((LocalTime.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)))
								.format(DateTimeFormatter.ofPattern("HH:mm:ss"))));
					}
					hearingService.updateHearing(hearing);
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				return "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		/*
		 * if (user != null && user.getRole().getUserType().getId() == 4) {
		 * return
		 * "redirect:/adjudicatorDashboardSupervisor/Complete/All/N/1/10"; }
		 * else { return "redirect:/adjudicatorDashboard/Complete/All/N/1/10"; }
		 */return "redirect:/adjudicatorDashboardSupervisor/Complete/All";
	}

	@PostMapping(value = { "/hearingDetailsAll/{violationId}/{hearingId}" })
	public String saveHearingDetails(@PathVariable String violationId, @PathVariable Long hearingId, Model model,
			@ModelAttribute("hearingDetailsForm") HearingDetail hearingForm, BindingResult bindingResult,
			HttpSession session, RedirectAttributes redirectAttributes) {
		Admin user = (Admin) session.getAttribute("admin");
		String output = "";
		boolean isCSApproved = false, isSuperApproved = false;
		try {
			Properties prop = new Properties();
			InputStream inputStream = HearingController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			if (user != null) {
				Hearing hearing = hearingService.findById(hearingId);
				if (hearingForm.getHearing().getHearingOfficer() != null
						&& hearingForm.getHearing().getHearingOfficer() != "") {
					Violation violation = violationService.findByViolationId(violationId);
					hearing.setHearingOfficer(hearingForm.getHearing().getHearingOfficer());
					// hearingForm.setDispositionCode(code);
					hearingForm.setUser(user.getUserName());
					hearingForm.setCreatedBy(user.getUserName());
					if (hearingForm.getDecision() != null) {
						HearingDecision decision = hearingDecisionService.findById(hearingForm.getDecision().getId());
						hearingForm.setDecision(decision);
					}
					if (hearingForm.getStatus().equalsIgnoreCase("PROGRESS")) {
						hearing.setHearingDate(
								new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
						Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(CSUMConstants.CSUM_PST_ZONE_ID));
						hearing.setHearingTime(calendar.getTime());
						hearingForm.setStatus("PENDINGREVIEW");
					} else if (hearingForm.getStatus().equalsIgnoreCase("PENDING")) {
						hearing.setHearingDate(
								new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
						Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(CSUMConstants.CSUM_PST_ZONE_ID));
						hearing.setHearingTime(calendar.getTime());
						hearingForm.setStatus("PENDINGREVIEW");
					} else if (hearingForm.getStatus().contains("REWORK")) {
						hearingForm.setStatus("PENDINGREVIEW");
					} else if (hearingForm.getStatus().contains("PENDINGREVIEW")) {
						if (user.getRole().getAdminType().getType().equalsIgnoreCase("HearingAdmin")) {
							if (violationId != null
									&& hearingForm.getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
								Payment payment = paymentDetailsService.findLatestByViolationId(violationId);
								if (violation != null) {
									BigDecimal totalDue = new BigDecimal(0.00);
									BigDecimal totalPaid = new BigDecimal(0.00);
									List<Payment> paymentsList = paymentDetailsService.findByViolationId(violationId);
									if (paymentsList.size() > 0 && paymentsList != null) {
										for (Payment p : paymentsList) {
											if (p.getAmount() != null) {
												totalPaid = totalPaid.add(p.getAmount());
											}
										}
									}
									violation.setTotalDue(totalDue);
									if (payment != null) {
										payment.setTotalDue(totalDue);
										payment.setOverPaid(totalPaid);
										payment.setViolation(violation);
										payment.setUser(user.getUserName());
										paymentDetailsService.updatePaymentDetails(payment);
									}
									if (totalDue.equals(new BigDecimal(0.00))
											&& totalPaid.compareTo(new BigDecimal(0.00)) == 0) {
										violation.setStatus("CLOSED");
									} else {
										violation.setStatus("PENDING");
									}
									if (totalPaid.compareTo(new BigDecimal(0.00)) == 1) {
										violation.setStatus("OVERPAID");
									}
									violation.setUser(user.getUserName());
									violationService.updateViolation(violation);
								}
							}
							isSuperApproved = true;
							if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableComSer")
									|| hearingForm.getDecision().getCode().equalsIgnoreCase("AdmsnComSer")) {
								isCSApproved = true;
								hearingForm.setStatus("COMPLETE");
							} else {
								hearingForm.setStatus("PENDINGMAIL");
							}
							Ipp ipp = ippService.findByViolationId(violationId);
							if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
								if (ipp == null) {
									ipp = ippService.recalculate(violationId, new Ipp());
									ipp.setStatus("PENDING");
									ipp.setUser(user.getUserName());
									ippService.save(ipp);
								}
							} else {
								if (ipp != null) {
									List<Payment> ippPayments = paymentDetailsService
											.findByPlanNumber(ipp.getPlanNumber());
									if (ippPayments.isEmpty() && ippPayments.size() == 0) {
										ippService.deleteById(ipp.getId());
									}
								}
							}

						} else {
							hearingForm.setStatus("PENDINGREVIEWUPDATED");
						}
					} else if (hearingForm.getStatus().contains("PENDINGMAIL")) {
						if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableComSer")
								|| hearingForm.getDecision().getCode().equalsIgnoreCase("AdmsnComSer")) {
							isCSApproved = true;
							hearingForm.setStatus("COMPLETE");
						}
						Ipp ipp = ippService.findByViolationId(violationId);
						if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
							if (ipp == null) {
								ipp = ippService.recalculate(violationId, new Ipp());
								ipp.setStatus("PENDING");
								ipp.setUser(user.getUserName());
								ippService.save(ipp);
							}
						} else {
							if (ipp != null) {
								List<Payment> ippPayments = paymentDetailsService.findByPlanNumber(ipp.getPlanNumber());
								if (ippPayments.isEmpty() && ippPayments.size() == 0) {
									ippService.deleteById(ipp.getId());
								}
							}
						}

					}
					if (hearingForm.getDecision() != null) {
						String code = "";
						if (hearingForm.getDecision().getCode().equalsIgnoreCase("Liable")) {
							code = prop.getProperty("violation.hearing.liable.disposition");
						} else if (hearingForm.getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
							code = prop.getProperty("violation.hearing.notliable.disposition");
						} else if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableComSer")) {
							code = prop.getProperty("violation.hearing.liablecomser.disposition");
						} else if (hearingForm.getDecision().getCode().equalsIgnoreCase("AdmsnComSer")) {
							code = prop.getProperty("violation.hearing.admsncomser.disposition");
						} else if (hearingForm.getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
							code = prop.getProperty("violation.hearing.liableipp.disposition");
						}
						DispositionCode dispCode = dispositionCodeService.findByCode(code);
						hearingForm.setDispositionCode(dispCode);
						hearing.setDisposition(code);
						hearing.setDecision(hearingForm.getDecision());
					}
					hearing.setDispositionDate(
							new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate());
					hearing.setDispositionTime(new Date());
					if (hearingForm.getStatus().equalsIgnoreCase("PENDINGMAIL")
							|| hearingForm.getStatus().equalsIgnoreCase("COMPLETE") || isSuperApproved) {
						if (hearing.getTotalDue() != null && hearingForm.getHearing().getTotalDue() != null
								&& (hearing.getTotalDue().compareTo(hearingForm.getHearing().getTotalDue()) != 0
										|| hearingForm.getHearing().getTotalDue()
												.compareTo(violation.getTotalDue()) != 0)
								&& !hearingForm.getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
							// BigDecimal changedtotalDue =
							// hearingForm.getHearing().getTotalDue();
							BigDecimal diff = hearingForm.getHearing().getReduction();
							if (hearingForm.getHearing().getTotalDue().compareTo(BigDecimal.ZERO) == -1) {
								hearing.setReduction(diff);
								hearing.setTotalDue(BigDecimal.ZERO);
								violation.setTotalDue(BigDecimal.ZERO);
								violation.setStatus("OVERPAID");
								BigDecimal reducedDue = hearingForm.getHearing().getTotalDue()
										.negate(new MathContext(5));
								Payment latestPayment = paymentDetailsService
										.findLatestByViolationId(violation.getViolationId());
								if (latestPayment != null) {
									latestPayment.setTotalDue(violation.getTotalDue());
									latestPayment.setOverPaid(reducedDue);
									latestPayment.setUser(user.getUserName());
									paymentDetailsService.updatePaymentDetails(latestPayment);
								} else {
									latestPayment = new Payment();
									Comment comment = new Comment();
									comment.setCommentType("I");
									comment.setComment(
											"There is no payment for this violation.User changing the total due with negative value.So we are inserting one payment with $0 for refund process");
									comment.setViolation(violation);
									violationService.addComment(comment);
									latestPayment.setViolation(violation);
									latestPayment
											.setProcessedOn(LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)));
									latestPayment.setAccount("PAY-BY-WEB");
									PaymentMethod paymentMethod = new PaymentMethod();
									paymentMethod = paymentMethodService.findByType(5);
									latestPayment.setPaymentMethod(paymentMethod);
									latestPayment.setMethodId(paymentMethod.getTypCd());
									latestPayment.setAmount(BigDecimal.ZERO);
									latestPayment.setPaymentDate(CSUMDateUtils.getDateInString(new Date()));
									latestPayment.setTotalDue(violation.getTotalDue());
									latestPayment.setOverPaid(reducedDue);
									if (user != null && user.getUserName() != null && user.getUserName() != "") {
										latestPayment.setProcessedBy(user.getUserName());
										latestPayment.setUser(user.getUserName());
									}
									paymentDetailsService.save(latestPayment);
								}
							} else if (diff.signum() != -1) {
								hearing.setReduction(diff);
								hearing.setTotalDue(hearingForm.getHearing().getTotalDue());
								violation.setTotalDue(hearingForm.getHearing().getTotalDue());
								if (hearingForm.getHearing().getTotalDue().compareTo(new BigDecimal(0.00)) == 0) {
									violation.setStatus("PAID");
								} else {
									violation.setStatus("PENDING");
								}
							}
							/*
							 * violation.setUser(user.getUserName());
							 * violationService.updateviolation(violation);
							 */
						} else if (hearingForm.getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
							hearing.setTotalDue(hearingForm.getHearing().getTotalDue());
							hearing.setReduction(hearingForm.getHearing().getReduction());
							Payment payment = paymentDetailsService.findLatestByViolationId(violationId);
							if (violation != null) {
								BigDecimal totalDue = new BigDecimal(0.00);
								BigDecimal totalPaid = new BigDecimal(0.00);
								List<Payment> paymentsList = paymentDetailsService.findByViolationId(violationId);
								if (paymentsList.size() > 0 && paymentsList != null) {
									for (Payment p : paymentsList) {
										if (p.getAmount() != null) {
											totalPaid = totalPaid.add(p.getAmount());
										}
									}
								}
								violation.setTotalDue(totalDue);
								if (payment != null) {
									payment.setTotalDue(totalDue);
									payment.setOverPaid(totalPaid);
									payment.setViolation(violation);
									payment.setUser(user.getUserName());
									paymentDetailsService.updatePaymentDetails(payment);
								}
								if (totalDue.equals(new BigDecimal(0.00))
										&& totalPaid.compareTo(new BigDecimal(0.00)) == 0) {
									violation.setStatus("CLOSED");
								}
								if (totalDue.compareTo(new BigDecimal(0.00)) == 1
										&& totalPaid.compareTo(new BigDecimal(0.00)) == 1) {
									violation.setStatus("OVERPAID");
								} /*
									 * violation.setUser(user.getUserName());
									 * violationService.updateviolation(
									 * violation);
									 */
							}
						}
						if (hearingForm.getDispositionCode() != null
								&& hearingForm.getDispositionCode().getDays() != 0) {
							SimpleDateFormat sdfnew = new SimpleDateFormat("MM/dd/yyyy");
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(new Date());
							/*
							 * if (isCSApproved) { calendar.add(Calendar.MONTH,
							 * 3); calendar.set(Calendar.DATE, 15); String
							 * certDueDateString =
							 * CSUMDateUtils.getDateInString(calendar.getTime())
							 * ;
							 * violation.setSuspendProcessDate(certDueDateString
							 * ); } else {
							 */
							calendar.add(Calendar.DATE, hearingForm.getDispositionCode().getDays());
							violation.setSuspendProcessDate(sdfnew.format(calendar.getTime()));
							/* } */
							violation.setUser(user.getUserName());
							violationService.updateViolation(violation);
						}
					} else {
						if (hearingForm != null
								/*
								 * && user.getRole().getUserType().getType().
								 * equalsIgnoreCase("HearingOfficer")
								 */
								&& hearingForm.getStatus() != null && hearingForm.getStatus().contains("PENDINGREVIEW")
								&& hearingForm.getDecision() != null
								&& hearingForm.getDecision().getCode().equalsIgnoreCase("Not_Liable")
								&& hearingForm.getHearing() != null) {
							BigDecimal red = BigDecimal.ZERO;
							if (hearingForm.getHearing().getReduction() != null) {
								red = red.add(hearingForm.getHearing().getReduction());
							}
							if (hearingForm.getHearing().getTotalDue() != null) {
								red = red.add(hearingForm.getHearing().getTotalDue());
							}
							hearingForm.getHearing().setReduction(red);
							hearingForm.getHearing().setTotalDue(BigDecimal.ZERO);
						}
						hearing.setReduction(hearingForm.getHearing().getReduction());
						hearing.setTotalDue(hearingForm.getHearing().getTotalDue());
					}
					if (hearingForm != null && hearingForm.getHearing() != null) {
						hearing.setNoFineChange(hearingForm.getHearing().isNoFineChange());
					}
					hearing.setUser(user.getUserName());
					hearingForm.setHearing(hearing);
					hearingDetailsService.save(hearingForm);
					// saveLetterDetails(hearing.getId(), user, isCSApproved,
					// false);
					/*
					 * if(user.getRole().getUserType().getType().
					 * equalsIgnoreCase("HearingAdmin") &&
					 * (hearingForm.getDecision().equalsIgnoreCase(
					 * "LiableComSer") ||
					 * hearingForm.getDecision().equalsIgnoreCase("AdmsnComSer")
					 * )){ addFAQandMTCRefferal(violationId); }
					 */
				} else {
					hearing.setStatus("PENDING");
					hearing.setHearingOfficer("");
					hearing.setUser(user.getUserName());
					hearingService.updateHearing(hearing);
					hearingDetailsService.deleteByHearingId(hearingId);
				}
				/*
				 * if (user != null && user.getRole().getUserType().getId() ==
				 * 4) { output =
				 * "redirect:../../adjudicatorDashboardSupervisor/Complete/All/N/1/10";
				 * } else { output =
				 * "redirect:../../adjudicatorDashboard/Complete/All/N/1/10"; }
				 */
				output = "redirect:../../hearingsEdit/" + violationId + "/" + hearingId;
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				output = "redirect:/adjudicatorLogin";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return output;
	}

	@PostMapping(value = { "/exportCaseHistory/{violationId}/{hearingId}" })
	@ResponseBody
	public void exportCaseHistory(@PathVariable String violationId, @PathVariable String hearingId,
			HttpSession httpSession, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		HistoryBean historyBean;
		/*
		 * List<HearingHistoryView> hearinghstryList = new ArrayList<>();
		 * HearingHistoryView hearingHstryView=new HearingHistoryView();
		 */
		try {
			// Violation violation =
			// violationService.findByViolationId(violationId);
			historyBean = historyService.getRequiredHistory("All", violationId);
			String fileName = "Case_History_" + violationId + ".csv";
			response.setHeader("Content-disposition", "attachment;filename=" + fileName);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getOutputStream().println(csvHeader());
			List<HearingHistoryView> historyViews = new ArrayList<>();
			HearingHistoryView historyView = new HearingHistoryView();
			if (historyBean != null) {
				if (historyBean.getPlateHistory() != null && historyBean.getPlateHistory().size() > 0) {
					for (PlateEntity plateHstry : historyBean.getPlateHistory()) {
						if (plateHstry != null) {
							historyView = plateEntityToViewBean(plateHstry);
							historyViews.add(historyView);
						}
					}
				}
			}
			/*
			 * if (addressHistory != null && addressHistory.size() > 0) { for
			 * (Address address : addressHistory) { if (address != null &&
			 * ((address.getFormattedAddress() != null &&
			 * address.getFormattedAddress().trim().length() > 0))) {
			 * historyView = addressToViewBean(address);
			 * historyViews.add(historyView); } } } if (ippHstry != null) { for
			 * (Ipp planhstry : ippHstry) { if (planhstry != null) { historyView
			 * = ippToViewBean(planhstry); historyViews.add(historyView); } } }
			 */
			if (historyBean.getPenaltyHistory() != null && historyBean.getPenaltyHistory().size() > 0) {
				for (Penalty pnltyHstry : historyBean.getPenaltyHistory()) {
					if (pnltyHstry != null) {
						historyView = penaltyToViewBean(pnltyHstry);
						historyViews.add(historyView);
					}
				}
			}
			if (historyBean.getPaymentHistory() != null && historyBean.getPaymentHistory().size() > 0) {
				for (List<Payment> paymentHstry : historyBean.getPaymentHistory()) {
					if (paymentHstry != null && paymentHstry.size() > 0) {
						for (Payment pymntHstry : paymentHstry) {
							if (pymntHstry != null) {
								historyView = paymentToViewBean(pymntHstry);
								historyViews.add(historyView);
							}
						}
					}
				}
			}
			if (historyBean.getHearingHistory() != null && historyBean.getHearingHistory().size() > 0) {
				for (Hearing hearinghstry : historyBean.getHearingHistory()) {
					if (hearinghstry != null) {
						historyView = hearingToViewBean(hearinghstry);
						historyViews.add(historyView);
					}
				}
			}
			if (historyBean.getSuspendHistory() != null && historyBean.getSuspendHistory().size() > 0) {
				for (List<Suspends> susHstry : historyBean.getSuspendHistory()) {
					if (susHstry != null && susHstry.size() > 0) {
						for (Suspends suspend : susHstry) {
							if (suspend != null) {
								historyView = suspendToViewBean(suspend);
								historyViews.add(historyView);
							}
						}
					}
				}
			}
			if (historyBean.getCorrespHistory() != null && historyBean.getCorrespHistory().size() > 0) {
				for (List<Correspondence> corresphstry : historyBean.getCorrespHistory()) {
					if (corresphstry != null && corresphstry.size() > 0) {
						for (Correspondence corresp : corresphstry) {
							if (corresp != null) {
								historyView = correspToViewBean(corresp);
								historyViews.add(historyView);
							}
						}
					}
				}
			}
			if (historyBean.getNoticeHistory() != null && historyBean.getNoticeHistory().size() > 0) {
				for (List<Notices> citationNotices : historyBean.getNoticeHistory()) {
					if (citationNotices != null && citationNotices.size() > 0) {
						for (Notices notice : citationNotices) {
							if (notice != null) {
								historyView = noticeToViewBean(notice);
								historyViews.add(historyView);
							}
						}
					}
				}
			}
			if (historyBean.getIppHistory() != null && historyBean.getIppHistory().size() > 0) {
				for (Ipp ipp : historyBean.getIppHistory()) {
					if (ipp != null) {
						historyView = ippToViewBean(ipp);
						historyViews.add(historyView);
					}
				}
			}
			Collections.sort(historyViews, new Comparator<HearingHistoryView>() {
				public int compare(HearingHistoryView v1, HearingHistoryView v2) {
					return v1.getHstryDateTime().compareTo(v2.getHstryDateTime());
				}
			});
			Collections.reverse(historyViews);
			if (historyViews != null && historyViews.size() > 0) {
				for (HearingHistoryView view : historyViews) {
					if (view != null && view.getDescription() != null && view.getDescription().trim().length() > 0) {
						StringBuilder viewLine = new StringBuilder("");
						String date = "", time = "";
						if (view.getDateTimeInString() != null) {
							date = view.getDateTimeInString().split(" ")[0];
							time = view.getDateTimeInString().split(" ", 2)[1];
						}
						viewLine.append(date).append(",").append(time).append(",").append(view.getType().toUpperCase())
								.append(",").append(view.getDescription().toUpperCase());
						if (viewLine != null && viewLine.toString().trim().length() > 0) {
							response.getOutputStream().println(viewLine.toString().trim());
						}
					}
				}
			}
			response.getOutputStream().flush();
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

	private HearingHistoryView plateEntityToViewBean(PlateEntity plateHstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String plateData = "";
		if (plateHstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(plateHstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(plateHstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(plateHstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(plateHstry.getCreatedAt());
		}
		if (plateHstry.getLicenceNumber() != null && plateHstry.getLicenceNumber().trim().length() > 0) {
			plateData += plateHstry.getLicenceNumber().trim();
		}
		if (plateHstry.getVinNumber() != null && plateHstry.getVinNumber().trim().length() > 0) {
			plateData += " " + plateHstry.getVinNumber().trim();
		}
		if (plateHstry.getMonth() != null && plateHstry.getMonth().trim().length() > 0) {
			plateData += " " + plateHstry.getMonth();
		}
		if (plateHstry.getYear() != null && plateHstry.getYear().trim().length() > 0) {
			plateData += " " + plateHstry.getYear();
		}
		if (plateHstry.getBodyType() != null && plateHstry.getBodyType().trim().length() > 0) {
			plateData += " " + plateHstry.getBodyType().trim();
		}
		if (plateHstry.getVehicleMake() != null && plateHstry.getVehicleMake().trim().length() > 0) {
			plateData += " " + plateHstry.getVehicleMake().trim();
		}
		if (plateHstry.getVehicleModel() != null && plateHstry.getVehicleModel().trim().length() > 0) {
			plateData += " " + plateHstry.getVehicleModel().trim();
		}
		if (plateHstry.getVehicleColor() != null && plateHstry.getVehicleColor().trim().length() > 0) {
			plateData += " " + plateHstry.getVehicleColor().trim();
		}
		historyView.setDescription("\"" + plateData.toUpperCase() + "\"");
		historyView.setType("PLATE ENTITY");
		return historyView;
	}

	private String csvHeader() {
		StringBuilder header = new StringBuilder("");
		header.append("Date").append(",").append("Time").append(",").append("History Type").append(",")
				.append("Comments");
		return header.toString();
	}

	private HearingHistoryView noticeToViewBean(Notices noticehstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String noticeData = "";
		if (noticehstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(noticehstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(noticehstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(noticehstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(noticehstry.getCreatedAt());
		}
		if (noticehstry.getNoticeType() != null && noticehstry.getNoticeType().getFullNm() != null
				&& noticehstry.getNoticeType().getFullNm().trim().length() > 0) {
			noticeData += " " + noticehstry.getNoticeType().getFullNm().trim();
		}
		if (noticehstry.getSentDate() != null) {
			noticeData += " " + noticehstry.getSentDate();
		}
		historyView.setType("NOTICES");
		historyView.setDescription("\"" + noticeData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView ippToViewBean(Ipp planhstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String ippData = "";
		if (planhstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(planhstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(planhstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(planhstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(planhstry.getCreatedAt());
		}
		if (planhstry.getPlanNumber() != 0) {
			ippData += "Plan Number: " + planhstry.getPlanNumber() + " ";
		}
		if (planhstry.getStartDate() != null) {
			ippData += "Start Date: " + planhstry.getStartDate() + " ";
		}
		if (planhstry.getEnrollAmount().compareTo(BigDecimal.ZERO) == 1) {
			ippData += "Enroll Amount: " + planhstry.getEnrollAmount() + " ";
		}
		if (planhstry.getDownPayment().compareTo(BigDecimal.ZERO) == 1) {
			ippData += "Down Payment: " + planhstry.getDownPayment() + " ";
		}
		if (planhstry.getInstallmentAmount().compareTo(BigDecimal.ZERO) == 1) {
			ippData += "Installment Amount: " + planhstry.getInstallmentAmount() + " ";
		}
		if (planhstry.getNoOfPayments() != 0) {
			ippData += "No Of Payments: " + planhstry.getNoOfPayments() + " ";
		}
		if (planhstry.getStatus() != null) {
			ippData += "Status: " + planhstry.getStatus() + " ";
		}
		if (planhstry.getType() != null) {
			ippData += "Type: " + planhstry.getType() + " ";
		}
		historyView.setType("IPP");
		historyView.setDescription("\"" + ippData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView correspToViewBean(Correspondence corresphstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String correspData = "";
		if (corresphstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(corresphstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(corresphstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(corresphstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(corresphstry.getCreatedAt());
		}
		if (corresphstry.getCorrespCode() != null && corresphstry.getCorrespCode().getCorrespDesc() != null
				&& corresphstry.getCorrespCode().getCorrespDesc().trim().length() > 0) {
			correspData += " " + corresphstry.getCorrespCode().getCorrespDesc().trim();
		}
		if (corresphstry.getFormattedCorresDate() != null
				&& corresphstry.getFormattedCorresDate().trim().length() > 0) {
			correspData += " " + corresphstry.getFormattedCorresDate().trim();
		}
		if (corresphstry.getCorresp_time() != null && corresphstry.getCorresp_time().trim().length() > 0) {
			correspData += " " + corresphstry.getCorresp_time().trim();
		}
		if (corresphstry.isLetterSent() == true) {
			correspData += " Yes";
		} else {
			correspData += " No";
		}
		historyView.setType("CORRESPONDENCE");
		historyView.setDescription("\"" + correspData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView suspendToViewBean(Suspends suspendhstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String suspendData = "";
		if (suspendhstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(suspendhstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(suspendhstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(suspendhstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(suspendhstry.getCreatedAt());
		}
		if (suspendhstry.getSuspendedCodes() != null && suspendhstry.getSuspendedCodes().getDescription() != null
				&& suspendhstry.getSuspendedCodes().getDescription().trim().length() > 0) {
			suspendData += " " + suspendhstry.getSuspendedCodes().getDescription().trim();
		}
		if (suspendhstry.getSuspendedCodes() != null) {
			suspendData += " " + suspendhstry.getSuspendedCodes().getCode();
		}
		if (suspendhstry.getFormattedSuspendDate() != null
				&& suspendhstry.getFormattedSuspendDate().trim().length() > 0) {
			suspendData += " " + suspendhstry.getFormattedSuspendDate().trim();
		}
		if (suspendhstry.getSuspended_time() != null && suspendhstry.getSuspended_time().trim().length() > 0) {
			suspendData += " " + suspendhstry.getSuspended_time().trim();
		}
		if (suspendhstry.getFormattedProcessedOn() != null
				&& suspendhstry.getFormattedProcessedOn().trim().length() > 0) {
			suspendData += " " + suspendhstry.getFormattedProcessedOn().trim();
		}
		if (suspendhstry.getReduction() != null) {
			suspendData += " $" + suspendhstry.getReduction();
		}
		if (suspendhstry.getTotalDue() != null) {
			suspendData += " $" + suspendhstry.getTotalDue();
		}
		historyView.setType("SUSPENDS");
		historyView.setDescription("\"" + suspendData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView hearingToViewBean(Hearing hearinghstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String hearingData = "", hearingStatus = "", decision = "", hearingType = "";
		if (hearinghstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(hearinghstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(hearinghstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(hearinghstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(hearinghstry.getCreatedAt());
		}
		if (hearinghstry.getStatus() != null && hearinghstry.getStatus().trim().length() > 0) {
			if (hearinghstry.getStatus() != null) {
				if (hearinghstry.getStatus().equalsIgnoreCase("PROGRESS")) {
					hearingStatus = "In-Progress";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("PENDINGREVIEWUPDATED")) {
					hearingStatus = "Pending Review-Updated";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("PENDINGREVIEW")) {
					hearingStatus = "Pending Review";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("COMPLETE")) {
					hearingStatus = "Complete";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("PENDINGMAIL")) {
					hearingStatus = "Pending Mail";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("REWORK")) {
					hearingStatus = "Needs Rework";
				} else if (hearinghstry.getStatus().equalsIgnoreCase("Failed to Appear")) {
					hearingStatus = "Failed to Appear";
				} else {
					hearingStatus = "Pending";
				}
			}
			hearingData += "Hearing Status: " + hearingStatus;
		}
		if (hearinghstry.getDecision() != null) {
			if (hearinghstry.getDecision() != null) {
				if (hearinghstry.getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
					decision = "Not Liable";
				} else if (hearinghstry.getDecision().getCode().equalsIgnoreCase("LiableComSer")) {
					decision = "Liable with Community Service";
				} else if (hearinghstry.getDecision().getCode().equalsIgnoreCase("AdmsnComSer")) {
					decision = "Admission Community Service";
				} else if (hearinghstry.getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
					decision = "Liable with IPP";
				} else if (hearinghstry.getDecision().getCode().equalsIgnoreCase("Liable")) {
					decision = "Liable";
				}
			}
			hearingData += " " + decision;
		}
		if (hearinghstry.getFormattedHearingDate() != null
				&& hearinghstry.getFormattedHearingDate().trim().length() > 0) {
			hearingData += " " + hearinghstry.getFormattedHearingDate().trim();
		}
		if (hearinghstry.getHearingTime() != null) {
			hearingData += " " + hearinghstry.getHearingTime();
		}
		if (hearinghstry.getFormattedDispDate() != null && hearinghstry.getFormattedDispDate().trim().length() > 0) {
			hearingData += " " + hearinghstry.getFormattedDispDate().trim();
		}
		if (hearinghstry.getDispositionTime() != null) {
			hearingData += " " + hearinghstry.getDispositionTime();
		}
		if (hearinghstry.getDisposition() != null && hearinghstry.getDisposition().trim().length() > 0) {
			hearingData += " " + hearinghstry.getDisposition().trim();
		}
		if (hearinghstry.getHearingOfficer() != null && hearinghstry.getHearingOfficer().trim().length() > 0) {
			hearingData += " " + hearinghstry.getHearingOfficer().trim();
		}
		if (hearinghstry.getType() != null) {
			if (hearinghstry.getType().equalsIgnoreCase("WRITTEN")) {
				hearingType = "Written";
			} else if (hearinghstry.getType().equalsIgnoreCase("PERSON")) {
				hearingType = "IN-PERSON / SCHEDULED";
			} else if (hearinghstry.getType().equalsIgnoreCase("WALKIN")) {
				hearingType = "IN-PERSON / WALK IN";
			}
			hearingData += " " + hearingType.trim();
		}
		if (hearinghstry.getReduction() != null) {
			hearingData += " $" + hearinghstry.getReduction();
		} else {
			hearingData += " $0.00";
		}
		if (hearinghstry.getTotalDue() != null) {
			hearingData += " $" + hearinghstry.getTotalDue();
		} else {
			hearingData += " $0.00";
		}
		historyView.setType("HEARING");
		historyView.setDescription("\"" + hearingData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView paymentToViewBean(Payment pymntHstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String paymentData = "";
		if (pymntHstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(pymntHstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(pymntHstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(pymntHstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(pymntHstry.getCreatedAt());
		}
		if (pymntHstry.getAmount() != null) {
			paymentData += " " + pymntHstry.getAmount();
		}
		if (pymntHstry.getPaymentDate() != null) {
			paymentData += " " + pymntHstry.getPaymentDate();
		}
		paymentData += " PAYMENT";
		if (pymntHstry.getPaymentMethod() != null && pymntHstry.getPaymentMethod().getDescription() != null
				&& pymntHstry.getPaymentMethod().getDescription().trim().length() > 0) {
			paymentData += " " + pymntHstry.getPaymentMethod().getDescription().trim();
		}
		if (pymntHstry.getAccount() != null && pymntHstry.getAccount().trim().length() > 0) {
			paymentData += " " + pymntHstry.getAccount().trim();
		}
		if (pymntHstry.getProcessedOn() != null) {
			paymentData += " " + pymntHstry.getProcessedOn();
		}
		if (pymntHstry.getProcessedBy() != null && pymntHstry.getProcessedBy().trim().length() > 0) {
			paymentData += " " + pymntHstry.getProcessedBy();
		}
		if (pymntHstry.getOverPaid() != null) {
			paymentData += " $" + pymntHstry.getOverPaid();
		}
		if (pymntHstry.getTotalDue() != null) {
			paymentData += " $" + pymntHstry.getTotalDue();
		}
		if (pymntHstry.getTransaction() != null && pymntHstry.getTransaction().getId() != null) {
			paymentData += " " + pymntHstry.getTransaction().getId();
		}
		historyView.setType("PAYMENT");
		historyView.setDescription("\"" + paymentData.toUpperCase() + "\"");
		return historyView;
	}

	private HearingHistoryView penaltyToViewBean(Penalty pnltyHstry) {
		HearingHistoryView historyView = new HearingHistoryView();
		String penaltyData = "";
		if (pnltyHstry.getUpdatedAt() != null) {
			historyView.setDateTimeInString(pnltyHstry.getFormattedUpdatedAt());
			historyView.setHstryDateTime(pnltyHstry.getUpdatedAt());
		} else {
			historyView.setDateTimeInString(pnltyHstry.getFormattedCreatedAt());
			historyView.setHstryDateTime(pnltyHstry.getCreatedAt());
		}
		if (pnltyHstry.getPenaltyCode() != null) {
			if (pnltyHstry.getPenaltyCode().getPenalty1() != null) {
				penaltyData += " $" + pnltyHstry.getPenaltyCode().getPenalty1();
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			} else {
				penaltyData += " $0.00";
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			}
			if (pnltyHstry.getPenaltyCode().getPenalty1() != null) {
				penaltyData += " $" + pnltyHstry.getPenaltyCode().getPenalty1();
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			} else {
				penaltyData += " $0.00";
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			}
			if (pnltyHstry.getPenaltyCode().getPenalty1() != null) {
				penaltyData += " $" + pnltyHstry.getPenaltyCode().getPenalty1();
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			} else {
				penaltyData += " $0.00";
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty1() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty1();
				}
			}
			if (pnltyHstry.getPenaltyCode().getPenalty2() != null) {
				penaltyData += " $" + pnltyHstry.getPenaltyCode().getPenalty2();
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty2() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty2();
				}
			} else {
				penaltyData += " $0.00";
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty2() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty2();
				}
			}
			if (pnltyHstry.getPenaltyCode().getPenalty3() != null) {
				penaltyData += " $" + pnltyHstry.getPenaltyCode().getPenalty3();
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty3() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty3();
				}
			} else {
				penaltyData += " $0.00";
				if (pnltyHstry.getPenaltyCode().getFormattedpenalty3() != null) {
					penaltyData += " /" + pnltyHstry.getPenaltyCode().getFormattedpenalty3();
				}
			}
		}
		if (pnltyHstry.getViolation() != null && pnltyHstry.getViolation().getTotalDue() != null) {
			penaltyData += " /$" + pnltyHstry.getViolation().getTotalDue();
		}
		historyView.setType("PENALTY");
		historyView.setDescription("\"" + penaltyData.toUpperCase() + "\"");
		return historyView;
	}

	@PostMapping(value = { "/exportHearings/{hearingRequired}/{checkbox}" })
	@ResponseBody
	public String exportHearings(@PathVariable String hearingRequired, @PathVariable String checkbox,
			@RequestParam(required = false) String hearingBasis, @RequestParam(required = false) String requiredDate,
			ModelMap model, HttpSession httpSession, RedirectAttributes redirectAttributes, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("exportHearings: " + hearingRequired + " " + checkbox + " " + hearingBasis + " " + requiredDate);
		model.addAttribute("hearingRequired", hearingRequired);
		model.addAttribute("checkbox", checkbox);
		Hearing hearingForm = new Hearing();
		// Admin user = (Admin) httpSession.getAttribute("admin");
		try {
			if (hearingRequired.equalsIgnoreCase("Complete")) {
				hearingForm.setHearingBasis(hearingBasis);
				hearingForm.setRequiredDate(requiredDate);
			}
			List<Hearing> hearingList;
			/*
			 * if (user != null && user.getRole().getAdminType().getId() == 4) {
			 */
			hearingList = adjudicatorDashboardSupervisorList(hearingRequired, checkbox, hearingForm);
			/*
			 * } else { hearingList =new Map<String, List<Hearing>>();
			 * adjudicatorDashboardList(hearingRequired, checkbox, "N", model,
			 * httpSession, hearingForm, pageNo, redirectAttributes, noOfRec); }
			 */

			Properties prop = new Properties();
			StringBuilder stringBuilderHeader = new StringBuilder("");
			InputStream inputStream = HearingController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						prop.load(inputStream);
						String path = prop.getProperty("violation.mailprocess.path");
						LocalDate date = LocalDate.now(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID));
						String reportName = "Hearings" + date + ".csv";
						FileWriter writer = new FileWriter(path + reportName);
						try {
							logger.debug("exportHearings" + hearingList.size());
							stringBuilderHeader.append("Hearing Date").append(",").append("Hearing Time").append(",")
									.append("Violation Id").append(",").append("Status").append(",")
									.append("Hearing Type").append(",").append("Translator").append(",")
									.append("Last Name").append(",").append("First Name").append(",")
									.append("Violation Code").append(",").append("Lic.Plate# or ID#").append(",")
									.append("Hearing Officer").append(",").append("Decision").append(",")
									.append("Notes").append(",").append("Date Mailed");
							writer.append(stringBuilderHeader.toString());
							writer.append("\n");
							/*
							 * for (Map.Entry<String, List<Hearing>> entry :
							 * hearingList.entrySet()) { List<Hearing>
							 * hearingList = entry.getValue();
							 */
							logger.debug("HearingList >>" + hearingList.size());
							for (Hearing hearing : hearingList) {
								writer.append(beanToCsv(hearing));
								writer.append("\n");
							}
							/* } */
							writer.flush();
							writer.close();
							String URL = prop.getProperty("reports.url").trim() + reportName.trim();
							StringBuilder sb = new StringBuilder(2000);
							sb.append("Hi,<br/>");
							sb.append("<br/> ");
							sb.append(" Please download Hearings using following link ");
							sb.append("<br/> ");
							sb.append(URL);
							sb.append("<br/> ");
							sb.append("<br/> ");
							sb.append("Thanks, <br/> ");
							sb.append("Metro Team");
							prop.put("mail.smtp.auth", "true");
							prop.put("mail.smtp.starttls.enable", "true");
							prop.put("mail.smtp.host", prop.getProperty("violation.mail.host"));
							prop.put("mail.smtp.port", prop.getProperty("violation.mail.port"));
							String to = prop.getProperty("violation.hearing.mail.reports.to");
							// Get the Session object.
							Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(prop.getProperty("violation.mail.id"),
											prop.getProperty("violation.mail.password"));
								}
							});
							String[] tos = to.split(",");
							javax.mail.Address[] address = new javax.mail.Address[tos.length];
							for (int i = 0; i < tos.length; i++) {
								address[i] = new InternetAddress(tos[i]);
							}
							Message message = new MimeMessage(session);
							message.setFrom(new InternetAddress(prop.getProperty("violation.mail.id")));
							message.setRecipients(Message.RecipientType.TO, address);
							message.setSubject("List of Hearing");
							message.setContent(sb.toString(), "text/html; charset=utf-8");
							Transport.send(message);
						} catch (Exception e) {
							logger.error(e, e);
						}
					} catch (IOException e) {
						logger.error(e, e);
					}
				}
			});
			t.start();
		} catch (Exception e1) {
			logger.error(e1, e1);
		}
		return hearingBasis;
	}

	private String beanToCsv(Hearing hearing) {
		StringBuilder stringBuilder = new StringBuilder("");
		String translator = "", decision = "", notes = "", lastName = "", firstName = "", vlnCode = "", licNum = "",
				hearingStatus = "", hearingType = "", violationId = "";
		if (hearing != null) {
			if (hearing.getStatus() != null) {
				if (hearing.getStatus().equalsIgnoreCase("PROGRESS")) {
					hearingStatus = "In-Progress";
				} else if (hearing.getStatus().equalsIgnoreCase("PENDINGREVIEWUPDATED")) {
					hearingStatus = "Pending Review-Updated";
				} else if (hearing.getStatus().equalsIgnoreCase("PENDINGREVIEW")) {
					hearingStatus = "Pending Review";
				} else if (hearing.getStatus().equalsIgnoreCase("COMPLETE")) {
					hearingStatus = "Complete";
				} else if (hearing.getStatus().equalsIgnoreCase("PENDINGMAIL")) {
					hearingStatus = "Pending Mail";
				} else if (hearing.getStatus().equalsIgnoreCase("REWORK")) {
					hearingStatus = "Needs Rework";
				} else if (hearing.getStatus().equalsIgnoreCase("Failed to Appear")) {
					hearingStatus = "Failed to Appear";
				}
			}
			if (hearing.getType() != null) {
				if (hearing.getType().equalsIgnoreCase("WRITTEN")) {
					hearingType = "Written";
				} else if (hearing.getType().equalsIgnoreCase("PERSON")) {
					hearingType = "IN-PERSON / SCHEDULED";
				} else if (hearing.getType().equalsIgnoreCase("WALKIN")) {
					hearingType = "IN-PERSON / WALK IN";
				}

			}
			if (hearing.getHearingdetail() != null) {
				translator = hearing.getHearingdetail().getTranslation() == null ? " "
						: hearing.getHearingdetail().getTranslation();
				if (hearing.getHearingdetail().getDecision() != null) {
					if (hearing.getHearingdetail().getDecision().getCode().equalsIgnoreCase("Not_Liable")) {
						decision = "Not Liable";
					} else if (hearing.getHearingdetail().getDecision().getCode().equalsIgnoreCase("LiableComSer")) {
						decision = "Liable with Community Service";
					} else if (hearing.getHearingdetail().getDecision().getCode().equalsIgnoreCase("AdmsnComSer")) {
						decision = "Admission Community Service";
					} else if (hearing.getHearingdetail().getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
						decision = "Liable with IPP";
					} else if (hearing.getHearingdetail().getDecision().getCode().equalsIgnoreCase("Liable")) {
						decision = "Liable";
					}
				}
				notes = hearing.getHearingdetail().getNotes() == null ? " " : hearing.getHearingdetail().getNotes();
			}
			if (hearing.getViolation() != null) {
				violationId = hearing.getViolation().getViolationId() + "\t";
				if (hearing.getViolation().getPatron() != null) {
					lastName = hearing.getViolation().getPatron().getLastName() == null ? " "
							: hearing.getViolation().getPatron().getLastName();
					firstName = hearing.getViolation().getPatron().getFirstName() == null ? " "
							: hearing.getViolation().getPatron().getFirstName();
				}
				if (hearing.getViolation().getViolationCode() != null) {
					vlnCode = (hearing.getViolation().getViolationCode().getCode() == null ? " "
							: hearing.getViolation().getViolationCode().getCode()) + " "
							+ (hearing.getViolation().getViolationCode().getDescription() == null ? " "
									: hearing.getViolation().getViolationCode().getDescription());
				}
				if (hearing.getViolation().getPlateEntity() != null) {
					licNum = hearing.getViolation().getPlateEntity().getLicenceNumber() == null ? " "
							: hearing.getViolation().getPlateEntity().getLicenceNumber();
				}
			}
		}
		stringBuilder.append(hearing.getHearingDate()).append(",").append(hearing.getHearingTime()).append(",")
				.append(violationId).append(",").append(hearingStatus).append(",").append(hearingType).append(",")
				.append(translator).append(",").append(lastName).append(",").append(firstName).append(",")
				.append(vlnCode).append(",").append(licNum).append(",")
				.append(hearing.getHearingOfficerName() == null ? " " : hearing.getHearingOfficerName()).append(",")
				.append(decision).append(",").append(notes).append(",")
				.append(hearing.getFormattedDateMailed() != null ? hearing.getFormattedDateMailed() : "");
		return stringBuilder.toString().toUpperCase();
	}

	private List<Hearing> adjudicatorDashboardSupervisorList(String hearingRequired, String checkbox,
			Hearing hearingForm) {
		List<Hearing> hearingDetailList = null;
		try {
			List<Hearing> hearingList = null;
			LocalDate date = new Date().toInstant().atZone(ZoneId.of(CSUMConstants.CSUM_PST_ZONE_ID)).toLocalDate();
			/*
			 * String presentTime = CSUMDateUtils.getFormattedLocalTime( new
			 * Date().toInstant().atZone(ZoneId.of(CSUMConstants.
			 * CSUM_PST_ZONE_ID)).toLocalTime());
			 */
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			boolean flag;
			// String presentDate = "";
			LocalDate date1 = null, date2 = null;
			if (hearingForm != null) {
				if (hearingForm.getRequiredDate() != null && hearingForm.getRequiredDate() != "") {
					if (hearingForm.getHearingBasis() != null
							&& hearingForm.getHearingBasis().equalsIgnoreCase("Monthly")) {
						flag = true;
						Calendar cal = Calendar.getInstance();
						String requiredDate[] = hearingForm.getRequiredDate().split(",");
						if (requiredDate.length > 1) {
							Date date3 = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(requiredDate[0].trim());
							cal.setTime(date3);
							int year = Integer.parseInt(requiredDate[1].trim());
							cal.set(Calendar.YEAR, year);
							cal.set(Calendar.DATE, 1);
							date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							int lastDate = cal.getActualMaximum(Calendar.DATE);
							cal.add(Calendar.DATE, lastDate - 1);
							date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							// presentDate = requiredDate[0].trim() + ", " +
							// requiredDate[1].trim();

						} else {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							cal.setTime(sdf.parse(requiredDate[0].trim()));
							int number = 0;
							number = -cal.get(Calendar.DATE) + 1;
							cal.add(Calendar.DATE, number);
							date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							int lastDate = cal.getActualMaximum(Calendar.DATE);
							cal.add(Calendar.DATE, lastDate - 1);
							date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							/*
							 * int num = cal.get(Calendar.MONTH);
							 * DateFormatSymbols dfs = new DateFormatSymbols();
							 * String[] months = dfs.getMonths();
							 */
							// presentDate = months[num] + ", " +
							// cal.get(Calendar.YEAR);
						}

					} else if (hearingForm.getHearingBasis() != null
							&& hearingForm.getHearingBasis().equalsIgnoreCase("Annually")) {
						flag = true;
						Calendar cal = Calendar.getInstance();
						String requiredDate[] = hearingForm.getRequiredDate().split("-");
						if (requiredDate.length > 1) {
							date1 = LocalDate.parse(requiredDate[0].trim(), formatter);
							date2 = LocalDate.parse(requiredDate[1].trim(), formatter);
							/*
							 * presentDate =
							 * CSUMDateUtils.getFormattedLocalDate(date1) + "-"
							 * + CSUMDateUtils.getFormattedLocalDate(date2);
							 */
						} else if (requiredDate.length > 0) {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							cal.setTime(sdf.parse(requiredDate[0].trim()));
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(sdf.parse(CSUMDateUtils.getFormattedLocalDate(date)));
							if (cal.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)) {
								cal.set(Calendar.DATE, 31);
								cal.set(Calendar.MONTH, 11);
							} else {
								cal.setTime(sdf.parse(CSUMDateUtils.getFormattedLocalDate(date)));
							}
							date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							cal.set(Calendar.DATE, 1);
							cal.set(Calendar.MONTH, 0);
							date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							/*
							 * presentDate =
							 * CSUMDateUtils.getFormattedLocalDate(date1) + "-"
							 * + CSUMDateUtils.getFormattedLocalDate(date2);
							 */
						}
					} else if (hearingForm.getHearingBasis() != null
							&& hearingForm.getHearingBasis().equalsIgnoreCase("Weekly")) {
						flag = true;
						String requiredDate[] = hearingForm.getRequiredDate().split("-");
						if (requiredDate.length > 1) {
							date1 = LocalDate.parse(requiredDate[0].trim(), formatter);
							date2 = LocalDate.parse(requiredDate[1].trim(), formatter);
							/*
							 * presentDate =
							 * CSUMDateUtils.getFormattedLocalDate(date1) + "-"
							 * + CSUMDateUtils.getFormattedLocalDate(date2);
							 */
						} else {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							Calendar cal = Calendar.getInstance();
							cal.setTime(sdf.parse(requiredDate[0].trim()));
							int number = 0;
							number = -cal.get(Calendar.DAY_OF_WEEK) + 1;
							cal.add(Calendar.DATE, number);
							date1 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							cal.add(Calendar.DATE, 6);
							date2 = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							/*
							 * presentDate =
							 * CSUMDateUtils.getFormattedLocalDate(date1) + "-"
							 * + CSUMDateUtils.getFormattedLocalDate(date2);
							 */
						}
					} else {
						flag = false;
						date = LocalDate.parse(hearingForm.getRequiredDate(), formatter);
						// presentDate =
						// CSUMDateUtils.getFormattedLocalDate(date);
					}
				} else {
					flag = false;
					// presentDate = CSUMDateUtils.getFormattedLocalDate(date);
				}

			} else {
				flag = false;
				// presentDate = CSUMDateUtils.getFormattedLocalDate(date);
			}
			if (hearingRequired.equalsIgnoreCase("Pending")) {
				if (flag == false) {
					switch (checkbox) {
					case "All":
						hearingList = hearingService.findAllPendingHearingsList(date);
						break;
					case "InPerson":
						hearingList = hearingService.findAllPendingHearingsList(date, "PERSON");
						break;
					case "WalkIn":
						hearingList = hearingService.findAllPendingHearingsList(date, "WALKIN");
						break;
					case "Written":
						hearingList = hearingService.findAllWrittenHearingsList();
						break;
					}
				} else {
					switch (checkbox) {
					case "All":
						hearingList = hearingService.findAllPendingHearingsList(date1, date2);
						break;
					case "InPerson":
						hearingList = hearingService.findAllPendingHearingsList(date1, date2, "PERSON");
						break;
					case "WalkIn":
						hearingList = hearingService.findAllPendingHearingsList(date1, date2, "WALKIN");
						break;
					case "Written":
						hearingList = hearingService.findAllWrittenHearingsList();
						break;
					}
				}
			} else {
				if (flag == false) {
					switch (checkbox) {
					case "All":
						hearingList = hearingService.findAllCompletedHearingsList(date);
						break;
					case "InProgress":
						hearingList = hearingService.findAllCompletedHearingsList("Progress", "Rework", date);
						break;
					case "PendingReview":
						hearingList = hearingService.findAllCompletedHearingsList("PENDINGREVIEW",
								"PENDINGREVIEWUPDATED", date);
						break;
					case "PendingMail":
						hearingList = hearingService.findAllCompletedHearingsList("PendingMail", date);
						break;
					case "FTA":
						hearingList = hearingService.findAllCompletedHearingsList("Failed to Appear", date);
						break;
					case "Complete":
						hearingList = hearingService.findAllCompletedHearingsList("Complete", date);
						break;
					}
				} else {
					switch (checkbox) {
					case "All":
						hearingList = hearingService.findAllCompletedHearingsList(date1, date2);
						break;
					case "InProgress":
						hearingList = hearingService.findAllCompletedHearingsList("Progress", "Rework", date1, date2);
						break;
					case "PendingReview":
						hearingList = hearingService.findAllCompletedHearingsList("PendingReview",
								"PENDINGREVIEWUPDATED", date1, date2);
						break;
					case "PendingMail":
						hearingList = hearingService.findAllCompletedHearingsList("PendingMail", date1, date2);
						break;
					case "FTA":
						hearingList = hearingService.findAllCompletedHearingsList("Failed to Appear", date1, date2);
						break;
					case "Complete":
						hearingList = hearingService.findAllCompletedHearingsList("Complete", date1, date2);
						break;
					}
				}
			}
			if (hearingList != null && hearingList.size() > 0) {
				hearingDetailList = setHearingAllDetails(hearingList);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return hearingDetailList;
	}
	
	@PostMapping(value = { "/addHearingNotes/{violationId}/{hearingId}" })
	private String addHearingNotes(@PathVariable String violationId, @PathVariable Long hearingId, ModelMap model,
			HttpSession httpSession, RedirectAttributes redirectAttributes,
			@ModelAttribute("hearingDetailsForm") HearingDetail letterHearingDetail) throws IOException {
		String result = "";
		try {
			Admin user = (Admin) httpSession.getAttribute("admin");
			if (user != null) {
				if (violationId != null && letterHearingDetail != null && letterHearingDetail.getNotes() != null
						&& letterHearingDetail.getNotes().trim().length() > 0) {
					Comment comments = historyService.findCommentByMode(violationId, "HR");
					Violation violation = violationService.findByViolationId(violationId);
					if (comments == null || (comments != null
							&& !comments.getComment().equalsIgnoreCase(letterHearingDetail.getNotes().trim()))) {
						Comment comment = new Comment();
						comment.setViolation(violation);
						comment.setCommentType("I");
						comment.setComment(letterHearingDetail.getNotes().trim());
						comment.setMode("HR");
						comment.setUser(user.getUserName());
						violationService.addComment(comment);
						result = "Notes is successfully added.";

					} else {
						result = "Same notes is already added.";
					}
				} else {
					result = "Please enter notes.";
				}
			} else {
				redirectAttributes.addFlashAttribute("resultMessage", "Session Expired. please re-login");
				model.addAttribute("adjudicatorLoginBean", new LoginBean());
				result = "redirect:/adjudicatorLogin/";
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		redirectAttributes.addFlashAttribute("noteErrorMsg", result);
		redirectAttributes.addFlashAttribute("hearingDetail", letterHearingDetail);
		return "redirect:../../hearingsEdit/" + violationId + "/" + hearingId;

	}
}
