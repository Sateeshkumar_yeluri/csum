package com.csum.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Violation;
import com.csum.model.ViolationSearch;
import com.csum.service.ViolationService;

@Controller
public class ConsumerSearchController {

	public static final Logger logger =LogManager.getLogger(ConsumerSearchController.class);
	
	@Autowired
	private ViolationService violationService;
	
	@GetMapping(value={"/consumerLogin"})
	public String consumerSearch(ModelMap model){
		if (!model.containsAttribute("violationSearchForm")) {
			model.addAttribute("violationSearchForm", new ViolationSearch());
		}
		return "consumerSearch";		
	}
	
	@PostMapping(value={"/consumerSearch"})
	public String consumerSearchResults(@ModelAttribute("violationSearchForm") ViolationSearch violationSearchForm, ModelMap model){
		List<Violation> violationList;
		try{
			if(violationSearchForm.getTicketNumber()!=null && violationSearchForm.getTicketNumber().trim().length()>0 &&
					violationSearchForm.getLastName()!=null && violationSearchForm.getLastName().trim().length()>0){
				violationSearchForm.setSearchType("VN&LName");
			}else if(violationSearchForm.getTicketNumber()!=null && violationSearchForm.getTicketNumber().trim().length()>0){
				violationSearchForm.setSearchType("VN");
			}else if(violationSearchForm.getLastName()!=null && violationSearchForm.getLastName().trim().length()>0){
				violationSearchForm.setSearchType("LName");
			}			
			violationList = violationService.findBySearchCriteria(violationSearchForm);
			model.addAttribute("violationList",violationList);			
		}catch(Exception e){
			logger.error(e,e);
		}		
		return "consumerSearch";
	}
	
}
