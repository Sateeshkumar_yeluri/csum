package com.csum.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

//import com.codesnippets4all.json.parsers.JSONParser;
import com.csum.model.Inspector;
import com.csum.model.Permit;
import com.csum.model.Violation;
import com.csum.model.ViolationCode;
import com.csum.model.ViolationImages;
import com.csum.service.InspectorService;
import com.csum.service.PermitService;
import com.csum.service.ViolationCodeService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.google.gson.Gson;

@RestController
public class ApiController {

	private static final Logger logger = LogManager.getLogger(ApiController.class);

	@Autowired
	private InspectorService inspectorService;

	@Autowired
	private PermitService permitService;
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private ViolationImagesService violationImagesService;
	
	@Autowired
	private ViolationCodeService violationCodeService;

	@GetMapping(value = { "/getDBVersion" })
	public String getDBVersion() throws JSONException {
		JSONObject output = new JSONObject();
		try {
			output.put("Status", 200);
		} catch (Exception e) {
			output.put("Status", 500);
			logger.error(e, e);
		}
		return output.toString();
	}

	@GetMapping(value = { "/getInspector" })
	public String getInspecor(@RequestParam(value = "userName", required = true) String username,
			@RequestParam(value = "password", required = true) String password) throws JSONException {
		JSONObject output = new JSONObject();
		Gson gson = new Gson();
		JSONParser jsonParser = new JSONParser();
		try {
			Inspector inspector = inspectorService.findByUserName(username);
			if (inspector != null && inspector.getPassword() != null && inspector.getPassword().equals(password)) {
				output.put("Status", 200);
				output.put("response", "Success");
				output.put("inspector", jsonParser.parse(gson.toJson(inspector, Inspector.class)));
			} else {
				output.put("Status", 400);
				output.put("response", "There is no inspector with the credentials");

			}
		} catch (Exception e) {
			output.put("Status", 500);
			logger.error(e, e);
		}
		return output.toString();
	}

	@GetMapping(value = { "/validateSlot" })
	public String validateSlot(@RequestParam(value = "licenceNumber", required = true) String licenceNumber,
			@RequestParam(value = "parkingSlot", required = true) String parkingSlot,
			@RequestParam(value = "scannedDate", required = true) String scannedDate,
			@RequestParam(value = "scannedTime", required = true) String scannedTime) throws JSONException {
		JSONObject output = new JSONObject();
		try {
			if (licenceNumber != null && parkingSlot != null && scannedDate != null && scannedTime != null
					&& licenceNumber.trim().length() > 0 && parkingSlot.trim().length() > 0
					&& scannedDate.trim().length() > 0 && scannedTime.trim().length() > 0) {
				Permit permit = permitService.findValidPermit(licenceNumber, parkingSlot,
						LocalDateTime.of(LocalDate.parse(scannedDate), LocalTime.parse(scannedTime)));
				if (permit != null) {
					output.put("Status", 200);
					output.put("response", "Success");
				} else {
					output.put("Status", 400);
					output.put("response", "There is no valid permit with the Licence Number " + licenceNumber);
				}
			} else {
				output.put("Status", 400);
				output.put("response",
						"licenceNumber, parkingSlot, scannedDate and scannedTime Should not be null or empty");
			}
		} catch (Exception e) {
			output.put("Status", 500);
			logger.error(e, e);
		}
		return output.toString();

	}

	@PostMapping(value = "/createViolations", produces = APPLICATION_JSON_VALUE)
	@ResponseBody
	public String createViolations(MultipartHttpServletRequest multipartRequest) throws JSONException {
		JSONArray output = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		JSONParser jsonParser = new JSONParser();
		try {
			org.json.simple.JSONArray array = new org.json.simple.JSONArray();
			Set set = multipartRequest.getFileMap().entrySet();
			Iterator it = set.iterator();
			while (it.hasNext()) {
				Map.Entry me = (Map.Entry) it.next();
				String fileName = (String) me.getKey();
				MultipartFile multipartFile = (MultipartFile) me.getValue();
				if (fileName.equalsIgnoreCase("input.json")) {
					File convFile = new File(fileName);
					convFile.createNewFile();
					FileOutputStream fos = new FileOutputStream(convFile);
					IOUtils.copy(multipartFile.getInputStream(), fos);
					fos.close();
					Object obj = jsonParser.parse(new FileReader(convFile));
					array = (org.json.simple.JSONArray) obj;
				}
			}
			if (array != null) {
				for (int i = 0; i < array.size(); i++) {
					output.put(createViolation(array.get(i).toString()));
				}
			}
			set = multipartRequest.getFileMap().entrySet();
			it = set.iterator();
			while (it.hasNext()) {
				Map.Entry me = (Map.Entry) it.next();
				String fileName = (String) me.getKey();
				MultipartFile multipartFile = (MultipartFile) me.getValue();
				if (!fileName.equalsIgnoreCase("input.json")) {
					if (array != null && fileName.contains("_")) {
						String citationId = fileName.split("_")[0];
						Violation violation = violationService.findByViolationId(citationId);
						if (violation != null) {
							ViolationImages images = new ViolationImages();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							InputStream is = multipartFile.getInputStream();
							int reads = is.read();
							while (reads != -1) {
								baos.write(reads);
								reads = is.read();
							}
							images.setViolation(violation);
							images.setImage(baos.toByteArray());
							images.setImageName(FilenameUtils.getBaseName(fileName));
							images.setImageType(FilenameUtils.getExtension(fileName));
							images.setUploadedDate(sdf.format(new Date()));
							violationImagesService.save(images);
						}
					}
				}
			}
			

		} catch (Exception e) {
			logger.error(e, e);
		}
		return output.toString();
	}
	
	private String getFileName(final Part part) {
		final String partHeader = part.getHeader("content-disposition");
		logger.debug("Part Header = {0}" + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}
	
	public JSONObject createViolation(@RequestBody String inputJson) throws JSONException {
		JSONObject output = new JSONObject();
		// logger.debug("createViolation " + inputJson);
		try {
			logger.info("createViolation"+inputJson);
			Gson gson = new Gson();
			Violation violation = gson.fromJson(inputJson, Violation.class);
			logger.info("violation.getViolationId() "+violation.getViolationId());
			if (violation != null && violation.getViolationId() != null) {
				Violation violationExists = violationService.findByViolationId(violation.getViolationId());
				logger.info("violation.getViolationId() 2 "+violation.getViolationId());
				if (violationExists != null) {
					output.put("Status", 400);
					output.put("Violation Number", violation.getViolationId());
					output.put("Description", " Violation already exists");
				} else {
					if (violation.getViolationCode() != null && violation.getViolationCode().getCode() != null) {
						ViolationCode violationCode = violationCodeService
								.findByCode(violation.getViolationCode().getCode());
						violation.setViolationCode(violationCode);
					} else {
						violation.setViolationCode(new ViolationCode());
					}
					violationService.saveViolation(violation);
					output.put("Status", 200);
					output.put("Violation Number", violation.getViolationId());
					output.put("Description", violation.getViolationId() + " Violation created successfully");
				}
			} else {
				output.put("Status", 500);
				JSONObject jsonObject = new JSONObject(inputJson);
				if (jsonObject.has("citationId")) {
					output.put("Violation Number", jsonObject.get("citationId"));
				}
				output.put("Description", "Invalid Input details");
			}

		} catch (Exception e) {
			output.put("Status", 500);
			JSONObject jsonObject = new JSONObject(inputJson);
			if (jsonObject.has("citationId")) {
				output.put("Violation Number", jsonObject.get("citationId"));
			}
			output.put("Description", "Failed to create violation");
			logger.error(e, e);
		}
		return output;
	}
	
	@GetMapping(value = { "/reportsDownload" })
	public void downloadReport(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "name", required = true) String name) throws JSONException {
		try {
			Properties prop = new Properties();
			InputStream inputStream = ApiController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			if (name != null && name.trim().length() > 0) {
				String path = prop.getProperty("violation.mailprocess.path");
				Path file = Paths.get(path, name);
				if (Files.exists(file)) {
					response.setContentType("text/csv");
					response.addHeader("Content-Disposition", "attachment; filename=" + name);
					try {
						Files.copy(file, response.getOutputStream());
						response.getOutputStream().flush();
					} catch (IOException ex) {
						logger.error(ex, ex);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
	}

}
