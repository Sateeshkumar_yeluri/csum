package com.csum.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.csum.model.Suspends;
import com.csum.model.Violation;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.SuspendsService;
import com.csum.service.ViolationService;

@Controller
public class SuspendsController {
	
	public static final Logger logger = LogManager.getLogger(SuspendsController.class);
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private SuspendsService suspendsService; 
	
	@Autowired
	private IppService ippService;
	
	@GetMapping(value = { "/suspendsDetails/{violationId}" })
	public String getSuspendsDetails(@PathVariable String violationId, ModelMap model) {
		Violation violation;
		String licNum = "";
		List<Suspends> suspendsList = null;
		boolean hearingSchedulerFlag = false;
		try{
			violation = violationService.findByViolationId(violationId);
			if(violation!=null && violation.getPlateEntity()!=null){
				licNum = violation.getPlateEntity().getLicenceNumber();
			}
			suspendsList = suspendsService.findByViolationId(violationId);
			hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
		}catch(Exception e){
			logger.error(e,e);
		}		
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		model.addAttribute("suspendsList",suspendsList);
		model.addAttribute("licenseNumber",licNum);
		model.addAttribute("violationId",violationId);
		return "suspendsDetails";
	}
}
