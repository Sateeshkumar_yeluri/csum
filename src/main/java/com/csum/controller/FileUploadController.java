package com.csum.controller;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.media.jai.JAI;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.csum.model.Admin;
import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.ViolationImagesService;
import com.csum.service.ViolationService;
import com.csum.utils.CSUMDateUtils;
import com.csum.utils.PDFTOIMGConverter;
import com.csum.view.model.ConvertedImage;
import com.csum.view.model.UploadForm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;

@Controller
public class FileUploadController {

	public static final Logger logger = LogManager.getLogger(FileUploadController.class);

	@Autowired
	private ViolationImagesService violationImagesService;

	@Autowired
	private HearingService hearingService;

	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private IppService ippService;

	@GetMapping(value = { "/attachments/{violationId}" })
	public String violationAttachmnets(@PathVariable String violationId, Model model) {
		UploadForm uploadForm = null;
		boolean hearingSchedulerFlag = false;
		String licNumber = "";
		List<ViolationImages> images = null;
		List<ViolationImages> checks =null;
		 List<ViolationImages> corresp = null;
		try {
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			uploadForm = new UploadForm();
			Violation violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				images = violationImagesService.findByType(violationId,"Attachments");
				checks = violationImagesService.findByType(violationId, "Checks");
				corresp = violationImagesService.findByType(violationId, "Correspondence");
				if (violation.getPlateEntity() != null && violation.getPlateEntity().getLicenceNumber() != null) {
					licNumber = violation.getPlateEntity().getLicenceNumber();
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		model.addAttribute("uploadForm", uploadForm);
		model.addAttribute("licenseNumber", licNumber);
		model.addAttribute("images", images);
		 model.addAttribute("checks", checks);
		 model.addAttribute("corresp", corresp);
		model.addAttribute("violationId", violationId);
		return "attachments";
	}

	@PostMapping(value = { "/fileupload/{violationId}" })
	public String attachFiles(@ModelAttribute("uploadForm") UploadForm uploadForm,
			@RequestParam("files") MultipartFile[] files, BindingResult bindingResult, Model model,
			@PathVariable String violationId, HttpSession session) {
		Violation violation;
		try {
			Admin user = (Admin) session.getAttribute("admin");
			violation = violationService.findByViolationId(violationId);
			List<MultipartFile> file = uploadForm.getFiles();
			logger.debug("files >>" + files);
			if (null != file && file.size() > 0) {
				for (MultipartFile imageDetails : file) {
					if (imageDetails != null && imageDetails.getSize() > 0) {
						ViolationImages images = new ViolationImages();
						String fileName = imageDetails.getOriginalFilename();
						String fileType = fileName.substring(fileName.lastIndexOf('.') + 1);
						// logger.debug("File name >> " + fileName + " " +
						// fileName.substring(fileName.lastIndexOf('.')));
						images.setImageName(fileName.substring(0, fileName.lastIndexOf('.')));
						images.setViolation(violation);
						images.setImageType(fileType.toUpperCase());
						String uploadedDate = CSUMDateUtils.getDateInString(new Date());
						// logger.debug("uploadedDate >> " + uploadedDate);
						images.setUploadedDate(uploadedDate);
						images.setType(uploadForm.getFileType());
						images.setImage(imageDetails.getBytes());
						images.setUser(user.getUserName());
						violationImagesService.save(images);
						if (fileType.equalsIgnoreCase("PDF") || fileType.equalsIgnoreCase("4X6M")) {
							// PDF to Image Conversions
							CommonsMultipartFile commonsMultipartFile = (CommonsMultipartFile) imageDetails;
							FileItem fileItem = commonsMultipartFile.getFileItem();
							DiskFileItem diskFileItem = (DiskFileItem) fileItem;
							String absPath = diskFileItem.getStoreLocation().getAbsolutePath();

							// logger.debug("File Path:: " + absPath);
							File convertionFile = new File(absPath);
							List<ViolationImages> imagesList = PDFTOIMGConverter.processConvertion(violation,
									images.getId(), convertionFile);
							if (imagesList != null) {
								for (ViolationImages image : imagesList) {
									image.setType(uploadForm.getFileType());
									image.setUser(user.getUserName());
									image.setImage(image.getImage());
									violationImagesService.save(image);
								}
							} else {
								model.addAttribute("PDFToImageError",
										"Failed To Convert PDF to Images, Try with valid PDF");
							}
							// PDF to Image Conversions
						}
					}
				}
			}
		} catch (IOException e) {
			logger.error(e, e);
		}
		model.addAttribute("result", "File Uploaded Successfully.");
		return "redirect:/attachments/" + violationId;
	}

	@GetMapping(value = { "/viewAttachmentAsImages/{fileId}/{violationId}" })
	public String viewAttachmentAsImages(@PathVariable long fileId, @PathVariable String violationId, Model model) {
		Violation violation;
		String licNumber = "";
		ViolationImages parentImage = null;
		List<ViolationImages> images = null;
		String parentImageName = "";
		boolean hearingSchedulerFlag = false;
		List<ConvertedImage> convertedImageList = new ArrayList<ConvertedImage>();
		try {
			boolean ippFlag = ippService.showIpp(violationId);
			model.addAttribute("ippFlag", ippFlag);
			hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
			violation = violationService.findByViolationId(violationId);
			if (violation != null) {
				if (violation.getPlateEntity() != null) {
					licNumber = violation.getPlateEntity().getLicenceNumber();
				}
				images = violationImagesService.findByParentId(fileId);
				parentImage = violationImagesService.findByImageId(fileId);
				if (images == null || images.isEmpty()) {
					if (parentImage != null && (parentImage.getImageType().equalsIgnoreCase("JPG")
							|| parentImage.getImageType().equalsIgnoreCase("JPEG")
							|| parentImage.getImageType().equalsIgnoreCase("TIFF")
							|| parentImage.getImageType().equalsIgnoreCase("TIF")
							|| parentImage.getImageType().equalsIgnoreCase("PNG"))) {
						images.add(parentImage);
					}
				}
				if (parentImage != null) {
					parentImageName = parentImage.getImageName();
				}
				for (ViolationImages img : images) {
					ConvertedImage conImg = new ConvertedImage(img.getId(), img.getImageName());
					convertedImageList.add(conImg);
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		model.addAttribute("licenseNumber", licNumber);
		model.addAttribute("violationId", violationId);
		if (images != null && !images.isEmpty()) {
			model.addAttribute("defaultPreview", images.get(0).getId());
		}
		model.addAttribute("parentImageName", parentImageName);
		model.addAttribute("parentFileId", fileId);
		model.addAttribute("images", images);
		model.addAttribute("convertedImageList", convertedImageList);
		return "showPDFImages";
	}

	@GetMapping(value = { "/imageSourceImage/{id}" })
	public String showImageSourceImages(HttpServletResponse response, @PathVariable long id, Model model) {
		model.addAttribute("defaultPreview", id);
		return "showImageSourceImages";
	}

	@GetMapping(value = { "/previewImage/{id}" })
	public void previewImage(HttpServletResponse response, @PathVariable long id) {
		ViolationImages images = violationImagesService.findByImageId(id);
		Properties prop = new Properties();
		try {
			InputStream inputStream = FileUploadController.class.getClassLoader()
					.getResourceAsStream("application.properties");
			prop.load(inputStream);
			if (images != null) {
				byte[] image = null;
				image = images.getImage();
				String imageType = images.getImageType();
				if ((imageType != null && imageType.equalsIgnoreCase("PNG")) || imageType.equalsIgnoreCase("png")
						|| imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
					response.setContentType("image/jpeg");
				} else if (imageType.equalsIgnoreCase("tiff") || imageType.equalsIgnoreCase("tif")) {
					final String outDir = prop.getProperty("violation.image.tempfilesloc");
					try {
						String writeFileInTempLoc = outDir + images.getImageName() + "_tmp_" + images.getId() + "."
								+ images.getImageType();
						FileOutputStream fos = new FileOutputStream(new File(writeFileInTempLoc));
						fos.write(image);
						FileSeekableStream stream = null;
						stream = new FileSeekableStream(writeFileInTempLoc);
						ImageDecoder dec = ImageCodec.createImageDecoder("tiff", stream, null);
						RenderedImage rendImg = dec.decodeAsRenderedImage(0);
						String targetfilename = outDir + images.getImageName() + "_tmp_" + images.getId() + ".jpeg";
						JAI.create("filestore", rendImg, targetfilename, "JPEG");
						// logger.debug("Convereted:: " + targetfilename);
						Path path = Paths.get(targetfilename);
						image = Files.readAllBytes(path);
						fos.close();
						File deleteFileTmp1 = new File(writeFileInTempLoc);
						File deleteFileTmp2 = new File(targetfilename);
						if (deleteFileTmp1.exists()) {
							deleteFileTmp1.delete();
						}
						if (deleteFileTmp2.exists()) {
							deleteFileTmp2.delete();
						}
					} catch (IOException e) {
						logger.error(e, e);
					}
					response.setContentType("image/jpeg");
				} else {
					response.setContentType("application/" + imageType);
				}
				response.setHeader("Cache-Control", "no-store");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", 0);
				ServletOutputStream responseOutputStream = response.getOutputStream();
				responseOutputStream.write(image);
				responseOutputStream.flush();
				responseOutputStream.close();
			}
		} catch (IOException e) {
			logger.error(e, e);
		}
	}

	@GetMapping(value = { "/updateAllConvertedImageNames/{violationId}/{parentId}/{updatedNamesJson}" })
	public String updateAllConvertedImageName(@PathVariable String violationId, @PathVariable long parentId,
			@PathVariable String updatedNamesJson, Model model, HttpSession session) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			Gson gson = new Gson();
			List<ConvertedImage> convertedImageList = gson.fromJson(updatedNamesJson,
					new TypeToken<List<ConvertedImage>>() {
					}.getType());
			Violation violation = violationService.findByViolationId(violationId);
			if (convertedImageList != null && violation != null) {
				for (ConvertedImage image : convertedImageList) {
					ViolationImages vlnImage = violationImagesService.findByImageId(image.getfId());
					vlnImage.setImageName(image.getfName());
					vlnImage.setUser(user.getUserName());
					vlnImage.setViolation(violation);
					violationImagesService.updateImage(vlnImage);
				}
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../../../viewAttachmentAsImages/" + parentId + "/" + violationId;
	}

	@GetMapping(value = { "/updateAttachmentFileName/{fileName}/{fileId}/{violationId}" })
	public String updateAttachmentFileName(@PathVariable String fileName, @PathVariable String fileId,
			@PathVariable String violationId, Model model, HttpSession session) {
		try {
			Admin user = (Admin) session.getAttribute("admin");
			logger.debug("updateAttachmentFileName" + fileId + " " + fileName);
			if (fileId != null && !fileId.isEmpty()) {
				ViolationImages images = violationImagesService.findByImageId(Long.parseLong(fileId));
				images.setImageName(fileName);
				images.setUser(user.getUserName());
				violationImagesService.updateImage(images);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "redirect:../../../attachments/" + violationId;
	}

	@GetMapping(value = { "/downloadImage/{id}" })
	public void downloadImage(HttpServletResponse response, @PathVariable long id) {
		try {
			ViolationImages images = violationImagesService.findByImageId(id);
			if (images != null) {
				byte[] image = null;
				image = images.getImage();
				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"",
						images.getImageName() + "." + images.getImageType());
				response.setContentType(images.getImageType());
				response.setHeader(headerKey, headerValue);
				response.setHeader("Cache-Control", "no-store");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", 0);
				ServletOutputStream responseOutputStream = response.getOutputStream();
				responseOutputStream.write(image);
				responseOutputStream.flush();
				responseOutputStream.close();
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
	
	@GetMapping(value = { "/deleteImage/{id}/{violationId}" })
	public String deleteImage(@PathVariable long id, @PathVariable long violationId) {
		ViolationImages images = violationImagesService.findByImageId(id);
		if (images != null) {
			violationImagesService.delete(id);
		}
		return "redirect:/attachments/" + violationId;
	}
	
	@GetMapping(value = { "/printPDFPages/{fileId}/{citationId}" })
	public String printPDFPages(@PathVariable long fileId, @PathVariable String citationId, Model model) {
		List<ViolationImages> images =  violationImagesService.findByParentId(fileId);
		model.addAttribute("images", images);
		return "printPDFImages";
	}
	
	@GetMapping(value = { "/viewAttachmentImages/{fileId}/{violationId}" })
	public String viewAttachmentImages(@PathVariable long fileId, @PathVariable String violationId, Model model) {
		List<ViolationImages> images = violationImagesService.findByParentId(fileId);
		Violation violation = violationService.findByViolationId(violationId);
		if (violation!= null) {
			if (violation.getPlateEntity() != null) {
				model.addAttribute("licenseNumber", violation.getPlateEntity().getLicenceNumber());
			}
		/*
		 * logger.debug("viewAttachmentAsImages:: " + fileId + " images:: " +
		 * images);
		 */
		// for non PDF files and JPG or JPEG or TIFF (user uploaded pages not
		// converted one)
		if (images == null || images.isEmpty()) {
			ViolationImages image = violationImagesService.findByImageId(fileId);
			// logger.debug("if(images == null)::" + " image :: " + image);
			if (image.getImageType().equalsIgnoreCase("JPG") || image.getImageType().equalsIgnoreCase("JPEG")
					|| image.getImageType().equalsIgnoreCase("TIFF") || image.getImageType().equalsIgnoreCase("TIF")
					|| image.getImageType().equalsIgnoreCase("PNG")) {
				images.add(image);
			}
		}

		if (images != null && !images.isEmpty())
			model.addAttribute("defaultPreview", images.get(0).getId());
		ViolationImages parentImage = violationImagesService.findByImageId(fileId);
		model.addAttribute("parentImageName", parentImage.getImageName());
		model.addAttribute("parentFileId", fileId);
		model.addAttribute("images", images);

		List<ConvertedImage> convertedImageList = new ArrayList<ConvertedImage>();
		for (ViolationImages img : images) {
			// logger.debug(img.getId() + " " + img.getImageName());
			ConvertedImage conImg = new ConvertedImage(img.getId(), img.getImageName());
			convertedImageList.add(conImg);
		}
		model.addAttribute("convertedImageList", convertedImageList);
		}
		return "showImages";
	}

}
