package com.csum.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Permit;
import com.csum.model.Violation;
import com.csum.model.ViolationSearch;
import com.csum.service.PermitService;

@Controller
public class PermitSearchController {

	public static final Logger logger = LogManager.getLogger(PermitSearchController.class);
	
	@Autowired
	private PermitService permitService;

	@GetMapping(value={"/permitSearch"})
	public String searchPermit(ModelMap model){
		List<Permit> permitList;
		ViolationSearch violationSearchForm = new ViolationSearch();
		try {
			if (!model.containsAttribute("violationSearchForm")) {
				violationSearchForm.setSearchType("Latest");
				permitList = permitService.findBySearchCriteria(violationSearchForm);
				model.addAttribute("permitSearchForm", violationSearchForm);
				model.addAttribute("permitList",permitList);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return"permitSearch";
	}
	
	@PostMapping(value={"/permitSearch/{type}"})
	public String searchResults(@ModelAttribute("permitSearchForm") ViolationSearch permitSearchForm, ModelMap model, @PathVariable("type") String type){
		List<Permit> permitList;
		try{
			model.addAttribute("permitSearchForm",permitSearchForm);
			permitList = permitService.findBySearchCriteria(permitSearchForm);
			model.addAttribute("permitList",permitList);
			if(type!=null && type.equalsIgnoreCase("permit")){
			Permit permit = new Permit();
			model.addAttribute("permitForm",permit);
			return "permitPurchase";
			}
		}catch(Exception e){
			logger.error(e,e);
		}
		return "permitSearch";		
	}
	
}
