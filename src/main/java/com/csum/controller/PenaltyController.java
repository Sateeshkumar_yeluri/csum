package com.csum.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.csum.model.Penalty;
import com.csum.model.Violation;
import com.csum.service.HearingService;
import com.csum.service.IppService;
import com.csum.service.PenaltyService;
import com.csum.service.ViolationService;

@Controller
public class PenaltyController {
	
	public static final Logger logger = LogManager.getLogger(PenaltyController.class);
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private PenaltyService penaltyService;
	
	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private IppService ippService;
	
	@GetMapping(value = { "/penaltyDetails/{violationId}" })
	public String getPenalty(@PathVariable String violationId, ModelMap model) {
		String licenseNumber="";
		try{
		Violation violation= violationService.findByViolationId(violationId);
		if(violation!=null && violation.getPlateEntity()!=null && violation.getPlateEntity().getLicenceNumber()!=null){
			licenseNumber = violation.getPlateEntity().getLicenceNumber();
		}
		Penalty penalty = penaltyService.findByViolationId(violationId);
		model.addAttribute("penalty",penalty);
		model.addAttribute("licenseNumber",licenseNumber);
		model.addAttribute("violationId",violationId);
		}catch(Exception e){
			logger.error(e,e);
		}
		boolean ippFlag = ippService.showIpp(violationId);
		model.addAttribute("ippFlag", ippFlag);
		boolean hearingSchedulerFlag = hearingService.showHearingScheduler(violationId);
		model.addAttribute("hearingSchedulerFlag", hearingSchedulerFlag);
		return "penaltyDetails";
		
	}

}
