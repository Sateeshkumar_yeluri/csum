package com.csum.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.csum.model.Violation;
import com.csum.model.ViolationSearch;
import com.csum.service.ViolationService;

@Controller
public class ViolationSearchController {

	public static final Logger logger = LogManager.getLogger(ViolationSearchController.class);

	@Autowired
	private ViolationService violationService;

	@GetMapping(value = { "/home" })
	public String violationSearch(ModelMap model) {
		List<Violation> violationList;
		ViolationSearch violationSearchForm = new ViolationSearch();
		try {
			if (!model.containsAttribute("violationSearchForm")) {
				violationSearchForm.setSearchType("Latest");
				violationList = violationService.findBySearchCriteria(violationSearchForm);
				model.addAttribute("violationSearchForm", violationSearchForm);
				model.addAttribute("violationList", violationList);
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "home";
	}

	@PostMapping(value = { "/violationSearch" })
	public String searchResults(@ModelAttribute("violationSearchForm") ViolationSearch violationSearchForm,
			ModelMap model) {
		List<Violation> violationList;
		try {
			violationList = violationService.findBySearchCriteria(violationSearchForm);
			model.addAttribute("violationList", violationList);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return "home";

	}

}
