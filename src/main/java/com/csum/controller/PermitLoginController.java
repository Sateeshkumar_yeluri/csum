package com.csum.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csum.model.User;
import com.csum.model.ViolationSearch;
import com.csum.service.UserService;
import com.csum.view.model.LoginBean;

@Controller
public class PermitLoginController {
	
	public static final Logger logger = LogManager.getLogger(PermitLoginController.class);
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/permitLogin")
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, LoginBean loginBean) {
		ModelAndView model = new ModelAndView();
		// LoginBean loginBean = new LoginBean();
		model.addObject("permitLoginBean", loginBean);
		return model;
	}
	
	@PostMapping("/permitLogin")
	public String executeLogin(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("permitLoginBean") LoginBean loginBean, Model model) {
		User user = userService.findByUserName(loginBean.getUsername());
		if (loginBean != null && user != null && user.getPassword() != null && user.getPassword() != null
				&& user.getPassword().equals(loginBean.getPassword())) {
			logger.debug("successfully logged in Permit Portal");
			request.getSession().setAttribute("user", user);
			//model.addAttribute("permitSearchForm",new ViolationSearch());
			return "redirect:/permitPurchase";
		}
		model.addAttribute("resultMessage", "Invalid Credentials");
		return "permitLogin";
	}
	
	@GetMapping(value = "/permitLogout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/permitLogin?logout";
	}

}
