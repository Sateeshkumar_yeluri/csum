package com.csum.service;

import com.csum.model.User;

public interface UserService  {

	User findByUserName(String userName);

}
