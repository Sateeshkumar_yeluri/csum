package com.csum.service;

import com.csum.model.HearingDetail;

public interface HearingDetailsService {

	HearingDetail findByHearing(Long hearingId);

	void save(HearingDetail hearingForm);

	void deleteByHearingId(Long hearingId);

}
