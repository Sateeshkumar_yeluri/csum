package com.csum.service;

import java.util.List;

import com.csum.model.CorrespondenceCode;

public interface CorrespondenceCodesService {

	List<CorrespondenceCode> findAll();

	CorrespondenceCode findById(Long correspCodeId);

}
