package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.HistoryDao;
import com.csum.model.Address;
import com.csum.model.Comment;
import com.csum.model.Correspondence;
import com.csum.model.Hearing;
import com.csum.model.Ipp;
import com.csum.model.Notices;
import com.csum.model.Payment;
import com.csum.model.Penalty;
import com.csum.model.PlateEntity;
import com.csum.model.Suspends;
import com.csum.view.model.HistoryBean;

@Service("historyService")
@Transactional
public class HistoryServiceImpl implements HistoryService {

	public static final Logger logger = LogManager.getLogger(HistoryServiceImpl.class);

	@Autowired
	private HistoryDao historyDao;

	@Override
	public HistoryBean getRequiredHistory(String historyItem, String violationId) {
		HistoryBean historyBean = new HistoryBean();
		List<Address> addressHistory = null;
		List<Comment> commentHistory = null;
		List<Penalty> penaltyHistory = null;
		List<List<Payment>> paymentHistory = null;
		List<Hearing> hearingHistory = null;
		List<List<Suspends>> suspendHistory = null;
		List<List<Correspondence>> correspHistory = null;
		List<List<Notices>> noticeHistory = null;
		List<PlateEntity> plateEntityHistory = null;
		List<Ipp> ippHistory = null;
		try {
			historyBean.setHistoryItem(historyItem);
			switch (historyItem) {
			case "Penalty":
				penaltyHistory = historyDao.getAllPenaltyHistory(violationId);
				historyBean.setPenaltyHistory(penaltyHistory);
				break;
			case "Payment":
				paymentHistory = historyDao.getAllPaymentHistory(violationId);
				historyBean.setPaymentHistory(paymentHistory);
				break;
			case "Hearing":
				hearingHistory = historyDao.getAllHearingHistory(violationId);
				historyBean.setHearingHistory(hearingHistory);
				break;
			case "Suspend":
				suspendHistory = historyDao.getAllSuspendsHistory(violationId);
				historyBean.setSuspendHistory(suspendHistory);
				break;
			case "Correspondence":
				correspHistory = historyDao.getAllCorrespHistory(violationId);
				historyBean.setCorrespHistory(correspHistory);
				break;
			case "Notices":
				noticeHistory = historyDao.getAllNoticesHistory(violationId);
				historyBean.setNoticeHistory(noticeHistory);
				break;
			case "PlateEntity":
				plateEntityHistory = historyDao.getAllPlateHistory(violationId);
				historyBean.setPlateHistory(plateEntityHistory);
				break;
			case "Ipp" :
				ippHistory = historyDao.getAllIppHistory(violationId);
				historyBean.setIppHistory(ippHistory);
				break;
			case "All":
				plateEntityHistory = historyDao.getAllPlateHistory(violationId);
				historyBean.setPenaltyHistory(penaltyHistory);
				penaltyHistory = historyDao.getAllPenaltyHistory(violationId);
				historyBean.setPenaltyHistory(penaltyHistory);
				paymentHistory = historyDao.getAllPaymentHistory(violationId);
				historyBean.setPaymentHistory(paymentHistory);
				hearingHistory = historyDao.getAllHearingHistory(violationId);
				historyBean.setHearingHistory(hearingHistory);
				suspendHistory = historyDao.getAllSuspendsHistory(violationId);
				historyBean.setSuspendHistory(suspendHistory);
				correspHistory = historyDao.getAllCorrespHistory(violationId);
				historyBean.setCorrespHistory(correspHistory);
				noticeHistory = historyDao.getAllNoticesHistory(violationId);
				historyBean.setNoticeHistory(noticeHistory);
				ippHistory = historyDao.getAllIppHistory(violationId);
				historyBean.setIppHistory(ippHistory);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return historyBean;
	}

	@Override
	public void deleteAll(String createdBy) {
		historyDao.deleteAll(createdBy);
	}

	@Override
	public Comment findCommentByMode(String violationId, String mode) {		
		return historyDao.findCommentByMode(violationId, mode);
	}

}
