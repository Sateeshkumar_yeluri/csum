package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.ViolationDao;
import com.csum.model.Comment;
import com.csum.model.Violation;
import com.csum.model.ViolationSearch;

@Service("violationService")
@Transactional
public class ViolationServiceImpl implements ViolationService {
	
	@Autowired
	private ViolationDao violationDao;

	@Override
	public Violation findByViolationId(String violationId) {
		return violationDao.findByViolationId(violationId);
	}

	@Override
	public void saveViolation(Violation violationForm) {
		violationDao.saveViolation(violationForm);
	}

	@Override
	public List<Violation> findBySearchCriteria(ViolationSearch violationSearchForm) {
		return violationDao.findBySearchCriteria(violationSearchForm);
	}

	@Override
	public void updateViolation(Violation violation) {
		violationDao.updateViolation(violation);
	}

	@Override
	public void addComment(Comment comment) {
		violationDao.addComment(comment);
	}
}
