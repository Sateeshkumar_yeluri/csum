package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.CorrespondenceCodesDao;
import com.csum.model.CorrespondenceCode;

@Service("correspondenceCodesService")
@Transactional
public class CorrespondenceCodesServiceImpl implements CorrespondenceCodesService{
	
	@Autowired
	private CorrespondenceCodesDao correspondenceCodesDao;	

	@Override
	public List<CorrespondenceCode> findAll() {
		return correspondenceCodesDao.findAll();
	}

	@Override
	public CorrespondenceCode findById(Long correspCodeId) {
		return correspondenceCodesDao.findById(correspCodeId);
	}

}
