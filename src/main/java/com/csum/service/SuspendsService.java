package com.csum.service;

import java.util.List;

import com.csum.model.Suspends;

public interface SuspendsService {

	void save(Suspends suspend);

	List<Suspends> findByViolationId(String violationId);

	Suspends findLatestByViolationId(String violationId);

}
