package com.csum.service;

import java.util.List;

import com.csum.model.HearingScheduler;

public interface HearingSchedulerService {
	
	List<HearingScheduler> findAll();

}
