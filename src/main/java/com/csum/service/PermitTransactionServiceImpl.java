package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PermitTransactionDao;
import com.csum.model.PermitTransaction;

@Service("permitTransactionService")
@Transactional
public class PermitTransactionServiceImpl implements PermitTransactionService {
	
	@Autowired
	private PermitTransactionDao permitTransactionDao;

	@Override
	public void save(PermitTransaction transaction) {
		permitTransactionDao.save(transaction);
	}

}
