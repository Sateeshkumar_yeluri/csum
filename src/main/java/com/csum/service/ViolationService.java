package com.csum.service;

import java.util.List;

import com.csum.model.Comment;
import com.csum.model.Violation;
import com.csum.model.ViolationSearch;

public interface ViolationService {

	Violation findByViolationId(String violationId);

	void saveViolation(Violation violationForm);

	List<Violation> findBySearchCriteria(ViolationSearch violationSearchForm);

	void updateViolation(Violation violation);

	void addComment(Comment comment);

}
