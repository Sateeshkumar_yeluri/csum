package com.csum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.DispositionCodeDao;
import com.csum.model.DispositionCode;

@Service("dispositionCodeService")
public class DispositionCodeServiceImpl implements DispositionCodeService{
	
	@Autowired 
	private DispositionCodeDao dispositionCodeDao;

	@Override
	public List<DispositionCode> findAll() {
		return dispositionCodeDao.findAll();
	}

	@Override
	public DispositionCode findByCode(String dispCode) {
		return dispositionCodeDao.findByCode(dispCode);
	}

}
