package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.TransactionDao;
import com.csum.model.Transaction;

@Service("transactionService")
@Transactional
public class TransactionServiceImpl implements TransactionService{
	
	@Autowired
	private TransactionDao transactionDao;

	@Override
	public void save(Transaction transcation) {
		transactionDao.save(transcation);
	}
}
