package com.csum.service;

import java.util.List;

import com.csum.model.SuspendedCodes;

public interface SuspendedCodesService {

	List<SuspendedCodes> findAll();

	SuspendedCodes findByCode(int suspCode);

	SuspendedCodes findById(Long suspCodeId);

}
