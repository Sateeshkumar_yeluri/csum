package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.InspectorDao;
import com.csum.model.Inspector;


@Service("inspectorService")
@Transactional
public class InspectorServiceImpl implements InspectorService{
	
	@Autowired
	private InspectorDao inspectorDao;
	
	/*@Override
	public Inspector findByEmployeeId(String employeeId)
	{
		return inspectorDao.findByEmployeeId(employeeId);
	}*/

	@Override
	public Inspector findByUserName(String username) {
		return inspectorDao.findByUserName(username);
	}

}
