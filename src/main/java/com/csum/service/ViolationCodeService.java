package com.csum.service;

import java.util.List;

import com.csum.model.ViolationCode;

public interface ViolationCodeService {

	List<ViolationCode> findAll();
	
	ViolationCode findByCode(String code);

}
