package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.SuspendedCodesDao;
import com.csum.model.SuspendedCodes;

@Service("suspendedCodesService")
@Transactional
public class SuspendedCodesServiceImpl implements SuspendedCodesService{
	
	@Autowired
	private SuspendedCodesDao suspendedCodesDao;

	@Override
	public List<SuspendedCodes> findAll() {
		return suspendedCodesDao.findAll();
	}

	@Override
	public SuspendedCodes findByCode(int suspCode) {
		return suspendedCodesDao.findByCode(suspCode);
	}

	@Override
	public SuspendedCodes findById(Long suspCodeId) {
		return suspendedCodesDao.findById(suspCodeId);
	}

}
