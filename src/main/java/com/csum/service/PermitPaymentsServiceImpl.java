package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PermitPaymentsDao;
import com.csum.model.PermitPayments;

@Service("permitPaymentsService")
@Transactional
public class PermitPaymentsServiceImpl implements PermitPaymentsService{
	
	@Autowired
	private PermitPaymentsDao permitPaymentsDao;

	@Override
	public void savePermitPayment(PermitPayments permitPayments) {
		permitPaymentsDao.savePermitPayment(permitPayments);
	}

	@Override
	public PermitPayments findByTransactionId(Long id) {
		return permitPaymentsDao.findByTransactionId(id);
	}
	
	
}
