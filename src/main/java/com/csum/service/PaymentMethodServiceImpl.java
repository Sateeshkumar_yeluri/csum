package com.csum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PaymentMethodDao;
import com.csum.model.PaymentMethod;

@Service("paymentMethodService")
public class PaymentMethodServiceImpl implements PaymentMethodService{
	
	@Autowired 
	private PaymentMethodDao  paymentMethodDao;

	@Override
	public PaymentMethod findByType(int type) {
		return paymentMethodDao.findByType(type);
	}
	
	
	
}
