package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PaymentDetailsDao;
import com.csum.model.Payment;

@Service("paymentDetailsService")
@Transactional
public class PaymentDetailsServiceimpl implements PaymentDetailsService{
	
	@ Autowired
	private PaymentDetailsDao paymentDetailsDao;

	@Override
	public Payment findByTransactionId(Long id) {
		return paymentDetailsDao.findByTransactionId(id);
	}

	@Override
	public void save(Payment payment) {
		paymentDetailsDao.save(payment);
	}

	@Override
	public List<Payment> findByViolationId(String violationId) {
		return paymentDetailsDao.findByViolationId(violationId);
	}

	@Override
	public Payment findLatestByViolationId(String violationId) {
		return paymentDetailsDao.findLatestByViolationId(violationId);
	}

	@Override
	public void updatePaymentDetails(Payment payment) {
		paymentDetailsDao.updatePaymentDetails(payment);
	}

	@Override
	public List<Payment> findByPlanNumber(long planNumber) {
		return paymentDetailsDao.findByPlanNumber(planNumber);
	}

}
