package com.csum.service;

import java.util.List;

import com.csum.model.Admin;

public interface AdminService {

	Admin findByUserName(String username);

	List<Admin> findAllUsers();

}
