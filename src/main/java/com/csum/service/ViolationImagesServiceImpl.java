package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.ViolationImagesDao;
import com.csum.model.ViolationImages;

@Service("violationImagesService")
@Transactional
public class ViolationImagesServiceImpl implements ViolationImagesService{
	
	@Autowired
	private ViolationImagesDao violationImagesDao;

	@Override
	public void save(ViolationImages images) {
		violationImagesDao.save(images);
	}

	@Override
	public List<ViolationImages> findByViolationId(String violationId) {
		return violationImagesDao.findByViolationId(violationId);
	}

	@Override
	public List<ViolationImages> findByParentId(long parentId) {
		return violationImagesDao.findByParentId(parentId);
	}

	@Override
	public ViolationImages findByImageId(long imageId) {
		return violationImagesDao.findByImageId(imageId);
	}

	@Override
	public void updateImage(ViolationImages vlnImage) {
		violationImagesDao.updateImage(vlnImage);		
	}

	@Override
	public void delete(long id) {
		violationImagesDao.delete(id);
	}

	@Override
	public List<ViolationImages> findByType(String violationId, String type) {
		return violationImagesDao.findByType(violationId, type);
	}

}
