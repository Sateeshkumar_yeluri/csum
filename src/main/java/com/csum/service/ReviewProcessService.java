package com.csum.service;

import java.util.List;

import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;

public interface ReviewProcessService {

	List<ReviewComments> findAllReviewComments();

	ReviewProcess findByViolationId(String violationId);

	void save(ReviewProcess reviewProcess);

	void update(ReviewProcess reviewProcess);

}
