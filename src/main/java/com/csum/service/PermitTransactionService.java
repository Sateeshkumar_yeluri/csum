package com.csum.service;

import com.csum.model.PermitTransaction;

public interface PermitTransactionService {

	void save(PermitTransaction transaction);

}
