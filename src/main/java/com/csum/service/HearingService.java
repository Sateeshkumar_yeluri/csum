package com.csum.service;

import java.time.LocalDate;
import java.util.List;

import com.csum.model.Hearing;

public interface HearingService {

	Hearing findByViolationId(String violationId);

	List<Hearing> findAllPendingHearingsList(LocalDate curdate);

	List<Hearing> findLatestByDate(LocalDate date);

	void save(Hearing hearingForm);

	void updateHearing(Hearing hearingForm);
	
	boolean showHearingScheduler(String violationId);

	List<Hearing> findAllPendingHearingsList(LocalDate curDate, String hearingType);

	List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2);

	List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2, String hearingType);

	List<Hearing> findAllWrittenHearingsList();

	List<Hearing> findAllCompletedHearingsList(LocalDate curDate);

	List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate curDate);

	List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date);

	List<Hearing> findAllCompletedHearingsList(LocalDate date1, LocalDate date2);

	List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate date1, LocalDate date2);

	List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date1, LocalDate date2);

	Hearing findById(Long hearingId);

	void delete(Long hearingId);

}
