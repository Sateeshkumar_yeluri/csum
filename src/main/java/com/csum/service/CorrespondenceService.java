package com.csum.service;

import com.csum.model.Correspondence;

public interface CorrespondenceService {

	void save(Correspondence corresp);

	Correspondence findLatestByViolationId(String violationId);

}
