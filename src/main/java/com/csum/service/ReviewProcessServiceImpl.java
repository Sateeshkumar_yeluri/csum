package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.ReviewProcessDao;
import com.csum.model.ReviewComments;
import com.csum.model.ReviewProcess;

@Service("reviewProcessService")
@Transactional
public class ReviewProcessServiceImpl implements ReviewProcessService {

	@Autowired
	private ReviewProcessDao reviewProcessDao;

	@Override
	public List<ReviewComments> findAllReviewComments() {
		return reviewProcessDao.findAllReviewComments();
	}

	@Override
	public ReviewProcess findByViolationId(String violationId) {
		return reviewProcessDao.findByViolationId(violationId);
	}

	@Override
	public void save(ReviewProcess reviewProcess) {
		reviewProcessDao.save(reviewProcess);
		
	}

	@Override
	public void update(ReviewProcess reviewProcess) {
		reviewProcessDao.update(reviewProcess);
	}
	
	
}
