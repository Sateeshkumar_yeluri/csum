package com.csum.service;

import com.csum.model.Inspector;

public interface InspectorService {
	
	//Inspector findByEmployeeId(String employeeId);

	Inspector findByUserName(String username);

}
