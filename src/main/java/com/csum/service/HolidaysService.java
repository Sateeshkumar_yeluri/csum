package com.csum.service;

import java.util.List;

import com.csum.model.Holidays;

public interface HolidaysService {

	List<Holidays> findAll();

}
