package com.csum.service;

import com.csum.model.PaymentMethod;

public interface PaymentMethodService {

	PaymentMethod findByType(int i);

}
