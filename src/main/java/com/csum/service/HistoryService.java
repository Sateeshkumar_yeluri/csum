package com.csum.service;

import com.csum.model.Comment;
import com.csum.view.model.HistoryBean;

public interface HistoryService {

	HistoryBean getRequiredHistory(String historyItem, String violationId);

	void deleteAll(String createdBy);

	Comment findCommentByMode(String violationId, String mode);

}
