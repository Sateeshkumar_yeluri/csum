package com.csum.service;

import java.util.List;

import com.csum.model.DispositionCode;

public interface DispositionCodeService {

	List<DispositionCode> findAll();

	DispositionCode findByCode(String dispCode);

}
