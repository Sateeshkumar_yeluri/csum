package com.csum.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PermitDao;
import com.csum.model.Permit;
import com.csum.model.ViolationSearch;

@Service("permitService")
@Transactional
public class PermitServiceImpl implements PermitService{
	
	@Autowired
	private PermitDao permitDao;

	@Override
	public void savePermit(Permit permit) {
		permitDao.savePermit(permit);
	}

	@Override
	public Permit findByPermitId(String permitId) {
		return permitDao.findByPermitId(permitId);
	}

	@Override
	public List<Permit> findBySearchCriteria(ViolationSearch permitSearchForm) {
		return permitDao.findBySearchCriteria(permitSearchForm);
	}

	@Override
	public Permit findValidPermit(String licenceNumber, String parkingSlot, LocalDateTime scannedDateTime) {
		return permitDao.findValidPermit(licenceNumber, parkingSlot, scannedDateTime);
	}

	@Override
	public void deleteAllPermitData(String createdBy) {
		 permitDao.deleteAllPermitData(createdBy);
	}

}
