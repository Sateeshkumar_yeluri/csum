package com.csum.service;

import com.csum.model.Ipp;

public interface IppService {

	Ipp findByViolationId(String violationId);

	boolean showIpp(String violationId);

	Ipp recalculate(String violationId, Ipp ipp);

	void save(Ipp ippObj);

	Ipp findIppByPlanNumber(Long planNumber);

	void updateIpp(Ipp ipp);

	void deleteById(Long id);

}
