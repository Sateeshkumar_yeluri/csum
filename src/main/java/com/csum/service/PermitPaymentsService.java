package com.csum.service;

import com.csum.model.PermitPayments;

public interface PermitPaymentsService {

	void savePermitPayment(PermitPayments permitPayments);

	PermitPayments findByTransactionId(Long id);

}
