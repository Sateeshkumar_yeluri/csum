package com.csum.service;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.HearingDao;
import com.csum.model.Hearing;

@Service("hearingService")
@Transactional
public class HearingServiceImpl implements HearingService {
	
	public static final Logger logger = LogManager.getLogger(HearingServiceImpl.class);
	
	@Autowired
	private HearingDao hearingDao;

	@Override
	public Hearing findByViolationId(String violationId) {
		return hearingDao.findByViolationId(violationId);
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate curdate) {
		return hearingDao.findAllPendingHearingsList(curdate);
	}

	@Override
	public List<Hearing> findLatestByDate(LocalDate date) {
		return hearingDao.findLatestByDate(date);
	}

	@Override
	public void save(Hearing hearingForm) {
		hearingDao.save(hearingForm);
	}

	@Override
	public void updateHearing(Hearing hearingForm) {
		hearingDao.updateHearing(hearingForm);
	}

	@Override
	public boolean showHearingScheduler(String violationId) {
		boolean hearingSchedulerFlag = true;
		try{
		if(violationId!=null){
			Hearing hearing = hearingDao.findByViolationId(violationId);
			if (hearing != null) {
				if(hearing.isRescheduled() || !hearing.getStatus().equalsIgnoreCase("PENDING") ){
					hearingSchedulerFlag = false;
				}
			}			
		}
		}catch(Exception e){
			logger.error(e,e);
		}
		return hearingSchedulerFlag;		
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate curDate, String hearingType) {
		return hearingDao.findAllPendingHearingsList(curDate, hearingType);
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2) {
		return hearingDao.findAllPendingHearingsList(date1, date2);
	}

	@Override
	public List<Hearing> findAllPendingHearingsList(LocalDate date1, LocalDate date2, String hearingType) {
		return hearingDao.findAllPendingHearingsList(date1, date2, hearingType);
	}

	@Override
	public List<Hearing> findAllWrittenHearingsList() {
		return hearingDao.findAllWrittenHearingsList();
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(LocalDate curDate) {
		return hearingDao.findAllCompletedHearingsList(curDate);
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate curDate) {
		return hearingDao.findAllCompletedHearingsList(hearingStatus1, hearingStatus2, curDate);
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date) {
		return hearingDao.findAllCompletedHearingsList(hearingStatus, date);
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(LocalDate date1, LocalDate date2) {
		return hearingDao.findAllCompletedHearingsList(date1, date2);
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus1, String hearingStatus2, LocalDate date1,
			LocalDate date2) {
		return hearingDao.findAllCompletedHearingsList(hearingStatus1, hearingStatus2, date1, date2);
	}

	@Override
	public List<Hearing> findAllCompletedHearingsList(String hearingStatus, LocalDate date1, LocalDate date2) {
		return hearingDao.findAllCompletedHearingsList(hearingStatus, date1, date2);
	}

	@Override
	public Hearing findById(Long hearingId) {
		return hearingDao.findById(hearingId);
	}

	@Override
	public void delete(Long hearingId) {
		hearingDao.delete(hearingId);
	}
}
