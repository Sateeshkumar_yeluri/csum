package com.csum.service;

import java.util.List;

import com.csum.model.Notices;

public interface NoticesService {

	List<Notices> findByViolationId(String violationId);

	Notices findLatestByViolationId(String violationId);

}
