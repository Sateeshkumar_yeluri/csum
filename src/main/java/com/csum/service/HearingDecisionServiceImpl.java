package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.HearingDecisionDao;
import com.csum.model.HearingDecision;

@Service("hearingDecisionService")
@Transactional
public class HearingDecisionServiceImpl implements HearingDecisionService{
	
	@Autowired
	private HearingDecisionDao hearingDecisionDao;

	@Override
	public List<HearingDecision> findAll() {
		return hearingDecisionDao.findAll();
	}

	@Override
	public HearingDecision findById(Long id) {
		return hearingDecisionDao.findById(id);
	}
	
	
}
