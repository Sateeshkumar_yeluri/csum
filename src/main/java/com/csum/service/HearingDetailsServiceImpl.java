package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.HearingDetailsDao;
import com.csum.model.HearingDetail;

@Service("hearingDetailsService")
@Transactional
public class HearingDetailsServiceImpl implements HearingDetailsService {
	
	@Autowired
	private HearingDetailsDao hearingDetailsDao;

	@Override
	public HearingDetail findByHearing(Long hearingId) {
		return hearingDetailsDao.findByHearing(hearingId);
	}

	@Override
	public void save(HearingDetail hearingForm) {
		hearingDetailsDao.save(hearingForm);
	}

	@Override
	public void deleteByHearingId(Long hearingId) {
		hearingDetailsDao.deleteByHearingId(hearingId);
	}

}
