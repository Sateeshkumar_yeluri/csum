package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.CorrespondenceDao;
import com.csum.model.Correspondence;

@Service("correspondenceService")
@Transactional
public class CorrespondenceServiceImpl implements CorrespondenceService{
	
	@Autowired
	private CorrespondenceDao correspondenceDao;

	@Override
	public void save(Correspondence corresp) {
		correspondenceDao.save(corresp);
	}

	@Override
	public Correspondence findLatestByViolationId(String violationId) {
		return correspondenceDao.findLatestByViolationId(violationId);
	}

}
