package com.csum.service;

import java.util.List;

import com.csum.model.HearingDecision;

public interface HearingDecisionService {

	List<HearingDecision> findAll();

	HearingDecision findById(Long id);

}
