package com.csum.service;

import java.time.LocalDateTime;
import java.util.List;

import com.csum.model.Permit;
import com.csum.model.ViolationSearch;

public interface PermitService {

	void savePermit(Permit permit);

	Permit findByPermitId(String permitId);

	List<Permit> findBySearchCriteria(ViolationSearch permitSearchForm);

	Permit findValidPermit(String licenceNumber, String parkingSlot, LocalDateTime scannedDateTime);

	void deleteAllPermitData(String createdBy);

}
	