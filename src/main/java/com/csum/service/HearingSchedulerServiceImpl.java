package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.HearingSchedulerDao;
import com.csum.model.HearingScheduler;

@Service("hearingSchedulerService")
@Transactional
public class HearingSchedulerServiceImpl implements HearingSchedulerService{
	
	@Autowired 
	private HearingSchedulerDao hearingSchedulerDao;

	@Override
	public List<HearingScheduler> findAll() {
		return hearingSchedulerDao.findAll();
	}
	
	

}
