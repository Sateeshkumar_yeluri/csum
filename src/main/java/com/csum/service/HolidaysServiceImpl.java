package com.csum.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.csum.dao.HolidaysDao;
import com.csum.model.Holidays;

@Service("holidaysService")
@Transactional
public class HolidaysServiceImpl implements HolidaysService{
	
	@Autowired
	private HolidaysDao holidaysDao;

	@Override
	public ArrayList<Holidays> findAll() {
		return holidaysDao.findAll();
	}
}
