package com.csum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.PenaltyDao;
import com.csum.model.Penalty;

@Service("penaltyService")
@Transactional
public class PenaltyServiceImpl implements PenaltyService {
	
	@Autowired
	private PenaltyDao penaltyDao;

	@Override
	public Penalty findByViolationId(String violationId) {
		return penaltyDao.findByViolationId(violationId);
	}
}
