package com.csum.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.IppDao;
import com.csum.model.Hearing;
import com.csum.model.Ipp;
import com.csum.model.Payment;
import com.csum.model.Violation;
import com.csum.utils.CSUMDateUtils;


@Transactional
@Service("ippService")
public class IppServiceImpl implements IppService{
	
	public static final Logger logger = LogManager.getLogger(IppServiceImpl.class);
	
	@Autowired
	private IppDao ippDao;
	
	@Autowired
	private ViolationService violationService;
	
	@Autowired
	private HearingService hearingService;
	
	@Autowired
	private PaymentDetailsService paymentDetailsService;

	@Override
	public Ipp findByViolationId(String violationId) {
		return ippDao.findByViolationId(violationId);
	}

	@Override
	public boolean showIpp(String violationId) {
		boolean ippFlag = false;
		try {
			if (violationId != null) {
				/*Hearing hearing = hearingDetailsService.findLatestByCitation(citationId);
				if (hearing != null) {
					HearingDetail hearingDetail = hearingDetailsAllService.findByHearing(hearing.getId());
					if (hearingDetail != null
							&& (hearingDetail.getStatus().equalsIgnoreCase("Complete"))
							&& hearingDetail.getDecision().getCode().equalsIgnoreCase("LiableIpp")) {
						ippFlag = true;
					}
				}*/
				 Violation violation = violationService.findByViolationId(violationId);
	                Ipp ipp = findByViolationId(violationId);
	                if(ipp != null){
                        ippFlag = true;
                    }else if(violation != null && violation.getTotalDue().compareTo(new BigDecimal(0.00)) == 0 ){
                            ippFlag = false;    
                    }
                    else
                    {
                    	ippFlag = true;
                    }
	                /*else{
                        if(citation != null && citation.getTotalPenaltyAmount().compareTo(new BigDecimal(0.00)) == 0){
                            ippFlag = true;
                        }else{
                            ippFlag = false;
                        }       
                    }*/
			}
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
		return ippFlag;
	}

	@Override
	public Ipp recalculate(String violationId, Ipp ippForm) {
		Ipp ippObj = new Ipp();
		try {
			if (ippForm != null && violationId != null) {
				Violation violation = violationService.findByViolationId(violationId);
				Hearing hearing  = hearingService.findByViolationId(violationId);
				BigDecimal reductionAmt = BigDecimal.ZERO;
				if(hearing!=null && hearing.getReduction()!=null){
					reductionAmt = hearing.getReduction();
				}
				BigDecimal fineAmount = violation.getFineAmount();
				BigDecimal total = new BigDecimal(0.00);
				BigDecimal due = new BigDecimal(0.00);
				BigDecimal enrollAmount = new BigDecimal(0.00);
				List<Payment> paymentList = paymentDetailsService.findByViolationId(violationId);
				for (Payment payment : paymentList) {
					if (payment.isIpp() == false) {
						total = payment.getAmount();
						due = due.add(total);	
					}
				}
				enrollAmount = violation.getTotalDue();
				BigDecimal totalDue = violation.getTotalDue();
				BigDecimal previousCredits = due;
				if(reductionAmt != null){
				previousCredits = due.add(reductionAmt);
				}
				BigDecimal downPayment = new BigDecimal(0.00);
				BigDecimal installmentAmount = new BigDecimal(0.00);
				int noOfPayments = 0;
				if (ippForm.getDownPayment() != null && ippForm.getInstallmentAmount() != null) {
					downPayment = ippForm.getDownPayment();
					installmentAmount = ippForm.getInstallmentAmount();
					noOfPayments = ippForm.getNoOfPayments();
				} else {
					downPayment = enrollAmount.multiply(new BigDecimal(0.30), new MathContext(4)).setScale(0,
                            RoundingMode.CEILING);
                    installmentAmount = (enrollAmount.subtract(downPayment)).multiply(new BigDecimal(0.50)).setScale(2, RoundingMode.HALF_UP);
					noOfPayments = 3;
				}
				ippObj.setType("Plate");
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				long planNumber = calendar.getTimeInMillis();
				ippObj.setPlanNumber(planNumber);
				Date date = Calendar.getInstance().getTime();
				String curDate = null;
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				curDate = sdf.format(date);
				ippObj.setStartDate(curDate);
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, 21);
				date = cal.getTime();
				curDate = sdf.format(date);
				if(ippForm.getInstallmentDate1() != null){
					ippObj.setInstallmentDate1(ippObj.getInstallmentDate1());						
				}else{
					ippObj.setInstallmentDate1(curDate);
				}
				cal.add(Calendar.DATE, 30);
				date = cal.getTime();
				curDate = sdf.format(date);
				if(ippForm.getInstallmentDate2() != null){
					ippObj.setInstallmentDate2(ippObj.getInstallmentDate2());						
				}else{
					ippObj.setInstallmentDate2(curDate);
				}
				cal.add(Calendar.DATE, 30);
				date = cal.getTime();
				curDate = sdf.format(date);
				if(ippForm.getInstallmentDate3() != null){
					ippObj.setInstallmentDate3(ippObj.getInstallmentDate3());						
				}else{
					ippObj.setInstallmentDate3(curDate);
				}
				String newDate = CSUMDateUtils.getDateInString(new Date());
				if(ippForm.getStartDate() != null){
				ippObj.setStartDate(ippObj.getStartDate());
				}else{
					ippObj.setStartDate(newDate);
				}
				ippObj.setPreviousCredits(previousCredits);
				ippObj.setOriginalDue(fineAmount);
				ippObj.setEnrollAmount(totalDue);
				ippObj.setInstallmentAmount(installmentAmount);
				ippObj.setUnenrollAmount(due);
				ippObj.setDownPayment(downPayment);
				ippObj.setNoOfPayments(noOfPayments);
				ippObj.setViolation(violation);
				ippObj.setStatus("NEW");
			}
		} catch (NoResultException ex) {
			return null;
		}
		return ippObj;
	}

	@Override
	public void save(Ipp ippObj) {
		ippDao.save(ippObj);
	}

	@Override
	public Ipp findIppByPlanNumber(Long planNumber) {
		return ippDao.findIppByPlanNumber(planNumber);
	}

	@Override
	public void updateIpp(Ipp ipp) {
		ippDao.updateIpp(ipp);
	}

	@Override
	public void deleteById(Long id) {
		ippDao.deleteById(id);
	}	
	
}
