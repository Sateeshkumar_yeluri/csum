package com.csum.service;

import java.util.List;

import com.csum.model.Payment;

public interface PaymentDetailsService {

	Payment findByTransactionId(Long id);

	void save(Payment payment);

	List<Payment> findByViolationId(String violationId);

	Payment findLatestByViolationId(String violationId);

	void updatePaymentDetails(Payment payment);

	List<Payment> findByPlanNumber(long planNumber);

}
