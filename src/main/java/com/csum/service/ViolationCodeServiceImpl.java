package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.ViolationCodeDao;
import com.csum.model.ViolationCode;

@Service("violationCodeService")
@Transactional
public class ViolationCodeServiceImpl implements  ViolationCodeService{

	@Autowired
	private ViolationCodeDao violationCodeDao;

	@Override
	public List<ViolationCode> findAll() {
		return violationCodeDao.findAll();
	}
	
	@Override
	public ViolationCode findByCode(String code){
		return violationCodeDao.findByCode(code);
	}
}
