package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.SuspendsDao;
import com.csum.model.Suspends;

@Service("suspendsService")
@Transactional
public class SuspendsServiceImpl implements SuspendsService{
	
	@Autowired
	private SuspendsDao suspendsDao;

	@Override
	public void save(Suspends suspend) {
		suspendsDao.save(suspend);
	}

	@Override
	public List<Suspends> findByViolationId(String violationId) {
		return suspendsDao.findByViolationId(violationId);
	}

	@Override
	public Suspends findLatestByViolationId(String violationId) {
		return suspendsDao.findLatestByViolationId(violationId);
	}

}
