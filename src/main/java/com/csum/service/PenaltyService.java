package com.csum.service;

import com.csum.model.Penalty;

public interface PenaltyService {

	Penalty findByViolationId(String violationId);

}
