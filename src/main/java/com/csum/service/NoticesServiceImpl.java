package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.NoticesDao;
import com.csum.model.Notices;

@Service("noticesService")
@Transactional
public class NoticesServiceImpl implements NoticesService{
	
	@Autowired
	private NoticesDao noticesDao;

	@Override
	public List<Notices> findByViolationId(String violationId) {
		return noticesDao.findByViolationId(violationId);
	}

	@Override
	public Notices findLatestByViolationId(String violationId) {
		return noticesDao.findLatestByViolationId(violationId);
	}

}
