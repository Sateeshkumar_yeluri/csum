package com.csum.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csum.dao.AdminDao;
import com.csum.model.Admin;

@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	private AdminDao adminDao;

	@Override
	public Admin findByUserName(String username) {
		return adminDao.findByUserName(username);
	}

	@Override
	public List<Admin> findAllUsers() {
		return adminDao.findAllUsers();
	}
}
