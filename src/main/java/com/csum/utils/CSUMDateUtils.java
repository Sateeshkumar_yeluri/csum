package com.csum.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

public class CSUMDateUtils {

	public static final Logger logger = LogManager.getLogger(CSUMDateUtils.class);

	public static final String FORMAT = "MM/dd/yyyy";
	
	public static final String FORMAT1 = "MM/dd/yyyy HH:mm:ss";

	public static final String FORMAT2 = "hh:mm a";

	public static final String FORMAT3 = "MM/dd/yyyy hh:mm:ss a";

	public static final String FORMAT4 = "MM/dd/yyyy hh:mm a";
	
	public static final String FORMAT5 = "HH:mm a";

	public static String getDateInString(Date date) {
		String s = null;
		try {

			SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
			String timeZone = "PST";

			if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
				timeZone = Calendar.getInstance().getTimeZone().getID();
			}
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			s = sdf.format(date);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return s;
	}

	public static String getFormatedDateTime(Date date) {
		String strDate = null;
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(FORMAT4);
			strDate = sdf.format(date);
		}
		return strDate;
	}

	public static String getFormattedLocalDate(LocalDate localDate) {
		if (localDate != null) {
			return localDate.format(DateTimeFormatter.ofPattern(FORMAT));
		} else {
			return null;
		}
	}

	public static String getFormattedLocalTime(LocalTime localTime) {
		if (localTime != null) {
			return localTime.format(DateTimeFormatter.ofPattern(FORMAT2));
		} else {
			return null;
		}
	}

	public static String getFormattedLocalDateTime(LocalDateTime localDateTime) {
		if (localDateTime != null) {
			return localDateTime.format(DateTimeFormatter.ofPattern(FORMAT3));
		} else {
			return null;
		}
	}

	public static String getDayOfWeek(String date) {
		String weekDay = null;
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Calendar calendar = Calendar.getInstance();
		try {
			if (date != null) {
				calendar.setTime(sdf.parse(date));
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
				if (Calendar.MONDAY == dayOfWeek) {
					weekDay = "MONDAY";
				} else if (Calendar.TUESDAY == dayOfWeek) {
					weekDay = "TUESDAY";
				} else if (Calendar.WEDNESDAY == dayOfWeek) {
					weekDay = "WEDNESDAY";
				} else if (Calendar.THURSDAY == dayOfWeek) {
					weekDay = "THURSDAY";
				} else if (Calendar.FRIDAY == dayOfWeek) {
					weekDay = "FRIDAY";
				} else if (Calendar.SATURDAY == dayOfWeek) {
					weekDay = "SATURDAY";
				} else if (Calendar.SUNDAY == dayOfWeek) {
					weekDay = "SUNDAY";
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error(e, e);
		}
		return weekDay;
	}
	
	public static Date getFormattedDate(String strDate) {
		Date date = null;
		if (!StringUtils.isEmpty(strDate)) {
			try {
				if (strDate.trim().length() > 10) {
					SimpleDateFormat sdf = new SimpleDateFormat(FORMAT1);
					date = sdf.parse(strDate);
				} else {
					SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
					date = sdf.parse(strDate);
				}
			} catch (ParseException pe) {
				logger.error("parse exception", pe);
			}
		}
		return date;
	}
	
	public static LocalDate getFormattedLocalDate(String source) {
		if (source == null || source.isEmpty()) {
			return null;
		}
		return LocalDate.parse(source, DateTimeFormatter.ofPattern(FORMAT));
	}
	
	public static String getFormattedLocalTime(String source) {
		String strTime = null;
		LocalTime time = null;
		if (source == null || source.isEmpty()) {
			return null;
		}
		try {
			time = LocalTime.parse(source, DateTimeFormatter.ofPattern(FORMAT5));
		} catch (DateTimeParseException dtpe) {
			logger.error(dtpe.getMessage());
		}
		if (time == null) {
			return null;
		}
		try {
			strTime = time.format(DateTimeFormatter.ofPattern(FORMAT2));
		} catch (DateTimeException dte) {
			logger.error(dte.getMessage());
		}
		return strTime;
	}
}
