package com.csum.utils;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.csum.model.Violation;
import com.csum.model.ViolationImages;
import com.csum.view.model.PDFPages;

public class PDFTOIMGConverter {

	private static final Logger LOG = LogManager.getLogger(PDFTOIMGConverter.class);

	/**
	 * Create an image; the part between the smaller and the larger image is
	 * painted black, the rest in white
	 *
	 * @param minWidth
	 *            width of the smaller image
	 * @param minHeight
	 *            width of the smaller image
	 * @param maxWidth
	 *            height of the larger image
	 * @param maxHeight
	 *            height of the larger image
	 *
	 * @return
	 */
	private BufferedImage createEmptyDiffImage(int minWidth, int minHeight, int maxWidth, int maxHeight) {
		BufferedImage bim3 = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_RGB);
		Graphics graphics = bim3.getGraphics();
		if (minWidth != maxWidth || minHeight != maxHeight) {
			graphics.setColor(Color.BLACK);
			graphics.fillRect(0, 0, maxWidth, maxHeight);
		}
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, minWidth, minHeight);
		graphics.dispose();
		return bim3;
	}

	/**
	 * Get the difference between two images, identical colors are set to white,
	 * differences are xored, the highest bit of each color is reset to avoid
	 * colors that are too light
	 *
	 * @param bim1
	 * @param bim2
	 * @return If the images are different, the function returns a diff image If
	 *         the images are identical, the function returns null If the size
	 *         is different, a black border on the botton and the right is
	 *         created
	 *
	 * @throws IOException
	 */
	private BufferedImage diffImages(BufferedImage bim1, BufferedImage bim2) throws IOException {
		int minWidth = Math.min(bim1.getWidth(), bim2.getWidth());
		int minHeight = Math.min(bim1.getHeight(), bim2.getHeight());
		int maxWidth = Math.max(bim1.getWidth(), bim2.getWidth());
		int maxHeight = Math.max(bim1.getHeight(), bim2.getHeight());
		BufferedImage bim3 = null;
		if (minWidth != maxWidth || minHeight != maxHeight) {
			bim3 = createEmptyDiffImage(minWidth, minHeight, maxWidth, maxHeight);
		}
		for (int x = 0; x < minWidth; ++x) {
			for (int y = 0; y < minHeight; ++y) {
				int rgb1 = bim1.getRGB(x, y);
				int rgb2 = bim2.getRGB(x, y);
				if (rgb1 != rgb2
						// don't bother about differences of 1 color step
						&& (Math.abs((rgb1 & 0xFF) - (rgb2 & 0xFF)) > 1
								|| Math.abs(((rgb1 >> 8) & 0xFF) - ((rgb2 >> 8) & 0xFF)) > 1
								|| Math.abs(((rgb1 >> 16) & 0xFF) - ((rgb2 >> 16) & 0xFF)) > 1)) {
					if (bim3 == null) {
						bim3 = createEmptyDiffImage(minWidth, minHeight, maxWidth, maxHeight);
					}
					int r = Math.abs((rgb1 & 0xFF) - (rgb2 & 0xFF));
					int g = Math.abs((rgb1 & 0xFF00) - (rgb2 & 0xFF00));
					int b = Math.abs((rgb1 & 0xFF0000) - (rgb2 & 0xFF0000));
					bim3.setRGB(x, y, 0xFFFFFF - (r | g | b));
				} else {
					if (bim3 != null) {
						bim3.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
		}
		return bim3;
	}

	public static List<ViolationImages> processConvertion(Violation violation, long parentId, final File file)
			throws IOException {
		// System.out.println("TT");
		PDDocument document = null;
		boolean failed = false;
		List<ViolationImages> citImagesList = new ArrayList<ViolationImages>();

		Properties prop = new Properties();
		InputStream inputStream = PDFTOIMGConverter.class.getClassLoader()
				.getResourceAsStream("application.properties");
		prop.load(inputStream);
		final String outDir = prop.getProperty("violation.image.tempfilesloc");
		LOG.info("Opening: " + file.getName());
		try {
			LOG.debug("Using Temporary Location:: " + outDir);
			new FileOutputStream(new File(outDir, file.getName() + ".parseerror")).close();
			document = PDDocument.load(file, (String) null);
			String outputPrefix = outDir + file.getName() + "-";
			int numPages = document.getNumberOfPages();
			if (numPages < 1) {
				failed = true;
				LOG.error("file " + file.getName() + " has < 1 page");
			} else {
				new File(outDir, file.getName() + ".parseerror").delete();
			}

			LOG.info("Rendering: " + file.getName());
			PDFRenderer renderer = new PDFRenderer(document);
			for (int i = 0; i < numPages; i++) {
				String fileName = outputPrefix + (i + 1) + ".png";
				new FileOutputStream(new File(fileName + ".rendererror")).close();
				BufferedImage image = renderer.renderImageWithDPI(i, 96); // Windows
																			// native
																			// DPI
				new File(fileName + ".rendererror").delete();
				LOG.info("Writing: " + fileName);
				new FileOutputStream(new File(fileName + ".writeerror")).close();
				File resultantFile = new File(fileName);
				ImageIO.write(image, "PNG", new File(fileName));

				ViolationImages citImages = new ViolationImages();
				// String imageName =
				// violation.getviolationId()+"_"+parentId+"_"+i+".png";

				byte[] fileBytes = Files.readAllBytes(resultantFile.toPath());
				citImages.setImageName(violation.getViolationId() + "_" + parentId + "_" + i);
				citImages.setViolation(violation);
				citImages.setImage(fileBytes);
				String uploadedDate = CSUMDateUtils.getDateInString(new Date());
				citImages.setUploadedDate(uploadedDate);
				citImages.setImageType("PNG");
				citImages.setParentFileId(parentId);
				LOG.debug("CitImage:: " + citImages);
				citImagesList.add(citImages);

				new File(fileName + ".writeerror").delete();
			}

			// test to see whether file is destroyed in pdfbox
			new FileOutputStream(new File(outDir, file.getName() + ".saveerror")).close();
			File tmpFile = File.createTempFile("pdfbox", ".pdf");
			document.setAllSecurityToBeRemoved(true);
			document.save(tmpFile);
			new File(outDir, file.getName() + ".saveerror").delete();
			new FileOutputStream(new File(outDir, file.getName() + ".reloaderror")).close();
			PDDocument.load(tmpFile, (String) null).close();
			new File(outDir, file.getName() + ".reloaderror").delete();
			tmpFile.delete();
		} catch (IOException e) {
			failed = true;
			LOG.error("Error converting file " + file.getName());
			throw e;
		} finally {
			if (document != null) {
				document.close();
			}
		}

		LOG.info("Comparing: " + file.getName());

		// Now check the resulting files ... did we get identical PNG(s)?
		try {
			new File(outDir + file.getName() + ".cmperror").delete();
			File[] outFiles = new File(outDir).listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return (name.endsWith(".png") && name.startsWith(file.getName(), 0))
							&& !name.endsWith(".png-diff.png");
				}
			});
			if (outFiles.length == 0) {
				failed = true;
				LOG.warn("*** TEST FAILURE *** Output missing for file: " + file.getName());
			}
		} catch (Exception e) {
			new FileOutputStream(new File(outDir, file.getName() + ".cmperror")).close();
			failed = true;
			LOG.error("Error comparing file output for " + file.getName(), e);
		}
		// return !failed;
		return citImagesList;
	}

	public static List<PDFPages> pdfToImageConvertionOnly(long fileId, String outDir, File file) throws IOException {
		PDDocument document = null;
		boolean failed = false;
		List<PDFPages> pagesList = new ArrayList<PDFPages>();
		LOG.debug("Using Temporary Location:: " + outDir);
		LOG.info("Opening: " + file.getName());
		try {
			new FileOutputStream(new File(outDir, file.getName() + ".parseerror")).close();
			document = PDDocument.load(file, (String) null);
			String outputPrefix = outDir + file.getName() + "-";
			int numPages = document.getNumberOfPages();
			if (numPages < 1) {
				failed = true;
				LOG.error("file " + file.getName() + " has < 1 page");
			} else {
				new File(outDir, file.getName() + ".parseerror").delete();
			}
			LOG.info("Rendering: " + file.getName());
			PDFRenderer renderer = new PDFRenderer(document);
			for (int i = 0; i < numPages; i++) {
				String outputPageName = file.getName() + "-" + (i + 1) + ".png";
				String fileName = outputPrefix + (i + 1) + ".png";
				new FileOutputStream(new File(fileName + ".rendererror")).close();
				BufferedImage image = renderer.renderImageWithDPI(i, 96); // Windows
																			// native
																			// DPI
				new File(fileName + ".rendererror").delete();
				LOG.info("Writing: " + fileName);
				new FileOutputStream(new File(fileName + ".writeerror")).close();
				File resultantFile = new File(fileName);
				ImageIO.write(image, "PNG", new File(fileName));

				PDFPages pages = new PDFPages();
				byte[] fileBytes = Files.readAllBytes(resultantFile.toPath());
				pages.setPageName(outputPageName);
				pages.setPageContent(fileBytes);
				LOG.debug("PDFPages:: " + pages);
				pagesList.add(pages);
				new File(fileName + ".writeerror").delete();
			}
			// test to see whether file is destroyed in pdfbox
			new FileOutputStream(new File(outDir, file.getName() + ".saveerror")).close();
			File tmpFile = File.createTempFile("pdfbox", ".pdf");
			document.setAllSecurityToBeRemoved(true);
			document.save(tmpFile);
			new File(outDir, file.getName() + ".saveerror").delete();
			new FileOutputStream(new File(outDir, file.getName() + ".reloaderror")).close();
			PDDocument.load(tmpFile, (String) null).close();
			new File(outDir, file.getName() + ".reloaderror").delete();
			tmpFile.delete();
		} catch (IOException e) {
			failed = true;
			LOG.error("Error converting file " + file.getName());
			throw e;
		} finally {
			if (document != null) {
				document.close();
			}
		}
		return pagesList;
	}

	/*
	 * private boolean filesAreIdentical(File left, File right) throws
	 * IOException { if (left != null && right != null && left.exists() &&
	 * right.exists()) { if (left.length() != right.length()) { return false; }
	 * 
	 * FileInputStream lin = new FileInputStream(left); FileInputStream rin =
	 * new FileInputStream(right); try { byte[] lbuffer = new byte[4096]; byte[]
	 * rbuffer = new byte[lbuffer.length]; int lcount; while ((lcount =
	 * lin.read(lbuffer)) > 0) { int bytesRead = 0; int rcount; while ((rcount =
	 * rin.read(rbuffer, bytesRead, lcount - bytesRead)) > 0) { bytesRead +=
	 * rcount; } for (int byteIndex = 0; byteIndex < lcount; byteIndex++) { if
	 * (lbuffer[byteIndex] != rbuffer[byteIndex]) { return false; } } } }
	 * finally { lin.close(); rin.close(); } return true; } else { return false;
	 * } }
	 */

	public static void main(String[] args) {

		try {
			Violation vln = new Violation();
			vln.setViolationId("T123456");

			// new PDFTOIMGConverter().processConvertion(cit,1,new
			// File("C:\\Users\\govind\\Downloads\\August 2014 MTA Monthly
			// Operations Report - Parking.pdf"));

			// violation violation,long parentId,

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
