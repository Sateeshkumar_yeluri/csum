package com.csum.utils;

public interface CSUMConstants {
	
	public static final String CSUM_PST_ZONE_ID = "America/Los_Angeles";
	
	public static final String CSUM_MYSQL_DATE_FUNCTION = "STR_TO_DATE";
	public static final String CSUM_MYSQL_DATE_LITERAL = "%m/%d/%Y";
	public static final String CSUM_LIKE_SEARCH_SUFFIX = "%";

}
