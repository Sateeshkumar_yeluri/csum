<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content">
		<br /> <br />
		<div class="page-bar" style="margin: -25px 0px 0;">
			<ul class="page-breadcrumb">
				<li><a style="margin-left: 50px;"
					href="${pageContext.request.contextPath}/home"><spring:message
							code="lbl.home" /></a> <i class="fa fa-circle"></i></li>
				<li><span><spring:message
							code="lbl.topmenu.tab.violation.entry" /></span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form:form id="citationForm" action="#"
					modelAttribute="violationForm" method="POST" autocomplete="on">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Violation
										Number</label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											placeholder="Violation Number" path="violationId"
											style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Location
										of Violation</label>
									<div class="col-md-9">
										<form:input class="form-control input-sm" path="location"
											placeholder="Location" style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Date Of
										Violation</label>
									<div class="col-md-9">
										<div class="input-group input-group-sm date date-picker"
											data-date-format="mm/dd/yyyy" data-date-end-date="0d"
											data-date-viewmode="years">
											<form:input path="dateOfViolation" class="form-control" />
											<span class="input-group-btn">
												<button class="btn default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
										<span class="help-block">&nbsp;</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Time</label>
									<div class="col-md-9">
										<div
											class="input-group input-group-sm bootstrap-timepicker timepicker">
											<form:input path="timeOfViolation" type="text"
												class="form-control" data-minute-step="1"></form:input>
											<span class="input-group-addon"><i
												class="glyphicon glyphicon-time"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">First
										Name </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.firstName" placeholder="First Name"
											id="fnamePatron" style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="patron.firstName" cssClass="alert-danger" /> <span
											class="alert-danger" id="fnameError"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Middle
										Name</label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.middleName" placeholder="Middle Name"
											id="mnamePatron" style="text-transform: uppercase;"></form:input>
										<span class="help-block"> <span class="alert-danger"
											id="mnameError"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Last
										Name </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.lastName" placeholder="Last Name"
											id="lnamePatron" style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="patron.lastName" cssClass="alert-danger" /> <span
											class="alert-danger" id="lnameError"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Address</label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.address.address" placeholder="Address"
											id="patronAddress" style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="patron.address.address" cssClass="alert-danger" /> <span
											class="alert-danger" id="aptError"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">City </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.address.city" placeholder="City" id="cityPatron"
											style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="patron.address.city" cssClass="alert-danger" /> <span
											class="alert-danger" id="cityError"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Address
										State</label>
									<div class="col-md-9">
										<form:select class="bs-select form-control"
											style="font-size:13px;width:455px;"
											path="patron.address.state" placeholder="State">
											<form:option value="" label="Select an Option"></form:option>
											<c:forEach items="${states}" var="state">
												<form:option value="${state.description}"
													label="${state.description}"
													style="text-transform: uppercase;"></form:option>
											</c:forEach>
										</form:select>
										<span class="help-block">&nbsp;<form:errors
												path="patron.address.state" cssClass="alert-danger" /></span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Zip Code
									</label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="patron.address.zip" placeholder="Zip Code"
											id="zipcodePatron" style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="patron.address.zip" cssClass="alert-danger" /> <span
											class="alert-danger" id="zipcodeError"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">Vehicle
										Plate Number </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="plateEntity.licenceNumber"
											placeholder="Vehicle Plate Number" id="vhclLicNmbr"
											style="text-transform: uppercase;"></form:input>
										<span class="help-block"><form:errors
												path="plateEntity.licenceNumber" cssClass="alert-danger" /></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label label-sm col-md-3">VIN
										Number </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="plateEntity.vinNumber" placeholder="VIN Number"
											style="text-transform: uppercase;"></form:input>
										<span class="help-block"><form:errors
												path="plateEntity.vinNumber" cssClass="alert-danger" /></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Vehicle Body Type
									</label>
									<div class="col-md-9">
										<form:select path="plateEntity.bodyType"
											class="bs-select form-control input-sm">
											<form:option value="" label="Select an Option"></form:option>
											<form:option value="PASS" label="PASS"></form:option>
											<form:option value="TRUCK" label="TRUCK"></form:option>
											<form:option value="MOTORCYCLE" label="MOTORCYCLE"></form:option>
											<form:option value="SUV" label="SUV"></form:option>
											<form:option value="OTHER" label="OTHER"></form:option>
											<form:option value="" label="Select an Option"></form:option>
											<%-- <c:forEach items="${vhclBodyTypes}" var="vhclBodyType">
																<form:option value="${vhclBodyType.code}"
																	label="${vhclBodyType.description}" style="text-transform: uppercase;"></form:option>
															</c:forEach> --%>
										</form:select>
										<span class="help-block">&nbsp;<form:errors
												path="plateEntity.bodyType" cssClass="alert-danger" /></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Vehicle Make </label>
									<div class="col-md-9" id="makeDiv">
										<form:select path="plateEntity.vehicleMake" id="vhclMake"
											class="bs-select form-control input-sm">
											<form:option value="" label="Select an Option"></form:option>
											<form:option value="BMW" label="BMW"></form:option>
											<form:option value="BUICK" label="BUICK"></form:option>
											<form:option value="CAD" label="CAD"></form:option>
											<form:option value="CHEV" label="CHEV"></form:option>
											<form:option value="CADI" label="CADI"></form:option>
											<form:option value="DODG" label="DODG"></form:option>
											<form:option value="FORD" label="FORD"></form:option>
											<form:option value="GMC" label="GMC"></form:option>
											<form:option value="HOND" label="HOND"></form:option>
											<form:option value="HYUN" label="HYUN"></form:option>
											<form:option value="INFI" label="INFI"></form:option>
											<form:option value="JEEP" label="JEEP"></form:option>
											<form:option value="KIA" label="KIA"></form:option>
											<form:option value="LEX" label="LEX"></form:option>
											<form:option value="MAZD" label="MAZD"></form:option>
											<form:option value="MER" label="MER"></form:option>
											<form:option value="NISS" label="NISS"></form:option>
											<form:option value="PONT" label="PONT"></form:option>
											<form:option value="SCION" label="SCION"></form:option>
											<form:option value="TOYT" label="TOYT"></form:option>
											<form:option value="VOLK" label="VOLK"></form:option>
											<%-- <form:option value="OTHER" label="OTHER"></form:option> --%>
											<form:option value="" label="Select an Option"></form:option>
											<%-- <c:forEach items="${vhclMakes}" var="vhclMake">
																<form:option value="${vhclMake.code}"
																	label="${vhclMake.description}" style="text-transform: uppercase;"></form:option>
															</c:forEach> --%>
										</form:select>
										<span class="help-block">&nbsp;<form:errors
												path="plateEntity.vehicleMake" cssClass="alert-danger" /></span>
									</div>

									<%-- <div class="col-md-4" style="display: none" id="otherVhclMake">
										<form:input class="form-control input-sm"
											path="plateEntity.otherVhclMake" placeholder="Vehicle Moke"></form:input>
									</div> --%>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Vehicle Model </label>
									<div class="col-md-9">
										<form:input class="form-control input-sm"
											path="plateEntity.vehicleModel" placeholder="Vehicle Model"
											style="text-transform: uppercase;"></form:input>
										<span class="help-block">&nbsp;<form:errors
												path="plateEntity.vehicleModel" cssClass="alert-danger" /></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Vehicle Color </label>
									<div class="col-md-9">
										<form:select path="plateEntity.vehicleColor"
											class="bs-select form-control input-sm">
											<form:option value="" label="Select an Option"></form:option>
											<form:option value="BGE" label="BGE"></form:option>
											<form:option value="BLK" label="BLK"></form:option>
											<form:option value="BLU" label="BLU"></form:option>
											<form:option value="BRN" label="BRN"></form:option>
											<form:option value="GLD" label="GLD"></form:option>
											<form:option value="GRY" label="GRY"></form:option>
											<form:option value="GRN" label="GRN"></form:option>
											<form:option value="MAR" label="MAR"></form:option>
											<form:option value="RED" label="RED"></form:option>
											<form:option value="SIL" label="SIL"></form:option>
											<form:option value="WHI" label="WHI"></form:option>
											<form:option value="YEL" label="YEL"></form:option>
											<form:option value="OTHER" label="OTHER"></form:option>
											<form:option value="" label="Select an Option"></form:option>
											<c:forEach items="${vhclColors}" var="vhclColor">
												<form:option value="${vhclColor.code}"
													label="${vhclColor.description}"
													style="text-transform: uppercase;"></form:option>
											</c:forEach>
										</form:select>
										<span class="help-block">&nbsp;<form:errors
												path="plateEntity.vehicleColor" cssClass="alert-danger" /></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6"></div>
							<div class="col-md-6"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6"></div>
							<div class="col-md-6"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6"></div>
							<div class="col-md-6"></div>
						</div>
					</div>
					<div class="form-actions">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-offset-12 col-md-12">
															<input type="submit" class="btn green btn-sm"
																style="position: static; top: -2px; -left: -51px; right: 30px; bottom: 40px; float: none;"
																value="SAVE" id="blockui_savebtn_parking" />
															<button type="button"
																onclick="window.location='${pageContext.request.contextPath}/citationentry';"
																class="btn green btn-sm"
																style="position: static; float: none; top: -2px; left: -500px; right: 30px; bottom: 40px">CANCEL</button>
														</div>
													</div>
												</div>
												<div class="col-md-6"></div>
											</div>
										</div>
				</form:form>
			</div>
		</div>
	</div>
</div>